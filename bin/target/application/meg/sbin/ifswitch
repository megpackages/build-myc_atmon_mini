#!/bin/sh +x
# http://www.meg.com.tr

IF_ETH=0
IF_PPP=1
PRIMARY_IF=$IF_ETH              # default primary interface is eth or ppp
DB_VERSION=1                    # assume db is v1 instead of v2
PING_COUNT=1                    # try each ping once
PING_DEADLINE=3                 # in seconds
SLEEP_PERIOD=10                 # in seconds
MAX_BACKUP_UP_TIME=3600         # in seconds, def: 3600, 0: infinite
PING_MAX_RETRY_COUNT_PRIMARY=10 # 
PING_MAX_RETRY_COUNT_BACKUP=5   # 
PING_MAX_RETRY_COUNT=5          #
PPP_UP=0                        # eth0 is up initially
SET_GW=0                        # 1: set gateway
PPP_MAX_RETRY_COUNT=5           #
WAIT_PPP_UP=0                   # 1: wait ppp up
CONNECTING=0
ACTIVE_IF=$PRIMARY_IF
PPP_UP_TIME=0
ETH_UP_TIME=0
PPP_ENABLED=0
SECONDS=0
INIT_SECONDS=$(date +%s)
FILE_PATH="/meg/sbin/"
LOG_FILE="/var/log/logifswitch.txt"
#PPP_SERVER="google.com"

log()
{
    if [ -f $LOG_FILE ]; then
        echo $(date +"(%F %T) $1") >> $LOG_FILE
    fi
}

checkCarrier()
{
    RESULT=$(cat /sys/class/net/$1/carrier)
    if [ $? -eq 0 ]; then
        if [ $RESULT = "1" ]; then
            log "$1 is up."
            return 0
        else
            log "$1 is down."
            return 1
        fi
    else
        if [ "$1" == "ppp0" ]; then
            log "$1 is down. /sys/class/net/$1 folder doesn't exist."
            return 1
        else
            return 123  # error
        fi
    fi
}

updateDBVersion()
{
    RESULT=$(python ${FILE_PATH}dbread.py "SELECT PRIMARY_INTERFACE FROM PPP_SETTINGS;")
    if [ $? -eq 0 ]; then
        if [ "$RESULT" == "ppp" ]; then
            PRIMARY_IF=$IF_PPP
        else
            PRIMARY_IF=$IF_ETH
        fi
        DB_VERSION=2
        return 0
    else
        DB_VERSION=1
        log "Cannot read Primary Interface from DB."
        return 123  # error
    fi
}

updatePingMaxRetryCountPrimary()
{
    RESULT=$(python ${FILE_PATH}dbread.py "SELECT MAX_PING_RETRY_COUNT FROM PPP_SETTINGS;")
    if [ $? -eq 0 ]; then
        PING_MAX_RETRY_COUNT_PRIMARY=$RESULT
        return 0
    else
        log "Cannot read max Ping Retry Count Primary from DB."
        return 123  # error
    fi
}

updatePingMaxRetryCountBackup()
{

    RESULT=$(python ${FILE_PATH}dbread.py "SELECT MAX_PING_RETRY_COUNT FROM PPP_SETTINGS;")
    if [ $? -eq 0 ]; then
        PING_MAX_RETRY_COUNT_BACKUP=$RESULT
        return 0
    else
        log "Cannot read max Ping Retry Count Backup from DB."
        return 123  # error
    fi
}

updatePingPeriod()
{
    RESULT=$(python ${FILE_PATH}dbread.py "SELECT PING_PERIOD FROM PPP_SETTINGS;")
    if [ $? -eq 0 ]; then
        SLEEP_PERIOD=$RESULT
        return 0
    else
        log "Cannot read Ping Period from DB."
        return 123  # error
    fi
}

pingServer()
{
        ping -c $PING_COUNT -w $PING_DEADLINE $PING_SERVER > /dev/null
        return $?
}

updateMaxBackupUpTime()
{
    RESULT=$(python ${FILE_PATH}dbread.py "SELECT MAX_BACKUP_TIME FROM PPP_SETTINGS;")
    if [ $? -eq 0 ]; then
        MAX_BACKUP_UP_TIME=$RESULT
        return 0
    else
        log "Cannot read max PPP Up Time from DB."
        return 123  # error
    fi
}

getPPPEnabled()
{
    RESULT=$(python ${FILE_PATH}dbread.py "SELECT PPP_ENABLED FROM PPP_SETTINGS;")
    if [ $? -eq 0 ]; then
        if [ $RESULT = "1" ]; then
            return 0
        else
            return 1
        fi
    else
        log "Cannot read Interface Switch Enabled Status from DB."
        return 123  # error
    fi
}

updatePPPSettings()
{
    local TMP
    RESULT=$(python ${FILE_PATH}dbread.py "SELECT PPP_ENABLED, PRIMARY_INTERFACE, PING_SERVER_IP, MAX_PING_RETRY_COUNT, PING_PERIOD, MAX_BACKUP_TIME FROM PPP_SETTINGS;")
    if [ $? -eq 0 ]; then
        PPP_ENABLED=$(echo $RESULT | awk -F\$ '{print $1}';)
        TMP=$(echo $RESULT | awk -F\$ '{print $2}';)
        if [ "$TMP" == "ppp" ]; then
            PRIMARY_IF=$IF_PPP
        else
            PRIMARY_IF=$IF_ETH
        fi
        PPP_SERVER=$(echo $RESULT | awk -F\$ '{print $3}';)
        PING_MAX_RETRY_COUNT_PRIMARY=$(echo $RESULT | awk -F\$ '{print $4}';)
        PING_MAX_RETRY_COUNT_BACKUP=$PING_MAX_RETRY_COUNT_PRIMARY
        SLEEP_PERIOD=$(echo $RESULT | awk -F\$ '{print $5}';)
        MAX_BACKUP_UP_TIME=$(echo $RESULT | awk -F\$ '{print $6}';)
        return 0
    else
        log "Cannot read PPP Settings from DB."
        return 123  # error
    fi
}

updateXMLServers()
{
    RESULT=$(python ${FILE_PATH}dbread.py "SELECT SERVER_IP FROM XML_SERVERS;")
    if [ $? -eq 0 ]; then
        #PING_SERVER=$(echo $RESULT | awk -F# -v cnt=$1 '{print $cnt}';)
        XML_SERVERS=$RESULT
        return 0
    else
        log "Cannot read XML Server from DB."
        return 123  # error
    fi
}

    updateTimers()
{
    #seconds
    local CURRENT_SECONDS=$(date +%s)
    local OLDSECONDS=$SECONDS
    SECONDS=$((CURRENT_SECONDS-INIT_SECONDS))
    #timers
    PPP_UP_TIME=$(($PPP_UP_TIME+($SECONDS-$OLDSECONDS)))
    ETH_UP_TIME=$(($ETH_UP_TIME+($SECONDS-$OLDSECONDS)))
}

updatePPPConnectionFiles()
{
    local CNT
    local USERNAME
    local PASSWORD
    local APN
    local STATIC_IP
    local OK_COUNT=0
    #
    cp /etc/ppp/peers/sensplorer /dev/shm/sensplorer
    cp /etc/chatscripts/sensplorerChatScript /dev/shm/sensplorerChatScript
    ls /dev/ttyACM* > /dev/null
    if [ $? -eq 0 ]; then
        CNT=0
        ls /dev/ttyACM$CNT > /dev/null
        while [ $? -ne 0 ]; do
            CNT=$(($CNT+1))
            if [ $CNT -ge 255 ]; then
                break
            fi
            ls /dev/ttyACM$CNT > /dev/null
        done
        if [ $CNT -lt 255 ]; then   #there is an ACM device
            log "Trying /dev/ttyACM$CNT."
            OK_COUNT=$((OK_COUNT+1))
        fi
    fi
    #
    RESULT=$(python ${FILE_PATH}dbread.py "SELECT STATIC_IP, USERNAME, PASSWORD, APN FROM PPP_SETTINGS;")
    if [ $? -eq 0 ]; then
        STATIC_IP=$(echo $RESULT | awk -F\$ '{print $1}';)
        USERNAME=$(echo $RESULT | awk -F\$ '{print $2}';)
        PASSWORD=$(echo $RESULT | awk -F\$ '{print $3}';)
        APN=$(echo $RESULT | awk -F\$ '{print $4}';)
        OK_COUNT=$((OK_COUNT+1))
    else
        log "Cannot read APN Settings from DB."
    fi
    #
    if [ $OK_COUNT -eq 2 ]; then    #no error
        cat /etc/ppp/peers/sensplorer | awk -v cnt=$CNT -v user=$USERNAME \
            '{ \
                if($0~"/dev/ttyACM") \
                    printf("/dev/ttyACM%d\n", cnt) ; \
                else if($0~"user") \
                    printf("user \"%s\"\n", user) ; \
                else if($0~"connect") \
                    printf("connect \"/sbin/chat -v -f /dev/shm/sensplorerChatScript\"\n", user) ; \
                else print $0 ; \
            }' > /dev/shm/sensplorer
        cat /etc/chatscripts/sensplorerChatScript | awk -v user=$USERNAME -v pass=$PASSWORD -v apn=$APN -v ip=$STATIC_IP \
            '{ \
                if($0~"# ispnumber") \
                    printf("OK AT+CGDCONT=1,\"IP\",\"%s\"\n# ispnumber\n", apn) ; \
                else if($0~"# ispname") \
                    if(user=="") print $0 ; \
                    else printf("# ispname\nogin: \"%s\"\n", user) ; \
                else if($0~"# isppassword") \
                    if(pass=="") print $0 ; \
                    else printf("# isppassword\nssword: \"\\q%s\"\n", pass) ; \
                else print $0 ; \
            }' > /dev/shm/sensplorerChatScript
        return 0
    else
        return 123  #error
    fi
}

#initial assignments
updateDBVersion
updateTimers
updatePPPSettings
checkCarrier ppp0
CARRIER_PPP0=$?
if [ -s /var/run/resolv.conf.ppporig ] && [ $CARRIER_PPP0 -ne 0 ]; then
    cp /var/run/resolv.conf.ppporig /var/run/resolv.conf
    rm /var/run/resolv.conf.ppporig
    log "resolv.conf.ppporig restored."
elif [ $CARRIER_PPP0 -eq 0 ]; then
    ACTIVE_IF=$IF_PPP   #initially ppp0 up
    log "ppp0 was up."
fi
log "ifswitch started."
if [ $PRIMARY_IF -eq $IF_PPP ] && [ $CARRIER_PPP0 -ne 0 ]; then
    # connect ppp if primary is ppp and not connected yet
    PPP_UP=1
    CONNECTING=1
fi
#: '
echo "PRIMARY_IF=$PRIMARY_IF"
echo "DB_VERSION=$DB_VERSION"
echo "PING_COUNT=$PING_COUNT"
echo "PING_DEADLINE=$PING_DEADLINE"
echo "SLEEP_PERIOD=$SLEEP_PERIOD"
echo "MAX_BACKUP_UP_TIME=$MAX_BACKUP_UP_TIME"
echo "PING_MAX_RETRY_COUNT_PRIMARY=$PING_MAX_RETRY_COUNT_PRIMARY"
echo "PING_MAX_RETRY_COUNT_BACKUP=$PING_MAX_RETRY_COUNT_BACKUP"
echo "PPP_MAX_RETRY_COUNT=$PPP_MAX_RETRY_COUNT"
echo "INIT_SECONDS=$INIT_SECONDS"
#'


#main loop

while true; do
    #echo "SECONDS=$SECONDS"
    if [ $PPP_ENABLED -ne 0 ]; then
        if [ $CONNECTING -ne 0 ]; then
            if [ $PPP_UP -ne 0 ]; then  #ppp0
                PPP_RETRY_COUNT=0
                while [ $PPP_RETRY_COUNT -lt $PPP_MAX_RETRY_COUNT ];    do
                    #echo "{a}"
                    killall -HUP pppd       #shutdown ppp0 first
                    sleep 5
                    updatePPPConnectionFiles
                    pppd file /dev/shm/sensplorer  #connect
                    sleep 60
                    checkCarrier ppp0
                    CARRIER_PPP0=$?
                    if [ $CARRIER_PPP0 -eq 0 ]; then    #ppp0 up
                        #echo "{b}"
                        break
                    fi
                    PPP_RETRY_COUNT=$((PPP_RETRY_COUNT+1))
                done
                if [ $CARRIER_PPP0 -eq 0 ]; then
                    CONNECTING=0
                    #echo "{c}"
                    if [ $ACTIVE_IF -eq $IF_ETH ]; then
                        #echo "{d}"
                        SET_GW=1
                        ACTIVE_IF=$IF_PPP
                        PPP_UP_TIME=0   #start counting
                    fi
                    log "ppp0 is active."
                else
                    #echo "{e}"
                    PPP_UP=0
                    log "ppp0 call retry is over $PPP_MAX_RETRY_COUNT."
                fi
            else    #$eth0
                if [ $ACTIVE_IF -ne $IF_ETH ]; then
                    #echo "{f}"
                    killall -HUP pppd       #shutdown ppp0
                    SET_GW=1
                    ACTIVE_IF=$IF_ETH
                    ETH_UP_TIME=0   #start counting
                else
                    #echo "{g}"
                    # !!! SET_GW ?
                    CONNECTING=0
                    log "eth0 is active."
                fi
            fi
            #set gw
            if [ $SET_GW -ne 0 ]; then
                #echo "{h}"
                while [ $? -eq 0 ]; do
                    #echo "{i}"
                    route del default
                done
                if [ $PPP_UP -eq 0 ]; then  #eth0
                    #echo "{j}"
                    GATEWAY_ADDR=$(python ${FILE_PATH}dbread.py "SELECT GATEWAY_ADDR FROM NETWORK_SETTINGS;")
                    GATEWAY_ADDR_OK=$?
                    if [ $GATEWAY_ADDR_OK -eq 0 ]; then
                        #echo "{k}"
                        route add default gw $GATEWAY_ADDR eth0
                        log "Router added for eth0."
                    else
                        #echo "{l}"
                        log "No gateway address in db, restarting network."
                        /etc/init.d/network restart
                    fi
                else    #ppp0
                    #echo "{m}"
                    route add default ppp0
                    log "Router added for ppp0."
                fi
                SET_GW=0
            fi
        else    #[ $CONNECTING -eq 0 ]; then
            updatePPPSettings
            if [ $PRIMARY_IF -ne $ACTIVE_IF ] && [ $ACTIVE_IF -eq $IF_PPP ] && [ $MAX_BACKUP_UP_TIME -ne 0 ] && [ $PPP_UP_TIME -ge $MAX_BACKUP_UP_TIME ]; then
                #echo "{n}"
                PPP_UP=0
                CONNECTING=1
                log "ppp0 up time is over $MAX_BACKUP_UP_TIME."
            elif [ $PRIMARY_IF -ne $ACTIVE_IF ] && [ $ACTIVE_IF -eq $IF_ETH ] && [ $MAX_BACKUP_UP_TIME -ne 0 ] && [ $ETH_UP_TIME -ge $MAX_BACKUP_UP_TIME ]; then
                #echo "{n}"
                PPP_UP=1
                CONNECTING=1
                log "eth0 up time is over $MAX_BACKUP_UP_TIME."
            else
                CNT=0
                PING_OK=0
                updateXMLServers
                if [ $? -eq 0 ]; then   #xml servers found
                    for CNT in 1 2 3 4 5 6 7 8 9 10; do
                        PING_SERVER=$(echo $XML_SERVERS | awk -F# -v cnt=$CNT '{print $cnt}';)
                        if [ -z $PING_SERVER ]; then
                            break
                        else
                            pingServer
                            if [ $? -eq 0 ]; then
                                PING_OK=1
                                break
                            fi
                        fi
                    done
                fi
                if [ $PING_OK -eq 0 ]; then
                    PING_SERVER=$PPP_SERVER
                    pingServer
                    if [ $? -eq 0 ]; then
                        PING_OK=1
                    fi
                fi
                if [ $PING_OK -ne 0 ]; then
                    #echo "{o}"
                    PING_RETRY_COUNT=0
                else
                    #echo "{p}"
                    PING_RETRY_COUNT=$((PING_RETRY_COUNT+1))
                    if [ $PRIMARY_IF -eq $ACTIVE_IF ]; then
                        PING_MAX_RETRY_COUNT=$PING_MAX_RETRY_COUNT_PRIMARY
                    else
                        PING_MAX_RETRY_COUNT=$PING_MAX_RETRY_COUNT_BACKUP
                    fi
                    if [ $PING_RETRY_COUNT -ge $PING_MAX_RETRY_COUNT ]; then
                        #echo "{q}"
                        PING_RETRY_COUNT=0
                        log "No ping reply in $PING_MAX_RETRY_COUNT retries."
                        if [ $ACTIVE_IF -eq $IF_ETH ]; then
                            #echo "{r}"
                            PPP_UP=1
                            log "Activate ppp0."
                        else
                            #echo "{s}"
                            PPP_UP=0
                            log "Activate eth0."
                        fi
                        CONNECTING=1
                    fi
                fi
            fi
            #echo "PING_RETRY_COUNT=$PING_RETRY_COUNT"
            #if [ $PPP_MODE -ne 0 ]; then
                #echo "PPP_UP_TIME=$((PPP_UP_TIME-SECONDS))"
            #fi
        fi
    fi
    #timers
    sleep $SLEEP_PERIOD
    updateTimers
done
