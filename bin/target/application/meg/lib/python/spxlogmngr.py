import os
import sys
import time

class _Singleton(type):
    """ A metaclass that creates a Singleton base class when called. """
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(_Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

class Singleton(_Singleton('SingletonMeta', (object,), {})):
    pass

class Logger(Singleton):  
    def __init__(self, lvlmask= 255, logfile=None):
        self.lvlmask         = lvlmask      
        self.logfile            = None      

        try:     
            if logfile:
                self.logfile = open( logfile,'a+')
                
   
        except BaseException, e:
            print "\nException: " + str(e)

    def __del__(self):
        if self.logfile:
            self.logfile.close()
            
    def log(self, lvl, log="", function_deep=1):
        f = None
        try:
            raise Exception
        except:
            f = sys.exc_info()[2].tb_frame
            for i in range(function_deep):
                f= f.f_back
        rv = "(unknown file)", 0, "(unknown function)"
        while hasattr(f, "f_code"):
            co = f.f_code
            filename = os.path.normcase(co.co_filename)
            rv = (os.path.basename(co.co_filename), f.f_lineno, co.co_name)
            break
        
        color='\033[0m'
        lvl_decimal = 0
        if lvl == "ERROR":
            color='\033[91m'        # Red  
            lvl_decimal = 1
        elif lvl == "EXCEPTION":
            color='\033[91m'        # Red   
            lvl_decimal = 2
        elif lvl == "WARNING":
            color='\033[93m'        # Yellow
            lvl_decimal = 4
        elif lvl == "INFO":
            color='\033[92m'        # Green
            lvl_decimal = 8
        elif lvl == "DEBUG":
            color='\033[96m'        #  
            lvl_decimal = 16
        elif lvl   == "TRACE":
            color='\033[95m'        # 
            lvl_decimal = 32
        elif lvl == "ENTER":
            color='\033[90m'        # Grey
            lvl_decimal = 64
        elif lvl == "EXIT":
            color='\033[90m'        # Grey
            lvl_decimal = 128
        else :
            color='\033[0m'      
        
        strng =  color + "%14s %32s@%-4s %-9s %s" % (time.strftime("%6b%2d-%2H:%2M"), (rv[0] + "/" + rv[2]), rv[1], lvl,  log) + '\033[0m'
        
        if (self.lvlmask & lvl_decimal) == lvl_decimal:
            print strng
        if self.logfile:
            self.logfile.write(strng + "\n") 
