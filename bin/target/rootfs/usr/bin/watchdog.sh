#!/bin/sh
WDT_VALUE=1
echo "17" > /sys/class/gpio/export
echo "out" > /sys/class/gpio/gpio17/direction
while true
do
echo $WDT_VALUE > /sys/class/gpio/gpio17/value
if [ $WDT_VALUE -eq 1 ]; then
	WDT_VALUE=0
else
	WDT_VALUE=1
fi
sleep 5
done