#! /bin/sh 
#
# syslog	init.d script for busybox syslogd/klogd
#		Written by Robert Griebl <sandman@handhelds.org>
#               Configuration file added by <bruno.randolf@4g-systems.biz>

DESTINATION="buffer"	    # log destinations (buffer file remote)
MARKINT=20					# interval between --mark-- entries [min]
REDUCE=no					# reduced-size logging
BUFFERSIZE=128				# buffer: size of circular buffer [kByte]
LOGFILE=/media/mmc/log  	# file: where to log
ROTATESIZE=1024				# file: rotate log if grown beyond X [kByte] (busybox 1.2+)
ROTATEGENS=8				# file: keep X generations of rotated logs (busybox 1.2+)
REMOTE=loghost:514			# remote: where to log
FOREGROUND=no				# run in foreground (don't use!)

set -e

LOG_LOCAL=0
LOG_REMOTE=0
for D in $DESTINATION; do
	if [ "$D" = "buffer" ]; then
		SYSLOG_ARGS="$SYSLOG_ARGS -C$BUFFERSIZE"
		LOG_LOCAL=1
	elif [ "$D" = "file" ]; then
		if [ -n "$LOGFILE" ]; then
			SYSLOG_ARGS="$SYSLOG_ARGS -O $LOGFILE"
		fi
		if [ -n "$ROTATESIZE" ]; then
			SYSLOG_ARGS="$SYSLOG_ARGS -s $ROTATESIZE"
		fi
		if [ -n "$ROTATEGENS" ]; then
			SYSLOG_ARGS="$SYSLOG_ARGS -b $ROTATEGENS"
		fi
		LOCAL=0
	elif [ "$D" = "remote" ]; then
		SYSLOG_ARGS="$SYSLOG_ARGS -R $REMOTE"
		LOG_REMOTE=1
	fi
done
if [ "$LOG_LOCAL" = "1" -a "$LOG_REMOTE" = "1" ]; then
	SYSLOG_ARGS="$SYSLOG_ARGS -L"
fi
if [ -n "$MARKINT" ]; then
	SYSLOG_ARGS="$SYSLOG_ARGS -m $MARKINT"
fi
if [ "$REDUCE" = "yes" ]; then
	SYSLOG_ARGS="$SYSLOG_ARGS -S"
fi

case "$1" in
  start)
	echo -n "Starting syslogd/klogd: "
	start-stop-daemon -S -b -n syslogd -a /sbin/syslogd -- -n $SYSLOG_ARGS
	start-stop-daemon -S -b -n klogd -a /sbin/klogd -- -n
	echo "done"
	;;
  stop)
	echo -n "Stopping syslogd/klogd: "
	start-stop-daemon -K -n syslogd
	start-stop-daemon -K -n klogd
	echo "done"
	;;
  restart)
  	$0 stop
	$0 start
	;;
  *)
	echo "Usage: syslog { start | stop | restart }" >&2
	exit 1
	;;
esac

exit 0
