#!/usr/bin/python
import sys
import imp
import os
import optparse
import time

sys.path.append("/meg/lib/python")
import spxconfigutil

loglevel = 255
islogset = False
if os.path.exists("/meg/lib/python/spxlogmngr.py"):
    sys.path.append("/meg/lib/python/")
    import spxlogmngr
    islogset = True
    
def logger(lvl, log=""):
    global loglevel
    if islogset:
        spxlogmngr.Logger(loglevel, "megixinstaller.%s.log" % time.strftime("%Y%m%d.%H%M") ).log(lvl, log, 2)
    else:        
        print "%14s  %-8s %s" % (time.strftime("%6b%2d-%2H:%2M"), lvl,  log) 

def load_module(filepath, mdl = None):
    if not os.path.exists(filepath):
        raise Exception("module file does not exist !!! <" + filepath + ">")
    if not mdl:
        if (filepath.endswith(".py")):
            mdl = imp.new_module(filepath[:len(filepath)-3])
        else:
            mdl = imp.new_module(filepath)

    f = None
    try:
        f = open(filepath, "rU")
        src = f.read()
        exec(compile(src, filepath, 'exec'), mdl.__dict__)
    finally:
        if f:
            f.close()
    return mdl

def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    import re
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    '''
    return [ atoi(c) for c in re.split('(\d+)', text) ]

def getListVersion(): #convertto[version] dosyalarinin version bilgilerini listeler
    logger("ENTER")
    lstversion = []
    lstfile = os.listdir("/meg/lib/python/config")
    
    for name in lstfile:
        if name.startswith("convertto"):
            pos = name.index(".")
            version = name[9:pos]
            lstversion.append(version)
            logger("TRACE", "version :%s" % version)
    
    for i in lstversion:
        if i.find("_") == -1:
            index = valueToIndex(lstversion, i)
            i = "1_" + i + "_0"
            lstversion[index] = i
            logger("TRACE", "latest version :%s" % i)
    
    lstversion.sort(key=natural_keys)
    logger("EXIT")
    return lstversion

def valueToIndex(lst, version):
    index = -1
    for item in range(len(lst)):
        if lst[item] == version:
            index = item 
            return index
    return index
        
def fixedVersion(version):
    
    fv = ""
    for v in version:
        if v == ".":
            v="_"
        fv = fv + v
    
    return fv
            
def convertAll(new, path = None):
    logger("ENTER")
    
    if not path:
        path = spxconfigutil.dbpath()
    
    if not os.path.isfile(path):
        shutil.copy2(spxconfigutil.dbdefaulthpath(), path)
        logger("INFO", "SPXconfig.sqlite.default is copied as SPXconfig.sqlite")
        return
    
    old = spxconfigutil.getUsedVersion(path)
    old = fixedVersion(old)
    new = fixedVersion(new)
    logger("INFO", "Converting from old version<%s> to new version<%s>" % ( str(old), str(new) ))

    lst = getListVersion()
    for vers in lst:
        logger("DEBUG", "version :%s" % vers)
    

    oldIndex = valueToIndex(lst, old)
    newIndex = valueToIndex(lst, new)
    logger("TRACE", "oldIndex: %s , newIndex: %s" % (str(oldIndex) , str(newIndex)))
   
    if ( oldIndex == -1 and old != "2_2_6" ) or newIndex == -1:
        raise Exception("version list does not include old or new version")

    for index in range(oldIndex + 1, newIndex + 1):
        version = lst[index] 
        convertto = None
        convertto = load_module(os.path.join("/meg/lib/python/config/", "convertto" + version + ".py"), convertto)
        convertto.convert()   

    logger("EXIT")

parser = optparse.OptionParser(usage="spxconfigconverter.py [OPTIONS]")

def option():
    
    parser.add_option("-n", "--new", 
                    action="store",
                    type="string",
                    help="convert to new version. Ex[2_2_9]")     

    parser.add_option("-p", "--path", 
                    action="store",
                    type="string",
                    default="/meg/etc/SPXconfig.sqlite",
                    help="input file to read data from"
                    )  
                    
  
    
    (options, args) = parser.parse_args() 
    
    if not (options.old and options.new):
        parser.print_help()
        parser.error('Choose versions!!!')
    
    return  (options, args)

def main(argv):
    
    iRslt=1
    try:
        (options, args) = option()

        if(options.path):
            convertAll(options.new, options.path)
        if not(options.path):
            convertAll(options.new)

        iRslt=0  
    except BaseException, e:
        logger("EXCEPTION", str(e))
    finally:
        print "Bye Bye"
    sys.exit(iRslt)

if __name__ == "__main__":
    main(sys.argv)