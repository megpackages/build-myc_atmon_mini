#!/usr/bin/python

import sqlite3
import os
import imp
import sys
import time

sys.path.append("/meg/lib/python")
import spxconfigutil

loglevel = 255
islogset = False
if os.path.exists("/meg/lib/python/spxlogmngr.py"):
    sys.path.append("/meg/lib/python/")
    import spxlogmngr
    islogset = True
    
def logger(lvl, log=""):
    global loglevel
    if islogset:
        spxlogmngr.Logger(loglevel, "megixinstaller.%s.log" % time.strftime("%Y%m%d.%H%M") ).log(lvl, log, 2)
    else:        
        print "%14s  %-8s %s" % (time.strftime("%6b%2d-%2H:%2M"), lvl,  log) 

def find_interface_type(c, id):

    c.execute("select hwtype from system_info;")
    r = c.fetchone()
    hwtype = r[0]
    r = None
    c.execute("select number from mdlmngr_ports where id = " + str(id) + ";")
    r = c.fetchone()
    strPortNum = r[0]
    
    logger("TRACE", "hardwareType: %s" %hwtype)
    
    if (hwtype == "sensplorer_R2" or hwtype == "sensplorer_R5") and (strPortNum == 1 or strPortNum == 2):
        return "rs485"
    elif (hwtype == "sensplorer_R2" or hwtype == "sensplorer_R5") and (strPortNum == 3 or strPortNum == 4):
        return "rs232"
    elif (hwtype == "atmon" or hwtype == "atmon_mini") and (strPortNum == 1 or strPortNum == 2 or strPortNum == 3):
        return "rs485"
    elif (hwtype == "atmon" or hwtype == "atmon_mini") and strPortNum == 4:
        return "rs232"

def addnewcolumnin_mdlmngr_ports_serial(c, r):
    logger("ENTER")
    
    """SenPlorer 2.2.6 SPXconfig.sqlite da yer alan mdlmngr_ports_serial tablosuna interface kolonu eklenecek.
    """
    logger("DEBUG", "convertto2_2_7 test: interface column will be included mdlmngr_ports_serial table.")

    if not spxconfigutil.isColumnInTable("mdlmngr_ports_serial", "interface", c):
        c.execute("select id, boudrate, databits, stopbits, parity from mdlmngr_ports_serial")
        r = c.fetchall()
        c.execute("drop table mdlmngr_ports_serial")
        logger("DEBUG", "table:<mdlmngr_ports_serial> dropped")
        c.execute("CREATE TABLE mdlmngr_ports_serial (\
                    id INTEGER PRIMARY KEY, \
                    boudrate INTEGER,\
                    databits INTEGER,\
                    stopbits INTEGER,\
                    parity INTEGER,\
                    interface TEXT\
                );")
        logger("DEBUG", "created new table:<mdlmngr_ports_serial>")
        
        for i in r:
            id          =i[0]
            boudrate    =i[1]
            databits    =i[2]
            stopbits    =i[3]
            parity      =i[4]
            interface   =find_interface_type(c, id)
            
            sql = "insert into mdlmngr_ports_serial(id,\
                                         boudrate,\
                                         databits,\
                                         stopbits,\
                                         parity,\
                                         interface) values('" + str(id) + "', '" \
                                                         + str(boudrate) + "', '" \
                                                         + str(databits) + "', '" \
                                                         + str(stopbits) + "', '" \
                                                         + str(parity) + "', '" \
                                                         + str(interface) + "')"
                                                         
            logger("TRACE", "sql: %s" %sql)                           
            c.execute(sql)
            
        logger("INFO", "Column<%s> is inserted into table:<%s>" %("interface", "mdlmngr_ports_serial"))
        
    logger("EXIT")

def test1(c, r):
    
    """SenPlorer 2.2.6 SPXconfig.sqlite da yer alan ip_cameras tablosuna user kolonu eklenecek.
    Describe IP_CAMERAS
        CREATE TABLE ip_cameras(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        protocol TEXT NOT NULL,
        ip TEXT NOT NULL,
        port INTEGER NOT NULL,
        name TEXT NOT NULL,
        link TEXT NOT NULL,
        username TEXT NOT NULL DEFAULT('*'),
        password TEXT NOT NULL DEFAULT('*'))
    """
    c.execute("select firmware_version from system_info")
    r = c.fetchall()
    for i in r:
        for o in i:
            version = o
    
    print "convertto2_2_7 test: user column will be included ip_cameras table."
    if not spxconfigutil.isColumnInTable("ip_cameras", "user", c):
        c.execute("select * from ip_cameras")
        r = c.fetchall()
        c.execute("drop table ip_cameras")
        c.execute("CREATE TABLE ip_cameras (\
                    id INTEGER PRIMARY KEY AUTOINCREMENT,\
                    protocol TEXT NOT NULL,\
                    ip TEXT NOT NULL,\
                    port INTEGER NOT NULL,\
                    name TEXT NOT NULL,\
                    link TEXT NOT NULL,\
                    username TEXT NOT NULL DEFAULT('*'),\
                    password TEXT NOT NULL DEFAULT('*'),\
                    user TEXT NOT NULL\
                    );")
        
        for i in r:
            id          =i[0]
            protocol    =i[1]
            ip          =i[2]
            port        =i[3]
            name        =i[4]
            link        =i[5]
            username    =i[6]
            password    =i[7]
            user        = "suleyman"
            
            sql = "insert into ip_cameras(id,\
                                         protocol,\
                                         ip,\
                                         port,\
                                         name,\
                                         link,\
                                         username,\
                                         password,\
                                         user) values('" + str(id) + "', '" \
                                                         + str(protocol) + "', '" \
                                                         + str(ip) + "', '" \
                                                         + str(port) + "', '" \
                                                         + str(name) + "', '" \
                                                         + str(link) + "', '" \
                                                         + str(username) + "', '" \
                                                         + str(password) + "', '" \
                                                         + str(user) + "')"
                                                             
            c.execute(sql)
    

def convertToNewVersion(c):
    
    c.execute("UPDATE system_info SET firmware_version = '2.2.7'")
    logger("INFO", "firmware version updated to 2.2.7")

def convert(path=None):
    
    logger("ENTER")
    
    if not path:
        path = spxconfigutil.dbpath()
        
    db = sqlite3.connect(path)
    c = db.cursor()
    r = None
    
    convertToNewVersion(c)
    addnewcolumnin_mdlmngr_ports_serial(c, r)

    db.commit()
    db.close()
    
    logger("EXIT")
