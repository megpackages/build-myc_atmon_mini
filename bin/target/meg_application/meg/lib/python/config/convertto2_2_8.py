#!/usr/bin/python

import sqlite3
import os
import imp
import sys
import time

sys.path.append("/meg/lib/python")
import spxconfigutil

loglevel = 255
islogset = False
if os.path.exists("/meg/lib/python/spxlogmngr.py"):
    sys.path.append("/meg/lib/python/")
    import spxlogmngr
    islogset = True
    
def logger(lvl, log=""):
    global loglevel
    if islogset:
        spxlogmngr.Logger(loglevel, "megixinstaller.%s.log" % time.strftime("%Y%m%d.%H%M") ).log(lvl, log, 2)
    else:        
        print "%14s  %-8s %s" % (time.strftime("%6b%2d-%2H:%2M"), lvl,  log) 

def fix_timezone_in_ntp_server(c, r):
    logger("ENTER")
    """SenPlorer 2.2.7 SPXconfig.sqlite da yer alan ntp_server tablosunun zoninfo kolonunu düzenler.
    """
    logger("DEBUG", "convertto2_2_8 test: zoninfo column will be arranged in ntp_server table.")

    c.execute("select server_ip, server_ip_backup, timezone, active from ntp_server")
    r = c.fetchall()
    c.execute("drop table ntp_server")
    logger("DEBUG", "table:<ntp_server> dropped")
    
    c.execute("CREATE TABLE ntp_server (\
                server_ip TEXT,\
                server_ip_backup TEXT,\
                timezone INTEGER,\
                active INTEGER\
            );")
    logger("DEBUG", "created new table:<ntp_server>")
        
    for i in r:
        server_ip           =i[0]
        server_ip_backup    =i[1]
        timezone            =i[2]
        active              =i[3]
        
        if timezone == 0:
            timezone = 2
        elif timezone == 1:
            timezone = 3
        elif timezone >= 2 and timezone <= 5:
            timezone = timezone+5
        else:
            timezone = 8
        
        sql = "insert into ntp_server(server_ip,\
                                     server_ip_backup,\
                                     timezone,\
                                     active ) values('" + str(server_ip) + "', '" \
                                                        + str(server_ip_backup) + "', '" \
                                                         + str(timezone) + "', '" \
                                                         + str(active) + "')"
                                     
        logger("TRACE", "sql: %s" %sql)                           
        c.execute(sql)
            
    logger("INFO", "timezone changed as %d" %timezone )
        
    logger("EXIT")

    

def convertToNewVersion(c):
    
    c.execute("UPDATE system_info SET firmware_version = '2.2.8'")
    logger("INFO", "firmware version updated to 2.2.8")

def convert(path=None):
    
    logger("ENTER")
    
    if not path:
        path = spxconfigutil.dbpath()
        
    db = sqlite3.connect(path)
    c = db.cursor()
    r = None
    
    fix_timezone_in_ntp_server(c, r)
    convertToNewVersion(c)

    db.commit()
    db.close()
    
    logger("EXIT")
