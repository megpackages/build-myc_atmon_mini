#!/usr/bin/python

import sqlite3
import os
import imp
import sys
import time

sys.path.append("/meg/lib/python")
import spxconfigutil

loglevel = 255
islogset = False
if os.path.exists("/meg/lib/python/spxlogmngr.py"):
    sys.path.append("/meg/lib/python/")
    import spxlogmngr
    islogset = True
    
def logger(lvl, log=""):
    global loglevel
    if islogset:
        spxlogmngr.Logger(loglevel, "megixinstaller.%s.log" % time.strftime("%Y%m%d.%H%M") ).log(lvl, log, 2)
    else:        
        print "%14s  %-8s %s" % (time.strftime("%6b%2d-%2H:%2M"), lvl,  log) 


def addnewtable_mdlmngr_devices_infrared(c):
    logger("ENTER")
    
    """SenPlorer 2.3.0 SPXconfig.sqlite içerisine mdlmngr_devices_infrared tablosu eklenecek.
    """
    c.execute("CREATE TABLE IF NOT EXISTS mdlmngr_devices_infrared (\
                id INTEGER PRIMARY KEY, \
                model TEXT NOT NULL,\
                ref_file TEXT NOT NULL,\
                plugin_path TEXT NOT NULL\
            );")
    
    logger("INFO", "mdlmngr_devices_infrared table has been added in SPXconfig.sqlite")
        
    logger("EXIT")

    
def addnewtable_mdlmngr_devices_pcomm(c):
    logger("ENTER")
    
    """SenPlorer 2.3.0 SPXconfig.sqlite içerisine mdlmngr_devices_pcomm tablosu eklenecek.
    """
    c.execute("CREATE TABLE IF NOT EXISTS mdlmngr_devices_pcomm (\
                id INTEGER PRIMARY KEY, \
                ref_file TEXT NOT NULL\
            );")
    
    logger("INFO", "mdlmngr_devices_pcomm table has been added in SPXconfig.sqlite")
        
    logger("EXIT")



def addnewtable_mdlmngr_sensors_infrared(c):
    logger("ENTER")
    
    """SenPlorer 2.3.0 SPXconfig.sqlite içerisine mdlmngr_sensors_infrared tablosu eklenecek.
    """
    c.execute("CREATE TABLE IF NOT EXISTS mdlmngr_sensors_infrared (\
                id INTEGER PRIMARY KEY, \
                repeat_count TEXT NOT NULL\
            );")
    
    logger("INFO", "mdlmngr_sensors_infrared table has been added in SPXconfig.sqlite")
        
    logger("EXIT")

    
def addnewtable_mdlmngr_sensors_pcomm(c):
    logger("ENTER")
    
    """SenPlorer 2.3.0 SPXconfig.sqlite içerisine mdlmngr_sensors_pcomm tablosu eklenecek.
    """
    c.execute("CREATE TABLE IF NOT EXISTS mdlmngr_sensors_pcomm (\
                id INTEGER PRIMARY KEY, \
                sys_type TEXT NOT NULL,\
                sys_index TEXT NOT NULL,\
                subsys TEXT NOT NULL,\
                subsys_index TEXT NOT NULL,\
                data_obj TEXT NOT NULL,\
                data_type TEXT NOT NULL,\
                parser_type TEXT NOT NULL\
            );")
    
    logger("INFO", "mdlmngr_sensors_pcomm table has been added in SPXconfig.sqlite")
        
    logger("EXIT")


def addnewtable_mdlmngr_sensors_virtualdev(c):
    logger("ENTER")

    """SenPlorer 2.3.0 SPXconfig.sqlite içerisine mdlmngr_sensors_virtualdev tablosu eklenecek.
    """
    c.execute("CREATE TABLE IF NOT EXISTS mdlmngr_sensors_virtualdev (\
                id INTEGER PRIMARY KEY,\
                varnum INTEGER NOT NULL,\
                variables TEXT NOT NULL,\
                formula TEXT NOT NULL\
            );")

    logger("INFO", "mdlmngr_sensors_virtualdev table has been added in SPXconfig.sqlite")

    logger("EXIT")


def addnewcolumnin_ntfymngr_email(c, r):
    logger("ENTER")
    
    """SenPlorer 2.3.0 SPXconfig.sqlite da yer alan ntfymngr_email tablosuna receipt_enabled kolonu eklenecek.
    """
    logger("DEBUG", "convertto2_3_1 test: receipt_enabled column will be included ntfymngr_email table.")

    if not spxconfigutil.isColumnInTable("ntfymngr_email", "receipt_enabled", c):
        c.execute("select smtp_server, smtp_port, username, loginname, password from ntfymngr_email")
        r = c.fetchall()
        c.execute("drop table ntfymngr_email")
        logger("DEBUG", "table:<ntfymngr_email> dropped")
        c.execute("CREATE TABLE ntfymngr_email (\
                    smtp_server TEXT, \
                    smtp_port INTEGER,\
                    username TEXT,\
                    loginname TEXT,\
                    password TEXT,\
                    receipt_enabled INTEGER\
                );")
        logger("DEBUG", "created new table:<ntfymngr_email>")
        
        for i in r:
            smtp_server         =i[0]
            smtp_port           =i[1]
            username            =i[2]
            loginname           =i[3]
            password            =i[4]
            receipt_enabled     = 0
            
            sql = "insert into ntfymngr_email(smtp_server,\
                                        smtp_port,\
                                        username,\
                                        loginname,\
                                        password,\
                                        receipt_enabled) values('" + str(smtp_server) + "', '" \
                                                         + str(smtp_port) + "', '" \
                                                         + str(username) + "', '" \
                                                         + str(loginname) + "', '" \
                                                         + str(password) + "', '" \
                                                         + str(receipt_enabled) + "')"
                                                         
            logger("TRACE", "sql: %s" %sql)                           
            c.execute(sql)
            
        logger("INFO", "Column<%s> is inserted into table:<%s>" %("receipt_enabled", "ntfymngr_email"))
        
    logger("EXIT")
    
def addOffsetColumn_into_mdlmngr_sensors_float(c, r):
    
    if not spxconfigutil.isColumnInTable("mdlmngr_sensors_float", "offset", c):
        c.execute("select id, unit, warning_low, warning_high, alarm_low, alarm_high, time_limit, \
                 hys_time_limit, filter_type, filter_value, multiplier from mdlmngr_sensors_float")
        r = c.fetchall()
        c.execute("drop table mdlmngr_sensors_float")
        logger("DEBUG", "table:<mdlmngr_sensors_float> dropped")
        c.execute("CREATE TABLE mdlmngr_sensors_float (\
                    id INTEGER PRIMARY KEY, \
                    unit TEXT NOT NULL, \
                    warning_low REAL NOT NULL, \
                    warning_high REAL NOT NULL, \
                    alarm_low REAL NOT NULL, \
                    alarm_high REAL NOT NULL, \
                    time_limit INTEGER NOT NULL, \
                    hys_time_limit INTEGER NOT NULL, \
                    filter_type INTEGER NOT NULL, \
                    filter_value REAL NOT NULL, \
                    multiplier REAL NOT NULL, \
                    offset REAL \
                );")
        
        logger("DEBUG", "created new table:<mdlmngr_sensors_float>")
    
        for i in r:
            id              =i[0]
            unit            =i[1]
            warning_low     =i[2]
            warning_high    =i[3]
            alarm_low       =i[4]
            alarm_high      =i[5]
            time_limit      =i[6]
            hys_time_limit  =i[7]
            filter_type     =i[8]
            filter_value    =i[9]
            multiplier      =i[10]
            offset          = 0
            
            sql = "insert into mdlmngr_sensors_float(id,\
                                                    unit,\
                                                    warning_low,\
                                                    warning_high,\
                                                    alarm_low,\
                                                    alarm_high,\
                                                    time_limit,\
                                                    hys_time_limit,\
                                                    filter_type,\
                                                    filter_value,\
                                                    multiplier,\
                                                    offset) values('" + str(id) + "', '" \
                                                         + str(unit) + "', '" \
                                                         + str(warning_low) + "', '" \
                                                         + str(warning_high) + "', '" \
                                                         + str(alarm_low) + "', '" \
                                                         + str(alarm_high) + "', '" \
                                                         + str(time_limit) + "', '" \
                                                         + str(hys_time_limit) + "', '" \
                                                         + str(filter_type) + "', '" \
                                                         + str(filter_value) + "', '" \
                                                         + str(multiplier) + "', '" \
                                                         + str(offset) + "')"
                                                         
            logger("TRACE", "sql: %s" %sql)                           
            c.execute(sql)
            
        logger("INFO", "Column<%s> is inserted into table:<%s>" %("offset", "mdlmngr_sensors_float"))
    else:
        logger("DEBUG", "Column<%s> is already exist in table:<%s>" %("offset", "mdlmngr_sensors_float"))
    
    logger("EXIT")

def convertToNewVersion(c):
    
    c.execute("UPDATE system_info SET firmware_version = '2.3.1'")
    logger("INFO", "firmware version updated to 2.3.1")

def convert(path=None):
    
    logger("ENTER")
    
    if not path:
        path = spxconfigutil.dbpath()
        
    db = sqlite3.connect(path)
    c = db.cursor()
    r = None
    
    convertToNewVersion(c)
    addOffsetColumn_into_mdlmngr_sensors_float(c, r)
    addnewcolumnin_ntfymngr_email(c, r)
    addnewtable_mdlmngr_devices_infrared(c)
    addnewtable_mdlmngr_devices_pcomm(c)
    addnewtable_mdlmngr_sensors_infrared(c)
    addnewtable_mdlmngr_sensors_pcomm(c)
    addnewtable_mdlmngr_sensors_virtualdev(c)

    db.commit()
    db.close()
    
    logger("EXIT")
