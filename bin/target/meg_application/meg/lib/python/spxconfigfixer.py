#!/usr/bin/python
import imp
import os
import sys
import time

sys.path.append("/meg/lib/python")
import spxconfigutil

loglevel = 255
islogset = False
if os.path.exists("/meg/lib/python/spxlogmngr.py"):
    sys.path.append("/meg/lib/python/")
    import spxlogmngr
    islogset = True
    
def logger(lvl, log=""):
    global loglevel
    if islogset:
        spxlogmngr.Logger(loglevel, "megixinstaller.%s.log" % time.strftime("%Y%m%d.%H%M") ).log(lvl, log, 2)
    else:        
        print "%14s  %-8s %s" % (time.strftime("%6b%2d-%2H:%2M"), lvl,  log) 

def fixConfig(temp = None, defaults = None):

    compatible = True
    dbDflt          = spxconfigutil.connect(spxconfigutil.dbdefaultpath())
    cDflt           = spxconfigutil.getcursor(dbDflt)
    defaultConfig   = spxconfigutil.createConfigSchema(cDflt)
    spxconfigutil.close(dbDflt)
    logger("DEBUG", "Default config schema is created")

    dbSpx       = spxconfigutil.connect(spxconfigutil.dbpath())
    cSpx        = spxconfigutil.getcursor(dbSpx)
    spxConfig   = spxconfigutil.createConfigSchema(cSpx)
    logger("DEBUG", "Config schema is created")

    #spxconfigutil.printConfigSchema(defaultConfig)
    #spxconfigutil.printConfigSchema(spxConfig)

    for tablename in defaultConfig:
        logger("DEBUG", "Checking table:<%s>" % tablename)               

        dfltTable = defaultConfig.get(tablename)
        spxTable  = spxConfig.get(tablename)
        try:
            if spxTable == None:
                compatible = False
                logger("WARNING", "Table not found in spx.name:<%s>" % tablename)
                spxconfigutil.createTable(dfltTable, cSpx)
                logger("INFO", "Table is created in spx.name:<%s>" % tablename)
            else:
                lostCols = []
                incompCols = []
                extraCols  = []
                for colname in dfltTable:
                    if colname == "tablename":
                        continue
                    
                    logger("TRACE", "Checking column:<%s>" % colname) 
                    dfltCol = dfltTable.get(colname)
                    spxCol  = spxTable.get(colname)
                    if spxCol == None:
                        compatible = False
                        logger("WARNING", "Column:<%s> not found in table.name:<%s>" %(colname, tablename))            
                        lostCols.append(colname)
                    elif not spxconfigutil.isColumnsCompatible(dfltCol, spxCol):
                        compatible = False
                        logger("WARNING", "Column:<%s> not compatible in table.name:<%s>" %(colname, tablename))                
                        incompCols.append(colname)

                for colname in spxTable:
                    if colname == "tablename":
                        continue

                    dfltCol = dfltTable.get(colname)
                    spxCol  = spxTable.get(colname)
                    if dfltCol == None:
                        compatible = False
                        logger("WARNING", "Column:<%s> is extra in table.name:<%s>" %(colname, tablename))               
                        extraCols.append(colname)

                for i in range(len(lostCols)):
                    colname = lostCols[i]                    
                    dfltCol = dfltTable.get(colname)
                    spxconfigutil.addColumnToTable(tablename, dfltCol, cSpx)
                    logger("DEBUG", "Column:<%s> is added to table.name:<%s>" %(colname, tablename))              

                if len(extraCols) != 0 or len(incompCols) != 0:
                    spxconfigutil.recreateTable(dfltTable, cSpx)
                    for i in range(len(extraCols)):
                        colname = extraCols[i]
                        logger("DEBUG", "Column:<%s> is compatibilized with table.name:<%s>" %(colname, tablename))               
                    for i in range(len(incompCols)):
                        colname = incompCols[i]
                        logger("DEBUG", "Column:<%s> is dropped from table.name:<%s>" %(colname, tablename))                

        except Exception as e:
            logger("EXCEPTION", "Exception:%s" % str(e))
            dbSpx.rollback()

    for tablename in spxConfig:
        dfltTable = defaultConfig.get(tablename)
        spxTable  = spxConfig.get(tablename)
        try:
            if dfltTable == None:
                logger("WARNING", "Table:<%s> not found in default config" % tablename)             
                spxconfigutil.dropTable(tablename, cSpx)
                logger("DEBUG", "Table:<%s> dropped from spx config" % tablename)                
        except Exception as e:
            logger("EXCEPTION", "Exception:%s" % str(e))
    
    spxconfigutil.commit(dbSpx)
    spxconfigutil.close(dbSpx)
    return compatible

    return True

def main(argv):
    
    iRslt=1
    try:
        fixConfig()
        iRslt=0  
    except BaseException, e:
        logger("EXCEPTION", "Exception:%s" % str(e))
    finally:
        logger("INFO", "Bye Bye")
    sys.exit(iRslt)

if __name__ == "__main__":
    main(sys.argv)