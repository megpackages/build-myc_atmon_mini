Settings.xmlServerData = {};

Settings.xmlServerList = [
	{
		id: 0,
		name: "Sunucu 1"
	},
	{
		id: 1,
		name: "Sunucu 2"
	},
	{
		id: 2,
		name: "Sunucu 3"
	},
	{
		id: 3,
		name: "Sunucu 4"
	},
];

Settings.callXmlServer = function() {
	$("div#settings div.form").html("");
	var html = '<div class="alert"></div>'
				+ '<div class="alertNaN"></div>'
				+ '<ul class="element" id="xmlserverform" style="display:none;">'
				+	'<li id="server"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="xmlactive" style="display: none;"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="detailed" style="display: none;"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="serveraddr" style="display: none;"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="serverport" style="display: none;"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="xmlperiod" style="display: none;"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="xmlservice" style="display: none;"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="buttons"></li>'
				+ '</ul>'
				+ '<div id="alertDelete" style="width: 400px;" class="panelDiv xmlServer">'
					+ '<span class="text"></span>'
					+ '<div class="buttons">'
					+ '	<input type="submit" id="buttonRemove" name="buttonRemove" class="submit">'
					+ '	<input type="submit" id="buttonClose" name="buttonClose" class="submit">'
					+ '</div>'
				+ '</div>';
	$("div#settings div.form").html(html);
	Settings.getXmlServerList();
	//Settings.createXmlServerForm();
};

Settings.getXmlServerList = function() {
	var data = $.getJSON("http://" + hostName + "/cgi-bin/networksettings.cgi?getXmlServers");	 
	data.success(function() {
		Settings.xmlServerList = jQuery.parseJSON(data.responseText);
		Settings.createXmlServerForm();
		Main.unloading();
	});
	data.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
	    Main.unloading();
	});
};

Settings.getXmlServerItem = function(id) {
	var data = $.getJSON("http://" + hostName + "/cgi-bin/networksettings.cgi?getXmlServer&id="+id);	 
	data.success(function() {
		Settings.xmlServerData = jQuery.parseJSON(data.responseText);
		Settings.getXmlServerForm();
		Main.unloading();
	});
	data.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Settings.removeXmlServer = function(id) { 
	$("div#overlay").css("display","none");
	$("#content div#settings div.form div#alertDelete").css("display","none");
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/networksettings.cgi?removeXmlServer&id=" + id;
	$.ajax({
	    url : url,
	    type: "POST",
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			Settings.callXmlServer();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.addXmlServer = function() {
	Main.loading();
	json = JSON.stringify(Settings.xmlServerData);
	var url = "http://" + hostName + "/cgi-bin/networksettings.cgi?addXmlServer";
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			Main.alert(language.Settings_server_add_success);
			Settings.callXmlServer();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.updateXmlServer = function(id) {
	Main.loading();
	json = JSON.stringify(Settings.xmlServerData);
	var url = "http://" + hostName + "/cgi-bin/networksettings.cgi?updateXmlServer&id=" + id;
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			Main.alert(language.Settings_server_update_success);
			Settings.callXmlServer();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.testXmlServer = function(id) {
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/networksettings.cgi?testXmlServer&id=" + id;
	$.ajax({
	    url : url,
	    type: "POST",
        dataType: 'text',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			Main.alert(language.Settings_xmlserver_test_success);
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.createXmlServerForm = function() {
	Main.unloading();

	if(Settings.xmlServerList.length == 0) {
		$("li#server").css("display","none");
		$("div#settings div.alertNaN").html(language.settings_server_not);
	} else {
		$("li#server").css("display","block");
		$("div#settings div.alertNaN").html("");
	}

	var item = Settings.xmlServerList;
	var id = item.id;
	var active = item.active;
	var detailed = item.detailed;
	var server = item.server;
	var port = item.port;
	var period = item.period;
	var webservice = item.webservice;

	var xmlServerList = document.createElement("select");
	xmlServerList.setAttribute("id","xmlServerList");
	var option0 = document.createElement("option");
	option0.setAttribute("value","none");
	option0.innerHTML = language.Settings_xml_server_select;

	xmlServerList.appendChild(option0);

	for(var i = 0; i < Settings.xmlServerList.length; i++) {
		var list = Settings.xmlServerList[i];
		var option = document.createElement("option");
		option.setAttribute("value",list.id);
		option.innerHTML = list.server_ip;
		xmlServerList.appendChild(option);
	}

	$("li#server div.left").html(language.Settings_xml_server_list);
	$("li#server div.right").append(xmlServerList);

	var activeInput = document.createElement("input");
	activeInput.setAttribute("type","checkbox");
	activeInput.setAttribute("id","activeInput");
	$("li#xmlactive div.left").html(language.Settings_xml_server_active);
	$("li#xmlactive div.right").append(activeInput);

	var detailedInput = document.createElement("input");
	detailedInput.setAttribute("type","checkbox");
	detailedInput.setAttribute("id","detailedInput");
	$("li#detailed div.left").html(language.Settings_xml_server_detailed);
	$("li#detailed div.right").append(detailedInput);

	var serverInput = document.createElement("input");
	serverInput.setAttribute("type","text");
	serverInput.setAttribute("id","serverInput");
	$("li#serveraddr div.left").html(language.Settings_xml_server_addr);
	$("li#serveraddr div.right").append(serverInput);

	var portInput = document.createElement("input");
	portInput.setAttribute("type","text");
	portInput.setAttribute("id","portInput");
	$("li#serverport div.left").html(language.Settings_xml_server_port);
	$("li#serverport div.right").append(portInput);

	var periodInput = document.createElement("input");
	periodInput.setAttribute("type","text");
	periodInput.setAttribute("id","periodInput");
	$("li#xmlperiod div.left").html(language.Settings_xml_server_period);
	$("li#xmlperiod div.right").append(periodInput);

	var serviceInput = document.createElement("input");
	serviceInput.setAttribute("type","text");
	serviceInput.setAttribute("id","serviceInput");
	$("li#xmlservice div.left").html(language.Settings_xml_server_service);
	$("li#xmlservice div.right").append(serviceInput);

	var testInput = document.createElement("input");
	if (Settings.xmlServerList.length == 0)
		testInput.style.display = "none";
	testInput.setAttribute("type","submit");
	testInput.setAttribute("id","testXml");
	testInput.setAttribute("value", language.Settings_xml_server_test);
	
	var updateInput = document.createElement("input");
	if (Settings.xmlServerList.length == 0)
		updateInput.style.display = "none";
	updateInput.setAttribute("type","submit");
	updateInput.setAttribute("id","Update");
	updateInput.setAttribute("value", language.Settings_xml_server_update);	
	
	var newInput = document.createElement("input");
	newInput.setAttribute("type","submit");
	newInput.setAttribute("id","new");
	newInput.setAttribute("value", language.Settings_xml_server_new);


	var saveInput = document.createElement("input");
	saveInput.setAttribute("type","submit");
	saveInput.setAttribute("id","save");
	saveInput.style.display = "none";
	saveInput.setAttribute("value", language.Settings_xml_server_save);
	
	var removeInput = document.createElement("input");
	removeInput.setAttribute("type","submit");
	removeInput.setAttribute("id","remove");
	removeInput.style.display = "none";
	removeInput.setAttribute("value", language.Settings_xml_server_remove);

	var cancelInput = document.createElement("input");
	cancelInput.setAttribute("type","submit");
	cancelInput.setAttribute("id","cancelXml");
	cancelInput.style.display = "none";
	cancelInput.setAttribute("value", language.Settings_xml_server_cancel);


	$("li#buttons").append(testInput);
	$("li#buttons").append(updateInput);
	$("li#buttons").append(newInput);
	$("li#buttons").append(saveInput);
	$("li#buttons").append(removeInput);
	$("li#buttons").append(cancelInput);

	$("div#settings ul.element").css("display","block");
};

Settings.getXmlServerForm = function() {
	var item = Settings.xmlServerData;
	var id = item.id;
	var active = item.send_enabled;
	var detailed = item.detailed;
	var server = item.server_ip;
	var port = item.server_port;
	var period = item.send_interval;
	var webservice = item.server_page;

	$("li#server").css("display","none");
	$("li#xmlactive").css("display","block");
	$("li#detailed").css("display","block");
	$("li#serveraddr").css("display","block");
	$("li#serverport").css("display","block");
	$("li#xmlperiod").css("display","block");
	$("li#xmlservice").css("display","block");
	$("li#buttons input#new").css("display","none");
	$("li#buttons input#testXml").css("display","none");
	$("li#buttons input#Update").css("display","none");
	$("li#buttons input#save").css("display","inline-block");
	$("li#buttons input#cancelXml").css("display","inline-block");
	$("li#buttons input#remove").css("display","inline-block");

	if(active == 1) {
		$("li#xmlactive input").prop("checked",true);
	} else {
		$("li#xmlactive input").prop("checked",false);
	}

	if(detailed == 1) {
		$("li#detailed input").prop("checked",true);
	} else {
		$("li#detailed input").prop("checked",false);
	}

	$("li#serveraddr input").val(server);
	$("li#serverport input").val(port);
	$("li#xmlperiod input").val(period);
	$("li#xmlservice input").val(webservice);
};

Settings.newXmlServerForm = function() {
	$("li#server").css("display","none");
	$("li#xmlactive").css("display","block");
	$("li#detailed").css("display","block");
	$("li#serveraddr").css("display","block");
	$("li#serverport").css("display","block");
	$("li#xmlperiod").css("display","block");
	$("li#xmlservice").css("display","block");
	$("li#buttons input#new").css("display","none");
	$("li#buttons input#testXml").css("display","none");
	$("li#buttons input#Update").css("display","none");
	$("li#buttons input#save").css("display","inline-block");
	$("li#buttons input#cancelXml").css("display","inline-block");
	$("li#buttons input#remove").css("display","none");

	$("li#xmlactive input").prop("checked",false);
	$("li#detailed input").prop("checked",false);
	$("li#serveraddr input").val("");
	$("li#serverport input").val("");
	$("li#xmlperiod input").val("");
	$("li#xmlservice input").val("");
};

Settings.clearXmlServerForm = function() {

	if(Settings.xmlServerList.length == 0) {
		$("li#server").css("display","none");
		$("div#settings div.alertNaN").html(language.settings_server_not);
		$("li#buttons input#testXml").css("display","none");
		$("li#buttons input#Update").css("display","none");
	} else {
		$("li#server").css("display","block");
		$("li#server option:first").prop("selected",true);
		$("div#settings div.alertNaN").html("");
		$("li#buttons input#testXml").css("display","inline-block");
		$("li#buttons input#Update").css("display","inline-block");
	}

	$("li#xmlactive").css("display","none");
	$("li#detailed").css("display","none");
	$("li#serveraddr").css("display","none");
	$("li#serverport").css("display","none");
	$("li#xmlperiod").css("display","none");
	$("li#xmlservice").css("display","none");
	$("li#buttons input#new").css("display","inline-block");
	
	
	$("li#buttons input#save").css("display","none");
	$("li#buttons input#cancelXml").css("display","none");
	$("li#buttons input#remove").css("display","none");

	$("li#xmlactive input").prop("checked",false);
	$("li#serveraddr input").val("");
	$("li#serverport input").val("");
	$("li#xmlperiod input").val("");
	$("li#xmlservice input").val("");
};

Settings.validateXmlServerForm = function(page) {	
	$("div#settings div.alert").html("");
	var alertHtml = $("div#settings div.alert").html();
	$("li#serveraddr").removeClass("alert");
	$("li#serverport").removeClass("alert");
	$("li#xmlperiod").removeClass("alert");
	$("li#xmlservice").removeClass("alert");

	var active 	 = $("li#xmlactive input").prop("checked");
	var detailed = $("li#detailed input").prop("checked");
	var ip 	     = $("li#serveraddr input").val();
	var port     = $("li#serverport input").val();
	var period   = $("li#xmlperiod input").val();
	var service  = $("li#xmlservice input").val();

	var isvalid = true;

	//validation of ip
	if(!Validator.validate( { "val" : ip, "type" : "domain", "ismandatory" : "true", "obj" : $("li#serveraddr"), "objfocus" : $("li#serveraddr input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	//validation of port
	if(!Validator.validate( { "val" : port, "type" : "port", "ismandatory" : "true", "obj" : $("li#serverport"), "objfocus" : $("li#serverport input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	//validation of period
	if(!Validator.validate( { "val" : period, "type" : "i", "min" : "1", "max" : "43200", "ismandatory" : "true", "obj" : $("li#xmlperiod"), "objfocus" : $("li#xmlperiod input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	//validation of service
	if(!Validator.validate( { "val" : service, "type" : "url", "ismandatory" : "false", "obj" : $("li#xmlservice"), "objfocus" : $("li#xmlservice input"), "page" : $("div#settings div.alert")}))
		isvalid = false;

	if(isvalid) {

		if(active) {
			active = "1";
		} else {
			active = "0";
		}
		if(detailed) {
			detailed = "1";
		} else {
			detailed = "0";
		}

		Settings.xmlServerData = {
			send_enabled: active,
			detailed: detailed,
			server_ip: ip,
			server_port: port,
			send_interval: period,
			server_page: service
		};
	}

	return isvalid;
};

$(function() {
	
	$("div#settings").on( "click", "ul#xmlserverform input#testXml", function() {
		var id = $( "ul#xmlserverform select#xmlServerList option:selected" ).val();
		if(id == "none") {
			Main.alert(language.Setting_select_one_xmlserver);
		}
		else
		{
			Settings.testXmlServer(id);
		}
	});
	
	$("div#settings").on( "click", "ul#xmlserverform input#Update", function() {
		var id = $( "ul#xmlserverform select#xmlServerList option:selected" ).val();
		if(id != "none") {
			$("li#buttons input#save").attr("data-page","update");
			Settings.getXmlServerItem(id);
		}
		else
		{
			Main.alert(language.Setting_select_one_xmlserver);
		}
	});
	
	$("div#settings").on( "click", "ul#xmlserverform input#new", function() {
		$("div#settings div.alertNaN").html("");
		Settings.newXmlServerForm();
		$("li#buttons input#save").attr("data-page","new");
	});

	$("div#settings").on( "click", "ul#xmlserverform input#save", function() {
		var page = $("li#buttons input#save").attr("data-page");
		if(Settings.validateXmlServerForm()) {
			switch(page) {
				case "new":
					Settings.addXmlServer();
				break;
				case "update":
					var id = $( "ul#xmlserverform select#xmlServerList option:selected" ).val();
					Settings.updateXmlServer(id);
				break;
			};
		}
	});

	$("div#settings").on( "click", "ul#xmlserverform input#cancelXml", function() {
		Settings.clearXmlServerForm();
	});

	$("div#settings").on( "click", "ul#xmlserverform input#remove", function() {
		var id = $( "ul#xmlserverform select#xmlServerList option:selected" ).val();
		$("div#overlay").css("display","block");
		$("#content div#settings div.form div#alertDelete").css("display","block");
		$("#content div#settings div.form div#alertDelete span.text").html(language.Settings_remove_server);
		$("#content div#settings div.form div#alertDelete input#buttonRemove").val(language.remove);
		$("#content div#settings div.form div#alertDelete input#buttonRemove").attr("data-id",id);
		$("#content div#settings div.form div#alertDelete input#buttonClose").val(language.cancel);
	});

	$("div#settings").on( "click", "div.xmlServer input#buttonRemove", function() {
		var id = $( this ).attr("data-id");
		Settings.removeXmlServer(id);
	});

	$("div#settings").on( "click", "div.xmlServer input#buttonClose", function() {
		$("div#overlay").css("display","none");
		$("#content div#settings div.form div#alertDelete").css("display","none");
	});
});