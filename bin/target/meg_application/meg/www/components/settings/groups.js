var mailCalenderData = "*";
var smsCalenderData = "*";
var callCalenderData = "*";
var normalCalenderData = "*";
var newgroupstatus = false;
var xxxWidth = 0;
Settings.callGroups = function() {
	CALENDAR="groups";
	newgroupstatus = false;
	$("div#settings div.form").html("");
	var html = 	'<ul class="block">'
					+ '<li id="grouplist"><span></span><select id="groupselectlist"></select></li>'
					+ '<li id="groupperms"><span></span><select id="grouppremlist"></select></li>'
					+ '<li id="groupnameinput"><span></span><input type="text" id="groupnameinput"></li>'
				+ '</ul>'
				+ '<div class="alert"></div>'
				+ '<div class="leftCol">'
					+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="head">'
						+ '	<tr>'
						+ '		<td id="moduldevicename"></td>'
						+ '	</tr>'
					+ '</table>'
					+ '<div id="settingsDevices" class="tableDiv">'
						+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="list">'
						+ '</table>'				
					+ '</div>'				
				+ '</div>'
				+ '<div class="rightCol">'
					+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="sensorhead">'
					+ '	<tr>'
					+ '		<td id="modulsensorname" width="100"><label><input type="checkbox" id="sensorname" name="sensorname"></label></td>'
					+ '		<td id="modulsensormail" width="30"><label class="pad30"><input type="checkbox" id="sensormail" name="sensormail"></label><span class="clock" data-page="mailClock"></span></td>'
					+ '		<td id="modulsensorsms" width="30"><label class="pad30"><input type="checkbox" id="sensorsms" name="sensorsms"></label><span class="clock" data-page="smsClock"></span></td>'
					+ '		<td id="modulsensorcall" width="30"><label class="pad30"><input type="checkbox" id="sensorcall" name="sensorcall"></label><span class="clock" data-page="callClock"></span></td>'
					+ '		<td id="modulsensornormal" width="30"><label class="pad30"><input type="checkbox" id="sensornormal" name="sensornormal"></label><span class="clock" data-page="normalClock"></span></td>'
					+ '		<td id="modulsensorinjob" width="70"><label class="pad30"><input type="checkbox" id="sensorinjob" name="sensorinjob"></label></td>'
					+ '		<td id="modulsensoroutjob" width="70"><label class="pad30"><input type="checkbox" id="sensoroutjob" name="sensoroutjob"></label></td>'
					+ '	</tr>'
					+ '</table>'
					+ '<div id="settingsSensors" class="tableDiv">'
						+ '<table cellpadding="0" cellspacing="0" border="0" width="100%">'
						+ '</table>'				
					+ '</div>'
				+ '</div>'
				+ '<div class="button"><input id="deleteGroup" type="submit"><input id="updateGroup" type="submit"><input id="newGroup" type="submit"><input id="cancelGroup" type="submit"><input id="saveGroup" type="submit"></div>';

	$("div#settings div.form").html(html);

	$("div#settings div.leftCol table.head td#moduldevicename").html(language.Settings_groups_table_devices);
	$("div#settings div.rightCol table.sensorhead td#modulsensorname label").append(language.Settings_groups_table_sensors);
	$("div#settings div.rightCol table.sensorhead td#modulsensormail label").append(language.Settings_groups_table_mail);
	$("div#settings div.rightCol table.sensorhead td#modulsensorsms label").append(language.Settings_groups_table_sms);
	$("div#settings div.rightCol table.sensorhead td#modulsensorcall label").append(language.Settings_groups_table_call);
	$("div#settings div.rightCol table.sensorhead td#modulsensornormal label").append(language.Settings_groups_table_normal);
	$("div#settings div.rightCol table.sensorhead td#modulsensorinjob label").append(language.Settings_groups_table_injob);
	$("div#settings div.rightCol table.sensorhead td#modulsensoroutjob label").append(language.Settings_groups_table_outjob);
	$("div#settings div.button input#deleteGroup").val(language.Settings_groups_table_delgroup);
	$("div#settings div.button input#updateGroup").val(language.Settings_groups_table_updgroup);
	$("div#settings div.button input#newGroup").val(language.Settings_groups_table_newgroup);
	$("div#settings div.button input#cancelGroup").val(language.Settings_groups_table_cancel);
	$("div#settings div.button input#saveGroup").val(language.Settings_groups_table_save);
	$("div#settings div.button input#cancelGroup").css("display","none");
	$("div#settings div.button input#saveGroup").css("display","none");
	$("div#settings div.form li#grouplist span").html(language.Settings_groups_table_grouplist);
	$("div#settings div.form li#groupperms span").html(language.Settings_groups_table_permlist);
	$("div#settings div.form li#groupnameinput span").html(language.Settings_groups_table_groupname);

	$("div#calender select option#Select").html(language.Settings_calender_select);
	$("div#calender select option#ALLDAY").html(language.Settings_calender_select_allday);
	$("div#calender select option#WEEKDAY").html(language.Settings_calender_select_weekday);
	$("div#calender select option#WEEKEND").html(language.Settings_calender_select_weekend);
	$("div#calender select option#WORKINGHOUR").html(language.Settings_calender_select_workinghour);
	$("div#calender select option#NONWORKINGHOUR").html(language.Settings_calender_select_nonworkinghour);
	$("div#calender select option#MANUEL").html(language.Settings_calender_select_manuel);
	$("div#calender input#saveButton").val(language.Settings_calender_save);
	$("div#calender input#cancelButton").val(language.Settings_calender_close);
	$("div#calender select#time_selection").removeAttr("style");

	Settings.onGroupLoad("groups");

	CALENDAR = "groups";
	Settings.getInjobHours();

};

Settings.getInjobHours = function() {
	var data = $.getJSON("http://" + hostName + "/cgi-bin/systeminfo.cgi?getSystemInfo");	 
	data.success(function() {
		if(data.responseText != "{}") {
			Settings.INJOB = jQuery.parseJSON(data.responseText).injob;
		}
		else
			Settings.INJOB = "*";
	});
	data.error(function(jqXHR, textStatus, errorThrown){
		Settings.INJOB = "*";
	});
};

Settings.getDeviceList = function(name) {
	var groupname = name;
	var getDevicesData = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?getDevices");	 
	getDevicesData.done(function() {
		if(groupname == undefined) {
			groupname = $("div#settings input#groupnameinput").val();
		}

		Settings.createDeviceList(jQuery.parseJSON(getDevicesData.responseText), groupname);
		var url = $("div#settingsDevices table tr td:first").data("id");
		Settings.getSensors(url, groupname);
	});
};


Settings.sort_Sensors = function()
{
	var tmpList = [];
	var len = Settings.sensors.length;
	for (var i = 0; i < len; i++)
	{
		tmpList[parseInt(Settings.sensors[i].ionum)] = Settings.sensors[i];
	}

	var tmpList2 = []
	var k = 0;
	for (var i = 0; i < tmpList.length; i++)
	{
		if (tmpList[i] != undefined)
			tmpList2[k++] = tmpList[i]	
	}

	Settings.sensors = tmpList2;
};

Settings.getSensors = function(url) {
	var getSensorData = $.getJSON("http://" + hostName + "/cgi-bin/megweb.cgi?"+ url);	 
	getSensorData.done(function() {
		Settings.sensors = jQuery.parseJSON(getSensorData.responseText);
		Settings.sort_Sensors();
		Settings.createSensors();
	});	
};

Settings.deleteGroup = function(name) {
	Main.loading(); 
	var url = "http://" + hostName + "/cgi-bin/userandgroups.cgi?removeGroup&groupname=" + name;
	$.ajax({
	    url : url,
	    type: "POST",
	    data : {},
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			Settings.callGroups();
	    },
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    	Main.alert(language.groups_remove_fail + ":" + jqXHR.responseText);
			console.log(jqXHR)
			Main.unloading();
	    }
	});
}

Settings.newGroupSave = function(json,name) {
	Main.loading();
	json = JSON.stringify(json);
	console.log(json)
	var url = "http://" + hostName + "/cgi-bin/megweb.cgi?addGroup&groupname=" + name;

	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			Settings.callGroups();
	    },
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    	Main.alert(language.groups_add_fail + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.updateGroup = function(json,name) {
	Main.loading();
	json = JSON.stringify(json);
	var name = $("div#settings select#groupselectlist option:selected").text();
	var url = "http://" + hostName + "/cgi-bin/megweb.cgi?updateGroup&groupname=" + name;

	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Settings.callGroups();
			Main.unloading();
	    },
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    	Main.alert(language.groups_update_fail + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.updateClock = function(json,name) {
	Main.loading();
	json = JSON.stringify(json);
	var name = $("div#settings select#groupselectlist option:selected").text();
	var url = "http://" + hostName + "/cgi-bin/megweb.cgi?updateGroup&groupname=" + name;

	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			calenderClear();
			$("div#calender").css("display","none");
			$("div#overlay").css("display","none");
	    },
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.createGroupSelect = function() {
	for(var i = 0; i < Settings.groups.length; i++) {
		var option = document.createElement("option");
		option.setAttribute("id","user" + i);
		
		if(i == 0) {
			option.selected = true;
			$("div#settings input#groupnameinput").val(Settings.groups[i].name);
		}

		option.setAttribute("value", Settings.groups[i].type);
		option.innerHTML = Settings.groups[i].name;	
		$("div#settings select#groupselectlist").append(option);
	}
};

Settings.createGroupPermission = function() {
	var val = parseInt($("div#settings select#groupselectlist option:selected").val());
	
	for(var i = 0; i < Settings.permission.length; i++) {
		var option = document.createElement("option");
		option.setAttribute("id","user" + i);
		if(Settings.permission[i].value == val) {
			option.selected = true;
		} else {
			option.selected = false;
		 }
		option.setAttribute("value", Settings.permission[i].value);
		option.innerHTML = eval("language." + Settings.permission[i].name);
		$("div#settings select#grouppremlist").append(option);
	}
};

Settings.createDeviceList = function(items, name) {

	var groupname = name;

	for(var i = 0; i < items.length; i++) {
		var name = Monitoring.characterLimit(items[i].name,14);
		var ID = items[i].objectid.split("/");
		var getSensor = "getGroupPickedSensors&groupname=" + groupname + "&hwaddr="+ ID[0] + "&portnum="+ ID[1] +"&devaddr="+ ID[2];
		var tr = document.createElement("tr");
		var tdName = document.createElement("td");
		tdName.setAttribute("id", "tdName");
		if (items[i].name.length > 14)
			tdName.setAttribute("data-title", items[i].name);
		tdName.setAttribute("data-id",getSensor);
		tdName.setAttribute("data-count",i);
		tdName.setAttribute("data-name",name);
		if(i == 0) tdName.setAttribute("class","active");
		tdName.innerHTML = name;

		var span = document.createElement("span");
		span.setAttribute("class","plus");
		tdName.appendChild(span);

		tr.appendChild(tdName);
		$("div#settings div#settingsDevices table.list").append(tr);

		Settings.backupChecklist[i] = {

		};		
	}
};

Settings.createSensors = function() {

	var x = $("div#settings div.leftCol td.active").data("count");

	for(var i = 0; i < Settings.sensors.length; i++) {
		var dname = $("div#settingsDevices table td.active").data("name");
		var name = Monitoring.characterLimit(Settings.sensors[i].name, 12);
		var type = Settings.sensors[i].type;

		var tr = document.createElement("tr");		
		tr.setAttribute("data-id",Settings.sensors[i].id);
		tr.setAttribute("data-type",type);
		tr.setAttribute("data-name",  dname + " > " + name);
		tr.setAttribute("class","type"+type);

		var tdName = document.createElement("td");
		tdName.setAttribute("id", "tdName");
		if (Settings.sensors[i].name.length > 10)
			tdName.setAttribute("data-title", Settings.sensors[i].name);
		var tdNameInput = document.createElement("input");
		var tdNameLabel = document.createElement("label");
		tdNameInput.setAttribute("type","checkbox");
		tdNameInput.setAttribute("class","nameCbox");
		tdNameInput.setAttribute("data-count", i);
		if(Settings.backupChecklist[x][i] == undefined) {
			if(Settings.sensors[i].picked == 0) {
				tdNameInput.checked = false;
			} else {
				tdNameInput.checked = true;
			}
		} else {
			if(Settings.backupChecklist[x][i].picked == 0) {
				tdNameInput.checked = false;
			} else {
				tdNameInput.checked = true;
			}
		}

		tdNameInput.setAttribute("id","name" + Settings.sensors[i].id);
		tdName.setAttribute("width","178");

		tdNameLabel.innerHTML = name;
		tdNameLabel.appendChild(tdNameInput);

		tdName.appendChild(tdNameLabel);

		var tdmail = document.createElement("td");
		var tdmailCbox = document.createElement("input");
		tdmailCbox.setAttribute("type","checkbox");
		tdmailCbox.setAttribute("class","mailCbox");
		tdmailCbox.setAttribute("id","mail" + Settings.sensors[i].id);
		tdmailCbox.setAttribute("data-count", i);

		if(Settings.backupChecklist[x][i] == undefined) {
			if(Settings.sensors[i].email_enable == 0) {
				tdmailCbox.checked = false;
			} else {
				tdmailCbox.checked = true;
			}
		} else {
			if(Settings.backupChecklist[x][i].email_enable == 0) {
				tdmailCbox.checked = false;
			} else {
				tdmailCbox.checked = true;
			}
		}

		tdmail.setAttribute("width","90");
		tdmail.appendChild(tdmailCbox);

		var tdsms = document.createElement("td");
		var tdsmsCbox = document.createElement("input");
		tdsmsCbox.setAttribute("type","checkbox");
		tdsmsCbox.setAttribute("class","smsCbox");
		tdsmsCbox.setAttribute("data-count", i);

		if(Settings.backupChecklist[x][i] == undefined) {
			if(Settings.sensors[i].sms_enable == 0) {
				tdsmsCbox.checked = false;
			} else {
				tdsmsCbox.checked = true;
			}
		} else {
			if(Settings.backupChecklist[x][i].sms_enable == 0) {
				tdsmsCbox.checked = false;
			} else {
				tdsmsCbox.checked = true;
			}
		}

		tdsmsCbox.setAttribute("id","sms" + Settings.sensors[i].id);
		tdsms.setAttribute("width","90");
		tdsms.appendChild(tdsmsCbox);

		var tdcall = document.createElement("td");
		var tdcallCbox = document.createElement("input");
		tdcallCbox.setAttribute("type","checkbox");
		tdcallCbox.setAttribute("class","callCbox");
		tdcallCbox.setAttribute("data-count", i);

		if(Settings.backupChecklist[x][i] == undefined) {
			if(Settings.sensors[i].dial_enable == 0) {
				tdcallCbox.checked = false;
			} else {
				tdcallCbox.checked = true;
			}
		} else {
			if(Settings.backupChecklist[x][i].dial_enable == 0) {
				tdcallCbox.checked = false;
			} else {
				tdcallCbox.checked = true;
			}
		}

		tdcallCbox.setAttribute("id","call" + Settings.sensors[i].id);
		tdcall.setAttribute("width","90");
		tdcall.appendChild(tdcallCbox);

		var tdnormal = document.createElement("td");
		var tdnormalCbox = document.createElement("input");
		tdnormalCbox.setAttribute("type","checkbox");
		tdnormalCbox.setAttribute("class","normalCbox");
		tdnormalCbox.setAttribute("data-count", i);

		if(Settings.backupChecklist[x][i] == undefined) {
			if(Settings.sensors[i].normal_enable == 0) {
				tdnormalCbox.checked = false;
			} else {
				tdnormalCbox.checked = true;
			}
		} else {
			if(Settings.backupChecklist[x][i].normal_enable == 0) {
				tdnormalCbox.checked = false;
			} else {
				tdnormalCbox.checked = true;
			}
		}

		tdnormalCbox.setAttribute("id","normal" + Settings.sensors[i].id);
		tdnormal.setAttribute("width","90");
		tdnormal.appendChild(tdnormalCbox);
				
		
		var tdinjob = document.createElement("td");
		var tdinjobCbox = document.createElement("input");
		tdinjobCbox.setAttribute("type","checkbox");
		tdinjobCbox.setAttribute("class","injobCbox");
		tdinjobCbox.setAttribute("data-count", i);

		if(Settings.backupChecklist[x][i] == undefined) {
			if(Settings.sensors[i].injob_alarm == 0) {
				tdinjobCbox.checked = false;
			} else {
				tdinjobCbox.checked = true;
			}
		} else {
			if(Settings.backupChecklist[x][i].injob_alarm == 0) {
				tdinjobCbox.checked = false;
			} else {
				tdinjobCbox.checked = true;
			}
		}

		tdinjobCbox.setAttribute("id","injob" + Settings.sensors[i].id);
		tdinjob.setAttribute("width","90");
		tdinjob.appendChild(tdinjobCbox);
		
		var tdoutjob = document.createElement("td");
		var tdoutjobCbox = document.createElement("input");
		tdoutjobCbox.setAttribute("type","checkbox");
		tdoutjobCbox.setAttribute("class","outjobCbox");
		tdoutjobCbox.setAttribute("data-count", i);

		if(Settings.backupChecklist[x][i] == undefined) {
			if(Settings.sensors[i].outjob_alarm == 0) {
				tdoutjobCbox.checked = false;
			} else {
				tdoutjobCbox.checked = true;
			}
		} else {
			if(Settings.backupChecklist[x][i].outjob_alarm == 0) {
				tdoutjobCbox.checked = false;
			} else {
				tdoutjobCbox.checked = true;
			}
		}

		tdoutjobCbox.setAttribute("id","outjob" + Settings.sensors[i].id);
		tdoutjob.setAttribute("width","90");
		tdoutjob.appendChild(tdoutjobCbox);

		tr.appendChild(tdName);
		tr.appendChild(tdmail);
		tr.appendChild(tdsms);
		tr.appendChild(tdcall);
		tr.appendChild(tdnormal);
		tr.appendChild(tdinjob);
		tr.appendChild(tdoutjob);
		$("div#settingsSensors table").append(tr);

		if(Settings.backupChecklist[x][i] == undefined) {
			Settings.backupChecklist[x][i] = {
				id: Settings.sensors[i].id,
				picked: Settings.sensors[i].picked,
				email_enable: Settings.sensors[i].email_enable,
				sms_enable: Settings.sensors[i].sms_enable,
				dial_enable: Settings.sensors[i].dial_enable,
				normal_enable: Settings.sensors[i].normal_enable,
				injob_alarm: Settings.sensors[i].injob_alarm,
				outjob_alarm: Settings.sensors[i].outjob_alarm
			};

			Settings.backupChecklist[x].length = i+1;
		}
	}
};

Settings.createJson = function(name,type,page) {
	var json = [];
	var count = 0;
	var posJson = '{ "name" : "' + name + '", "type":"' + type +'"';

	if(page == "newgroup") {
		posJson+= ',"email_calendar" : "' + mailCalenderData + '", "sms_calendar":"' + smsCalenderData +'", "dial_calendar":"' + callCalenderData +'", "normal_calendar":"' + normalCalenderData + '"';
	}

	console.log(Settings.backupChecklist.length);
	for(var i = 0; i < Settings.backupChecklist.length; i++) {
		for(var j = 0; j < Settings.backupChecklist[i].length; j++) {
			//if(Settings.backupChecklist[i][j].picked == 1) {
				var parse = ', "' + Settings.backupChecklist[i][j].id + '" : "' + Settings.backupChecklist[i][j].picked + "," 
				+ Settings.backupChecklist[i][j].email_enable + "," + Settings.backupChecklist[i][j].sms_enable + "," 
				+ Settings.backupChecklist[i][j].dial_enable  + "," + Settings.backupChecklist[i][j].normal_enable + "," 
				+ Settings.backupChecklist[i][j].injob_alarm + ","+ Settings.backupChecklist[i][j].outjob_alarm + '"';
				json[count]= parse;
				count++;
			//}
		}
	}

	for(var k = 0; k < count; k++) {
		posJson+=json[k];			
		/*if(count-1 != k) {
			posJson+= ",";
		}*/
	}

	posJson+= "}";
	posJson = jQuery.parseJSON(posJson);
	switch(page) {
		case "update": 
			var nm = $("div#settings select#grouppremlist option:selected").text();
			Settings.updateGroup(posJson,name);
		break;
		case "newgroup":
			Settings.newGroupSave(posJson,name);
		break;		
	}
};

Settings.newGroupList = function() {
	Settings.backupChecklist = [];
	Settings.getDeviceList();
	$("div#settings li#grouplist").css("display","none");
	$("div#settings div#settingsDevices table").html("");
	$("div#settings div#settingsSensors table").html("");
	$("div#settings input#groupnameinput").val("");
	$("div#settings input#sensorname").prop("checked", false);
	$("div#settings input#sensormail").prop("checked", false);
	$("div#settings input#sensorsms").prop("checked", false);
	$("div#settings input#sensorcall").prop("checked", false);
	$("div#settings input#sensornormal").prop("checked", false);
	$("div#settings input#sensorinjob").prop("checked", false);
	$("div#settings input#sensoroutjob").prop("checked", false);
};

Settings.clearGroup = function(name) {	
	Settings.backupChecklist = [];
	Settings.getDeviceList(name);
	$("div#settings div#settingsDevices table").html("");
	$("div#settings div#settingsSensors table").html("");
	$("div#settings input#groupnameinput").val(name);
	$("div#settings input#sensorname").prop("checked", false);
	$("div#settings input#sensormail").prop("checked", false);
	$("div#settings input#sensorsms").prop("checked", false);
	$("div#settings input#sensorcall").prop("checked", false);
	$("div#settings input#sensornormal").prop("checked", false);
	$("div#settings input#sensorinjob").prop("checked", false);
	$("div#settings input#sensoroutjob").prop("checked", false);
};

Settings.nameInputValidate = function() {
	$("div#settings div.alert").html("");
	var status = true;
	var iChars = "!@#$%^&*()+=[]\\\';,./{}|\":<>?";
	var alertHtml = "";
	$("li#groupperms").removeClass("alert");
	$("li#groupnameinput").removeClass("alert");

	for (var i = 0; i <  $("li#groupnameinput input").val().length; i++) {
		if (iChars.indexOf($("li#groupnameinput input").val().charAt(i)) != -1) {
			$("li#groupnameinput").addClass("alert");
			alertHtml = language.Settings_groups_alert_special;
			$("div#settings div.alert").html(alertHtml);
			$("li#groupnameinput input").focus();
			status = false;
			break;
		}
	}

	if($("li#groupnameinput input").val() == "") {
		$("li#groupnameinput").addClass("alert");
		alertHtml = language.Settings_groups_alert_groupname;
		$("div#settings div.alert").html(alertHtml);
		$("li#groupnameinput input").focus();
		status = false;
	}

	if(newgroupstatus) {
		if($("select#grouppremlist option:selected").val() == "0") {
			$("li#groupperms").addClass("alert");
			alertHtml+= "<br />" + language.Settings_groups_alert_groupprems;
			$("div#settings div.alert").html(alertHtml);
			status = false;
		}
	}

	return status;
};


Settings.calenderDraw = function(splitData) {
	bFill=true;
	loadCalender(splitData);
};

$(function() {

	$("div#settings").on( "click", "span.clock", function() {
		var page = $(this).data("page");
		var groupname = $("div#settings select#groupselectlist option:selected").text();
	    var getGroup = $.getJSON("http://" + hostName +"/cgi-bin/userandgroups.cgi?getGroup&groupname=" + groupname);    
	    
	    getGroup.done(function() {
	        var data = jQuery.parseJSON(getGroup.responseText);
	        var splitData = null;
	        if(data != null && data !=undefined && data != "") {

	           	switch(page) {
	           		case "mailClock":
	           			splitData = data.email_calendar;
	           			if(newgroupstatus) splitData = "*";
	           		break;
	           		case "smsClock":
	           			splitData = data.sms_calendar;
	           			if(newgroupstatus) splitData = "*";
	           		break;
	           		case "callClock":
	           			splitData = data.dial_calendar;
	           			if(newgroupstatus) splitData = "*";
	           		break;
	           		case "normalClock":
	           			splitData = data.normal_calendar;
	           			if(newgroupstatus) splitData = "*";
	           		break;
	           	}
			   	
			   	Settings.calenderDraw(splitData);
			   	$("select#time_selection option").each(function(){
					var val = $(this).val();
					if(val == language.Settings_calender_select){
						$( this ).prop('selected', true);
					} else {
						$( this ).prop('selected', false);
					}
				});
			   	$("input#saveButton").attr("data-page",page);
			   	$("div#calender").css("display","block");
			   	$("div#overlay").css("display","block");
	        } else {
				bFill=false;
	            FillCells([0,0],[23,6]);            
	        }
	    });
	});

	$("div#calender").on( "click", "input#cancelButton", function() {
		calenderClear();
		$("div#calender").css("display","none");
		$("div#overlay").css("display","none");
	});

	$("div#calender").on( "click", "input#saveButton", function() {

		$("div#calender").css("display","none");
		$("div#overlay").css("display","none");
	
		var groupname = "";
		var page = "";
		if (CALENDAR == "groups")
		{
			groupname = $("div#settings select#groupselectlist option:selected").text();
			page = $("input#saveButton").attr("data-page");
		}
		else// systeminfo
		{
			groupname = "injob";
		}

		calendarSaved(page, groupname);
	});

	$("div#settings").on( "click", "div#settingsDevices table.list td", function() {
		$("div#settingsSensors table").html("");
		var url = $(this).data("id");
		$("div#settingsDevices table td").removeClass("active");
		$(this).addClass("active");
		$("div#settings input#sensorname").prop("checked", false);
		$("div#settings input#sensormail").prop("checked", false);
		$("div#settings input#sensorsms").prop("checked", false);
		$("div#settings input#sensorcall").prop("checked", false);
		$("div#settings input#sensornormal").prop("checked", false);
		$("div#settings input#sensorinjob").prop("checked", false);
		$("div#settings input#sensoroutjob").prop("checked", false);
		Settings.getSensors(url);
	});

	$("div#settings").on( "click", "input#sensorname", function() {
		var status = $(this).prop("checked");
		var x = $("div#settings div.leftCol td.active").data("count");
		for(var i = 0; i < Settings.sensors.length; i++) {
			$("div#settings input#name" + Settings.sensors[i].id).prop("checked", status);

			var value = 0;

			if(status) {
				value = 1;
			}

			Settings.backupChecklist[x][i].picked = value;
		}
	});

	$("div#settings").on( "click", "input#sensormail", function() {
		var status = $(this).prop("checked");
		var x = $("div#settings div.leftCol td.active").data("count");
		for(var i = 0; i < Settings.sensors.length; i++) {
			$("div#settings input#mail" + Settings.sensors[i].id).prop("checked", status);

			var value = 0;

			if(status) {
				value = 1
			}

			Settings.backupChecklist[x][i].email_enable = value;
		}
	});

	$("div#settings").on( "click", "input#sensorsms", function() {
		var status = $(this).prop("checked");
		var x = $("div#settings div.leftCol td.active").data("count");
		for(var i = 0; i < Settings.sensors.length; i++) {
			$("div#settings input#sms" + Settings.sensors[i].id).prop("checked", status);

			var value = 0;

			if(status) {
				value = 1
			}

			Settings.backupChecklist[x][i].sms_enable = value;
		}
	});

	$("div#settings").on( "click", "input#sensorcall", function() {
		var status = $(this).prop("checked");
		var x = $("div#settings div.leftCol td.active").data("count");
		for(var i = 0; i < Settings.sensors.length; i++) {
			$("div#settings input#call" + Settings.sensors[i].id).prop("checked", status);

			var value = 0;

			if(status) {
				value = 1
			}

			Settings.backupChecklist[x][i].dial_enable = value;
		}
	});

	$("div#settings").on( "click", "input#sensornormal", function() {
		var status = $(this).prop("checked");
		var x = $("div#settings div.leftCol td.active").data("count");
		for(var i = 0; i < Settings.sensors.length; i++) {
			$("div#settings input#normal" + Settings.sensors[i].id).prop("checked", status);

			var value = 0;

			if(status) {
				value = 1
			}

			Settings.backupChecklist[x][i].normal_enable = value;
		}
	});
	
	$("div#settings").on( "click", "input#sensorinjob", function() {
		var status = $(this).prop("checked");
		var x = $("div#settings div.leftCol td.active").data("count");
		for(var i = 0; i < Settings.sensors.length; i++) {
			$("div#settings input#injob" + Settings.sensors[i].id).prop("checked", status);

			var value = 0;

			if(status) {
				value = 1;
			}

			Settings.backupChecklist[x][i].injob_alarm = value;
		}
	});
	
	$("div#settings").on( "click", "input#sensoroutjob", function() {
		var status = $(this).prop("checked");
		var x = $("div#settings div.leftCol td.active").data("count");
		for(var i = 0; i < Settings.sensors.length; i++) {
			$("div#settings input#outjob" + Settings.sensors[i].id).prop("checked", status);

			var value = 0;

			if(status) {
				value = 1;
			}

			Settings.backupChecklist[x][i].outjob_alarm = value;
		}
	});
	
	$("div#settings").on( "click", "input.nameCbox", function() {
		var status = $(this).prop("checked");
		var x = $("div#settings div.leftCol td.active").data("count");
		var y = $(this).data("count");
		var value = 0;

		if(status) {
			value = 1;
		}
		
		Settings.backupChecklist[x][y].picked = value;
	});

	$("div#settings").on( "click", "input.mailCbox", function() {
		var status = $(this).prop("checked");
		var x = $("div#settings div.leftCol td.active").data("count");
		var y = $(this).data("count");
		var value = 0;

		if(status) {
			value = 1
		}

		Settings.backupChecklist[x][y].email_enable = value;
	});

	$("div#settings").on( "click", "input.smsCbox", function() {
		var status = $(this).prop("checked");
		var x = $("div#settings div.leftCol td.active").data("count");
		var y = $(this).data("count");

		var value = 0;

		if(status) {
			value = 1
		}

		Settings.backupChecklist[x][y].sms_enable = value;
	});

	$("div#settings").on( "click", "input.callCbox", function() {
		var status = $(this).prop("checked");
		var x = $("div#settings div.leftCol td.active").data("count");
		var y = $(this).data("count");

		var value = 0;

		if(status) {
			value = 1;
		}

		Settings.backupChecklist[x][y].dial_enable = value;
	});

	$("div#settings").on( "click", "input.normalCbox", function() {
		var status = $(this).prop("checked");
		var x = $("div#settings div.leftCol td.active").data("count");
		var y = $(this).data("count");

		var value = 0;

		if(status) {
			value = 1;
		}

		Settings.backupChecklist[x][y].normal_enable = value;
	});

	
	
	$("div#settings").on( "click", "input.injobCbox", function() {
		var status = $(this).prop("checked");
		var x = $("div#settings div.leftCol td.active").data("count");
		var y = $(this).data("count");

		var value = 0;

		if(status) {
			value = 1;
		}

		Settings.backupChecklist[x][y].injob_alarm = value;
	});

	
	$("div#settings").on( "click", "input.outjobCbox", function() {
		var status = $(this).prop("checked");
		var x = $("div#settings div.leftCol td.active").data("count");
		var y = $(this).data("count");

		var value = 0;

		if(status) {
			value = 1;
		}

		Settings.backupChecklist[x][y].outjob_alarm = value;
	});
	$("div#settings").on( "click", "input#newGroup", function() {
		newgroupstatus = true;
		$("div#settings div.button input#deleteGroup").css("display","none");
		$("div#settings div.button input#updateGroup").css("display","none");
		$("div#settings div.button input#newGroup").css("display","none");
		$("div#settings div.button input#cancelGroup").css("display","inline-block");
		$("div#settings div.button input#saveGroup").css("display","inline-block");
		$("div#settings div.button input#saveGroup").attr("data-page","newgroup");
		Settings.newGroupList();
	});

	$("div#settings").on( "click", "input#updateGroup", function() {	
		$("div#settings div.button input#deleteGroup").css("display","none");
		$("div#settings div.button input#updateGroup").css("display","none");
		$("div#settings div.button input#newGroup").css("display","none");
		$("div#settings div.button input#cancelGroup").css("display","inline-block");
		$("div#settings div.button input#saveGroup").css("display","inline-block");
		$("div#settings div.button input#saveGroup").attr("data-page","update");
	});

	$("div#settings").on( "click", "input#deleteGroup", function() {
		var txt = $("div#settings select#groupselectlist option:selected").text();
		$("div#overlay").css("display","block");
		$("div#alertDelete").css("display","block");

		$("div#alertDelete input#buttonRemove").val(language.remove);
		$("div#alertDelete input#buttonRemove").attr("data-group", txt);
		$("div#alertDelete input#buttonRemove").addClass("groupDelete");

		$("div#alertDelete input#buttonClose").val(language.close);
		$("div#alertDelete span.text").html( txt + ", " + language.Settings_delete_group);	
	});

	$("div#settings").on( "click", "input#cancelGroup", function() {
		newgroupstatus = false;
		var name = $("div#settings select#groupselectlist option:selected").text();
		Settings.backupChecklist = [];
		$("div#settings input#groupnameinput").val(name);
		$("div#settings div.alert").html("");
		$("div#settings div#settingsDevices table").html("");
		$("div#settings div#settingsSensors table").html("");
		$("div#settings li#grouplist").css("display","block");
		$("li#groupperms").removeClass("alert");
		$("li#groupnameinput").removeClass("alert");
		$("div#settings div.button input#deleteGroup").css("display","inline-block");
		$("div#settings div.button input#updateGroup").css("display","inline-block");
		$("div#settings div.button input#newGroup").css("display","inline-block");
		$("div#settings div.button input#cancelGroup").css("display","none");
		$("div#settings div.button input#saveGroup").css("display","none");
		$("div#settings div.button input#saveGroup").removeAttr( "data-page" )
		var value = parseInt($("div#settings select#groupselectlist option:selected").val());
	    $( "div#settings select#grouppremlist option" ).each(function() {
	      	if(value == parseInt($(this).val())) {
	      		$(this).prop('selected', true);
	      	} else {
	      		$(this).prop("selected", false);
	      	}
	    });	
		Settings.getDeviceList();
	});

	$("div#settings").on( "click", "input#saveGroup", function() {
		var page = $(this).data("page");
		switch(page) {
			case "update":
				if(Settings.nameInputValidate()) {
					var name = $("div#settings input#groupnameinput").val();
					var type = $("div#settings select#grouppremlist option:selected").val();
					Settings.createJson(name,type,page);
				}
			break;
				case "newgroup":
				if(Settings.nameInputValidate()) {
					var name = $("div#settings input#groupnameinput").val();
					var type = $("div#settings select#grouppremlist option:selected").val();
					Settings.createJson(name,type,page);
				}
			break;
		}
	});
	
	$("div#settings").on( "change", "select#groupselectlist", function() {
		var name = $("div#settings select#groupselectlist option:selected").text();
		var value = parseInt($("div#settings select#groupselectlist option:selected").val());
	    $( "div#settings select#grouppremlist option" ).each(function() {
	      	if(value == parseInt($(this).val())) {
	      		$(this).prop('selected', true);
	      	} else {
	      		$(this).prop("selected", false);
	      	}
	    });	

		Settings.clearGroup(name);
  	});

	$("div#settings").on( "click", "div#alertDelete input.groupDelete", function() {
		var deleteGroup = $(this).attr("data-group");
		$("div#overlay").css("display","none");
		$("div#alertDelete").css("display","none");
		$("div#alertDelete input#buttonRemove").removeClass("groupDelete");
		Settings.deleteGroup(deleteGroup);
	});	
	
		
	$("div#settings").on("mouseover", "div#settingsSensors table tr td#tdName, div#settingsDevices table tr td#tdName", function(){
  		var title = $(this).attr("data-title");
		if(title != undefined ) {
			var t = document.createElement("b");
			t.setAttribute("id","tooltip");
			t.innerHTML = title;
			$(this).append(t);
			$(this).addClass("tooltip");
		}
	});
	
	$("div#settings").on("mouseout", "div#settingsSensors table tr td.tooltip, div#settingsDevices table tr td.tooltip", function(){
		$(this).removeClass("tooltip");
		$("#tooltip").remove();
		$("div#settings div#settingsDevices").css("overflow", "hidden");
		$("div#settings div#settingsDevices").css("overflow", "auto");
		$("div#settings div#settingsSensors").css("overflow", "hidden");
		$("div#settings div#settingsSensors").css("overflow", "auto");
		console.log("s");
	});

});