var Settings = {
	onLoad: null,
	close: null,
	html: null,
	currentTab : null,
	backupChecklist: [],
	icongroups: {
		A:{
			name: "Settings_iconlist_groups", 
			icons:[
			{
				label : "Settings_tab_groups",
				status: false,
				func : "callGroups();",
				background: "../images/settings_group.jpg"
			},
			{
				label : "Settings_tab_users",
				status: false,
				func : "callUsers();",
				background: "../images/settings_user.jpg"
			}
			]
		},
		B:{
			name: "Settings_iconlist_notifier", 
			icons:[
			{
				label : "Settings_tab_smtp",
				status: false,
				func : "callSmtp();",
				background: "../images/settings_email.jpg"
			},
			{
				label : "Settings_tab_sms",
				status: false,
				func : "callSms();",
				background: "../images/settings_sms.jpg"
			},
			{
				label : "Settings_tab_periodicjobs",
				status: false,
				func : "callPeriodicJobs();",
				background: "../images/periodicjobs.png"
			}
			]
		},
		C:{
			name: "Settings_iconlist_servers", 
			icons:[
			{
				label : "Settings_tab_xml",
				status: false,
				func : "callXmlServer();",
				background: "../images/settings_xml.jpg"
			},
			{
				label : "Settings_tab_snmpservers",
				status: false,
				func : "callSnmpServers();",
				background: "../images/snmp.png"
			},
			{
				label : "Settings_tab_ipcam",
				status: false,
				func : "callIpCam();",
				background: "../images/settings_ipcam.jpg"
			},
			{
				label : "Settings_tab_syslogservers",
				status: false,
				func : "callSysLogServers();",
				background: "../images/syslog.png"
			}
			]
		},
		D:{
			name: "Settings_iconlist_system", 
			icons:[
			{
				label : "Settings_tab_tcpip",
				status: false,
				func : "callTcpIp();",
				background: "../images/settings_tcp.jpg"
			},
			{
				label : "Settings_tab_ftp",
				status: false,
				func : "callFtp();",
				background: "../images/settings_ftp.jpg"
			},
			{
				label : "Settings_tab_snmpmanager",
				status: false,
				func : "callSnmpManagerSettings();",
				background: "../images/settings_snmpmanager.png"
			},
			{
				label : "Settings_tab_gsm",
				status: false,
				func : "callGsm();",
				background: "../images/settings_ppp.jpg"
			},
			{
				label : "Settings_tab_ntpserver",
				status: true,
				func : "callNtpServer();",
				background: "../images/settings_ntp.jpg"
			},
			{
				label : "Settings_tab_sysinfo",
				status: false,
				func : "callSystemInfo();",
				background: "../images/settings_sysinfo.jpg"
			},
			{
				label : "Settings_tab_syssettings",
				status: false,
				func : "callSystemSettings();",
				background: "../images/settings_syssettings.jpg"
			},
			{
				label : "Settings_tab_log",
				status: false,
				func : "callLog();",
				background: "../images/settings_log.jpg"
			},
			{
				label : "Settings_tab_generate_reffile",
				status: false,
				func : "callRefFileGenerate();",
				background: "../images/settings_reffile.png"
			},
			{
				label : "Settings_tab_ldap",
				status: false,
				func : "callLdap();",
				background: "../images/settings_ldap.png"
			}
			]
		}
	},
	permission: [
	{	
		name: "Settings_superuser",
		value: 0
	},
	{	
		name: "Settings_admin",
		value: 1
	},
	{	
		name: "Settings_user",
		value: 2
	},
	]
};

Settings.html = '<div id="alertDelete" style="width: 400px;" class="panelDiv">'
					+ '<span class="text"></span>'
					+ '<div class="buttons">'
					+ '	<input type="submit" id="buttonRemove" name="buttonRemove" class="submit">'
					+ '	<input type="submit" id="buttonClose" name="buttonClose" class="submit">'
					+ '</div>'
				+ '</div>'
				+ '<div class="back"><a id="back"><a id="title"></div><ul class="icons"></ul><div class="form">';

Settings.onLoad = function() {
	$("div#settings").css("display","block");
	$("div#settings").html(Settings.html);
	$("div#settings div.back a#back").html(language.back);
	Settings.createTabs();
};

Settings.close = function() {
	$("div#settings").css("display","none");
	$("div#settings").html("");
};

Settings.onGroupLoad = function(page) {
	go = page;
	var getGroupsData = $.getJSON("http://" + hostName + "/cgi-bin/userandgroups.cgi?getGroups");	 
	getGroupsData.success(function() {
		Settings.groups = jQuery.parseJSON(getGroupsData.responseText);

		if(page == "groups") {
			Settings.createGroupSelect();
			Settings.getDeviceList();
			Settings.createGroupPermission();
		} else {
			Settings.createElements();		
		}

		Main.unloading();
	});	

	getGroupsData.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Settings.createTabs = function()
{
	for (var j in Settings.icongroups)
	{
		var name = Settings.icongroups[j].name;
		var li0 = document.createElement("li");
		li0.setAttribute("class", "group");
		var p0 = document.createElement("p");
		p0.innerHTML = eval("language." + name);
		li0.appendChild(p0);
		var divUnderline = document.createElement("div");
		divUnderline.setAttribute("style", "height:20px;width:100%;background:url('../images/underline3.png') no-repeat left;background-size: 25% auto;");
		li0.appendChild(divUnderline);
		$("div#settings ul.icons").append(li0);
		var icons =  Settings.icongroups[j].icons; 
		var index = 0;
		var appended = true;
		var li = document.createElement("li");
		li.setAttribute("class", "icon");
		for(var i = 0; i < icons.length; i++)
		{
			var parentDiv = document.createElement("div");
			parentDiv.setAttribute("class", "parent");
			parentDiv.setAttribute("data-func", icons[i].func);
			parentDiv.setAttribute("title", eval("language." + icons[i].label));
			var icon = document.createElement("div");
			icon.setAttribute("class", "icon");
			icon.setAttribute("style", "background: url('" + icons[i].background + "') no-repeat center;background-size: 40% auto;");
			var iconName = document.createElement("div");
			iconName.setAttribute("class","iconName");
			var p = document.createElement("p");
			p.innerHTML = eval("language." + icons[i].label);
			iconName.appendChild(p);
			parentDiv.appendChild(icon);
			parentDiv.appendChild(iconName);
			li.appendChild(parentDiv);
			appended = false;
			index++;

			if (index == 5)
			{
				index = 0;
				$("div#settings ul.icons").append(li);
				li = document.createElement("li");
				li.setAttribute("class", "icon");
				appended = true;
			}

		}
		if (appended == false)
		{
			$("div#settings ul.icons").append(li);
		}
		var liTmp = document.createElement("li");
		var divTmp = document.createElement("div");
		divTmp.setAttribute("style", "width: 100%; height: 20px;");
		liTmp.appendChild(divTmp);
		$("div#settings ul.icons").append(liTmp);
	}


	var list = $("div#settings ul.icons li.icon div p");
	for (var i = 0; i < list.length; i++)
	{
		var p = $(list[i]);
		var height = p.height();
		p.css("margin-top", (((40 - height)/2).toString() + "px"));
	}
	Main.unloading();
};

Settings.iconsClose = function() {
	for(var i = 0; i < Settings.icons.length; i++) {
		if(Settings.icons[i].status) {
			Settings.icons[i].status = false;
			eval("Settings." + Settings.icons[i].func);
		}
	}
};

Settings.iconsCloseAll = function() {
	for(var i = 0; i < Settings.icons.length; i++) {
		Settings.icons[i].status = false;
	}
};

Settings.callModuls = function() {	
	$("div#settings div.form").html("");
};

$(function() {
	$("div#settings").on( "click", "ul.icons li div.parent", function() {
		Main.loading();
		$("div#settings ul.icons").css("display", "none");
		$("div#settings div.form").css("display", "block");
		$("div#settings div.back").css("display", "block");
		var func = $(this).data("func");
		eval("Settings." + func);
		$("div#settings div.back a#title").html($(this).attr("title"));
	});

	$("div#settings").on("click", "div.back a#back", function() {
		$("div#settings ul.icons").css("display", "block");
		$("div#settings div.form").css("display", "none");
		$("div#settings div.back").css("display", "none");
	});
});
