Settings.tcpipData = {};

Settings.callTcpIp = function() {
	Settings.htmlTCPIP();
	Settings.getTCPIP();
};

Settings.htmlTCPIP = function() {
	$("div#settings div.form").html("");
	var html = '<div class="alert"></div>'
				+ '<ul class="element" id="tcpipform" style="display:none;">'
				+	'<li id="dhcp"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="ip"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="mask"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="gateway"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="dns"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="mac"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="xmlport"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="buttons"><div class="left"></div><div class="right"></div></li>'
				+ '</ul>';
	$("div#settings div.form").html(html);
};

Settings.getTCPIP = function() {
	var data = $.getJSON("http://" + hostName + "/cgi-bin/networksettings.cgi?getNetworkSettings");	 
	data.success(function() {
		Settings.tcpipData = jQuery.parseJSON(data.responseText);
		Settings.createTcpIpForm();
		Main.unloading();
	});
	data.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Settings.postTCPIP = function() {
	Main.loading();
	json = JSON.stringify(Settings.tcpipData);

	var url = "http://" + hostName + "/cgi-bin/networksettings.cgi?updateNetworkSettings";
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			//Settings.callSmtp();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};
Settings.createTcpIpForm = function() {	
	Main.unloading();
	var item = Settings.tcpipData;
	var dhcp = item.dhcp_enabled;
	var ip = item.ip_addr;
	var mask = item.subnet_mask;
	var gateway = item.gateway_addr;
	var dns = item.dns_server_ip;
	var mac = item.mac_addr;
	var xmlport = item.xml_server_port;

	var dhcpInput = document.createElement("input");
	dhcpInput.setAttribute("type","checkbox");
	dhcpInput.setAttribute("id","dhcpInput");
	if(dhcp == 1) {
		dhcpInput.checked = true;
	}
	$("li#dhcp div.left").html(language.Settings_tcpip_dhcp);
	$("li#dhcp div.right").append(dhcpInput);

	var ipInput = document.createElement("input");
	ipInput.setAttribute("type","text");
	ipInput.setAttribute("id","ipInput");
	ipInput.setAttribute("value",ip);
	if(dhcp == 1) ipInput.setAttribute("disabled","disabled");
	$("li#ip div.left").html(language.Settings_tcpip_ip);
	$("li#ip div.right").append(ipInput);

	var maskInput = document.createElement("input");
	maskInput.setAttribute("type","text");
	maskInput.setAttribute("id","maskInput");
	maskInput.setAttribute("value",mask);
	if(dhcp == 1) maskInput.setAttribute("disabled","disabled");
	$("li#mask div.left").html(language.Settings_tcpip_mask);
	$("li#mask div.right").append(maskInput);

	var gatewayInput = document.createElement("input");
	gatewayInput.setAttribute("type","text");
	gatewayInput.setAttribute("id","gatewayInput");
	gatewayInput.setAttribute("value",gateway);
	if(dhcp == 1) gatewayInput.setAttribute("disabled","disabled");
	$("li#gateway div.left").html(language.Settings_tcpip_gateway);
	$("li#gateway div.right").append(gatewayInput);

	var dnsInput = document.createElement("input");
	dnsInput.setAttribute("type","text");
	dnsInput.setAttribute("id","dnsInput");
	dnsInput.setAttribute("value",dns);
	if(dhcp == 1) dnsInput.setAttribute("disabled","disabled");
	$("li#dns div.left").html(language.Settings_tcpip_dns);
	$("li#dns div.right").append(dnsInput);


	$("li#mac div.left").html(language.Settings_tcpip_mac);
	$("li#mac div.right").append(mac);

	var xmlportInput = document.createElement("input");
	xmlportInput.setAttribute("type","text");
	xmlportInput.setAttribute("id","xmlportInput");
	xmlportInput.setAttribute("value",xmlport);
	//if(dhcp == 1) xmlportInput.setAttribute("disabled","disabled");
	$("li#xmlport div.left").html(language.Settings_tcpip_xmlport);
	$("li#xmlport div.right").append(xmlportInput);

	var saveInput = document.createElement("input");
	saveInput.setAttribute("type","submit");
	saveInput.setAttribute("id","save");
	//if(dhcp == 1) saveInput.setAttribute("disabled","disabled");
	saveInput.setAttribute("value", language.Settings_ftp_save);
	$("li#buttons div.left").html("");
	$("li#buttons div.right").append(saveInput);

	$("div#settings ul.element").css("display","block");
};

Settings.validateTcpIpForm = function(page) {	
	$("div#settings div.alert").html("");
	var alertHtml = $("div#settings div.alert").html();
	$("li#ip").removeClass("alert");
	$("li#mask").removeClass("alert");
	$("li#gateway").removeClass("alert");
	$("li#dns").removeClass("alert");
	$("li#xmlport").removeClass("alert");

	var dhcp = $("li#dhcp input").prop("checked");
	var ip = $("li#ip input").val();
	var mask = $("li#mask input").val();
	var gateway = $("li#gateway input").val();
	var dns = $("li#dns input").val();
	var xmlport = $("li#xmlport input").val();
	
	var validateip = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
	var value = true;

	if(!ip.match(validateip)) {
	    $("li#ip").addClass("alert");
	    alertHtml = language.Settings_tcpip_iperror;
		$("div#settings div.alert").html(alertHtml);
	    $("li#ip input").focus();
	    value = false;
    }

	if(!mask.match(validateip)) {
	    $("li#mask").addClass("alert");
	    alertHtml = alertHtml + "<br/>" + language.Settings_tcpip_iperror;
		$("div#settings div.alert").html(alertHtml);
	    $("li#mask input").focus();
	    value = false;
    }

	if(!gateway.match(validateip)) {
	    $("li#gateway").addClass("alert");
	    alertHtml = alertHtml + "<br/>" + language.Settings_tcpip_iperror;
		$("div#settings div.alert").html(alertHtml);
	    $("li#gateway input").focus();
	    value = false;
    }

	if(!dns.match(validateip)) {
	    $("li#dns").addClass("alert");
	    alertHtml = alertHtml + "<br/>" + language.Settings_tcpip_iperror;
		$("div#settings div.alert").html(alertHtml);
	    $("li#dns input").focus();
	    value = false;
    }

	if(xmlport.length == 0) {
	    $("li#xmlport").addClass("alert");
	    alertHtml = alertHtml + "<br />" + language.Settings_smtp_error_noempty;
		$("div#settings div.alert").html(alertHtml);
	    value = false;
	} else if(!isFinite(xmlport) && parseInt(xmlport)) {
		$("li#xmlport").addClass("alert");
		alertHtml = alertHtml + "<br />" + language.Settings_smtp_error_port;
		$("div#settings div.alert").html(alertHtml);			
		value = false;
	}

	if(value) {

		if(dhcp) {
			dhcp = "1";
		} else {
			dhcp = "0";
		}

		Settings.tcpipData = {
			dhcp_enabled: dhcp,
			ip_addr: ip,
			subnet_mask: mask,
			gateway_addr: gateway,
			dns_server_ip: dns,
			xml_server_port: xmlport
		};
	}

	return value;
};

$(function() {
	$("div#settings").on( "click", "ul#tcpipform input#save", function() {
		if(Settings.validateTcpIpForm()) {
			Settings.postTCPIP();
		}
	});
	$("div#settings").on( "change", "ul#tcpipform input#dhcpInput", function() {

		Settings.htmlTCPIP();
		
		var status = $(this).prop("checked");
		if(status) {
			Settings.tcpipData.dhcp_enabled = "1";
		} else {
			Settings.tcpipData.dhcp_enabled = "0";
		}
		
		Settings.createTcpIpForm();
	});
});