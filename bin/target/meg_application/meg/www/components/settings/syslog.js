
Settings.syslogData = {};

Settings.callSysLogServers = function()
{
    $("div#settings div.form").html("");
    var html = '<div class="alert"></div>'
                + '<div class="alertNaN"></div>'
                + '<ul class="element" id="syslogserverform" style="display:none;">'
                +   '<li id="server-select" style="display: none;">'
                    +   '<div class="left"></div>'
                    +   '<div class="right">'
                            +   '<select id="serverList"></select>'
                    +   '</div>'
                +   '</li>'             
                +   '<li id="version" style="display: none;">'
                    +   '<div class="left"></div>'
                    +   '<div class="right">'
                        +   '<select id="syslog_version">'
                            +   '<option id="v1"></option>'
                            +   '<option id="v2"></option>'
                        +   '</select>'
                        +   '<p></p>'
                    +   '</div>'
                +   '</li>'
                +   '<li id="active" style="display: none;">'
                    +   '<div class="left"></div>'
                    +   '<div class="right">'
                        +   '<input type="checkbox">'
                    +   '</div>'
                +   '</li>'
                +   '<li id="server_ip" style="display: none;">'
                    +   '<div class="left"></div>'
                    +   '<div class="right">'
                        +   '<input type="text">'
                    +   '</div>'
                +   '</li>'
               
                +   '<li id="server_port" style="display: none;">'
                    +   '<div class="left"></div>'
                    +   '<div class="right">'
                        +   '<input type="text">'
                    +   '</div>'
              
               
                +   '<li id="buttons">'
                + ' <input type="submit" id="syslogTest" name="syslogTest" class="submit "style="display:none;">'
                    + ' <input type="submit" id="syslogUpdate" name="syslogUpdate" class="submit" "style="display:none;">'
                    + ' <input type="submit" id="buttonNew" name="buttonNew" class="submit "style="display:none;">'
                    + ' <input type="submit" id="buttonSave" name="buttonSave" class="submit" "style="display:none;">'
                    + ' <input type="submit" id="buttonRemove" name="buttonRemove" class="submit" "style="display:none;">'
                    + ' <input type="submit" id="buttonCancel" name="buttonCancel" class="submit" "style="display:none;">'
                +   '</li>'
                + '</ul>'
                + '<div id="alertDelete" style="width: 400px;" class="panelDiv panelDivSyslog">'
                    + '<span class="text"></span>'
                    + '<div class="buttons">'
                    + ' <input type="submit" id="buttonRemove" name="buttonRemove" class="submit">'
                    + ' <input type="submit" id="buttonClose" name="buttonClose" class="submit">'
                    + '</div>'
                + '</div>';
    $("div#settings div.form").html(html);

    $("div#settings div.form ul#syslogserverform li#server-select div.left").html(language.Settings_server_syslog_list);
    $("div#settings div.form ul#syslogserverform li#version div.left").html(language.Settings_server_syslog_version);
    $("div#settings div.form ul#syslogserverform li#version div.right select option#v1").html(language.syslogv1);
    $("div#settings div.form ul#syslogserverform li#version div.right select option#v2").html(language.syslogv2);
    $("div#settings div.form ul#syslogserverform li#server_ip div.left").html(language.Settings_server_syslog_addr);
    $("div#settings div.form ul#syslogserverform li#server_port div.left").html(language.Settings_server_syslog_port);
    $("div#settings div.form ul#syslogserverform li#active div.left").html(language.Settings_server_syslog_enabled);

    $("div#settings div.form ul#syslogserverform input#buttonNew").attr("value",language.Settings_xml_server_new);
    $("div#settings div.form ul#syslogserverform input#syslogTest").attr("value",language.Settings_xml_server_test);
    $("div#settings div.form ul#syslogserverform input#syslogUpdate").attr("value",language.Settings_xml_server_update);
    $("div#settings div.form ul#syslogserverform input#buttonSave").attr("value",language.Settings_xml_server_save);
    $("div#settings div.form ul#syslogserverform input#buttonRemove").attr("value",language.Settings_xml_server_remove);
    $("div#settings div.form ul#syslogserverform input#buttonCancel").attr("value",language.Settings_xml_server_cancel);
    
    Settings.getSysLogServerList();
};

Settings.getSysLogServerList = function()
{
    Main.loading();
    var data = $.getJSON("http://" + hostName + "/cgi-bin/networksettings.cgi?getSyslogServers");  
    data.success(function() {
        Settings.SysLogServerList = jQuery.parseJSON(data.responseText);
        Settings.createsyslogserverform("list");
        Main.unloading();
    });
    data.error(function(jqXHR, textStatus, errorThrown){
        Main.alert(language.server_error + ":" + jqXHR.responseText);
        Main.unloading();
    });
};

Settings.activateSysLogElements = function(type)
{
    if (type == "list")
    {
        if (Settings.syslog_server_list_empty)
        {
            $("div#settings div.form ul#syslogserverform input#syslogTest").css("display", "none");
            $("div#settings div.form ul#syslogserverform input#syslogUpdate").css("display", "none");
        }
        else
        {
            $("ul#syslogserverform select#serverList").prop("selectedIndex","0");
            $("div#settings div.form ul#syslogserverform input#syslogTest").css("display", "inline-block");
            $("div#settings div.form ul#syslogserverform input#syslogUpdate").css("display", "inline-block");
        }
        $("div#settings div.form ul#syslogserverform input#buttonNew").css("display", "inline-block");
        $("div#settings div.form ul#syslogserverform input#buttonSave").css("display", "none");
        $("div#settings div.form ul#syslogserverform input#buttonRemove").css("display", "none");
        $("div#settings div.form ul#syslogserverform input#buttonCancel").css("display", "none");
        $("div#settings div.form ul#syslogserverform li#server-select").css("display", Settings.syslog_server_list_empty ? "none":"block");
        $("div#settings div.form ul#syslogserverform li#version").css("display", "none");
        $("div#settings div.form ul#syslogserverform li#active").css("display", "none");
        $("div#settings div.form ul#syslogserverform li#server_ip").css("display", "none");
        $("div#settings div.form ul#syslogserverform li#server_port").css("display", "none");
    }
    else if (type == "update")
    {
        $("div#settings div.form ul#syslogserverform input#buttonNew").css("display", "none");
            $("div#settings div.form ul#syslogserverform input#syslogTest").css("display", "none");
        $("div#settings div.form ul#syslogserverform input#syslogUpdate").css("display", "none");
        $("div#settings div.form ul#syslogserverform input#buttonSave").css("display", "inline-block");
        $("div#settings div.form ul#syslogserverform input#buttonRemove").css("display", "inline-block");
        $("div#settings div.form ul#syslogserverform input#buttonCancel").css("display", "inline-block");
        $("div#settings div.form ul#syslogserverform li#server-select").css("display", "none");

         if ($("div#settings div.form ul#syslogserverform li#version select").attr("disabled") != undefined)
            $("div#settings div.form ul#syslogserverform li#version select").removeAttr("disabled");
        $("div#settings div.form ul#syslogserverform li#version").css("display", "inline-block");
        $("div#settings div.form ul#syslogserverform li#version select").css("display", "nline-block");
        $("div#settings div.form ul#syslogserverform li#version p").css("display", "none");
        $("div#settings div.form ul#syslogserverform li#active").css("display", "inline-block");
        $("div#settings div.form ul#syslogserverform li#server_ip").css("display", "inline-block");
        $("div#settings div.form ul#syslogserverform li#server_port").css("display", "inline-block");
        var version = ($("div#settings div.form ul#syslogserverform li#version div.right select#syslog_version").prop("selectedIndex")).toString();
        if (version == "0")
        {
             $("div#settings div.form ul#syslogserverform li#version select").prop("selectedIndex","0");
        }
        else if (version == "1")
        {
            $("div#settings div.form ul#syslogserverform li#version select").prop("selectedIndex","1");
        }
      
    }
    else if (type == "new")
    {

        $("div#settings div.form ul#syslogserverform input#buttonNew").css("display", "none");
            $("div#settings div.form ul#syslogserverform input#syslogTest").css("display", "none");
        $("div#settings div.form ul#syslogserverform input#syslogUpdate").css("display", "none");
        $("div#settings div.form ul#syslogserverform input#buttonSave").css("display", "inline-block");
        $("div#settings div.form ul#syslogserverform input#buttonRemove").css("display", "none");
        $("div#settings div.form ul#syslogserverform input#buttonCancel").css("display", "inline-block");
        $("div#settings div.form ul#syslogserverform li#server-select").css("display", "none");
        if ($("div#settings div.form ul#syslogserverform li#version select").attr("disabled") != undefined)
            $("div#settings div.form ul#syslogserverform li#version select").removeAttr("disabled");
        $("div#settings div.form ul#syslogserverform li#version").css("display", "inline-block");
        $("div#settings div.form ul#syslogserverform li#version select").css("display", "inline-block");
        $("div#settings div.form ul#syslogserverform li#version p").css("display", "none");
        $("div#settings div.form ul#syslogserverform li#active").css("display", "inline-block");
        $("div#settings div.form ul#syslogserverform li#server_ip").css("display", "inline-block");
        $("div#settings div.form ul#syslogserverform li#server_port").css("display", "inline-block");

        $("div#settings div.form ul#syslogserverform li#version select").prop("selectedIndex","0");
        $("div#settings div.form ul#syslogserverform li#active input").prop("checked",false);
        $("div#settings div.form ul#syslogserverform li#server_ip input").val("");
        $("div#settings div.form ul#syslogserverform li#server_port input").val("");
    }
};

Settings.createSysLogServerList = function()
{
    $("div#settings div.form ul#syslogserverform li#server-select select#serverList option").empty();
    var opt0 = document.createElement("option");
    opt0.setAttribute("id", "none");
    opt0.innerHTML = language.Settings_server_snmp_select;
    $("div#settings div.form ul#syslogserverform li#server-select select#serverList").append(opt0);
    var len = Settings.SysLogServerList.length;
    if (len == 0)
        return true;

    for (var i = 0; i < len; i++)
    {
        Settings.insertToSysLogServerList(Settings.SysLogServerList[i], i, true);
    }
    Settings.activateSysLogElements("list");
    return false;
};

Settings.insertToSysLogServerList = function(item, pos,insert)
{
    Settings.SysLogServerList[pos] = item;
    if (insert)
    {
        var opt = document.createElement("option");
        opt.setAttribute("id", pos.toString());
        opt.innerHTML = item.server_ip + ":" + item.server_port;
        $("div#settings div.form ul#syslogserverform li#server-select select#serverList").append(opt);
    }
    else
    {
        var html =  item.server_ip + ":" + item.server_port;
        $("div#settings div.form ul#syslogserverform li#server-select select#serverList option:selected").html(html);
    }
};

Settings.removeFromSysLogServerList = function()
{
    var opt = $("div#settings div.form ul#syslogserverform li#server-select select#serverList option:selected");
    var index = parseInt(opt.attr("id")); 
    Settings.SysLogServerList[index] = undefined;
    opt.remove();
};

Settings.createSysLogServer = function(index)
{
    var item = Settings.SysLogServerList[parseInt(index)];
    if (item == undefined)
        return;
    $("div#settings div.form ul#syslogserverform li#version div.right select#syslog_version").prop("selectedIndex", item.version);
    $("div#settings div.form ul#syslogserverform li#active div.right input").prop("checked",item.active == "1");
    $("div#settings div.form ul#syslogserverform li#server_ip div.right input").val(item.server_ip);
    $("div#settings div.form ul#syslogserverform li#server_port div.right input").val(item.server_port);
      
};

Settings.createsyslogserverform = function(type, index)
{
    $("div#settings div.form ul#syslogserverform").css("display", "block");

    if (type == "list")
    {
        Settings.syslog_server_list_empty = Settings.createSysLogServerList();
        if (Settings.syslog_server_list_empty)
        {
            $("div#settings div.form div.alertNaN").html(language.settings_syslog_server_not);
        }
        else
        {
            $("div#settings div.form div.alertNaN").html("");
        }
        Settings.snmp_server_page = "existing";
    }
    else if (type == "update")
    {
        $("div#settings div.form div.alertNaN").html("");
        Settings.createSysLogServer(index);
    }
    else if (type == "new")
    {
        $("div#settings div.form div.alertNaN").html("");
    }
    Settings.activateSysLogElements(type);
};

Settings.validatesyslogserverform = function(type)
{
    var version 	= ($("div#settings div.form ul#syslogserverform li#version select#syslog_version").prop("selectedIndex")).toString();
    var active 		= ($("div#settings div.form ul#syslogserverform li#active input").prop("checked")) ? "1":"0";
    var server_ip 	= $("div#settings div.form ul#syslogserverform li#server_ip input").val();
    var server_port = $("div#settings div.form ul#syslogserverform li#server_port input").val();

    $("div#settings div.alert").html("");
    var alertHtml = $("div#settings div.alert").html();
    $("div#settings div.form ul#syslogserverform li#server_ip").removeClass("alert");
    $("div#settings div.form ul#syslogserverform li#server_port").removeClass("alert");
    $("div#settings div.form ul#syslogserverform").removeClass("alert");

	var isvalid = true;
	
	//validation of server_ip
	if(!Validator.validate( { "val" : server_ip, "type" : "domain", "ismandatory" : "true", "obj" : $("li#server_ip"), "objfocus" : $("li#server_ip input"), "page" : $("div#settings div.alert")}))
		isvalid = false;

	//validation of server_port
	if(!Validator.validate( { "val" : server_port, "type" : "port", "ismandatory" : "true", "obj" : $("li#server_port"), "objfocus" : $("li#server_port input"), "page" : $("div#settings div.alert")}))
		isvalid = false;

    return isvalid;
};

Settings.getNewSysLogServerInfo = function()
{
    var version = ($("div#settings div.form ul#syslogserverform li#version select#syslog_version").prop("selectedIndex")).toString();
    var active = ($("div#settings div.form ul#syslogserverform li#active input").prop("checked")) ? "1":"0";
    var server_ip = $("div#settings div.form ul#syslogserverform li#server_ip input").val();
    var server_port = $("div#settings div.form ul#syslogserverform li#server_port input").val();

    var value = Settings.validatesyslogserverform("new");

    if (value)
    {
        if (version == "0" || version == "1")
        {
            Settings.newSysLogServer = {
                "version": version,
                "active": active,
                "server_ip": server_ip,
                "server_port": server_port
            };
            return true;
        }
        else if (version == "2")
        {
            Settings.newSysLogServer = {
                "version": version,
                "active": active,
                "server_ip": server_ip,
                "server_port": server_port
            };
            return true;
        }
        else
        {
            return false;
        }
    }
    else
        return false;
};

Settings.getUpdateSysLogServerInfo = function()
{
    var version = ($("div#settings div.form ul#syslogserverform li#version select#syslog_version").prop("selectedIndex")).toString();
    var active = ($("div#settings div.form ul#syslogserverform li#active input").prop("checked")) ? "1":"0";
    var server_ip = $("div#settings div.form ul#syslogserverform li#server_ip input").val();
    var server_port = $("div#settings div.form ul#syslogserverform li#server_port input").val();

    var value = Settings.validatesyslogserverform("update");

    if (value)
    {
        if (version == "0" || version == "1")
        {
            Settings.updateSysLogServerInfo = {
                "version": version,
                "active": active,
                "server_ip": server_ip,
                "server_port": server_port
            };
            return true;
        }
        else if (version == "2")
        {
            if (password.length == 0)
            {
                Settings.updateSysLogServerInfo = {
                    "version": version,
                    "active": active,
                    "server_ip": server_ip,
                    "server_port": server_port
                };
            }
            else
                Settings.updateSysLogServerInfo = {
                    "version": version,
                    "active": active,
                    "server_ip": server_ip,
                    "server_port": server_port
                };
            return true;
        }
        else
        {
            return false;
        }
    }
    else
        return false;
};

Settings.addSysLogServer = function()
{
    Main.loading();
    json = JSON.stringify(Settings.newSysLogServer);
    var url = "http://" + hostName + "/cgi-bin/networksettings.cgi?addSyslogServer";
    $.ajax({
        url : url,
        type: "POST",
        data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
        success: function(data, textStatus, jqXHR)
        {
            json = jQuery.parseJSON(json);
            var id = jQuery.parseJSON(data).id;
            json["id"]= id;
            Main.unloading();
            Main.alert(language.Settings_server_add_success);
            Settings.insertToSysLogServerList(json, Settings.SysLogServerList.length, true);
            $("div#settings div.form div.alertNaN").html("");
            $("ul#syslogserverform select#serverList").prop("selectedIndex","0");
            var len = Settings.SysLogServerList.length;
            var i;
            for (i = 0; i < len; i++)
                if (Settings.SysLogServerList[i] != undefined)
                    break;
            if (i == len || len == 0)
            {
                $("div#settings div.form div.alertNaN").html(language.settings_syslog_server_not);
                Settings.syslog_server_list_empty = true;
            }
            else
            {
                $("div#settings div.form div.alertNaN").html("");
                Settings.syslog_server_list_empty = false;
            }
            Settings.activateSysLogElements("list");
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        Main.alert(language.server_error + ":" + jqXHR.responseText);
            Main.unloading();
        }
    });
};

Settings.updateSysLogServer = function()
{
    Main.loading();
    json = JSON.stringify(Settings.updateSysLogServerInfo);
    var opt = $("div#settings div.form ul#syslogserverform li#server-select select#serverList option:selected");
    var index = parseInt(opt.attr("id")); 
    var url = "http://" + hostName + "/cgi-bin/networksettings.cgi?updateSyslogServer&id=" + Settings.SysLogServerList[index].id;
    $.ajax({
        url : url,
        type: "POST",
        data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
        success: function(data, textStatus, jqXHR)
        {
            Main.unloading();
            Main.alert(language.Settings_server_update_success);
            var item = jQuery.parseJSON(json);
            Settings.insertToSysLogServerList(item, index,false);
            $("div#settings div.form div.alertNaN").html("");
            $("ul#syslogserverform select#serverList").prop("selectedIndex","0");
            var len = Settings.SysLogServerList.length;
            var i;
            for (i = 0; i < len; i++)
                if (Settings.SysLogServerList[i] != undefined)
                    break;
            if (i == len || len == 0)
            {
                $("div#settings div.form div.alertNaN").html(language.settings_syslog_server_not);
                Settings.syslog_server_list_empty = true;
            }
            else
            {
                $("div#settings div.form div.alertNaN").html("");
                Settings.syslog_server_list_empty = false;
            }
            Settings.activateSysLogElements("list");
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        Main.alert(language.server_error + ":" + jqXHR.responseText);
            Main.unloading();
        }
    });
};

Settings.removeSyslogServer = function()
{
    $("div#overlay").css("display","none");
    $("div#settings div.form div#alertDelete").css("display","none");
    Main.loading();
    var selected = parseInt($("div#settings div.form ul#syslogserverform li#server-select select#serverList").prop("selectedIndex"));
    var index = parseInt($("div#settings div.form ul#syslogserverform li#server-select select#serverList option:selected").attr("id")); 
    var url = "http://" + hostName + "/cgi-bin/networksettings.cgi?removeSyslogServer&id=" + Settings.SysLogServerList[index].id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
        success: function(data, textStatus, jqXHR)
        {
            Main.unloading();
            Main.alert(language.Settings_server_remove_success);
            Settings.removeFromSysLogServerList();
            $("ul#syslogserverform select#serverList").prop("selectedIndex","0");
            var len = Settings.SysLogServerList.length;
            var i;
            for (i = 0; i < len; i++)
                if (Settings.SysLogServerList[i] != undefined)
                    break;
            if (i == len || len == 0)
            {
                $("div#settings div.form div.alertNaN").html(language.settings_syslog_server_not);
                Settings.syslog_server_list_empty = true;
            }
            else
            {
                $("div#settings div.form div.alertNaN").html("");
                Settings.syslog_server_list_empty = false;
            }
            Settings.activateSysLogElements("list");
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        Main.alert(language.server_error + ":" + jqXHR.responseText);
            Main.unloading();
        }
    });
};

Settings.testSyslogServer = function()
{
    Main.loading();
    var index = parseInt($("div#settings div.form ul#syslogserverform li#server-select select#serverList option:selected").attr("id")); 
    var url = "http://" + hostName + "/cgi-bin/networksettings.cgi?testSyslogServer&id=" + Settings.SyslogServerList[index].id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: 'text',
        success: function(data, textStatus, jqXHR)
        {
            Main.unloading();
            Main.alert(language.Settings_syslogserver_test_success);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        Main.alert(language.server_error + ":" + jqXHR.responseText);
            Main.unloading();
        }
    });
};


$(
    function()
    {
        $("div#settings").on( "click", "div.form ul#syslogserverform input#syslogTest", function() {
            var selected = $("ul#syslogserverform select#serverList option:selected" ).prop("id");
            if (selected == "none")
            {
                Main.alert(language.Setting_select_one_syslogserver);
                return;
            }
            Settings.testSyslogServer();
        });
      
        
        $("div#settings").on( "click", "div.form ul#syslogserverform input#syslogUpdate", function() {
            var selected = $("ul#syslogserverform select#serverList option:selected" ).prop("id");
            if (selected == "none")
            {
                Main.alert(language.Setting_select_one_syslogserver);
                return;
            }
            Settings.snmp_form_type = "update";
            Settings.createsyslogserverform("update", selected);
        });

        $("div#settings").on( "click", "div.form ul#syslogserverform input#buttonNew", function() {
            Settings.snmp_form_type = "new";
            Settings.createsyslogserverform("new");
        });

        $("div#settings").on( "click", "div.form ul#syslogserverform input#buttonSave", function() {
            if (Settings.snmp_form_type == "new")
            {
                if(Settings.getNewSysLogServerInfo())                  
                    Settings.addSysLogServer();
             
            }
            else if (Settings.snmp_form_type == "update")
            {
                if(Settings.getUpdateSysLogServerInfo())
                    Settings.updateSysLogServer();
            }
        });

        $("div#settings").on( "click", "div.form ul#syslogserverform input#buttonRemove", function() {
            $("div#overlay").css("display","block");
            $("div#settings div.form div#alertDelete").css("display","block");
            $("div#settings div.form div#alertDelete span.text").html(language.Settings_remove_server);
            $("div#settings div.form div#alertDelete input#buttonRemove").val(language.remove);
            $("div#settings div.form div#alertDelete input#buttonClose").val(language.cancel);
        });

        $("div#settings").on( "click", "div.panelDivSyslog input#buttonRemove", function() {
            Settings.removeSyslogServer();
            
        });

        $("div#settings").on( "click", "div.panelDivSyslog input#buttonClose", function() {
            $("div#overlay").css("display","none");
            $("div#settings div.form div#alertDelete").css("display","none");
        });

        $("div#settings").on( "click", "div.form ul#syslogserverform input#buttonCancel", function() {
            $("div#settings div.alert").html("");
            $("div#settings div.form ul#syslogserverform li#server_ip").removeClass("alert");
            $("div#settings div.form ul#syslogserverform li#server_port").removeClass("alert");
            $("div#settings div.form ul#syslogserverform").removeClass("alert");
            $("ul#syslogserverform select#serverList").prop("selectedIndex","0");
            var len = Settings.SysLogServerList.length;
            var i;
            for (i = 0; i < len; i++)
                if (Settings.SysLogServerList[i] != undefined)
                    break;
            if (i == len || len == 0)
            {
                $("div#settings div.form div.alertNaN").html(language.settings_syslog_server_not);
                Settings.syslog_server_list_empty = true;
            }
            else
            {
                $("div#settings div.form div.alertNaN").html("");
                Settings.syslog_server_list_empty = false;
            }
            Settings.activateSysLogElements("list");
        });

        

    }
);

