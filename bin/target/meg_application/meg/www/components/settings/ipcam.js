
Settings.callIpCam = function()
{
	$("div#settings div.form").html("");
	
	var html = '<div class="alert"></div>'
				+ '<div class="alertNaN"></div>'
				+ '<ul class="element" id="ipcameraform" style="display: none;">'
					+'<li id="list">'
						+ '<div class="left"></div>'
						+ '<div class="right">'
							+ '<select>'
							+ '</select>'
						+ '</div>'
					+ '</li>'
					+'<li id="protocol">'
						+ '<div class="left"></div>'
						+ '<div class="right">'
							+ '<select>'
        + '<option id="HTTP"></option>'
								+ '<option id="RTSP"></option>'
							+ '</select>'
						+ '</div>'
					+ '</li>'
					+ '<li id="ip">'
						+ '<div class="left"></div>'
						+ '<div class="right">'
							+ '<input type="text">'
						+ '</div>'
					+ '</li>'
					+ '<li id="port">'
						+ '<div class="left"></div>'
						+ '<div class="right">'
							+ '<input type="text">'
						+ '</div>'
					+ '</li>'
					+ '</li>'
					+ '<li id="name">'
						+ '<div class="left"></div>'
						+ '<div class="right">'
							+ '<input type="text">'
						+ '</div>'
					+ '</li>'
					+ '<li id="link">'
						+ '<div class="left"></div>'
						+ '<div class="right">'
							+ '<input type="text">'
						+ '</div>'
					+ '</li>'
					+ '<li id="username">'
						+ '<div class="left"></div>'
						+ '<div class="right">'
							+ '<input type="text">'
						+ '</div>'
					+ '</li>'
					+ '<li id="password">'
						+ '<div class="left"></div>'
						+ '<div class="right">'
							+ '<input type="password">'
						+ '</div>'
					+ '</li>'
					+ '<li id="buttons">'
						+ '	<input type="submit" id="buttonAdd" name="buttonAdd" class="submit">'
						+ '	<input type="submit" id="buttonRemove" name="buttonRemove" class="submit">'
						+ '	<input type="submit" id="buttonSave" name="buttonSave" class="submit">'
						+ '	<input type="submit" id="buttonCancel" name="buttonCancel" class="submit">'
						+ '	<input type="submit" id="buttonWatch" name="buttonWatch" class="submit">'
					+ '</li>'
				+ '</ul>'
				+ '<ul class="element" id="watch" style="display: none;">'
					+'<li id="list">'
						+ '<div class="left">'
							+ '<select style="width: 300px;height: 40px;">'
							+ '</select>'
						+ '</div>'
						+ '<div class="right">'
							+ '<input type="submit" id="add" style="width: 300px;height: 40px;"></input>'
						+ '</div>'
					+ '</li>'
				+ '</ul>'
				+ '<ul class="element" id="watchList" style="display: none;">'
					+'<li id="header">'
						+ '<div class="left" style="width: 300px;height: 40px;"></div>'
						+ '<div class="right">'
							+ '<input id="watch" type="submit" style="width: 300px;height: 40px;"></input>'
						+ '</div>'
					+ '</li>'
				+ '</ul>'
				+ '<div id="alertDelete" style="width: 400px;" class="panelDiv ipcam">'
					+ '<span class="text"></span>'
					+ '<div class="buttons">'
					+ '	<input type="submit" id="buttonRemove" name="buttonRemove" class="submit">'
					+ '	<input type="submit" id="buttonClose" name="buttonClose" class="submit">'
					+ '</div>'
				+ '</div>'
				+ '<div class="watchScreen">'
				+ '</div>';
	
	$("div#settings div.form").html(html);

	$("div#settings div.form ul#ipcameraform li#list div.left").html(language.Settings_ipcam_list);
	$("div#settings div.form ul#ipcameraform li#protocol div.left").html(language.Settings_ipcam_protocol);
    $("div#settings div.form ul#ipcameraform li#protocol option#HTTP").html(language.Settings_ipcam_http);
	$("div#settings div.form ul#ipcameraform li#protocol option#RTSP").html(language.Settings_ipcam_rtsp);
	$("div#settings div.form ul#ipcameraform li#port div.left").html(language.Settings_ipcam_port);
	$("div#settings div.form ul#ipcameraform li#ip div.left").html(language.Settings_ipcam_ip);
	$("div#settings div.form ul#ipcameraform li#name div.left").html(language.Settings_ipcam_name);
	$("div#settings div.form ul#ipcameraform li#link div.left").html(language.Settings_ipcam_link);
	$("div#settings div.form ul#ipcameraform li#username div.left").html(language.Settings_ipcam_username);
	$("div#settings div.form ul#ipcameraform li#password div.left").html(language.Settings_ipcam_password);
	$("div#settings div.form ul#ipcameraform li#buttons input#buttonSave").val(language.save);
	$("div#settings div.form ul#ipcameraform li#buttons input#buttonAdd").val(language.neww);
	$("div#settings div.form ul#ipcameraform li#buttons input#buttonCancel").val(language.cancel);
	$("div#settings div.form ul#ipcameraform li#buttons input#buttonWatch").val(language.Settings_ipcam_watch);
	$("div#settings div.form ul#ipcameraform li#buttons input#buttonRemove").val(language.remove);
	$("div#settings div.form div#alertDelete input#buttonRemove").val(language.remove);
	$("div#settings div.form div#alertDelete input#buttonClose").val(language.cancel);
	$("div#settings div.alertNaN").html(language.settings_ipcam_not);
	$("div#settings div.form ul#watch input#add").val(language.Settings_ipcam_addwatchlist);
	$("div#settings div.form ul#watchList li#header div.left").html(language.Settings_ipcam_watchlist);
	$("div#settings div.form ul#watchList li#header input#watch").val(language.Settings_ipcam_watch);

	Settings.getIpCameras();
};

Settings.getIpCameras = function()
{
	Main.loading();
	var data = $.getJSON("http://" + hostName + "/cgi-bin/networksettings.cgi?getIpCameras");	 
	data.success(function() {
		Settings.ipCams = jQuery.parseJSON(data.responseText);
		var len = Settings.ipCams.length;

		var opt0 = document.createElement("option");
		opt0.innerHTML = language.Settings_ipcam_select;
		opt0.setAttribute("id", "0");
		$("ul#ipcameraform li#list select").append(opt0);
		for (var i = 0; i < len; i++)
		{
			var opt = document.createElement("option");
			opt.innerHTML = Settings.ipCams[i].name;
			opt.setAttribute("id", i.toString());
			$("ul#ipcameraform li#list select").append(opt);
			var opt_1 = document.createElement("option");
			opt_1.innerHTML = Settings.ipCams[i].name;
			opt_1.setAttribute("id", i.toString());
			$("ul#watch li#list select").append(opt_1);
		}
		if(len == 0)
		{
			Settings.activateIpCamFields("empty");
		}
		else
			Settings.activateIpCamFields("select");

		Main.unloading();
	});
	data.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Settings.activateIpCamFields = function(type)
{

	$("div#settings div.form div.watchScreen").css("display", "none");
	$("div#settings div.form ul#watch").css("display", "none");
	$("div#settings div.form ul#watchList").css("display", "none");
	//$("div#overlay").css("display","none");
	$("div#settings div.form div#alertDelete").css("display", "none");
	$("div#settings div.form div.alertNaN").css("display", "none");
	$("div#settings div.form ul#ipcameraform li#list").css("display", "none");
	$("div#settings div.form ul#ipcameraform li#protocol").css("display", "none");
	$("div#settings div.form ul#ipcameraform li#port").css("display", "none");
	$("div#settings div.form ul#ipcameraform li#ip").css("display", "none");
	$("div#settings div.form ul#ipcameraform li#name").css("display", "none");
	$("div#settings div.form ul#ipcameraform li#link").css("display", "none");
	$("div#settings div.form ul#ipcameraform li#username").css("display", "none");
	$("div#settings div.form ul#ipcameraform li#password").css("display", "none");
	$("div#settings div.form ul#ipcameraform li#buttons input#buttonSave").css("display", "none");
	$("div#settings div.form ul#ipcameraform li#buttons input#buttonAdd").css("display", "none");
	$("div#settings div.form ul#ipcameraform li#buttons input#buttonCancel").css("display", "none");
	$("div#settings div.form ul#ipcameraform li#buttons input#buttonRemove").css("display", "none");
	$("div#settings div.form ul#ipcameraform li#buttons input#buttonWatch").css("display", "none");
	$("div#settings div.form alertDelete input#buttonRemove").css("display", "none");
	$("div#settings div.form alertDelete input#buttonClose").css("display", "none");
	$("div#settings div.form ul#ipcameraform").css("display", "block");
	if (type == "select")
	{
		$("div#settings div.form ul#ipcameraform li#list").css("display", "block");
		$("div#settings div.form ul#ipcameraform li#buttons input#buttonAdd").css("display", "inline-block");
		$("div#settings div.form ul#ipcameraform li#buttons input#buttonWatch").css("display", "inline-block");
	}
	else if (type == "empty")
	{
		$("div#settings div.form div.alertNaN").css("display", "block");
		$("div#settings div.form ul#ipcameraform li#buttons input#buttonAdd").css("display", "inline-block");
		$("div#settings div.form ul#ipcameraform li#buttons input#buttonWatch").css("display", "none");
	}
	else if (type == "update")
	{
		$("div#settings div.form ul#ipcameraform li#protocol").css("display", "block");
		$("div#settings div.form ul#ipcameraform li#port").css("display", "block");
		$("div#settings div.form ul#ipcameraform li#ip").css("display", "block");
		$("div#settings div.form ul#ipcameraform li#name").css("display", "block");
		$("div#settings div.form ul#ipcameraform li#link").css("display", "block");
		$("div#settings div.form ul#ipcameraform li#username").css("display", "block");
		$("div#settings div.form ul#ipcameraform li#password").css("display", "block");
		$("div#settings div.form ul#ipcameraform li#buttons input#buttonSave").css("display", "inline-block");
		$("div#settings div.form ul#ipcameraform li#buttons input#buttonCancel").css("display", "inline-block");
		$("div#settings div.form ul#ipcameraform li#buttons input#buttonRemove").css("display", "inline-block");

	}
	else if (type == "add")
	{
		$("div#settings div.form ul#ipcameraform li#protocol").css("display", "block");
		$("div#settings div.form ul#ipcameraform li#port").css("display", "block");
		$("div#settings div.form ul#ipcameraform li#ip").css("display", "block");
		$("div#settings div.form ul#ipcameraform li#name").css("display", "block");
		$("div#settings div.form ul#ipcameraform li#link").css("display", "block");
		$("div#settings div.form ul#ipcameraform li#username").css("display", "block");
		$("div#settings div.form ul#ipcameraform li#password").css("display", "block");
		$("div#settings div.form ul#ipcameraform li#buttons input#buttonSave").css("display", "inline-block");
		$("div#settings div.form ul#ipcameraform li#buttons input#buttonCancel").css("display", "inline-block");
	}
	else if (type == "remove")
	{
		$("div#overlay").css("display","block");
		$("div#settings div.form div#alertDelete").css("display", "block");
	}
	else if (type == "watchList")
	{
		$("div#settings div.form ul#ipcameraform").css("display", "none");
		$("div#settings div.form ul#watch").css("display", "block");
		$("div#settings div.form ul#watchList").css("display", "block");
	}
	else if (type == "watch")
	{
		$("div#settings div.form ul#ipcameraform").css("display", "none");
		$("div#settings div.form ul#watch").css("display", "none");
		$("div#settings div.form ul#watchList").css("display", "none");
		$("div#settings div.form div.watchScreen").css("display", "block");
	}
};

Settings.createIpCamForm = function(type)
{
	if (type == "add")
	{
		$("div#settings div.form ul#ipcameraform li#protocol select").prop("selectedIndex", 0);
		$("div#settings div.form ul#ipcameraform li#port input").val("");
		$("div#settings div.form ul#ipcameraform li#ip input").val("");
		$("div#settings div.form ul#ipcameraform li#name input").val("");
		$("div#settings div.form ul#ipcameraform li#link input").val("");
		$("div#settings div.form ul#ipcameraform li#username input").val("");
		$("div#settings div.form ul#ipcameraform li#password input").val("");
	}
	else if (type == "update")
	{
		var index = parseInt($("div#settings div.form ul#ipcameraform li#list select option:selected").prop("id"));
		var item = Settings.ipCams[index];
		$("div#settings div.form ul#ipcameraform li#protocol select").prop("selectedIndex", $("div#settings div.form ul#ipcameraform li#protocol option#" + item.protocol).prop("index"));
		$("div#settings div.form ul#ipcameraform li#port input").val(item.port);
		$("div#settings div.form ul#ipcameraform li#ip input").val(item.ip);
		$("div#settings div.form ul#ipcameraform li#name input").val(item.name);
		$("div#settings div.form ul#ipcameraform li#link input").val(item.link);
		$("div#settings div.form ul#ipcameraform li#username input").val(item.username);
		$("div#settings div.form ul#ipcameraform li#password input").val(item.password);
	}
	Settings.activateIpCamFields(type);
};

Settings.validateIpCamForm = function()
{
	Settings.ipCamData = {};
	var protocol = $("div#settings div.form ul#ipcameraform li#protocol option:selected").prop("id");
	var ip       = $("div#settings div.form ul#ipcameraform li#ip input").val();
	var port     = $("div#settings div.form ul#ipcameraform li#port input").val();
	var name     = $("div#settings div.form ul#ipcameraform li#name input").val();
	var link     = $("div#settings div.form ul#ipcameraform li#link input").val();
	var username = $("div#settings div.form ul#ipcameraform li#username input").val();
	var password = $("div#settings div.form ul#ipcameraform li#password input").val();

	var isvalid = true;
	
	//validation of ip
	if(!Validator.validate( { "val" : ip, "type" : "domain", "ismandatory" : "true", "obj" : $("li#ip"), "objfocus" : $("li#ip input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	else
		Settings.ipCamData.ip = ip;	

	//validation of port
	if(!Validator.validate( { "val" : port, "type" : "port", "ismandatory" : "true", "obj" : $("li#port"), "objfocus" : $("li#port input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	else if(isvalid)
		Settings.ipCamData.port = port;
	
	//validation of name
	if(!Validator.validate( { "val" : name, "type" : "modulename", "ismandatory" : "true", "obj" : $("li#name"), "objfocus" : $("li#name input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	else if(isvalid)
		Settings.ipCamData.name = name;
	
	//validation of link
	if(!Validator.validate( { "val" : link, "type" : "url", "ismandatory" : "false", "obj" : $("li#link"), "objfocus" : $("li#link input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	else if(isvalid)
		Settings.ipCamData.link = link;	
	
	//validation of username
	if(!Validator.validate( { "val" : username, "type" : "camerausername", "ismandatory" : "false", "obj" : $("li#username"), "objfocus" : $("li#username input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	else if(isvalid)
		Settings.ipCamData.username = username;//kullanıcı istediği karakteri girebilir.
	
	//validation of password
	if(!Validator.validate( { "val" : password, "type" : "camerapassword", "ismandatory" : "false", "obj" : $("li#password"), "objfocus" : $("li#password input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	else if(isvalid)
		Settings.ipCamData.password = password;//kullanıcı istediği karakteri girebilir.
	
	Settings.ipCamData.protocol = protocol;
	
	return isvalid;
};

Settings.addIpCamera = function()
{
	Main.loading();
	json = JSON.stringify(Settings.ipCamData);
	var url = "http://" + hostName + "/cgi-bin/networksettings.cgi?addIpCamera";
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			Main.alert(language.Settings_ipcam_add_success);
			Settings.callIpCam();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.updateIpCamera = function()
{
	Main.loading();
	json = JSON.stringify(Settings.ipCamData);
	var id = Settings.ipCams[parseInt($("ul#ipcameraform li#list option:selected").prop("id"))].id;
	var url = "http://" + hostName + "/cgi-bin/networksettings.cgi?updateIpCamera&id=" + id;
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			Main.alert(language.Settings_ipcam_update_success);
			Settings.callIpCam();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.removeIpCamera = function()
{
	Main.loading();
	json = JSON.stringify(Settings.ipCamData);
	var id = Settings.ipCams[parseInt($("ul#ipcameraform li#list option:selected").prop("id"))].id;
	var url = "http://" + hostName + "/cgi-bin/networksettings.cgi?removeIpCamera&id=" + id;
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			Main.alert(language.Settings_ipcam_remove_success);
			Settings.callIpCam();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.createUrl = function(item)
{
	var url = item.protocol + "://" + item.username + ":" + item.password + "@" + item.ip + ":" + item.port;
	if (item.link != "")
	{
		url = url + "/" + item.link;
	}
	return url;
};

Settings.startUp = function (name, url, id) {
    var output = "";
    if (url.substr(0, 4) == "RTSP") {

        output = '<div class="Camera Camera' + id + '"><object \
	name="name1" \
	width="368" \
	height="250" \
	id="vlc" \
	classid="clsid:9BE31822-FDAD-461B-AD51-BE1D1C159921" \
	events="true" \
	codebase="http://download.videolan.org/pub/videolan/vlc/last/win64/axvlc.cab" >';
   
    output += '<param name="src" value="poster.mov">';
    output += '<param name="qtsrc" value="'+url+'">';
    output += '<param name="autoplay" value="false">';
    output += '<param name="wmode" value="transparent">';
    output += '<param name="controller" value="true">';

        output += '<embed \
    	loop="true" \
    	src="/poster.mov" \
    	bgcolor="000000" \
    	width="368" \
    	height="250" \
    	scale="ASPECT" \
    	qtsrc="' + url + '" \
    	kioskmode="true" \
    	showlogo=false" \
    	autoplay="false" \
    	controller="true" \
    	pluginspage="http://www.videolan.org" \
    	type="application/x-google-vlc-plugin" \
    	WMODE="transparent" >';
        output += '</embed></object><div class=cameraRTSPName'+id+' id=' + name + '>' + name + '</div></div>';
    }

    else if (url.substr(0, 4) == "HTTP") {
        output = '<div><object><embed><div><img \
    	class="shrinkToFit Camera Camera' + id + '"\
    	alt= "' + url + '" \
    	src= "' + url + '" \
    	height="250" \
    	width="368"> \
    	</img></div></embed></object><div class=cameraHTTPName'+id+' id=' + name + '>' + name + '</div></div>';
    }

    $("div.form div.watchScreen").append($(output));
};

$(function () {
        $("body").click(function (event) {
			try{
				var elemClass = event.target.getAttribute("class");
				var href = event.target.getAttribute("src");
				var element_class = event.target.getAttribute("class");
				var hasClass = $(event.target).hasClass("Camera");
			}catch (e){

			}

            if (hasClass) {
                var element_classes = element_class.split(" ");
                var last_class = element_classes[element_classes.length - 1];
                $("." + last_class).colorbox({
                    iframe: true,
                    width: '660px',
                    height: '550px',
                    fixed: true,
                    href: href
                });
            }
        });

        $("div#settings").on("change", "div.form ul#ipcameraform li#list select", function () {
			Settings.createIpCamForm("update");
			Settings.formType = "update";
		});

		$("div#settings").on("click", "div.form ul#ipcameraform li#buttons input#buttonAdd", function()
		{
			Settings.createIpCamForm("add");
			Settings.formType = "add";
		});

		$("div#settings").on("click", "div.form ul#ipcameraform li#buttons input#buttonSave", function()
		{
			$("div#settings div.alert").html("");
			$("ul#ipcameraform li").removeClass("alert");
			$("ul#ipcameraform li input").removeClass("alert");
			if (Settings.formType == "add")
			{
				if (Settings.validateIpCamForm())
				{
					Settings.addIpCamera();
				}
			}
			else if (Settings.formType == "update")
			{
				if (Settings.validateIpCamForm())
				{
					Settings.updateIpCamera();
				}
			}
		});

		$("div#settings").on("click", "div.form ul#ipcameraform li#buttons input#buttonRemove", function()
		{
			$("#content div#settings div.form div#alertDelete span.text").html(language.Settings_remove_ipcam);
			Settings.activateIpCamFields("remove");
		});

		$("div#settings").on("click", "div.form ul#ipcameraform li#buttons input#buttonCancel", function()
		{
			$("ul#ipcameraform li#list select").prop("selectedIndex", 0);
			$("div#settings div.alert").html("");
			$("ul#ipcameraform li").removeClass("alert");
			$("ul#ipcameraform li input").removeClass("alert");
			Settings.activateIpCamFields("select");
		});

		$("div#settings").on("click", "div.form ul#ipcameraform li#buttons input#buttonWatch", function()
		{
            $("#back").attr("id", "previous_back");
            $("input#add").trigger("click");
            //Settings.activateIpCamFields("watchList");
        });
        $("div#settings").on("click", "#previous_back", function(){
            $("#previous_back").attr("id","back");
            $("div#settings ul.icons").css("display", "none");
            $("div#settings div.form").css("display", "block");
            $("div#settings div.back").css("display", "clock");
            Settings.activateIpCamFields("select");
		});
		$("div#settings").on("click", "div.form div#alertDelete input#buttonRemove", function()
		{
			Settings.removeIpCamera();
		});

		$("div#settings").on("click", "div.form div#alertDelete input#buttonClose", function()
		{
			Settings.activateIpCamFields("update");
		});

		$("div#settings").on("click", "div.form ul#watch input#add", function()
		{
            var id = 0;
            var len = Settings.ipCams.length;
            $("div.form ul#watchList li").remove();
            for(var i=0;i<len;i++){
                $("div.form ul#watchList li#"+i).remove();
            }
            for (var i = 0; i < len; i++) {

			//var index = parseInt($("div.form ul#watch select option:selected").prop("id"));
			var item = Settings.ipCams[id];
			var url = Settings.createUrl(item);
			if ($("div.form ul#watchList li#" + item.id).length != 0)
			{
				Main.alert(language.Settings_ipcam_alreadyinwatchlist + ":" + url);
				return;
			}
			
			var html = '<div class="left" style="width: 200px;">' + item.name + '</div>'
					+ '<div class="right">' + url + '</div>'
					+ '<div id="trash"></div>';
			var li = document.createElement("li");
			li.setAttribute("url", url);
			li.setAttribute("name", item.name);
			li.setAttribute("id", item.id);
			li.innerHTML = html;
			$("div.form ul#watchList").append(li);

                ++id;
            }



            var list = $("div.form ul#watchList li");
            var len = list.length;
            var camera_id = 1;
            if (len > 0) {
                for (var i = 0; i < len; i++) {
                    Settings.startUp(list[i].getAttribute("name"), list[i].getAttribute("url"), camera_id);
                    ++camera_id;
                }
                Settings.activateIpCamFields("watch");
            }
            else
                Main.alert(language.Settings_ipcam_watchlist_empty);
		});

		$("div#settings").on("click", "div.form ul#watchList li div#trash", function()
		{
			$(this).parent().remove();

		});

		$("div#settings").on("click", "div.form ul#watchList li#header input#watch", function()
		{
			var list = $("div.form ul#watchList li");
			var len = list.length;
			if (len > 1)
			{
				for (var i = 1; i < len; i++)
				{
					var id = list[i].getAttribute("id");
                    Settings.startUp(list[i].getAttribute("name"), list[i].getAttribute("url"), id);
				}
				Settings.activateIpCamFields("watch");
			}
			else
				Main.alert(language.Settings_ipcam_watchlist_empty);
		});

	}
);