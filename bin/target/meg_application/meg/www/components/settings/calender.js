// JScript File
var splitted = null;

var jg = new jsGraphics("myCanvas");

var xTextCellOffset=20;
var yTextCellOffset=10;
var daysCellWidth=150;
var daysCellHeight=30;
var timeChartGridWidth=400;
var timeChartGridHeight=daysCellHeight*7;
var cellWidth=timeChartGridWidth/24;  // half an hour increments
var cellHeight=daysCellHeight;
var selectedCells=new Array();

function calenderClear() {
    selectedCells = new Array();
    //splitted = null;
    bFill=false;
    FillCells([0,0],[23,6]);
/*
    xTextCellOffset=20;
    yTextCellOffset=10;
    daysCellWidth=150;
    daysCellHeight=30;
    timeChartGridWidth=400;
    timeChartGridHeight=daysCellHeight*7;
    cellWidth=timeChartGridWidth/24;  // half an hour increments
    cellHeight=daysCellHeight;*/

}

function DrawGrid(x, y, width, height, numberOfRows, numberOfColumns)
{   
    for(row=0; row<=numberOfRows; row++)
    {
        var rowInPixles=y+row*height/numberOfRows;
        jg.drawLine(x, rowInPixles, x+width, rowInPixles); 
    }
    for(col=0; col<=numberOfColumns; col++)
    {
        var colInPixles=x+col*width/numberOfColumns;
        jg.drawLine(colInPixles, y, colInPixles, y+height); 
    }    
}

function DrawDaysAxies(type)
{
    ChristianWeekDays = new Array(language.Settings_calender_monday, language.Settings_calender_tuesday, language.Settings_calender_wednesday, language.Settings_calender_thursday, language.Settings_calender_friday, language.Settings_calender_saturday, language.Settings_calender_sunday);
    //ChristianWeekDays = new Array(  parsedLanguage["MONDAY"], parsedLanguage["TUESDAY"], parsedLanguage["WEDNESDAY"], parsedLanguage["THURSDAY"], parsedLanguage["FRIDAY"],parsedLanguage["SATURDAY"],parsedLanguage["SUNDAY"]);
    var days;
      
    //if(type==1)
    //{
       // days = IslamicWeekDays;
    //}
    //else if(type==2)
    //{
        days = ChristianWeekDays;
    //}

    // draw weekly holiday in red
    //jg.setColor("#FF0000");
    jg.drawString(days[0], xTextCellOffset, yTextCellOffset);
    jg.setColor("#000000");

    // draw all days of the week except for weekly holiday
    for(var day=1; day<7; day++)
        jg.drawString(days[day], xTextCellOffset, yTextCellOffset+day*daysCellHeight);        
        
    jg.setColor("DarkGray");
    DrawGrid(0, 0, daysCellWidth, daysCellHeight * 7, 7, 1);
    jg.setColor("Black");
}

function DrawTimeChartGrid()
{
    // Draw General Grid
    jg.setColor("DarkGray");
    DrawGrid(daysCellWidth, 0, timeChartGridWidth, timeChartGridHeight, 7, 24);     // 7 days, 24 hours per day
    
    // Draw Black Lines every 4 hours
    jg.setColor("Black");
    DrawGrid(daysCellWidth, 0, timeChartGridWidth, timeChartGridHeight, 1, 6);     // 4 hours chunks == 6 columns
}

function DrawTimeAxis()
{
    timesOfDay = new Array("00:00", "04:00", "08:00", "12:00", "16:00", "20:00", "24:00");
    jg.setColor("Black");
    var fourHoursWidth=timeChartGridWidth/6;
    for(var i=0; i<7; i++)    
        jg.drawString(timesOfDay[i], daysCellWidth-xTextCellOffset+i*fourHoursWidth, timeChartGridHeight);
}

function DrawCalendar(type)
{
    // Reset Selection Memory
    selectedCells = new Array();
    
    // Highlight the active Calendar type
    /*if(type==1)
    {
        //document.getElementById( "Islamic" ).style.color="Black";
       $( "label#Christian" ).css("color","#e10000");
       $("label#Christian").css("background","#e10000");
    }
    else if(type==2)
    {
        //document.getElementById( "Islamic" ).style.color="Gray";
       $("label#Christian").css("color","#e10000");
       $("label#Christian").css("background","#e10000");
    }*/
    
    jg.clear();
    DrawDaysAxies(type);
    DrawTimeAxis();
    DrawTimeChartGrid();    
    jg.paint();
}

DrawCalendar(1);
//DrawGrid(width, height, numRows, numCols);

var bSelectionInProgress=false;
var startCell=null;
var bFill=true;

function StartSelection(evt, obj)
{   
    if(!bSelectionInProgress)
    {        
        bSelectionInProgress=true;        
        //document.getElementById( "mouseCoordinates" ).innerHTML=bSelectionInProgress;
             
        // Reset
        lastCurrentCell=[-1,-1];
                
        startCell = PixelToCellPoint(getEventOffsetXY( evt, obj ));
        
        bFill=true;
        if(IndexOf(startCell)!=-1)
            bFill=false;        
        	
        HighlightCell(evt, obj);
    }            
}

var lastCurrentCell=[-1,-1];

function HighlightCell(evt, obj)
{                

    xy = getEventOffsetXY( evt, obj );
    // Move only within the grid area
    if(xy[0]>daysCellWidth && xy[0]<timeChartGridWidth+daysCellWidth && xy[1]< timeChartGridHeight )
    {                      
        var currentCell=PixelToCellPoint(xy);

        if(currentCell[0]!=lastCurrentCell[0] || currentCell[1]!=lastCurrentCell[1])
        {                
            // Update the red cursor position
            MoveCursor(lastCurrentCell, currentCell);
            lastCurrentCell = currentCell;        

            // Highlight any cells if we are selecting
            if(bSelectionInProgress==true)
            {                        
                FillCells(startCell, currentCell);                
            }
                
            //document.getElementById( "mouseCoordinates" ).innerHTML="x: "+xy[0]+" / y: "+xy[1];          
        }
    }
    
    //PrintSelectedCells("");
    //document.getElementById( "mouseCoordinates" ).innerHTML="x: "+xy[0]+" / y: "+xy[1];          
}



function HighlightCell2(xy, obj)
{                

    //xy = getEventOffsetXY( evt, obj );
    // Move only within the grid area
    if(xy[0]>daysCellWidth && xy[0]<timeChartGridWidth+daysCellWidth && xy[1]< timeChartGridHeight )
    {                      
        var currentCell=PixelToCellPoint(xy);

        if(currentCell[0]!=lastCurrentCell[0] || currentCell[1]!=lastCurrentCell[1])
        {                
            // Update the red cursor position
            MoveCursor(lastCurrentCell, currentCell);
            lastCurrentCell = currentCell;        

            // Highlight any cells if we are selecting
            if(bSelectionInProgress==true)
            {                        
                FillCells(startCell, currentCell);                
            }
                
            //document.getElementById( "mouseCoordinates" ).innerHTML="x: "+xy[0]+" / y: "+xy[1];          
        }
    }
    
    //PrintSelectedCells("");
    //document.getElementById( "mouseCoordinates" ).innerHTML="x: "+xy[0]+" / y: "+xy[1];          
}



function MoveCursor(lastCurrentCell, currentCell)
{    

    // Bring back the fill color of the cell where the cursor was previously highlighted
    if(IndexOf(lastCurrentCell)!=-1)
        jg.setColor("#3399cc");
    else 
        jg.setColor("White");
    jg.fillRect(lastCurrentCell[0]*cellWidth+1+daysCellWidth, lastCurrentCell[1]*cellHeight+1, cellWidth-2, cellHeight-2);    // cell margines So as not to have to redraw the lines
    
    // Now show the cell under the new cursor in red
    jg.setColor("Red");
    jg.fillRect(currentCell[0]*cellWidth+1+daysCellWidth, currentCell[1]*cellHeight+1, cellWidth-2, cellHeight-2);    // cell margines So as not to have to redraw the lines
    jg.paint();
}

function FillCells(startCell, endCell)
{
    // If the start cell is already selected, this is going to be an Unfill request    
    var startCol=startCell[0];
    var startRow=startCell[1];
    var endCol=endCell[0];            
    var endRow=endCell[1];
    
    // Allow selection in all 4 directions
    if (startCell[0] > endCell[0])
    {
        startCol = endCell[0];
        endCol = startCell[0];
    }
    
    if(startCell[1] > endCell[1])
    {
        startRow = endCell[1];
        endRow = startCell[1];
    }
    
    
    if(endCol>=startCol && endRow>=startRow)
    {
        var numColumnsBetween=endCol-startCol+1;
        var numRowsBetween=endRow-startRow+1;
        // fill rectangle from start point till this point                                        
        
        // Fill or Unfill
        if(bFill) 
            jg.setColor("#3399cc");
        else 
            jg.setColor("White");
            
        jg.fillRect(startCol*cellWidth+daysCellWidth, startRow*cellHeight, cellWidth*numColumnsBetween, cellHeight*numRowsBetween);
        DrawTimeChartGrid();
        
        var newSelectedCells=new Array();
        //selectedCells=new Array();
        // Add or remove all cells in between the two cells to the selectedCells for bookkeeping
        for(var row=startRow; row<startRow+numRowsBetween; row++)
            for(var col=startCol; col<startCol+numColumnsBetween; col++)
            {
                var cell=[col, row];
                if(row<24 && col < 24)//ben ekledim
                if(bFill)
                {                    
                    // Add the cell to the selected cells                                        
                    if(IndexOf( cell ) == -1)
                        selectedCells.push(cell);                                                                                                            
                }                                       
                else
                { 
                    // Remove the cell from the selected cells
                    var indexOfCell=IndexOf( cell );
                    if(indexOfCell!=-1)
                        selectedCells.remove(indexOfCell);
                }
            }

		//PrintSelectedCells(startCell);        
        jg.paint();              
    }
}

function fillLanguage()
{
	/*document.getElementById("ALLDAY").text = parsedLanguage["EVERYDAY"]
	
	document.getElementById("WEEKDAY").text = parsedLanguage["WEEKDAY"]
	document.getElementById("WEEKEND").text = parsedLanguage["WEEKAND"]
	document.getElementById("WORKINGHOUR").text = parsedLanguage["WORKINGHOUR"]
	document.getElementById("NONWORKINGHOUR").text = parsedLanguage["NONWORKINGHOUR"]
	document.getElementById("MANUEL").text = parsedLanguage["MANUELSLCT"]
	document.getElementById("CALENDER.TITLE").innerHTML = parsedLanguage["WEB.CALENDER.TITLE"]
	document.getElementById("CALENDER.INF").innerHTML = parsedLanguage["WEB.CALENDER.TITLE"]*/
}

//bunu ben yazdim
function onSelectedIndexChange()
{
	var e = document.getElementById("time_selection");
	//var selectedIndex = e.options[e.selectedIndex].value;	
	 bFill=false;
     FillCells([0,0],[23,6]);

	bFill=true;
       
	if(e.selectedIndex == 1) //her taraf
	{
       FillCells([0,0],[23,6]);

	} 
	else if(e.selectedIndex == 2) //hafta ici
	{
    	
		FillCells([0,0],[23,4]);
	}
	else if(e.selectedIndex == 3) //hafta sonu
	{
		FillCells([0,6],[23,6]);
		FillCells([0,5],[23,5]);
	}
	else if(e.selectedIndex == 4) //mesai saatleri
	{
		var splitData = Settings.INJOB ;
		bFill=true;
		loadCalender(splitData);
		
	}
	else if(e.selectedIndex == 5) //mesai saatleri disi
	{ 
		var splitData = Settings.INJOB ;
		bFill=false;
		loadCalenderInverse(splitData);
	}
}


function calendarSaved(page, groupname)
{

	for(var i=0;i<selectedCells.length;++i)
		{
				temp = selectedCells[i][0];
				selectedCells[i][0] = selectedCells[i][1];
				selectedCells[i][1] = temp;
		}

        var json = [];
        
        var data = arrayToStr(selectedCells.sort());

        
        selectedCells = new Array();

        if(newgroupstatus) {
            switch(page) {
                case "mailClock":
                    mailCalenderData = data;
                break;
                case "smsClock":
                    smsCalenderData = data;
                break;
                case "callClock":
                    callCalenderData = data;
                break;
            }
            
            calenderClear();
            $("div#calender").css("display","none");
            $("div#overlay").css("display","none");

        } else {
            switch(page) {
                case "mailClock":
                    json = {
                        groupname: groupname,
                        email_calendar: data
                    };
                break;
                case "smsClock":
                    json = {
                        groupname: groupname,
                        sms_calendar: data
                    };
                break;
                case "callClock":
                    json = {
                        groupname: groupname,
                        dial_calendar: data
                    };
                break;
            }
            if (groupname == "injob")
            {
                Settings.systemInfoData.injob = data;
                Settings.updateInjobHour();
            }
            else
                Settings.updateClock(json,groupname);
        }
}

function saveCalender_callback(http)
{
			if(http.readyState == 4 && http.status == 200)
			{
					if(http.responseText == "Success")
					{
							//alert(parsedLanguage["WEB.UPDATESUCCESS"])
							window.close()
					}
			}
	
}

function numberSorter(a, b) {
  return a - b;
}
function findSorted(stringOfSelectedCells)
{
		retString = ""
		splitted = stringOfSelectedCells.split("!")
		for(var i=0;i<splitted.length;++i)
		{
				innerSplitted = splitted[i].split(",")
				for(var p=0;p<innerSplitted.length;++p)
					innerSplitted[p] = parseInt(innerSplitted[p])
				
				if(splitted.length == i+1) {
                    console.log(innerSplitted.length + " : " + (i+1))
                    innerSplitted = innerSplitted.sort(numberSorter)
                    retString = retString +innerSplitted.join(',');
                } else if(innerSplitted.length > 1)
				{
					innerSplitted = innerSplitted.sort(numberSorter)
					retString = retString +innerSplitted.join(',') + "!";
				}
				else if(innerSplitted.length == 1 )
				{
					if(innerSplitted[0] != NaN)
					{
						retString = retString+ innerSplitted[0] +"!";
					
					}else
						retString = retString +"!";
				}
		
		}

        console.log(retString)
		
	return retString
}

function arrayToStr(selectedCells)
{
		//format soyle olsun
		//....|.....|....|..  => her gun icin bir | koyalim
		// 0,1,2,3,4,5|2,3|||||| => mesela boyle ise aradakiler bos
		var retString = ""
		var startDay = 0,i=0;//arrayin ilk elemani 0 ise pzt, 1 ise sali..
		for(i=0;i<selectedCells.length;++i)
		{
			if(selectedCells[i][0] != startDay)
			{
				if(retString[retString.length-1] == ',')
					retString = retString.substr(0,retString.length -1) //son virgulu sil
				
				
				retString = retString + "!"
				startDay = startDay + 1 
				i = i - 1
			
			}	
			else
				retString = retString + selectedCells[i][1] + "," 
		}
		
		if(retString[retString.length-1] == ',')
			retString = retString.substr(0,retString.length -1)
		
		for(var j=0;j<7;++j)
		if(j>startDay)
			retString = retString + "!"
			
			

	return findSorted(retString)
}



Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};

Array.prototype.find = function(searchStr) {
  var returnArray = false;
  for (i=0; i<this.length; i++) {
    if (typeof(searchStr) == 'function') {
      if (searchStr.test(this[i])) {
        if (!returnArray) { returnArray = [] }
        returnArray.push(i);
      }
    } else {
      if (this[i]===searchStr) {
        if (!returnArray) { returnArray = [] }
        returnArray.push(i);
      }
    }
  }
  return returnArray;
}

function IndexOf(cell)
{    
    /*var result=selectedCells.find(cell);
    if(result!=false)
        return result;*/
    for(var i=0; i<selectedCells.length; i++)
    {
        var currCell=selectedCells[i];
        if(cell[0] == currCell[0] && cell[1] == currCell[1])        
            return i;
    }
            
    return -1;
}

function PixelToCellPoint(xy)
{    
    // return col, row
    return [ integerDivide(xy[0]-daysCellWidth, cellWidth), integerDivide(xy[1], cellHeight) ];
}

function EndSelection(evt, obj)
{
    bSelectionInProgress=false;
}

function MouseExited(evt, obj)
{
    // If the mouse goes beyond the grid limits, end the selection
    xy = getEventOffsetXY( evt, obj );    
    var totalWidth=daysCellWidth+timeChartGridWidth;
    if(xy[0]<= daysCellWidth || xy[0]>= totalWidth || xy[1]> timeChartGridHeight || xy[1]<0)
        EndSelection(evt, obj);
    
}

function MapSelectedCellsToDbRecord()
{
    var dbRecord=new Array(168);
    // Initialize the record
    for(var j=0; j<dbRecord.length; j++)
        dbRecord[j]=0;

    // Mark the selected entries        
    for(var i=0; i<selectedCells.length; i++)
        dbRecord[MapSelectedCellToDbRecordIndex(selectedCells[i])] = 1;
        
    return dbRecord;      
}

function MapSelectedCellToDbRecordIndex(cell)
{
    return 24*cell[1] + cell[0];
}

function PrintSelectedCells(startCell)
{
    var cells="";
    for(var i=0; i<selectedCells.length; i++)
    {
        var row=selectedCells[i];
        cells+="["+row+"]";
    }
    
    cells+="---Start Cell:["+startCell+"]";
        
    document.getElementById( "mouseCoordinates" ).innerHTML=cells;
}

function integerDivide ( numerator, denominator ) 
{
    // In JavaScript, dividing integer values yields a floating point result (unlike in Java, C++, C)
    // To find the integer quotient, reduce the numerator by the remainder first, then divide.
    //var remainder = numerator % denominator;
    var quotient = numerator / denominator;

    // Another possible solution: Convert quotient to an integer by truncating toward 0.
    // Thanks to Frans Janssens for pointing out that the floor function is not correct for negative quotients.
    /****************************************************
    if ( quotient >= 0 )
        quotient = Math.floor( quotient );
    else  // negative
        quotient = Math.ceil( quotient );
     *****************************************************/

    return parseInt(quotient);
}

function getEventOffsetXY( evt, obj )
{

    xy = GetMousePosition(evt);

	/*if ( evt.offsetX != null )
		return [ evt.offsetX , evt.offsetY ];*/
    
    //var obj = evt.target || evt.srcElement;
   	setPageTopLeft( obj );
    //return [ ( evt.clientX - obj.pageLeft ) , ( evt.clientY - obj.pageTop ) ];
    return [ ( xy[0] - obj.pageLeft ) , ( xy[1] - obj.pageTop ) ];    
};

function setPageTopLeft( o )
{
    var top = 0,
    left = 0,
    obj = o;

    while ( o.offsetParent )
     {
         left += o.offsetLeft ;
         top += o.offsetTop ;
         o = o.offsetParent ;
    };

    obj.pageTop = top;
    obj.pageLeft = left;

};

function GetMousePosition(e) 
{	
    var posx = 0;	
    var posy = 0;	
    if (!e) 
        var e = window.event;	
        
    if (e.pageX || e.pageY) 	
    {		
        posx = e.pageX;		
        posy = e.pageY;	                
    }	
    else if (e.clientX || e.clientY) 	
    {		
        posx = e.clientX + document.body.scrollLeft			
        + document.documentElement.scrollLeft;		
        posy = e.clientY + document.body.scrollTop			
        + document.documentElement.scrollTop;	               
    }	
    
    return [ posx, posy ];
}

function loadCalender(http)
{
   // var http = "NaN!NaN!NaN!NaN!NaN!NaN!0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23";
    if(http == "*") {
        bFill=true;
        FillCells([0,0],[23,6]);
    } else {

        splitted = http.split("!");

        for(var i=0;i<7;++i) {       
            if(splitted[i] != "NaN")
            {
                innerSplitted = splitted[i].split(",");
                
                var startVal = parseInt(innerSplitted[0]);
                var previousVal = parseInt(innerSplitted[0]);
                
                for(var j=1;j<innerSplitted.length;++j)
                {
                    if(parseInt(innerSplitted[j]) - previousVal  != 1)
                    {
                        FillCells([startVal,i],[previousVal,i]) ;
                        startVal = parseInt(innerSplitted[j]);
                        previousVal = parseInt(innerSplitted[j]);
                        
                    }
                    else previousVal = parseInt(innerSplitted[j]);
                    
                }
                FillCells([startVal,i],[previousVal,i]);
            }   
        } 
    } 
};

function loadCalenderInverse(http)
{
   // var http = "NaN!NaN!NaN!NaN!NaN!NaN!0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23";
    if(http == "*") {
        bFill=false;
        FillCells([0,0],[23,6]);
    } else {
        bFill=true;
        FillCells([0,0],[23,6]);
        bFill=false;
        splitted = http.split("!");

        for(var i=0;i<7;++i) {       
            if(splitted[i] != "NaN")
            {
                innerSplitted = splitted[i].split(",");
                
                var startVal = parseInt(innerSplitted[0]);
                var previousVal = parseInt(innerSplitted[0]);
                
                for(var j=1;j<innerSplitted.length;++j)
                {
                    if(parseInt(innerSplitted[j]) - previousVal  != 1)
                    {
                        FillCells([startVal,i],[previousVal,i]) ;
                        startVal = parseInt(innerSplitted[j]);
                        previousVal = parseInt(innerSplitted[j]);
                        
                    }
                    else previousVal = parseInt(innerSplitted[j]);
                    
                }
                FillCells([startVal,i],[previousVal,i]);
            }   
        } 
    } 
};