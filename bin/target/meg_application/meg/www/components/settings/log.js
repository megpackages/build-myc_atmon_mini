Log = {};

Settings.callLog = function() {	
	Main.loading();
	$("div#settings div.form").html("");
	var html = 		'<div class="alert"></div>'
				+ 	'<ul class="element" id="logform">'
					+	'<li id="buttons">'
						+	'<input type="submit" id="getJournal">'
						+	'<input type="submit" id="getLog">'
						+	'<input type="submit" id="save">'
					+	'</li>'
					+	'<li id="modules">'
						+	'<div class="log">'
							+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="list" id="logmdlHeader">'
							+ '</table>'
						+	'</div>'
						+	'<div class="log">'
							+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="list" id="logmdl">'
							+ '</table>'
						+	'</div>'
					+	'</li>'
					+	'<li id="info">'
						+	'<div class="blk"><input type="submit" id="start" value=""><b id="start"></b></div>'
						+	'<div class="blk"><input type="submit" id="stop" value=""><b id="stop"></b></div>'
					+	'</li>'
				+	'</ul>';
			
	$("div#settings div.form").html(html);
	$("ul#logform input#getLog").val(language.Settings_log_getlog);
	$("ul#logform input#getJournal").val(language.Settings_log_getjournal);
	$("ul#logform input#save").val(language.Settings_log_savelog);
	$("div#logbuttons input#get").val(language.Settings_log_getlog);
	$("div#logbuttons input#savelog").val(language.savelog);
	$("div#logbuttons input#close").val(language.close);
	$("ul#logform li#info b#stop").html(language.stopped);
	$("ul#logform li#info b#start").html(language.running);
	Log.getAllServices();
};

Log.getAllServices = function()
{
	Main.loading();
	var data = $.getJSON("http://" + hostName + "/cgi-bin/loglvl.cgi?getAllServices");	 
	data.success(function(json, textStatus, jqXHR) {
		Log.Modules = json;
		Log.createLogModulesList();
		Main.unloading();
	});
	data.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Log.createLogModulesList = function()
{	
	console.log(Log.Modules);
	var len = Log.Modules.length;

	var trHead = document.createElement("tr");
	trHead.setAttribute("class", "head");

	var tdName = document.createElement("td");
	tdName.setAttribute("class","head");
	tdName.innerHTML = language.log_mdlname;
	var tdErr = document.createElement("td");
	tdErr.innerHTML = language.log_err;
	var tdExcp = document.createElement("td");
	tdExcp.innerHTML = language.log_excp;
	var tdWarn = document.createElement("td");
	tdWarn.innerHTML = language.log_warn;
	var tdInfo = document.createElement("td");
	tdInfo.innerHTML = language.log_inf;
	var tdDbg = document.createElement("td");
	tdDbg.innerHTML = language.log_dbg;
	var tdEntxt = document.createElement("td");
	tdEntxt.innerHTML = language.log_entext;


	var errCheckInput = document.createElement("input");
	errCheckInput.setAttribute("type", "checkbox");
	errCheckInput.checked = false;
	errCheckInput.setAttribute("checkType", "error");
	tdErr.appendChild(errCheckInput);

	var excpCheckInput = document.createElement("input");
	excpCheckInput.setAttribute("type", "checkbox");
	excpCheckInput.checked = false;
	excpCheckInput.setAttribute("checkType", "exception");
	tdExcp.appendChild(excpCheckInput);

	var warnCheckInput = document.createElement("input");
	warnCheckInput.setAttribute("type", "checkbox");
	warnCheckInput.checked = false;
	warnCheckInput.setAttribute("checkType", "warning");
	tdWarn.appendChild(warnCheckInput);

	var infoCheckInput = document.createElement("input");
	infoCheckInput.setAttribute("type", "checkbox");
	infoCheckInput.checked = false;
	infoCheckInput.setAttribute("checkType", "info");
	tdInfo.appendChild(infoCheckInput);

	var dbgCheckInput = document.createElement("input");
	dbgCheckInput.setAttribute("type", "checkbox");
	dbgCheckInput.checked = false;
	dbgCheckInput.setAttribute("checkType", "debug");
	tdDbg.appendChild(dbgCheckInput);

	var entxtCheckInput = document.createElement("input");
	entxtCheckInput.setAttribute("type", "checkbox");
	entxtCheckInput.checked = false;
	entxtCheckInput.setAttribute("checkType", "entext");
	tdEntxt.appendChild(entxtCheckInput);


	trHead.appendChild(tdName);
	trHead.appendChild(tdErr);
	trHead.appendChild(tdExcp);
	trHead.appendChild(tdWarn);
	trHead.appendChild(tdInfo);
	trHead.appendChild(tdDbg);
	trHead.appendChild(tdEntxt);

	$("table#logmdlHeader").append(trHead);

	for (var i = 0; i < len; i++)
	{
		var mdlname = Log.Modules[i].mdlname;
		var loglvl = Log.Modules[i].loglvl;

		var tr = document.createElement("tr");
		tr.setAttribute("index", i.toString());

		var tdName = document.createElement("td");
		tdName.setAttribute("class", "head");
		tdName.setAttribute("id", ((Log.Modules[i].status == "1") ? "start":"stop"));
		tdName.setAttribute("index", i.toString());

		var inError = document.createElement("input");
		inError.setAttribute("type", "checkbox");
		inError.setAttribute("id", i.toString() + "-error");
		inError.setAttribute("checkType", "error");
		inError.checked = Log.isChecked("error", loglvl);
		var tdError = document.createElement("td");
		tdError.appendChild(inError);

		var inException = document.createElement("input");
		inException.setAttribute("type", "checkbox");
		inException.setAttribute("id", i.toString() + "-exception");
		inException.setAttribute("checkType", "exception");
		inException.checked = Log.isChecked("exception", loglvl);
		var tdExcp = document.createElement("td");
		tdExcp.appendChild(inException);

		var inWarning = document.createElement("input");
		inWarning.setAttribute("type", "checkbox");
		inWarning.setAttribute("id", i.toString() + "-warning");
		inWarning.setAttribute("checkType", "warning");
		inWarning.checked = Log.isChecked("warning", loglvl);
		var tdWarn = document.createElement("td");
		tdWarn.appendChild(inWarning);

		var inInfo = document.createElement("input");
		inInfo.setAttribute("type", "checkbox");
		inInfo.setAttribute("id", i.toString() + "-info");
		inInfo.setAttribute("checkType", "info");
		inInfo.checked = Log.isChecked("info", loglvl);
		var tdInfo = document.createElement("td");
		tdInfo.appendChild(inInfo);

		var inDebug = document.createElement("input");
		inDebug.setAttribute("type", "checkbox");
		inDebug.setAttribute("id", i.toString() + "-debug");
		inDebug.setAttribute("checkType", "debug");
		inDebug.checked = Log.isChecked("debug", loglvl);
		var tdDebug = document.createElement("td");
		tdDebug.appendChild(inDebug);

		var inEntext = document.createElement("input");
		inEntext.setAttribute("type", "checkbox");
		inEntext.setAttribute("id", i.toString() + "-entext");
		inEntext.checked = Log.isChecked("entext", loglvl);
		inEntext.setAttribute("checkType", "entext");
		var tdEntext = document.createElement("td");
		tdEntext.appendChild(inEntext);


		/*var startStop = document.createElement("input");
		startStop.setAttribute("type","submit");
		startStop.setAttribute("id", (Log.Modules[i].status == "1") ? "stop":"start");
		startStop.setAttribute("index", i.toString());
		startStop.setAttribute("value", (Log.Modules[i].status == "1") ? "K":"S");
		tdName.appendChild(startStop);
		*/
		var aName = document.createElement("a");
		aName.innerHTML = mdlname;
		tdName.appendChild(aName);
		tr.setAttribute("id", ((Log.Modules[i].status == "1") ? "start":"stop"));
		tr.appendChild(tdName);
		tr.appendChild(tdError);
		tr.appendChild(tdExcp);
		tr.appendChild(tdWarn);
		tr.appendChild(tdInfo);
		tr.appendChild(tdDebug);
		tr.appendChild(tdEntext);

		$("table#logmdl").append(tr);

	}
};


Log.getLog = function()
{
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/megweb.cgi?logread";
	$.ajax({
	    url : url,
	    type: "GET",
        dataType: 'text',
	    success: function(data, textStatus, jqXHR)
	    {
			$("div.logscreen div#textarea").html(data);
			Main.unloading();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Log.setLoglevel = function(Url)
{
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/" + Url;
	$.ajax({
	    url : url,
	    type: "POST",
	    success: function()
	    {
			Main.unloading();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Log.startStopService = function(index,opt)
{
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/servctrl.cgi?" + opt + "&service=" + Log.Modules[index].mdlname;
	$.ajax({
	    url : url,
	    type: "POST",
	    success: function()
	    {
			Main.unloading();
			$("td#" + (opt == "start" ? "stop": "start") + "[index=\""+ index.toString()+"\"]").attr("id", opt);
			$("input#" + opt + "[index=\""+ index.toString()+"\"]").attr("value", opt == "start" ? "K": "S");
			$("input#" + opt + "[index=\""+ index.toString()+"\"]").attr("id", opt == "start" ? "stop": "start");
			Log.Modules[index].status = (opt == "start" ? 1:0);
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Log.mdlNameToGet = function(mdlname)
{
	var ret = "";
	ret = "loglvl.cgi?getLoglevel&modulename=" + mdlname;

	return ret;
};

Log.mdlNameToSet = function(mdlname)
{
	var ret = "";
	ret = "loglvl.cgi?setLoglevel&modulename=" + mdlname + "&loglevel=";

	return ret;
};

Log.isChecked = function(filter, lvl)
{
	var level = parseInt(lvl);
	var ret = true;

	if (filter == "error")
	{
		ret = (level >> 1)%2 ? true:false;
	}
	else if (filter == "exception")
	{
		ret = (level >> 2)%2 ? true:false;
	}
	else if (filter == "warning")
	{
		ret = (level >> 3)%2 ? true:false;
	}
	else if (filter == "info")
	{
		ret = (level >> 4)%2 ? true:false;
	}
	else if (filter == "debug")
	{
		ret = (level >> 5)%2 ? true:false;
	}
	else if (filter == "entext")
	{
		ret = (level >> 6)%2 ? true:false;
	}
	return ret;
};

Log.calcLoglevel = function(index)
{
	var mdlname = Log.Modules[index].mdlname;
	var error = $("input#" + index.toString() + "-error").prop("checked") ? 1:0;
	var exception = $("input#" + index.toString() + "-exception").prop("checked") ? 1:0;
	var warning = $("input#" + index.toString() + "-warning").prop("checked") ? 1:0;
	var info = $("input#" + index.toString() + "-info").prop("checked") ? 1:0;
	var debug = $("input#" + index.toString() + "-debug").prop("checked") ? 1:0;
	var entext = $("input#" + index.toString() + "-entext").prop("checked") ? 1:0;
	console.log("mdlname:" + mdlname);
	console.log("error:" + error.toString());
	console.log("exception:" + exception.toString());
	console.log("warning:" + warning.toString());
	console.log("info:" + info.toString());
	console.log("debug:" + debug.toString());
	console.log("entext:" + entext.toString());
	var result = (error << 1) + (exception << 2) + (warning << 3) + (info << 4) + (debug << 5) + entext*192 + 1;
	return result.toString();
};

Log.JournalPid2Process = function(processId)
{
	var process;
	switch(parseInt(processId))
	{
		case 0:
			process = "croptrol&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			break;
		case 1:
			process = "notifier&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			break;
		case 2:
			process = "croptrol-wrap";
			break;
		case 3:
			process = "cgi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			break;
		case 4:
			process = "xmlserver&nbsp;&nbsp;&nbsp;&nbsp;";
			break;
		case 5:
			process = "watchdog&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			break;
		case 6:
			process = "smsmngr&nbsp;&nbsp;&nbsp;&nbsp;";
			break;
		default: 
			process = "unknown&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	}
	return process;
};

Log.JournalPriorityColors = ["black", "blue", "orange", "red"];

Log.JournalPriority2Color = function(priority)
{
	var color;
	switch(parseInt(priority))
	{
		case 0:
			color = Log.JournalPriorityColors[0];
			break;
		case 1:
			color = Log.JournalPriorityColors[1];
			break;
		case 2:
			color = Log.JournalPriorityColors[2];
			break;
		case 3:
			color = Log.JournalPriorityColors[3];
			break;
		default:
			color = Log.JournalPriorityColors[0];
	}
	return color;
};

Log.getJournal = function(priority)
{
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/megweb.cgi?getJournals";
	if (priority != undefined)
		url += "&priority=" + priority;
	var journal = $.getJSON(url);
    journal.success(function(json, textStatus, jqXHR)
    {
    	var html = "";
    	for (var i = 0; i < json.length; i++)
    	{
    		var evnt = json[i];
    		var process = Log.JournalPid2Process(evnt.process_id);
    		var color = Log.JournalPriority2Color(evnt.priority);
    		var date = new Date(parseInt(evnt.time) * 1000);
    		html += "<font color='" + color +"'>" + date.toLocaleString('en-GB', {hour12:false}) + "&nbsp;&nbsp;&nbsp;&nbsp;" + process + "&nbsp;&nbsp;&nbsp;&nbsp;" +  evnt["event"] + "</font><br>";
    	}
		$("div.logscreen div#textarea").html(html);
		Main.unloading();
	});
    journal.error(function(jqXHR, textStatus, errorThrown)
    {
    	Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
    });
};

$(function() {
	$("div#settings").on( "click", "ul#logform input#save", function() {
		var list = $("table#logmdl tr");
		for (var i = 0; i < list.length; i++)
		{
			var index = parseInt(list[i].getAttribute("index"));
			if (Log.Modules[index].status == 1)
			{
				var lvl = Log.calcLoglevel(index);
				if (lvl != Log.Modules[index].loglvl)
				{
					Log.setLoglevel(Log.mdlNameToSet(Log.Modules[index].mdlname)+lvl.toString());
					Log.Modules[index].loglvl = lvl;
				}
			}
		}
	});

	$("div#settings").on("click", "ul#logform input#getLog", function() {
			Log.getLog();	
			$("div#logbuttons input#get").val(language.Settings_log_getlog);
			$("div#logbuttons input#get").addClass("log");
			$("div#logbuttons input#get").removeClass("journal");
			$("div#logbuttons input#savelog").val(language.savelog);
			$("div#logbuttons input#savelog").addClass("log");
			$("div#logbuttons input#savelog").removeClass("journal");
			$("div#overlay").css("display","block");
			$("div#logscreen").css("display","block");
		});


	$("div#content").on("click", "div#logscreen input#get.log", function() {	
		Log.getLog();
		});
		
		
	$("div#settings").on("click", "ul#logform input#getJournal", function() {
			Log.getJournal();	
			$("div#logbuttons input#get").val(language.Settings_log_getjournal);
			$("div#logbuttons input#get").removeClass("log");
			$("div#logbuttons input#get").addClass("journal");
			$("div#logbuttons input#savelog").val(language.savejournal);
			$("div#logbuttons input#savelog").removeClass("log");
			$("div#logbuttons input#savelog").addClass("journal");
			$("div#overlay").css("display","block");
			$("div#logscreen").css("display","block");
		});


	$("div#content").on("click", "div#logscreen input#get.journal", function() {	
		Log.getJournal();
		});

	$("div#content").on("click", "div#logscreen input#savelog.log", function() {	
		var blob = new Blob([$("div.logscreen div#textarea").html()], {type: "text/plain;charset=utf-8"});
		saveAs(blob,"SensplorerLog.txt");
		});
		
	$("div#content").on("click", "div#logscreen input#savelog.journal", function() {	
		var blob = new Blob([$("div.logscreen div#textarea").html()], {type: "text/plain;charset=utf-8"});
		saveAs(blob,"SensplorerJournal.txt");
		});

	$("div#content").on("click", "div#logscreen input#close", function() {	
			$("div.logscreen div#textarea").html("");
			$("div#overlay").css("display","none");
			$("div#logscreen").css("display","none");
		});


	$("div#settings").on("click", "div.log td input#start", function() {	
		var index = $(this).attr("index");
		Log.startStopService(parseInt(index), "start");
	});


	$("div#settings").on("click", "div.log td input#stop", function() {	
		var index = $(this).attr("index");
		Log.startStopService(parseInt(index),"stop");
	});

	$("div#settings").on("change", "li#modules table#logmdlHeader input[type='checkbox']", function() {	
		var checkType = $(this).attr("checkType");
		$("li#modules div.log input[type=\"checkbox\"][checkType=\"" + checkType + "\"]").prop("checked", $(this).prop("checked"));
	});


});
