Settings.smsData = {};
Settings.smsList = {};

Settings.callSms = function() {
	$("div#settings div.form").html("");
	var html = '<div class="alert"></div>'
				+ '<div class="alertNaN"></div>'
				+ '<ul class="element" id="smsform" style="display:none;">'
				+	'<li id="webservice"><div class="left"></div><div class="right"><select id="smsList"></select></div></li>'
				+	'<li id="type" style="display: none;"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="addr" style="display: none;">'
					+	'<div class="left"></div>'
					+	'<div class="right">'
						+	'<input type="text" id="serverInput">'
						+	'<p></p>'
					+	'</div>'
				+	'</li>'
				+	'<li id="addrAlert" style="display: none;"><div class="left">&nbsp;</div><div class="right"></div></li>'
				+	'<li id="count" style="display: none;"><div class="left"></div><div class="right"><input type="text" id="countInput"></div></li>'
				+	'<li id="buttons">' 
					+	'<input type="submit" id="save" style="display: none;">'
					+	'<input type="submit" id="smsTest" style="display: inline-block;">'
					+	'<input type="submit" id="dialTest" style="display: inline-block;">'
					+	'<input type="submit" id="updateSms" style="display: inline-block;">'
					+	'<input type="submit" id="new" style="display: inline-block;">'
					+	'<input type="submit" id="remove" style="display: none;">'
					+	'<input type="submit" id="cancelSms" style="display: none;">'
				+	'</li>'
				+ '</ul>'
				+ '<div id="alertDelete" style="width: 400px;" class="panelDiv sms">'
					+ '<span class="text"></span>'
					+ '<div class="buttons">'
					+ '	<input type="submit" id="buttonRemove" name="buttonRemove" class="submit">'
					+ '	<input type="submit" id="buttonClose" name="buttonClose" class="submit">'
					+ '</div>'
				+ '</div>'
				+ '<div id="windowEdit" style="width: 600px;" class="panelDiv">'
					+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="tableUpdate">'
					+ '	<tr>'
					+ '		<td id="modulsensorname" width="40%"></td>'
					+ '		<td id="voltaj" width="60%"></td>'
					+ '	</tr>'
					+ '<tr><td class="alert"></td></tr>'
					+ '</table>'
					+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="tableUpdateList">'
					+ '	<tr>'
					+ '		<td id="number" width="40%"></td>'
					+ '		<td width="60%"><input type=text id="number"></td>'
					+ '	</tr>'
					+ '</table>'
					+ '<div class="buttons">'
					+ '	<input type="submit" id="buttonTest" name="buttonUpdate" class="submit">'
					+ '	<input type="submit" id="buttonCancel" name="buttonCancel" class="submit">'
					+ '</div>'
				+ '</div>';
	$("div#settings div.form").html(html);

	$("li#webservice div.left").html(language.Settings_sms_webservice);
	$("li#count div.left").html(language.Settings_sms_count);
	$("li#buttons input#save").val(language.Settings_xml_server_save);
	$("li#buttons input#new").val(language.Settings_xml_server_new);
	$("li#buttons input#remove").val(language.Settings_xml_server_remove);
	$("li#buttons input#cancelSms").val(language.Settings_xml_server_cancel);
	$("li#buttons input#dialTest").val(language.Settings_dial_test);
	$("li#buttons input#smsTest").val(language.Settings_sms_test);
	$("li#buttons input#updateSms").val(language.update);
	$("div#windowEdit input#buttonTest").val(language.test);
	$("div#windowEdit input#buttonCancel").val(language.cancel);
	$("div#windowEdit td#number").html(language.Settings_users_phone);
	Settings.getSmsList();
};

Settings.sortSmsList = function()
{
	var len = Settings.smsList.length;
	var tmpList = [];
	for (var i = 0; i < len; i++)
	{
		tmpList[parseInt(Settings.smsList[i].id)] = Settings.smsList[i];
	}
	Settings.smsList = tmpList;
};

Settings.getSmsList = function() {
	var data = $.getJSON("http://" + hostName + "/cgi-bin/notifymngrconf.cgi?getSmsSettings");	 
	data.success(function() {
		Settings.smsList = jQuery.parseJSON(data.responseText);
		Settings.sortSmsList();
		Settings.createSmsForm();
		Main.unloading();
	});
	data.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};


Settings.removeSms = function(id) { 
	$("div#overlay").css("display","none");
	$("#content div#settings div.form div#alertDelete").css("display","none");
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/notifymngrconf.cgi?removeSmsSetting&id=" + id;
	$.ajax({
	    url : url,
	    type: "POST",
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			Settings.callSms();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.addSms = function() {
	Main.loading();
	json = JSON.stringify(Settings.smsData);
	var url = "http://" + hostName + "/cgi-bin/notifymngrconf.cgi?addSmsSetting";
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			Main.alert(language.Settings_webservice_add_success);
			Settings.callSms();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.updateSms = function(id) {
	Main.loading();
	json = JSON.stringify(Settings.smsData);
	var url = "http://" + hostName + "/cgi-bin/notifymngrconf.cgi?updateSmsSetting&id=" + id;
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			Main.alert(language.Settings_webservice_update_success);
			Settings.callSms();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.createSmsForm = function() {
	Main.unloading();

	if(Settings.smsList.length == 0) {
		$("li#webservice").css("display","none");
		$("div#settings div.alertNaN").html(language.settings_webservice_not);
	} else {
		$("li#webservice").css("display","block");
		$("div#settings div.alertNaN").html("");
	}

	var item = Settings.smsList;
	var id = item.id;
	var webservice = item.webservice_path;
	var retry_count = item.retry_count;

	var option0 = document.createElement("option");
	option0.setAttribute("value","none");
	option0.innerHTML = language.Settings_sms_select;

	var smsList = $("li#webservice select#smsList");
	smsList.append(option0);

	for(var i = 0; i < Settings.smsList.length; i++) {
		if (Settings.smsList[i] == undefined)
			continue;

		var list = Settings.smsList[i];
		var option = document.createElement("option");
		option.setAttribute("value",list.id);
		switch(list.type)
		{
			case "1":
				option.innerHTML = list.device_port;
				break;
			case "2":
				option.innerHTML = list.canid;
				break;
			case "3":
				option.innerHTML = list.webservice_path;
				break;
		}
		smsList.append(option);
	}

	$("div#settings ul.element").css("display","block");
};

Settings.getSmsType = function(type)
{
	var type;
	switch(type)
	{
	case "0":
		type = language.Settings_sms_type_none;
		break;
	case "1":
		type = language.Settings_sms_type_serial;
		break;
	case "2":
		type = language.Settings_sms_type_can;
		break;
	case "3":
		type = language.Settings_sms_type_webservice;
		break;
	}
	return type;
}

Settings.createGetSmsForm = function(id) {
	var item = Settings.smsList[id];
	var count = item.retry_count;
	var type = item.type;

	$("li#webservice").css("display","none");
	$("li#type").css("display","block");
	$("li#type div.right").html(Settings.getSmsType(type));
	$("li#type div.left").html(language.Settings_sms_type);
	$("li#addr").css("display","block");
	$("li#count").css("display","block");
	$("li#buttons input#new").css("display","none");
	$("li#buttons input#updateSms").css("display","none");
	$("li#buttons input#smsTest").css("display","none");
	$("li#buttons input#dialTest").css("display","none");
	$("li#buttons input#save").css("display","inline-block");
	$("li#buttons input#cancelSms").css("display","inline-block");
	$("li#buttons input#remove").css("display","inline-block");

	$("li#addr p").css("display", "none");
	$("li#addr input").css("display", "none");

	switch(type)
	{
	case "1":	
		$("li#addr p").css("display", "block");
		$("li#addr p").html(item.device_port);
		$("li#addr div.left").html(language.Settings_device_addr);
		break;
	case "2":
		$("li#addr p").css("display", "block");
		$("li#addr p").html(item.canid);
		$("li#addr div.left").html(language.Settings_can_addr);
		break;
	case "3":
		$("li#addr input").css("display", "block");
		$("li#addr input").val(item.webservice_path);
		$("li#addr div.left").html(language.Settings_webservice_addr);
		$("li#addrAlert").css("display","block");
		$("li#addrAlert div.right").html(language.Settings_sms_addr_alert);
		break;
	}
	$("li#count input").val(count);
};

Settings.newSmsForm = function() {
	$("li#webservice").css("display","none");
	$("li#addr").css("display","block");
	$("li#addrAlert").css("display","block");
	$("li#count").css("display","block");

	$("li#addr p").css("display", "none");
	$("li#addr input").css("display","block");
	$("li#addr div.left").html(language.Settings_webservice_addr);
		$("li#addrAlert div.right").html(language.Settings_sms_addr_alert);
	$("li#buttons input#new").css("display","none");
	$("li#buttons input#updateSms").css("display","none");
	$("li#buttons input#smsTest").css("display","none");
	$("li#buttons input#dialTest").css("display","none");
	$("li#buttons input#save").css("display","inline-block");
	$("li#buttons input#cancelSms").css("display","inline-block");
	$("li#buttons input#remove").css("display","none");

	$("li#addr input").val("");
	$("li#count input").val("");
};

Settings.validateSmsForm = function(id) {	
	$("div#settings div.alert").html("");
	var alertHtml = $("div#settings div.alert").html();
	$("li#addr").removeClass("alert");
	$("li#count").removeClass("alert");

	var addr = $("li#addr input").val();
	var count = $("li#count input").val();

	var iChars = "ğĞüÜşŞıİçÇöÖ";
	var validateaddr = /^((\bGET\=\b)|(\bPOST\=\b))?http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
	var value = true;
	var type = "3";
	if (id != 0)
	{
		type = Settings.smsList[id].type;
	}
	if (type == "3")
	{
		if(addr.length == 0) {
		    $("li#addr").addClass("alert");
		    alertHtml = alertHtml + "<br />" + language.Settings_error_noempty;
			$("div#settings div.alert").html(alertHtml);
		    value = false;
		} else if(!validateaddr.test(addr)) {
			$("li#addr").addClass("alert");
			alertHtml = alertHtml + "<br />" + language.Settings_sms_error_addr;
			$("div#settings div.alert").html(alertHtml);			
			value = false;
		}
	}

	if(count.length == 0) {
	    $("li#count").addClass("alert");
	    alertHtml = alertHtml + "<br />" + language.Settings_error_noempty;
		$("div#settings div.alert").html(alertHtml);
	    value = false;
	} else if(!isFinite(count)) {
		$("li#count").addClass("alert");
		alertHtml = alertHtml + "<br />" + language.Settings_xml_error_integer;
		$("div#settings div.alert").html(alertHtml);			
		value = false;
	}

	if(value)
	{
		switch(type)
		{
		case "1":
		case "2":
		Settings.smsData = {
			retry_count: count
		};
		break;
		case "3":
		Settings.smsData = {
			type : "3",
			webservice_path: addr,
			retry_count: count
		};
		break;
		}
	}

	return value;
};

Settings.clearSmsForm = function() {
	
	if(Settings.smsList.length == 0) {
		$("li#webservice").css("display","none");
		$("div#settings div.alertNaN").html(language.settings_webservice_not);
	} else {
		$("li#webservice").css("display","block");
		$("div#settings div.alertNaN").html("");
	}

	$("li#webservice option:first").prop("selected", true);
	$("li#addr").css("display","none");
	$("li#type").css("display","none");
	$("li#addrAlert").css("display","none");
	$("li#count").css("display","none");

	$("li#buttons input#new").css("display","inline-block");
	$("li#buttons input#updateSms").css("display","inline-block");
	$("li#buttons input#smsTest").css("display","inline-block");
	if (type != "3")
		$("li#buttons input#dialTest").css("display","inline-block");
	else
		$("li#buttons input#dialTest").css("display","none");
	$("li#buttons input#save").css("display","none");
	$("li#buttons input#cancelSms").css("display","none");
	$("li#buttons input#remove").css("display","none");

	$("li#addr input").val("");
	$("li#count input").val("");
};


Settings.testSms = function(id, destination)
{
	Main.loading();
	json = JSON.stringify(Settings.newSnmpServer);
	var url = "http://" + hostName + "/cgi-bin/notifymngrconf.cgi?testSms&id=" + id + "&destination=" + destination;
	$.ajax({
	    url : url,
	    type: "POST",
        dataType: 'text',
	    success: function(data, textStatus, jqXHR)
	    {
	    	json = jQuery.parseJSON(data);
			Main.unloading();
			if (json["return"] == "true")
	    		Main.alert(language.Settings_sms_test_success);
	    	else
	    		Main.alert(language.Settings_sms_test_fail + ":" + json["return"]);

		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
}


Settings.testDial = function(id, destination)
{
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/notifymngrconf.cgi?testDial&id=" + id + "&destination=" + destination;
	$.ajax({
	    url : url,
	    type: "POST",
        dataType: 'text',
	    success: function(data, textStatus, jqXHR)
	    {
	    	json = jQuery.parseJSON(data);
			Main.unloading();
			if (json["return"] == "true")
	    		Main.alert(language.Settings_dial_test_success);
	    	else
	    		Main.alert(language.Settings_dial_test_fail + ":" + json["return"]);

		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
}

$(function() {
	$("div#settings").on( "click", "ul#smsform input#save", function() {
		var page = $("li#buttons input#save").attr("data-page");
		
		switch(page)
		{
			case "new":
			if(Settings.validateSmsForm(0))
			{
				Settings.addSms();
			}
			break;
			case "update":
			var id = $( "ul#smsform select#smsList option:selected" ).val();
			if(Settings.validateSmsForm(id))
			{
				Settings.updateSms(id);
			}
			break;
		};
	});

	$("div#settings").on( "click", "ul#smsform input#new", function() {
		$("div#settings div.alertNaN").html("");
		Settings.newSmsForm();
		$("li#buttons input#save").attr("data-page","new");
	});

	$("div#settings").on( "click", "ul#smsform input#remove", function() {
		var id = $( "ul#smsform select#smsList option:selected" ).val();
		$("div#overlay").css("display","block");
		$("#content div#settings div.form div#alertDelete").css("display","block");
		$("#content div#settings div.form div#alertDelete span.text").html(language.Settings_remove_webservice);
		$("#content div#settings div.form div#alertDelete input#buttonRemove").val(language.remove);
		$("#content div#settings div.form div#alertDelete input#buttonRemove").attr("data-id",id);
		$("#content div#settings div.form div#alertDelete input#buttonClose").val(language.cancel);
	});

	$("div#settings").on( "click", "div.sms input#buttonRemove", function() {
		var id = $( this ).attr("data-id");
		Settings.removeSms(id);
	});

	$("div#settings").on( "click", "div.sms input#buttonClose", function() {
		$("div#overlay").css("display","none");
		$("#content div#settings div.form div#alertDelete").css("display","none");
	});

	$("div#settings").on( "click", "ul#smsform input#cancelSms", function() {
		Settings.clearSmsForm();
	});

	$("div#settings").on( "change", "ul#smsform select#smsList", function() {
		var id = $( "ul#smsform select#smsList option:selected" ).val();
		if (Settings.smsList[id].type != "3")
			$("li#buttons input#dialTest").css("display","inline-block");
		else
			$("li#buttons input#dialTest").css("display","none");	
	});


$("div#settings").on( "click", "ul#smsform input#updateSms", function() {
		var id = $( "ul#smsform select#smsList option:selected" ).val();
		$("li#buttons input#save").attr("data-page","update");
		if (id == "none")
			Main.alert(language.Settings_select_sms);
		Settings.createGetSmsForm(id);
	});



$("div#settings").on( "click", "ul#smsform input#smsTest", function() {
	$("div#windowEdit td.alert").html("");
	var id = $( "ul#smsform select#smsList option:selected" ).val();
	if (id == "none")
	{
		Main.alert(language.Settings_select_sms);
		return;
	}
	Settings.testType = "sms";
	$("div#overlay").css("display","block");
	$("div#windowEdit").css("display","block");
	$("div#windowEdit td#modulsensorname").html(language.Settings_sms_test + " » ");
	$("div#windowEdit td#voltaj").html($("ul#smsform select#smsList option:selected").html());
	});


$("div#settings").on( "click", "ul#smsform input#dialTest", function() {
	$("div#windowEdit td.alert").html("");
	var id = $( "ul#smsform select#smsList option:selected" ).val();
	if (id == "none")
	{
		Main.alert(language.Settings_select_sms);
		return;
	}
	Settings.testType = "dial";
	$("div#overlay").css("display","block");
	$("div#windowEdit").css("display","block");
	$("div#windowEdit td#modulsensorname").html(language.Settings_dial_test + " » ");
	$("div#windowEdit td#voltaj").html($("ul#smsform select#smsList option:selected").html());
	});


$("div#settings").on( "click", "div#windowEdit input#buttonTest", function() {
	$("div#windowEdit td.alert").html("");
	var id = $( "ul#smsform select#smsList option:selected" ).val();
	$("li#buttons input#save").attr("data-page","update");
	if (id == "none")
		Main.alert(language.Settings_select_sms);
	var rgxPhone = /^\d{12}$/;
	if (!rgxPhone.test($("div#windowEdit input#number").val()))
	{
		$("div#windowEdit td.alert").html(language.Settings_users_alert_phone);
		return;
	}
	
	$("div#overlay").css("display","none");
	$("div#windowEdit").css("display","none");
	if (Settings.testType == "dial")
		Settings.testDial(Settings.smsList[id].id, $("div#windowEdit input#number").val());
	else
		Settings.testSms(Settings.smsList[id].id, $("div#windowEdit input#number").val());
});

$("div#settings").on( "click", "div#windowEdit input#buttonCancel", function() {
	$("div#overlay").css("display","none");
	$("div#windowEdit").css("display","none");
});
});