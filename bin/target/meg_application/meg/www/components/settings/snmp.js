
Settings.snmpData = {};

Settings.callSnmpServers = function()
{
	$("div#settings div.form").html("");
	var html = '<div class="alert"></div>'
				+ '<div class="alertNaN"></div>'
				+ '<ul class="element" id="snmpserverform" style="display:none;">'
				+	'<li id="server-select" style="display: none;">'
					+	'<div class="left"></div>'
					+	'<div class="right">'
							+	'<select id="serverList"></select>'
					+	'</div>'
				+	'</li>'				
				+	'<li id="version" style="display: none;">'
					+	'<div class="left"></div>'
					+	'<div class="right">'
						+	'<select id="snmp_version">'
							+	'<option id="v1"></option>'
							+	'<option id="v2c"></option>'
							+	'<option id="v3"></option>'
						+	'</select>'
			    		+	'<p></p>'
					+	'</div>'
				+	'</li>'
				+	'<li id="trap_enabled" style="display: none;">'
					+	'<div class="left"></div>'
					+	'<div class="right">'
			    		+	'<input type="checkbox">'
			        +	'</div>'
				+   '</li>'
				+	'<li id="server_ip" style="display: none;">'
					+	'<div class="left"></div>'
					+	'<div class="right">'
						+	'<input type="text">'
					+	'</div>'
				+	'</li>'
				+   '</li>'
				+	'<li id="server_port" style="display: none;">'
					+	'<div class="left"></div>'
					+	'<div class="right">'
						+	'<input type="text">'
					+	'</div>'
				+	'</li>'
				+   '</li>'
				+	'<li id="community_name" style="display: none;">'
					+	'<div class="left"></div>'
					+	'<div class="right">'
						+	'<input type="text">'
					+	'</div>'
				+	'</li>'
				+   '</li>'
				+	'<li id="security_name" style="display: none;">'
					+	'<div class="left"></div>'
					+	'<div class="right">'
						+	'<input type="text">'
					+	'</div>'
				+	'</li>'
				+   '</li>'
				+	'<li id="password" style="display: none;">'
					+	'<div class="left"></div>'
					+	'<div class="right">'
						+	'<input type="password">'
					+	'</div>'
				+	'</li>'
				+	'<li id="buttons">'
					+ '	<input type="submit" id="snmpTest" name="snmpTest" class="submit "style="display:none;">'
					+ '	<input type="submit" id="snmpUpdate" name="snmpUpdate" class="submit" "style="display:none;">'
					+ '	<input type="submit" id="buttonNew" name="buttonNew" class="submit "style="display:none;">'
					+ '	<input type="submit" id="buttonSave" name="buttonSave" class="submit" "style="display:none;">'
					+ '	<input type="submit" id="buttonRemove" name="buttonRemove" class="submit" "style="display:none;">'
					+ '	<input type="submit" id="buttonCancel" name="buttonCancel" class="submit" "style="display:none;">'
				+   '</li>'
				+ '</ul>'
				+ '<div id="alertDelete" style="width: 400px;" class="panelDiv panelDivSnmp">'
					+ '<span class="text"></span>'
					+ '<div class="buttons">'
					+ '	<input type="submit" id="buttonRemove" name="buttonRemove" class="submit">'
					+ '	<input type="submit" id="buttonClose" name="buttonClose" class="submit">'
					+ '</div>'
				+ '</div>';
	$("div#settings div.form").html(html);

	$("div#settings div.form ul#snmpserverform li#server-select div.left").html(language.Settings_server_snmp_list);
	$("div#settings div.form ul#snmpserverform li#version div.left").html(language.Settings_server_snmp_version);
	$("div#settings div.form ul#snmpserverform li#version div.right select option#v1").html(language.snmpv1);
	$("div#settings div.form ul#snmpserverform li#version div.right select option#v2c").html(language.snmpv2c);
	$("div#settings div.form ul#snmpserverform li#version div.right select option#v3").html(language.snmpv3);
	$("div#settings div.form ul#snmpserverform li#server_ip div.left").html(language.Settings_server_snmp_addr);
	$("div#settings div.form ul#snmpserverform li#server_port div.left").html(language.Settings_server_snmp_port);
	$("div#settings div.form ul#snmpserverform li#trap_enabled div.left").html(language.Settings_server_snmp_trap_enabled);
	$("div#settings div.form ul#snmpserverform li#community_name div.left").html(language.Settings_server_snmp_community_name);
	$("div#settings div.form ul#snmpserverform li#security_name div.left").html(language.Settings_server_snmp_security_name);
	$("div#settings div.form ul#snmpserverform li#password div.left").html(language.Settings_server_snmp_password);

	$("div#settings div.form ul#snmpserverform input#buttonNew").attr("value",language.Settings_xml_server_new);
	$("div#settings div.form ul#snmpserverform input#snmpTest").attr("value",language.Settings_xml_server_test);
	$("div#settings div.form ul#snmpserverform input#snmpUpdate").attr("value",language.Settings_xml_server_update);
	$("div#settings div.form ul#snmpserverform input#buttonSave").attr("value",language.Settings_xml_server_save);
	$("div#settings div.form ul#snmpserverform input#buttonRemove").attr("value",language.Settings_xml_server_remove);
	$("div#settings div.form ul#snmpserverform input#buttonCancel").attr("value",language.Settings_xml_server_cancel);
	
	Settings.getSnmpServerList();
};

Settings.getSnmpServerList = function()
{
	Main.loading();
	var data = $.getJSON("http://" + hostName + "/cgi-bin/networksettings.cgi?getSnmpServers");	 
	data.success(function() {
		Settings.SnmpServerList = jQuery.parseJSON(data.responseText);
		Settings.createSnmpServerForm("list");
		Main.unloading();
	});
	data.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Settings.activateSnmpElements = function(type)
{
	if (type == "list")
	{
		if (Settings.snmp_server_list_empty)
		{
			$("div#settings div.form ul#snmpserverform input#snmpTest").css("display", "none");
			$("div#settings div.form ul#snmpserverform input#snmpUpdate").css("display", "none");
		}
		else
		{
			$("ul#snmpserverform select#serverList").prop("selectedIndex","0");
			$("div#settings div.form ul#snmpserverform input#snmpTest").css("display", "inline-block");
			$("div#settings div.form ul#snmpserverform input#snmpUpdate").css("display", "inline-block");
		}
		$("div#settings div.form ul#snmpserverform input#buttonNew").css("display", "inline-block");
		$("div#settings div.form ul#snmpserverform input#buttonSave").css("display", "none");
		$("div#settings div.form ul#snmpserverform input#buttonRemove").css("display", "none");
		$("div#settings div.form ul#snmpserverform input#buttonCancel").css("display", "none");
		$("div#settings div.form ul#snmpserverform li#server-select").css("display", Settings.snmp_server_list_empty ? "none":"block");
		$("div#settings div.form ul#snmpserverform li#version").css("display", "none");
		$("div#settings div.form ul#snmpserverform li#trap_enabled").css("display", "none");
		$("div#settings div.form ul#snmpserverform li#server_ip").css("display", "none");
		$("div#settings div.form ul#snmpserverform li#server_port").css("display", "none");
		$("div#settings div.form ul#snmpserverform li#community_name").css("display", "none");
		$("div#settings div.form ul#snmpserverform li#security_name").css("display", "none");
		$("div#settings div.form ul#snmpserverform li#password").css("display", "none");
	}
	else if (type == "update")
	{
		$("div#settings div.form ul#snmpserverform input#buttonNew").css("display", "none");
		$("div#settings div.form ul#snmpserverform input#snmpTest").css("display", "none");
		$("div#settings div.form ul#snmpserverform input#snmpUpdate").css("display", "none");
		$("div#settings div.form ul#snmpserverform input#buttonSave").css("display", "inline-block");
		$("div#settings div.form ul#snmpserverform input#buttonRemove").css("display", "inline-block");
		$("div#settings div.form ul#snmpserverform input#buttonCancel").css("display", "inline-block");
		$("div#settings div.form ul#snmpserverform li#server-select").css("display", "none");

		$("div#settings div.form ul#snmpserverform li#version select").prop("disabled",true);
		$("div#settings div.form ul#snmpserverform li#version").css("display", "inline-block");
		$("div#settings div.form ul#snmpserverform li#version select").css("display", "none");
		$("div#settings div.form ul#snmpserverform li#version p").css("display", "inline-block");
		$("div#settings div.form ul#snmpserverform li#trap_enabled").css("display", "inline-block");
		$("div#settings div.form ul#snmpserverform li#server_ip").css("display", "inline-block");
		$("div#settings div.form ul#snmpserverform li#server_port").css("display", "inline-block");
		$("div#settings div.form ul#snmpserverform li#community_name").css("display", "none");
		$("div#settings div.form ul#snmpserverform li#security_name").css("display", "none");
		$("div#settings div.form ul#snmpserverform li#password").css("display", "none");
		var version = ($("div#settings div.form ul#snmpserverform li#version div.right select#snmp_version").prop("selectedIndex")).toString();
		if (version == "0")
		{
			$("div#settings div.form ul#snmpserverform li#version p").html(language.snmpv1);
			$("div#settings div.form ul#snmpserverform li#community_name").css("display", "inline-block");
		}
		else if (version == "1")
		{
			$("div#settings div.form ul#snmpserverform li#version p").html(language.snmpv2c);
			$("div#settings div.form ul#snmpserverform li#community_name").css("display", "inline-block");
		}
		else if (version == "2")
		{
			$("div#settings div.form ul#snmpserverform li#security_name").css("display", "inline-block");
			$("div#settings div.form ul#snmpserverform li#password").css("display", "inline-block");
			$("div#settings div.form ul#snmpserverform li#version p").html(language.snmpv3);
		}
	}
	else if (type == "new")
	{

		$("div#settings div.form ul#snmpserverform input#buttonNew").css("display", "none");
		$("div#settings div.form ul#snmpserverform input#snmpTest").css("display", "none");
		$("div#settings div.form ul#snmpserverform input#snmpUpdate").css("display", "none");
		$("div#settings div.form ul#snmpserverform input#buttonSave").css("display", "inline-block");
		$("div#settings div.form ul#snmpserverform input#buttonRemove").css("display", "none");
		$("div#settings div.form ul#snmpserverform input#buttonCancel").css("display", "inline-block");
		$("div#settings div.form ul#snmpserverform li#server-select").css("display", "none");
		if ($("div#settings div.form ul#snmpserverform li#version select").attr("disabled") != undefined)
			$("div#settings div.form ul#snmpserverform li#version select").removeAttr("disabled");
		$("div#settings div.form ul#snmpserverform li#version").css("display", "inline-block");
		$("div#settings div.form ul#snmpserverform li#version select").css("display", "inline-block");
		$("div#settings div.form ul#snmpserverform li#version p").css("display", "none");
		$("div#settings div.form ul#snmpserverform li#trap_enabled").css("display", "inline-block");
		$("div#settings div.form ul#snmpserverform li#server_ip").css("display", "inline-block");
		$("div#settings div.form ul#snmpserverform li#server_port").css("display", "inline-block");
		$("div#settings div.form ul#snmpserverform li#community_name").css("display", "inline-block");
		$("div#settings div.form ul#snmpserverform li#security_name").css("display", "none");
		$("div#settings div.form ul#snmpserverform li#password").css("display", "none");

		$("div#settings div.form ul#snmpserverform li#version select").prop("selectedIndex","0");
		$("div#settings div.form ul#snmpserverform li#trap_enabled input").prop("checked",false);
		$("div#settings div.form ul#snmpserverform li#server_ip input").val("");
		$("div#settings div.form ul#snmpserverform li#server_port input").val("");
		$("div#settings div.form ul#snmpserverform li#community_name input").val("");
		$("div#settings div.form ul#snmpserverform li#security_name input").val("");
		$("div#settings div.form ul#snmpserverform li#password input").val("");
	}
};

Settings.createSnmpServerList = function()
{
	$("div#settings div.form ul#snmpserverform li#server-select select#serverList option").empty();
	var opt0 = document.createElement("option");
	opt0.setAttribute("id", "none");
	opt0.innerHTML = language.Settings_server_snmp_select;
	$("div#settings div.form ul#snmpserverform li#server-select select#serverList").append(opt0);
	var len = Settings.SnmpServerList.length;
	if (len == 0)
		return true;

	for (var i = 0; i < len; i++)
	{
		Settings.insertToSnmpServerList(Settings.SnmpServerList[i], i, true);
	}
	Settings.activateSnmpElements("list");
	return false;
};

Settings.insertToSnmpServerList = function(item, pos,insert)
{
	Settings.SnmpServerList[pos] = item;
	if (insert)
	{
		var opt = document.createElement("option");
		opt.setAttribute("id", pos.toString());
		opt.innerHTML = item.server_ip + ":" + item.server_port;
		$("div#settings div.form ul#snmpserverform li#server-select select#serverList").append(opt);
	}
	else
	{
		var html =  item.server_ip + ":" + item.server_port;
		$("div#settings div.form ul#snmpserverform li#server-select select#serverList option:selected").html(html);
	}
};

Settings.removeFromSnmpServerList = function()
{
	var opt = $("div#settings div.form ul#snmpserverform li#server-select select#serverList option:selected");
	var index = parseInt(opt.attr("id")); 
	Settings.SnmpServerList[index] = undefined;
	opt.remove();
};

Settings.createSnmpServer = function(index)
{
	var item = Settings.SnmpServerList[parseInt(index)];
	if (item == undefined)
		return;
	$("div#settings div.form ul#snmpserverform li#version div.right select#snmp_version").prop("selectedIndex", item.version);
	$("div#settings div.form ul#snmpserverform li#trap_enabled div.right input").prop("checked",item.trap_enabled == "1");
	$("div#settings div.form ul#snmpserverform li#server_ip div.right input").val(item.server_ip);
	$("div#settings div.form ul#snmpserverform li#server_port div.right input").val(item.server_port);
	if (item.version == "0" || item.version == "1")
	{
		$("div#settings div.form ul#snmpserverform li#community_name div.right input").val(item.community_name);
	}
	else if (item.version == "2")
	{
		$("div#settings div.form ul#snmpserverform li#security_name div.right input").val(item.security_name);
		$("div#settings div.form ul#snmpserverform li#password div.right input").val(item.password);
	}	
};

Settings.createSnmpServerForm = function(type, index)
{
	$("div#settings div.form ul#snmpserverform").css("display", "block");

	if (type == "list")
	{
		Settings.snmp_server_list_empty = Settings.createSnmpServerList();
		if (Settings.snmp_server_list_empty)
		{
			$("div#settings div.form div.alertNaN").html(language.settings_snmp_server_not);
		}
		else
		{
			$("div#settings div.form div.alertNaN").html("");
		}
		Settings.snmp_server_page = "existing";
	}
	else if (type == "update")
	{
		$("div#settings div.form div.alertNaN").html("");
		Settings.createSnmpServer(index);
	}
	else if (type == "new")
	{
		$("div#settings div.form div.alertNaN").html("");
	}
	Settings.activateSnmpElements(type);
};

Settings.validateSnmpServerForm = function(type)
{
	var version     	= ($("div#settings div.form ul#snmpserverform li#version select#snmp_version").prop("selectedIndex")).toString();
	var trap_enabled 	= ($("div#settings div.form ul#snmpserverform li#trap_enabled input").prop("checked")) ? "1":"0";
	var server_ip    	= $("div#settings div.form ul#snmpserverform li#server_ip input").val();
	var server_port    	= $("div#settings div.form ul#snmpserverform li#server_port input").val();
	var community_name 	= $("div#settings div.form ul#snmpserverform li#community_name input").val();
	var security_name 	= $("div#settings div.form ul#snmpserverform li#security_name input").val();
	var password 		= $("div#settings div.form ul#snmpserverform li#password input").val();


	$("div#settings div.alert").html("");
	var alertHtml = $("div#settings div.alert").html();
	$("div#settings div.form ul#snmpserverform li#server_ip").removeClass("alert");
	$("div#settings div.form ul#snmpserverform li#server_port").removeClass("alert");
	$("div#settings div.form ul#snmpserverform li#community_name").removeClass("alert");
	$("div#settings div.form ul#snmpserverform li#security_name").removeClass("alert");
	$("div#settings div.form ul#snmpserverform").removeClass("alert");


	var isvalid = true;
	
	//validation of server_ip
	if(!Validator.validate( { "val" : server_ip, "type" : "domain", "ismandatory" : "true", "obj" : $("li#server_ip"), "objfocus" : $("li#server_ip input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	
	//validation of server_port
	if(!Validator.validate( { "val" : server_port, "type" : "port", "ismandatory" : "true", "obj" : $("li#server_port"), "objfocus" : $("li#server_port input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	
	if (version == "0" || version == "1")
	{
		//validation of community_name
		if(!Validator.validate( { "val" : community_name, "type" : "snmpcommunity", "ismandatory" : "true", "obj" : $("li#community_name"), "objfocus" : $("li#community_name input"), "page" : $("div#settings div.alert")}))
			isvalid = false;
	}
	else if (version == "2")
	{
		//validation of security_name
		if(!Validator.validate( { "val" : security_name, "type" : "snmpsecurity", "ismandatory" : "true", "obj" : $("li#security_name"), "objfocus" : $("li#security_name input"), "page" : $("div#settings div.alert")}))
			isvalid = false;
		
		var ismandatory = false;
		if(password.length == 0 && type == "new")
			ismandatory = true;
		//validation of password
		if(!Validator.validate( { "val" : password, "type" : "snmppassword", "ismandatory" : ismandatory, "obj" : $("li#password"), "objfocus" : $("li#password input"), "page" : $("div#settings div.alert")}))
			isvalid = false;
	
	}
	
	return isvalid;
};

Settings.getNewSnmpServerInfo = function()
{
	var version = ($("div#settings div.form ul#snmpserverform li#version select#snmp_version").prop("selectedIndex")).toString();
	var trap_enabled = ($("div#settings div.form ul#snmpserverform li#trap_enabled input").prop("checked")) ? "1":"0";
	var server_ip = $("div#settings div.form ul#snmpserverform li#server_ip input").val();
	var server_port = $("div#settings div.form ul#snmpserverform li#server_port input").val();
	var community_name = $("div#settings div.form ul#snmpserverform li#community_name input").val();
	var security_name = $("div#settings div.form ul#snmpserverform li#security_name input").val();
	var password = $("div#settings div.form ul#snmpserverform li#password input").val();

	var value = Settings.validateSnmpServerForm("new");

	if (value)
	{
		if (version == "0" || version == "1")
		{
			Settings.newSnmpServer = {
				"version": version,
				"trap_enabled": trap_enabled,
				"server_ip": server_ip,
				"server_port": server_port,
				"community_name": community_name
			};
			return true;
		}
		else if (version == "2")
		{
			Settings.newSnmpServer = {
				"version": version,
				"trap_enabled": trap_enabled,
				"server_ip": server_ip,
				"server_port": server_port,
				"security_name": security_name,
				"password": password
			};
			return true;
		}
		else
		{
			return false;
		}
	}
	else
		return false;
};

Settings.getUpdateSnmpServerInfo = function()
{
	var version = ($("div#settings div.form ul#snmpserverform li#version select#snmp_version").prop("selectedIndex")).toString();
	var trap_enabled = ($("div#settings div.form ul#snmpserverform li#trap_enabled input").prop("checked")) ? "1":"0";
	var server_ip = $("div#settings div.form ul#snmpserverform li#server_ip input").val();
	var server_port = $("div#settings div.form ul#snmpserverform li#server_port input").val();
	var community_name = $("div#settings div.form ul#snmpserverform li#community_name input").val();
	var security_name = $("div#settings div.form ul#snmpserverform li#security_name input").val();
	var password = $("div#settings div.form ul#snmpserverform li#password input").val();

	var value = Settings.validateSnmpServerForm("update");

	if (value)
	{
		if (version == "0" || version == "1")
		{
			Settings.updateSnmpServerInfo = {
				"version": version,
				"trap_enabled": trap_enabled,
				"server_ip": server_ip,
				"server_port": server_port,
				"community_name": community_name
			};
			return true;
		}
		else if (version == "2")
		{
			if (password.length == 0)
			{
				Settings.updateSnmpServerInfo = {
					"version": version,
					"trap_enabled": trap_enabled,
					"server_ip": server_ip,
					"server_port": server_port,
					"security_name": security_name
				};
			}
			else
				Settings.updateSnmpServerInfo = {
					"version": version,
					"trap_enabled": trap_enabled,
					"server_ip": server_ip,
					"server_port": server_port,
					"security_name": security_name,
					"password": password
				};
			return true;
		}
		else
		{
			return false;
		}
	}
	else
		return false;
};

Settings.addSnmpServer = function()
{
	Main.loading();
	json = JSON.stringify(Settings.newSnmpServer);
	var url = "http://" + hostName + "/cgi-bin/networksettings.cgi?addSnmpServer";
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
	    	json = jQuery.parseJSON(json);
	    	var id = jQuery.parseJSON(data).id;
	    	json["id"]= id;
			Main.unloading();
			Main.alert(language.Settings_server_add_success);
			Settings.insertToSnmpServerList(json, Settings.SnmpServerList.length, true);
			$("div#settings div.form div.alertNaN").html("");
			$("ul#snmpserverform select#serverList").prop("selectedIndex","0");
			var len = Settings.SnmpServerList.length;
			var i;
			for (i = 0; i < len; i++)
				if (Settings.SnmpServerList[i] != undefined)
					break;
			if (i == len || len == 0)
			{
				$("div#settings div.form div.alertNaN").html(language.settings_snmp_server_not);
				Settings.snmp_server_list_empty = true;
			}
			else
			{
				$("div#settings div.form div.alertNaN").html("");
				Settings.snmp_server_list_empty = false;
			}
			Settings.activateSnmpElements("list");
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.updateSnmpServer = function()
{
	Main.loading();
	json = JSON.stringify(Settings.updateSnmpServerInfo);
	var opt = $("div#settings div.form ul#snmpserverform li#server-select select#serverList option:selected");
	var index = parseInt(opt.attr("id")); 
	var url = "http://" + hostName + "/cgi-bin/networksettings.cgi?updateSnmpServer&id=" + Settings.SnmpServerList[index].id;
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			Main.alert(language.Settings_server_update_success);
			var item = jQuery.parseJSON(json);
			Settings.insertToSnmpServerList(item, index,false);
			$("div#settings div.form div.alertNaN").html("");
			$("ul#snmpserverform select#serverList").prop("selectedIndex","0");
			var len = Settings.SnmpServerList.length;
			var i;
			for (i = 0; i < len; i++)
				if (Settings.SnmpServerList[i] != undefined)
					break;
			if (i == len || len == 0)
			{
				$("div#settings div.form div.alertNaN").html(language.settings_snmp_server_not);
				Settings.snmp_server_list_empty = true;
			}
			else
			{
				$("div#settings div.form div.alertNaN").html("");
				Settings.snmp_server_list_empty = false;
			}
			Settings.activateSnmpElements("list");
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.removeSnmpServer = function()
{
	$("div#overlay").css("display","none");
	$("div#settings div.form div#alertDelete").css("display","none");
	Main.loading();
	var selected = parseInt($("div#settings div.form ul#snmpserverform li#server-select select#serverList").prop("selectedIndex"));
	var index = parseInt($("div#settings div.form ul#snmpserverform li#server-select select#serverList option:selected").attr("id")); 
	var url = "http://" + hostName + "/cgi-bin/networksettings.cgi?removeSnmpServer&id=" + Settings.SnmpServerList[index].id;
	$.ajax({
	    url : url,
	    type: "POST",
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			Main.alert(language.Settings_server_remove_success);
			Settings.removeFromSnmpServerList();
			$("ul#snmpserverform select#serverList").prop("selectedIndex","0");
			var len = Settings.SnmpServerList.length;
			var i;
			for (i = 0; i < len; i++)
				if (Settings.SnmpServerList[i] != undefined)
					break;
			if (i == len || len == 0)
			{
				$("div#settings div.form div.alertNaN").html(language.settings_snmp_server_not);
				Settings.snmp_server_list_empty = true;
			}
			else
			{
				$("div#settings div.form div.alertNaN").html("");
				Settings.snmp_server_list_empty = false;
			}
			Settings.activateSnmpElements("list");
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.testSnmpServer = function()
{
	Main.loading();
	var index = parseInt($("div#settings div.form ul#snmpserverform li#server-select select#serverList option:selected").attr("id")); 
	var url = "http://" + hostName + "/cgi-bin/networksettings.cgi?testSnmpServer&id=" + Settings.SnmpServerList[index].id;
	$.ajax({
	    url : url,
	    type: "POST",
        dataType: 'text',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			Main.alert(language.Settings_snmpserver_test_success);
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

$(
	function()
	{
		$("div#settings").on( "click", "div.form ul#snmpserverform input#snmpTest", function() {
			var selected = $("ul#snmpserverform select#serverList option:selected" ).prop("id");
			if (selected == "none")
			{
				Main.alert(language.Setting_select_one_snmpserver);
				return;
			}
			Settings.testSnmpServer();
		});
		
		$("div#settings").on( "click", "div.form ul#snmpserverform input#snmpUpdate", function() {
			var selected = $("ul#snmpserverform select#serverList option:selected" ).prop("id");
			if (selected == "none")
			{
				Main.alert(language.Setting_select_one_snmpserver);
				return;
			}
			Settings.snmp_form_type = "update";
			Settings.createSnmpServerForm("update", selected);
		});

		$("div#settings").on( "click", "div.form ul#snmpserverform input#buttonNew", function() {
			Settings.snmp_form_type = "new";
			Settings.createSnmpServerForm("new");
		});

		$("div#settings").on( "click", "div.form ul#snmpserverform input#buttonSave", function() {
			if (Settings.snmp_form_type == "new")
			{
				if(Settings.getNewSnmpServerInfo())
					Settings.addSnmpServer();
			}
			else if (Settings.snmp_form_type == "update")
			{
				if(Settings.getUpdateSnmpServerInfo())
					Settings.updateSnmpServer();
			}
		});

		$("div#settings").on( "click", "div.form ul#snmpserverform input#buttonRemove", function() {
			$("div#overlay").css("display","block");
			$("div#settings div.form div#alertDelete").css("display","block");
			$("div#settings div.form div#alertDelete span.text").html(language.Settings_remove_server);
			$("div#settings div.form div#alertDelete input#buttonRemove").val(language.remove);
			$("div#settings div.form div#alertDelete input#buttonClose").val(language.cancel);
		});

		$("div#settings").on( "click", "div.panelDivSnmp input#buttonRemove", function() {
			Settings.removeSnmpServer();
		});

		$("div#settings").on( "click", "div.panelDivSnmp input#buttonClose", function() {
			$("div#overlay").css("display","none");
			$("div#settings div.form div#alertDelete").css("display","none");
		});

		$("div#settings").on( "click", "div.form ul#snmpserverform input#buttonCancel", function() {
			$("div#settings div.alert").html("");
			$("div#settings div.form ul#snmpserverform li#server_ip").removeClass("alert");
			$("div#settings div.form ul#snmpserverform li#server_port").removeClass("alert");
			$("div#settings div.form ul#snmpserverform li#community_name").removeClass("alert");
			$("div#settings div.form ul#snmpserverform li#security_name").removeClass("alert");
			$("div#settings div.form ul#snmpserverform").removeClass("alert");
			$("ul#snmpserverform select#serverList").prop("selectedIndex","0");
			var len = Settings.SnmpServerList.length;
			var i;
			for (i = 0; i < len; i++)
				if (Settings.SnmpServerList[i] != undefined)
					break;
			if (i == len || len == 0)
			{
				$("div#settings div.form div.alertNaN").html(language.settings_snmp_server_not);
				Settings.snmp_server_list_empty = true;
			}
			else
			{
				$("div#settings div.form div.alertNaN").html("");
				Settings.snmp_server_list_empty = false;
			}
			Settings.activateSnmpElements("list");
		});

		$("div#settings").on( "change", "div.form ul#snmpserverform li#version div.right select#snmp_version", function() {
			var selected = $("div#settings div.form ul#snmpserverform li#version div.right select#snmp_version").prop("selectedIndex");
			if (selected == 0 || selected == 1)
			{
				$("div#settings div.form ul#snmpserverform li#community_name").css("display", "block");
				$("div#settings div.form ul#snmpserverform li#security_name").css("display", "none");
				$("div#settings div.form ul#snmpserverform li#password").css("display", "none");
			}
			else if (selected == 2)
			{
				$("div#settings div.form ul#snmpserverform li#community_name").css("display", "none");
				$("div#settings div.form ul#snmpserverform li#security_name").css("display", "block");
				$("div#settings div.form ul#snmpserverform li#password").css("display", "block");
			}
		});

	}
);
