Settings.ldapData = {};

Settings.callLdap = function() {	
	$("div#settings div.form").html("");
	var html = '<div class="alert"></div>'
				+ '<ul class="element" id="ldapform" style="display:none;">'
				+	'<li id="ldapserver"><div class="left"></div><div class="right"><input type="text" id="serverInput"></div></li>'
				+	'<li id="ldapserverport"><div class="left"></div><div class="right"><input type="text" id="portInput"></div></li>'
				+	'<li id="ldapbasedn"><div class="left"></div><div class="right"><input type="text" id="ldapbasednInput"></div></li>'
				+	'<li id="buttons"><input type="submit" id="save"><input type="submit" id="test"></li>'
				+ '</ul>'
				+ '<div id="windowEdit" style="width: 600px;" class="panelDiv">'
					+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="tableUpdate">'
					+ '	<tr>'
					+ '		<td id="modulsensorname" width="40%"></td>'
					+ '		<td id="voltaj" width="60%"></td>'
					+ '	</tr>'
					+ '<tr><td class="alert"></td></tr>'
					+ '</table>'
					+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="tableUpdateList">'
					+ '	<tr>'
					+ '		<td id="ldapuser" width="40%"></td>'
					+ '		<td width="60%"><input type=text id="ldapuser"></td>'
					+ '	</tr>'
					+ '	<tr>'
					+ '		<td id="ldappassword" width="40%"></td>'
					+ '		<td width="60%"><input type=password id="ldappassword"></td>'
					+ '	</tr>'
					+ '</table>'
					+ '<div class="buttons">'
					+ '	<input type="submit" id="buttonTestLdap" name="buttonUpdate" class="submit">'
					+ '	<input type="submit" id="buttonCancel" name="buttonCancel" class="submit">'
					+ '</div>'
				+ '</div>';
	$("div#settings div.form").html(html);
	$("li#ldapserver div.left").html(language.Settings_ldap_server);
	$("li#ldapserverport div.left").html(language.Settings_ldap_serverport);
	$("li#ldapbasedn div.left").html(language.Settings_ldap_basedn);
	$("li#buttons input#save").val(language.Settings_ldap_save);
	$("li#buttons input#test").val(language.Settings_ldap_test);
	$("div#windowEdit td#ldapuser").html(language.Settings_ldap_username);
	$("div#windowEdit td#ldappassword").html(language.Settings_ldap_password);
	$("div#windowEdit input#buttonTestLdap").val(language.test);
	$("div#windowEdit input#buttonCancel").val(language.cancel);
	$("div#settings ul.element").css("display","block");
	Settings.getLdap();
};

Settings.getLdap = function() {
	var data = $.getJSON("http://" + hostName + "/cgi-bin/ldapsettings.cgi?getLdapSettings");	 
	data.success(function() {
		Settings.ldapData = jQuery.parseJSON(data.responseText);
		Settings.createldapform();
		Main.unloading();
	});
	data.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Settings.postLdap = function()
{
	Main.loading();
	json = JSON.stringify(Settings.ldapData);

	var url = "http://" + hostName + "/cgi-bin/ldapsettings.cgi?updateLdapSettings";
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			//Settings.callSmtp();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.createldapform = function() {
	var item = Settings.ldapData;

	var server = item.ldap_server;
	var port = item.ldap_server_port;
	var ldapbasedn = item.ldap_base_dn;

	if(item.ldap_server == undefined || item.ldap_server_port == undefined || item.ldap_base_dn == undefined ) {
		server = "";
		port = "";
		ldapbasedn = "";
	}

	$("input#serverInput").val(server);
	$("input#portInput").val(port);
	$("input#ldapbasednInput").val(ldapbasedn);
};

Settings.validateldapform = function(page) {	
	$("div#settings div.alert").html("");
	var alertHtml = $("div#settings div.alert").html();
	$("li#ldapserver").removeClass("alert");
	$("li#ldapserverport").removeClass("alert");
	$("li#ldapbasedn").removeClass("alert");

	var server = $("li#ldapserver input").val();
	var port = $("li#ldapserverport input").val();
	var ldapbasedn = $("li#ldapbasedn input").val();

	if (server.length == 0 && port.length == 0 && ldapbasedn.length == 0)
	{
		Settings.ldapData.ldap_server = "";
		Settings.ldapData.ldap_server_port = "";
		Settings.ldapData.ldap_base_dn = "";
		return true;
	}	
	
	var isvalid = true;
	
	//validation of server
	if(!Validator.validate( { "val" : server, "type" : "domain", "ismandatory" : "true", "obj" : $("li#ldapserver"), "objfocus" : $("li#ldapserver input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	else if(isvalid)
		Settings.ldapData.ldap_server = server;
	
	//validation of server port
	if(!Validator.validate( { "val" : port, "type" : "port", "ismandatory" : "true", "obj" : $("li#ldapserverport"), "objfocus" : $("li#ldapserverport input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	else if(isvalid)
		Settings.ldapData.ldap_server_port = port;
	
	//validation of ldapbasedn
	if(!Validator.validate( { "val" : ldapbasedn, "type" : "ldapbasedn", "ismandatory" : "true", "obj" : $("li#ldapbasedn"), "objfocus" : $("li#ldapbasedn input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	else if(isvalid)
		Settings.ldapData.ldap_base_dn = ldapbasedn;

	return isvalid;
};


Settings.CheckLdap = function(user,pass)
{
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/ldapsettings.cgi?checkLdap&user=" + user + "&pass=" + Base64.encode(pass);
	$.ajax({
	    url : url,
	    type: "POST",
	    dataType: "text",
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			if (data.toString() == "superuser" || data.toString() == "admin" || data.toString() == "user" )
	    		Main.alert(language.Settings_ldap_test_success + ":" + data.toString());
			else if (data.toString() == "error1")
	    		Main.alert(language.Settings_ldap_test_fail + ":" + data.toString());
	    	else
	    		Main.alert(language.Settings_ldap_test_fail + ":" + data.toString());

		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});

};

$(function() {
	$("div#settings").on( "click", "ul#ldapform input#save", function() {
		if(Settings.validateldapform()) {
			Settings.postLdap();
		}
	});

$("div#settings").on( "click", "ul#ldapform input#test", function() {
	$("div#overlay").css("display","block");
	$("div#windowEdit").css("display","block");
	$("div#windowEdit td.alert").html("");
	$("div#windowEdit td#modulsensorname").html(language.Settings_ldap_test);
	});


$("div#settings").on( "click", "div#windowEdit input#buttonTestLdap", function() {	
	$("div#windowEdit td.alert").html("");
	$("div#windowEdit input#ldapuser").removeClass("alert");
	$("div#windowEdit input#ldappassword").removeClass("alert");

	var alertHtml = "";
	var ldapuser     = $("div#windowEdit input#ldapuser").val();
	var ldappassword = $("div#windowEdit input#ldappassword").val();
	var isvalid = true;
	
	//validation of ldapuser
	if(!Validator.validate( { "val" : ldapuser, "type" : "ldapuser", "ismandatory" : "true", "obj" : $("div#windowEdit input#ldapuser"), "objfocus" : $("div#windowEdit input#ldapuser input"), "page" : $("div#windowEdit td.alert")})) 
		isvalid = false;	
	//validation of ldappassword
	if(!Validator.validate( { "val" : ldappassword, "type" : "ldappassword", "ismandatory" : "true", "obj" : $("div#windowEdit input#ldappassword"), "objfocus" : $("div#windowEdit input#ldappassword input"), "page" : $("div#windowEdit td.alert")})) 
		isvalid = false;	

	if(isvalid == false)
		return;

	$("div#overlay").css("display","none");
	$("div#windowEdit").css("display","none");
	Settings.CheckLdap(ldapuser, ldappassword);
});

$("div#settings").on( "click", "div#windowEdit input#buttonCancel", function() {
	$("div#windowEdit td.alert").html("");
	$("div#overlay").css("display","none");
	$("div#windowEdit").css("display","none");
});
});