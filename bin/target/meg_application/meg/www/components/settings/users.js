Settings.callUsers = function() {
	$("div#settings div.form").html("");
	var html = '<div class="alert"></div>'
			   + '<ul class="element" style="display:none;">'
			   +	'<li id="usergroups"><div class="left"></div><div class="right"></div></li>'
			   +	'<li id="username"><div class="left"></div><div class="right"></div></li>'
			   +	'<li id="password"><div class="left"></div><div class="right"></div></li>'
			   +	'<li id="passwordagain"><div class="left"></div><div class="right"></div></li>'
			   +	'<li id="mail"><div class="left"></div><div class="right"></div></li>'
			   +	'<li id="mailagain"><div class="left"></div><div class="right"></div></li>'
			   +	'<li id="phone"><div class="left"></div><div class="right"></div></li>'
			   +	'<li id="refreshperiod"><div class="left"></div><div class="right"></div></li>'
			   +	'<li id="cookietimeout"><div class="left"></div><div class="right"></div></li>'
			   +	'<li id="passreminder"><div class="left"></div><div class="right"></div></li>'
			   +	'<li id="language"><div class="left"></div><div class="right"></div></li>'
			   +	'<li id="groups"><div class="left"></div><div class="right"></div></li>'
			   +	'<li id="buttons"></li>'
			+ '</ul>';

	$("div#settings div.form").html(html);
	Settings.getUsers();
};

Settings.createElements = function() {
	var usergroups = document.createElement("select");
	$("li#usergroups div.left").html(language.Settings_users_userselect);
	$("li#usergroups div.right").append(usergroups);

	var oSelect = document.createElement("option");
	oSelect.setAttribute("selected","selected");
	oSelect.innerHTML = language.Settings_users_userselect;
	usergroups.appendChild(oSelect);

	for(var i = 0; i < Settings.users.length; i++) {
		var option = document.createElement("option");
		option.setAttribute("id","user" + i);
		option.setAttribute("value", Settings.users[i].name);
		option.innerHTML = Settings.users[i].name;
		usergroups.appendChild(option);
	}

	var username = document.createElement("input");
	username.setAttribute("type","text");
	$("li#username div.left").html(language.Settings_users_username);
	$("li#username div.right").append(username);

	var password = document.createElement("input");
	password.setAttribute("type","password");
	$("li#password div.left").html(language.Settings_users_password);
	$("li#password div.right").append(password);

	var passwordagain = document.createElement("input");
	passwordagain.setAttribute("type","password");
	$("li#passwordagain div.left").html(language.Settings_users_passwordagain);
	$("li#passwordagain div.right").append(passwordagain);

	var mail = document.createElement("input");
	mail.setAttribute("type","text");
	$("li#mail div.left").html(language.Settings_users_mail);
	$("li#mail div.right").append(mail);

	var mailagain = document.createElement("input");
	mailagain.setAttribute("type","text");
	$("li#mailagain div.left").html(language.Settings_users_mailagain);
	$("li#mailagain div.right").append(mailagain);

	var refreshperiod = document.createElement("input");
	refreshperiod.setAttribute("type","text");
	$("li#refreshperiod div.left").html(language.Settings_refresh_period);
	$("li#refreshperiod div.right").append(refreshperiod);


	var cookietimeout = document.createElement("input");
	cookietimeout.setAttribute("type","text");
	$("li#cookietimeout div.left").html(language.Settings_cookie_timeout);
	$("li#cookietimeout div.right").append(cookietimeout);

	var phone = document.createElement("input");
	phone.setAttribute("type","text");
	$("li#phone div.left").html(language.Settings_users_phone);
	$("li#phone div.right").append(phone);

	var passReminder = document.createElement("input");
	passReminder.setAttribute("type","text");
	$("li#passreminder div.left").html(language.Settings_users_passreminder);
	$("li#passreminder div.right").append(passReminder);

	var languageSelect = document.createElement("select");
	$("li#language div.left").html(language.Settings_users_lang);
	$("li#language div.right").append(languageSelect);

	for(var i = 0; i < Main.languages.length; i++) {
		var option = document.createElement("option");
		option.setAttribute("id", Main.languages[i].value);
		option.setAttribute("value", Main.languages[i].value);
		option.innerHTML = Main.languages[i].name;
		languageSelect.appendChild(option);
	}

	var groups = document.createElement("select");
	$("li#groups div.left").html(language.Settings_users_groups);
	$("li#groups div.right").append(groups);

	var oSelect = document.createElement("option");
	//oSelect.setAttribute("selected","selected");
	oSelect.innerHTML = language.Settings_users_groups;
	groups.appendChild(oSelect);

	for(var i = 0; i < Settings.groups.length; i++) {
		var option = document.createElement("option");
		option.setAttribute("id","user" + i);
		option.setAttribute("value", Settings.groups[i].id);
		option.innerHTML = Settings.groups[i].name;
		groups.appendChild(option);
	}

	var delbutton = document.createElement("input");
	delbutton.setAttribute("type","submit");
	delbutton.setAttribute("id","delete");
	delbutton.setAttribute("value", language.Settings_users_delete);

	var update = document.createElement("input");
	update.setAttribute("type","submit");
	update.setAttribute("id","update");
	update.setAttribute("value", language.Settings_users_update);

	var newuser = document.createElement("input");
	newuser.setAttribute("type","submit");
	newuser.setAttribute("id","newuser");
	newuser.setAttribute("value", language.Settings_users_newuser);

	var cancel = document.createElement("input");
	cancel.setAttribute("type","submit");
	cancel.setAttribute("id","cancel");
	cancel.setAttribute("value", language.Settings_users_cancel);

	var contin = document.createElement("input");
	contin.setAttribute("type","submit");
	contin.setAttribute("id","continue");
	contin.setAttribute("value", language.Settings_users_save);

	$("li#buttons").append(delbutton);
	$("li#buttons").append(update);
	$("li#buttons").append(newuser);
	$("li#buttons").append(cancel);
	$("li#buttons").append(contin);

	$("ul.element").css("display","block");

	Settings.usersDisplay("intro");	
	Main.unloading();
};

Settings.deleteUser = function(usr) {
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/userandgroups.cgi?removeUser&username=" + usr;
	$.ajax({
	    url : url,
	    type: "POST",
	    data : {},
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			Settings.callUsers();
	    },
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    	Main.alert(language.users_remove_fail + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.addNewUser = function(json, groupname) {
	Main.loading();
	json = JSON.stringify(json);
	var url = "http://" + hostName + "/cgi-bin/userandgroups.cgi?addUser&groupname=" + groupname;
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			Settings.callUsers();
	    },
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    	Main.alert(language.users_add_fail + ":" + jqXHR.responseText);
	    	Main.unloading();
	    }
	});
};

Settings.updateUser = function(usr, jSon) {
	Main.loading();

	json = JSON.stringify(jSon);

	var url = "http://" + hostName + "/cgi-bin/userandgroups.cgi?updateUser&username="+ Settings.currentUser.name;
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
	    	if (Settings.currentUser.id == Main.user.id)
	    	{
	    		
				var isLangUpdated = (jSon.language != Main.user.language);
				for (var key in jSon)
				{	
					if (key == undefined)
					continue;
					Main.user[key] = jSon[key];
				}
				var user = Base64.encode(JSON.stringify(Main.user));
				setCookie("Main.user",user,parseInt(user.cookie_timeout)); 

				if (isLangUpdated)
				{
					location.reload();
					return;
				}
				else
				{
					Main.unloading();
					Settings.callUsers();
				}
	    	}
	    	else
	    	{
				Main.unloading();
				Settings.callUsers();
	    	}
	    },
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    	Main.alert(language.users_update_fail + ":" + jqXHR.responseText);	
			Main.unloading();
	    }
	});
};

Settings.usersDisplay = function(item,user) {
	$("div#settings div.alert").html("");
	$("li#username").removeClass("alert");
	$("li#password").removeClass("alert");
	$("li#passwordagain").removeClass("alert");
	$("li#mail").removeClass("alert");
	$("li#mailagain").removeClass("alert");
	$("li#refreshperiod").removeClass("alert");
	$("li#cookietimeout").removeClass("alert");
	$("li#phone").removeClass("alert");
	$("li#passreminder").removeClass("alert");
	$("li#language").removeClass("alert");
	$("li#groups").removeClass("alert");

	switch(item) {
		case "intro":
			$("li#usergroups").css("display","block");
			$("li#usergroups option").each(function(){
				var val = $(this).val();
				if(val == language.Settings_users_userselect){
					$( this ).prop('selected', true);
				} else {
					$( this ).prop('selected', false);
				}
			});
			$("li#username").css("display","none");
			$("li#password").css("display","none");
			$("li#passwordagain").css("display","none");
			$("li#mail").css("display","none");
			$("li#mailagain").css("display","none");
			$("li#refreshperiod").css("display","none");
			$("li#cookietimeout").css("display","none");
			$("li#phone").css("display","none");
			$("li#passreminder").css("display","none");
			$("li#language").css("display","none");
			$("li#groups").css("display","none");
			$("input#delete").css("display","inline-block");
			$("input#update").css("display","inline-block");
			$("input#newuser").css("display","inline-block");
			$("input#cancel").css("display","none");
			$("input#continue").css("display","none");
		break;
		case "newuser":
			$("li#usergroups").css("display","none");
			$("li#username").css("display","block");
			$("li#username input").val("");
			$("li#password").css("display","block");
			$("li#password input").val("");
			$("li#passwordagain").css("display","block");
			$("li#passwordagain input").val("");
			$("li#mail").css("display","block");
			$("li#mail input").val("");
			$("li#mailagain").css("display","block");
			$("li#mailagain input").val("");
			$("li#refreshperiod").css("display","block");
			$("li#cookietimeout").css("display","block");
			$("li#refreshperiod input").val("");
			$("li#cookietimeout input").val("");
			$("li#phone").css("display","block");
			$("li#phone input").val("");
			$("li#passreminder").css("display","block");
			$("li#passreminder input").val("");
			$("li#language").css("display","block");
			$("li#language option").each(function(){
				var val = $(this).val();
				if(val == "tr"){
					$( this ).prop('selected', true);
				} else {
					$( this ).prop('selected', false);
				}
			});
			$("li#groups").css("display","block");
			$("li#groups option").each(function(){
				var val = $(this).val();
				if(val == language.Settings_users_groups){
					$( this ).prop('selected', true);
				} else {
					$( this ).prop('selected', false);
				}
			});
			$("input#delete").css("display","none");
			$("input#update").css("display","none");
			$("input#newuser").css("display","none");
			$("input#cancel").css("display","inline-block");
			$("input#continue").css("display","inline-block");
			$("input#continue").data("page","newuser");
		break;
		case "update":
			$("li#usergroups").css("display","none");
			$("li#username").css("display","block");
			$("li#username input").val(user.name);
			$("li#password").css("display","block");
			$("li#passwordagain").css("display","block");
			$("li#mail").css("display","block");
			$("li#mail input").val(user.email_addr);
			$("li#mailagain").css("display","block");
			$("li#mailagain input").val(user.email_addr);
			$("li#refreshperiod").css("display","block");
			$("li#cookietimeout").css("display","block");
			$("li#refreshperiod input").val(user.refresh_period);
			$("li#cookietimeout input").val(user.cookie_timeout);
			$("li#phone").css("display","block");
			$("li#phone input").val(user.phone_number);
			$("li#passreminder").css("display","block");			
			$("li#passreminder input").val(user.password_reminder);
			$("li#language").css("display","block");
			$("li#language option").each(function(){
				var val = $(this).val();
				if(val == user.language){
					$( this ).prop('selected', true);
				} else {
					$( this ).prop('selected', false);
				}
			});

			$("li#groups").css("display","block");

			$("li#groups option").each(function(){
				var val = $(this).val();
				if(val == user.groupid){
					$( this ).prop('selected', true);
				} else {
					$( this ).prop('selected', false);
				}
			});

			$("input#delete").css("display","none");
			$("input#update").css("display","none");
			$("input#newuser").css("display","none");
			$("input#cancel").css("display","inline-block");
			$("input#continue").css("display","inline-block");
			$("input#continue").data("page","update");
		break;

	}
};

Settings.userFormValidate = function(page) {
	
	$("div#settings div.alert").html("");
	var alertHtml = $("div#settings div.alert").html();
	$("li#username").removeClass("alert");
	$("li#password").removeClass("alert");
	$("li#passwordagain").removeClass("alert");
	$("li#mail").removeClass("alert");
	$("li#mailagain").removeClass("alert");
	$("li#phone").removeClass("alert");
	$("li#refreshperiod").removeClass("alert");
	$("li#cookietimeout").removeClass("alert");
	$("li#passreminder").removeClass("alert");
	$("li#language").removeClass("alert");
	$("li#groups").removeClass("alert");
	
	
	var username       = $("li#username input").val();
	var password       = $("li#password input").val();
	var passwordagain  = $("li#passwordagain input").val();
	var mail           = $("li#mail input").val();
	var mailagain      = $("li#mailagain input").val();
	var phone          = $("li#phone input").val();
	var refreshperiod  = $("li#refreshperiod input").val();
	var cookietimeout  = $("li#cookietimeout input").val();
	var passreminder   = $("li#passreminder input").val();
	
	var isvalid = true;
	
	//validation of username
	if(!Validator.validate( { "val" : username, "type" : "username", "ismandatory" : "true", "obj" : $("li#username"), "objfocus" : $("li#username input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	
	switch(page)
	{
		case "update":
			if (passwordagain.length != 0 || password.length != 0)
			{
				//validation of password
				if(!Validator.validate( { "val" : password, "type" : "userpassword", "ismandatory" : "false", "obj" : $("li#password"), "objfocus" : $("li#password input"), "page" : $("div#settings div.alert")}))
					isvalid = false;
				
				if(passwordagain != password)
				{	
					$("li#password").addClass("alert");
					$("li#passwordagain").addClass("alert");
					alertHtml = alertHtml + "<br />" + language.Settings_users_alert_passerror;
					$("div#settings div.alert").html(alertHtml);			
					isvalid = false;
				}
			}
			break;

		case "newuser":
			//validation of password
			if(!Validator.validate( { "val" : password, "type" : "userpassword", "ismandatory" : "false", "obj" : $("li#password"), "objfocus" : $("li#password input"), "page" : $("div#settings div.alert")}))
				isvalid = false;
			//validation of passwordagain
			if(!Validator.validate( { "val" : passwordagain, "type" : "userpassword", "ismandatory" : "true", "obj" : $("li#passwordagain"), "objfocus" : $("li#passwordagain input"), "page" : $("div#settings div.alert")}))
				isvalid = false;

			if(passwordagain != password)
			{	
				$("li#password").addClass("alert");
				$("li#passwordagain").addClass("alert");
				alertHtml = alertHtml + "<br />" + language.Settings_users_alert_passerror;
				$("div#settings div.alert").html(alertHtml);			
				isvalid = false;
			}
			break;
	}
	
	//validation of email address
	if(!Validator.validate( { "val" : mail, "type" : "email", "ismandatory" : "false", "obj" : $("li#mail"), "objfocus" : $("li#mail input"), "page" : $("div#settings div.alert")}))
		isvalid = false;

	if(mail != mailagain)
	{	
		$("li#mail").addClass("alert");
		$("li#mailagain").addClass("alert");
		alertHtml = alertHtml + "<br />" + language.Settings_users_alert_mailerror;
		$("div#settings div.alert").html(alertHtml);			
		isvalid = false;
	}
	
	//validation of phone
	if(!Validator.validate( { "val" : phone, "type" : "phonenumber", "ismandatory" : "false", "obj" : $("li#phone"), "objfocus" : $("li#phone input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	
	//validation of refreshperiod
	if(!Validator.validate( { "val" : refreshperiod, "type" : "i", "min" : "30", "ismandatory" : "false", "obj" : $("li#refreshperiod"), "objfocus" : $("li#refreshperiod input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	
	//validation of cookietimeout
	if(!Validator.validate( { "val" : cookietimeout, "type" : "i", "min" : "1",  "max" : "365", "ismandatory" : "false", "obj" : $("li#cookietimeout"), "objfocus" : $("li#cookietimeout input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	
	//validation of passreminder
	if(!Validator.validate( { "val" : passreminder, "type" : "username", "ismandatory" : "false", "obj" : $("li#passreminder"), "objfocus" : $("li#passreminder input"), "page" : $("div#settings div.alert")}))
		isvalid = false;

	if($("li#groups select").val() == language.Settings_users_groups)
	{
		$("li#groups").addClass("alert");
		alertHtml = alertHtml + "<br />" + language.Settings_users_alert_groups;
		$("div#settings div.alert").html(alertHtml);
		isvalid = false;
	}

	return isvalid;
};

Settings.getUsers = function() {
	var getUsersData = $.getJSON("http://" + hostName + "/cgi-bin/userandgroups.cgi?getUsers");	 
	getUsersData.success(function() {
		Settings.users = jQuery.parseJSON(getUsersData.responseText);
		Settings.onGroupLoad();
	});
	getUsersData.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);	
		Main.unloading();
	});
};

$(function() {

	$("div#settings").on( "click", "ul.element input#newuser", function() {
		Settings.usersDisplay("newuser");		
	});

	$("div#settings").on( "click", "ul.element input#continue", function() {
		var page = $(this).data("page");
		switch(page) {
			case "newuser":
				if(Settings.userFormValidate(page)) {

					var json = {
						"language": $("li#language select").val(),
						"name": $("li#username input").val(),
						"password": $("li#password input").val(),
						"groupid": $("li#groups select option:selected").val()
					};

					if ($("li#mail input").val().length != 0 && $("li#mail input").val() != undefined)
						json.email_addr = $("li#mail input").val();

					if ($("li#phone input").val().length != 0 && $("li#phone input").val() != undefined)
						json.phone_number = $("li#phone input").val();

					if ($("li#passreminder input").val().length != 0 && $("li#passreminder input").val() != undefined)
						json.password_reminder = $("li#passreminder input").val();

					if ($("li#refreshperiod input").val().length != 0 && $("li#refreshperiod input").val() != undefined)
						json.refresh_period = $("li#refreshperiod input").val();


					if ($("li#cookietimeout input").val().length != 0 && $("li#cookietimeout input").val() != undefined)
						json.cookie_timeout = $("li#cookietimeout input").val();

					Settings.addNewUser(json, $("li#groups select option:selected").text());
				}
			break;
			case "update":
				if(Settings.userFormValidate(page)) {
					var json = null;
					var usr = $("li#username input").val();
					if($("li#password input").val() == "" || $("li#password input").val() == null) {
						json = {
							"email_addr": $("li#mail input").val(),
							"language": $("li#language select").val(),
							"name": $("li#username input").val(),
							"password_reminder": $("li#passreminder input").val(),
							"refresh_period": $("li#refreshperiod input").val(),
							"cookie_timeout": $("li#cookietimeout input").val(),
							"phone_number": $("li#phone input").val(),
							"groupid": $("li#groups select option:selected").val()
						};
					} else {
						json = {
							"email_addr": $("li#mail input").val(),
							"language": $("li#language select").val(),
							"name": $("li#username input").val(),
							"password": $("li#password input").val(),
							"password_reminder": $("li#passreminder input").val(),
							"refresh_period": $("li#refreshperiod input").val(),
							"cookie_timeout": $("li#cookietimeout input").val(),
							"phone_number": $("li#phone input").val(),
							"groupid": $("li#groups select option:selected").val()
						};
					}

					Settings.updateUser(usr, json);
				}
			break;
		}
	});

	/*$("div#settings").on( "change", "li#usergroups select", function() {
		var val = $("li#usergroups select").val();
		if(val != null && val != undefined && val != "" && val != language.Settings_users_userselect) {
			Main.loading();
			var getUserData = $.getJSON("http://" + hostName + "/cgi-bin/userandgroups.cgi?getUser&username=" + val);	 
			getUserData.success(function() {
				Settings.currentUser = jQuery.parseJSON(getUserData.responseText);
				//var item = jQuery.parseJSON(getUserData.responseText);
				Settings.usersDisplay("update", Settings.currentUser);
				Main.unloading();
			});	
			getUserData.error(function(){
				Main.alert(language.server_error);	
				Main.unloading();
			});
		} else {
			Main.alert(language.Settings_update_no_select);
		}
	});*/

	$("div#settings").on( "click", "ul.element input#update", function() {
		var val = $("li#usergroups select").val();
		if(val != null && val != undefined && val != "" && val != language.Settings_users_userselect) {
			Main.loading();
			var getUserData = $.getJSON("http://" + hostName + "/cgi-bin/userandgroups.cgi?getUser&username=" + val);	 
			getUserData.success(function() {
				Settings.currentUser = jQuery.parseJSON(getUserData.responseText);
				//var item = jQuery.parseJSON(getUserData.responseText);
				Settings.usersDisplay("update", Settings.currentUser);
				Main.unloading();
			});	
			getUserData.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);	
				Main.unloading();
			});
		} else {
			Main.alert(language.Settings_update_no_select);
		}
	});

	$("div#settings").on( "click", "ul.element input#delete", function() {
		var val = $("li#usergroups select").val();
		if(val != null && val != undefined && val != "" && val != language.Settings_users_userselect) {
			var val = $("li#usergroups select").val();
			$("div#overlay").css("display","block");
			$("div#alertDelete").css("display","block");

			$("div#alertDelete input#buttonRemove").val(language.remove);
			$("div#alertDelete input#buttonRemove").attr("data-user", val);
			$("div#alertDelete input#buttonRemove").addClass("userDelete");
			$("div#alertDelete input#buttonClose").val(language.close);
			$("div#alertDelete span.text").html(language.Settings_delete_user);	
		} else {
			Main.alert(language.Settings_delete_no_select);
		}
	});

	$("div#settings").on( "click", "ul.element input#cancel", function() {
		Settings.usersDisplay("intro");		
	});

	$("div#settings").on( "click", "div#alertDelete input#buttonClose", function() {
		$("div#overlay").css("display","none");
		$("div#alertDelete").css("display","none");
	});

	$("div#settings").on( "click", "div#alertDelete input.userDelete", function() {
		var deleteUser = $(this).attr("data-user");
		$("div#overlay").css("display","none");
		$("div#alertDelete").css("display","none");
		$("div#alertDelete input#buttonRemove").removeClass("userDelete");
		Settings.deleteUser(deleteUser);
	});	
});