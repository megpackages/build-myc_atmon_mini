Settings.ntpData = {};

Settings.callNtpServer = function() {
	Settings.getNtpHtml();
	Settings.getNtpServer();
	//Settings.createFtpForm();
};

Settings.getNtpHtml = function() {

	$("div#settings div.form").html("");
	var html = '<div class="alert"></div>'
				+ '<ul class="element" id="ntpform" style="display:none;">'
				+	'<li id="system_clock"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="ntp_active"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="ntp_server"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="ntp_server_other"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="gmt"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="manuel_time"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="manuel_date"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="buttons"><div class="left"></div><div class="right"></div></li>'
				+ '</ul>';
	$("div#settings div.form").html(html);
};

Settings.getNtpServer = function() {
	var data = $.getJSON("http://" + hostName + "/cgi-bin/networksettings.cgi?getNtpServer");	 
	data.success(function() {
		Settings.ntpData = jQuery.parseJSON(data.responseText);
		Settings.date = Settings.ntpData.date;
		delete Settings.ntpData.date;
		Settings.gmtoff = Settings.ntpData.gmtoff;
		delete Settings.ntpData.gmtoff;
		Settings.createNtpServerForm();
		Main.unloading();
	});
	data.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Settings.postNtpServer = function() {
	Main.loading();
	json = JSON.stringify(Settings.ntpData);
	console.log(json);
	var url = "http://" + hostName + "/cgi-bin/networksettings.cgi?updateNtpServer";
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			//Settings.callSmtp();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.createNtpServerForm = function() {	
	Main.unloading();
	var item = Settings.ntpData;
	var date = parseInt(Settings.date);
	var gmtoff = parseInt(Settings.gmtoff);
	var active = item.active;
	var server_ip = item.server_ip;
	var server_ip_backup = item.server_ip_backup;
	var server_port = item.server_port;
	var timezone = item.timezone;
	var currentdate = new Date();
	currentdate.setTime((date+gmtoff)*1000);

	var datetime = Settings.getDate(currentdate);

	$("li#system_clock div.left").html(language.Settings_ntp_system_clock);
	$("li#system_clock div.right").html(datetime);

    clearInterval(Settings.ntpInterval);
    Settings.NtpDateInterval();

	var activeInput = document.createElement("input");
	activeInput.setAttribute("type","checkbox");
	activeInput.setAttribute("id","anonimInput");
	if(active == 1) {
		activeInput.checked = true;
	}
	$("li#ntp_active div.left").html(language.Settings_ntp_active);
	$("li#ntp_active div.right").append(activeInput);

	var ipInput = document.createElement("input");
	ipInput.setAttribute("type","text");
	ipInput.setAttribute("id","ipInput");
	ipInput.setAttribute("value",server_ip);
	if(active == 0) {
		ipInput.setAttribute("disabled","disabled");
	}
	$("li#ntp_server div.left").html(language.Settings_ntp_server);
	$("li#ntp_server div.right").append(ipInput);

	var ipBackupInput = document.createElement("input");
	ipBackupInput.setAttribute("type","text");
	ipBackupInput.setAttribute("id","ipBackupInput");
	ipBackupInput.setAttribute("value",server_ip_backup);
	if(active == 0) {
		ipBackupInput.setAttribute("disabled","disabled");
	}

	$("li#ntp_server_other div.left").html(language.Settings_ntp_server_backup);
	$("li#ntp_server_other div.right").append(ipBackupInput);

	$("li#ntp_server_other div.left").html(language.Settings_ntp_server_backup);
	$("li#ntp_server_other div.right").append(ipBackupInput);

	var year = currentdate.getUTCFullYear().toString();
	year = Util.feedZero(year, 2, "left");
	var month = (parseInt(currentdate.getUTCMonth().toString())+1).toString();
	month =  Util.feedZero(month, 2, "left");
	var day = currentdate.getUTCDate().toString();
	day =  Util.feedZero(day, 2, "left");
	
	var setMDate = year + "-" 
                +  month + "-"
                +  day;

	var hour = currentdate.getUTCHours().toString();
	hour =  Util.feedZero(hour, 2, "left");
	var minute =currentdate.getUTCMinutes().toString();
	minute =  Util.feedZero(minute, 2, "left");
	var second = currentdate.getUTCSeconds().toString();
	second =  Util.feedZero(second, 2, "left");

	var setMTime = hour + ":"  
                + minute + ":" 
                + second;

    Settings.ntpData.time = setMTime;
    Settings.ntpData.date = setMDate;

	var manuelDateType = "";
	var manuelTimeType = "";
	if(active == "1")
	{
		manuelTimeType = '<input id="time" type="time" step="2" name="time" value="' + setMTime + '" disabled="true">';
		manuelDateType = '<input id="date" type="date" name="date" value="' + setMDate + '" disabled="true">';
	}
	else
	{	
		manuelDateType = '<input id="date" type="date" name="date" value="' + setMDate + '">';
		manuelTimeType = '<input id="time" type="time" step="2" name="time" value="' + setMTime + '">';
	}


	$("li#manuel_date div.left").html(language.Settings_ntp_manuel_date);
	$("li#manuel_date div.right").html(manuelDateType);
	$("li#manuel_time div.left").html(language.Settings_ntp_manuel_time);
	$("li#manuel_time div.right").html(manuelTimeType);
	$("li#manuel_time input#time").attr("value",setMTime) ;
	$("li#manuel_date input#date").attr("value",setMDate) ;
	$("li#gmt div.left").html(language.Settings_ntp_gmt);
	$("li#gmt div.right").html(gmtSelect);
	$("div#settings select#timezoneSelect").prop("selectedIndex",timezone);

	var saveInput = document.createElement("input");
	saveInput.setAttribute("type","submit");
	saveInput.setAttribute("id","save");
	saveInput.setAttribute("value", language.Settings_ftp_save);
	$("li#buttons div.left").html("");
	$("li#buttons div.right").append(saveInput);

	$("div#settings ul.element").css("display","block");
};


Settings.leadZero = function(val) {
	if (10 > val)
		return "0"+val;
	else
		return val;
};

Settings.getDate = function(currentdate) {	
	var weekday=new Array(7);
	weekday[0]= language.day0;
	weekday[1]= language.day1;
	weekday[2]= language.day2;
	weekday[3]= language.day3;
	weekday[4]= language.day4;
	weekday[5]= language.day5;
	weekday[6]= language.day6;
	var month_of_year = ['Ocak','Şubat','Mart','Nisan','Mayıs','Haziran','Temmuz','Ağustos','Eylül','Ekim','Kasım','Aralık'];


	var datetime = currentdate.getUTCDate() + " "
                + month_of_year[currentdate.getUTCMonth()] + " " 
				+ weekday[currentdate.getUTCDay()] + " " 
                + currentdate.getUTCFullYear() + "  "  
                + Settings.leadZero(currentdate.getUTCHours()) + ":"  
                + Settings.leadZero(currentdate.getUTCMinutes()) + ":" 
                + Settings.leadZero(currentdate.getUTCSeconds());
    return datetime;
};

Settings.NtpDateInterval = function() {
    Settings.ntpInterval = setInterval(function() {
        var currentTime = parseInt(Settings.date*1000);
        var currentGMToff = parseInt(Settings.gmtoff*1000);
		Settings.date = parseInt(Settings.date) + 1;
		var currentdate = new Date();
		var currenttime = currentTime + currentGMToff;
		currentdate.setTime(currenttime);
		var datetime = Settings.getDate(currentdate);
		$("li#system_clock div.right").html(datetime);
	}, 1000);
};

Settings.validateNtpServerForm = function(page) {	
	$("div#settings div.alert").html("");
	var alertHtml = $("div#settings div.alert").html();
	$("li#ntp_server").removeClass("alert");
	$("li#ntp_server_other").removeClass("alert");

	var ntp_active = $("li#ntp_active input").prop("checked");
	var ntp_server = $("li#ntp_server input").val();
	var ntp_server_other = $("li#ntp_server_other input").val();
	var gmt = $("li#gmt select").val();
	
	var validateip = "";
	var value = true;

	if(!ntp_server.match(validateip)) {
	    $("li#ntp_server").addClass("alert");
	    alertHtml = language.Settings_tcpip_iperror;
		$("div#settings div.alert").html(alertHtml);
	    $("li#ntp_server input").focus();
	    value = false;
    }

	if(!ntp_server_other.match(validateip)) {
	    $("li#ntp_server_other").addClass("alert");
	    alertHtml = alertHtml + "<br/>" + language.Settings_tcpip_iperror;
		$("div#settings div.alert").html(alertHtml);
	    $("li#ntp_server_other input").focus();
	    value = false;
    }

	if(value) {

		if(ntp_active) {
			ntp_active = "1";
		} else {
			ntp_active = "0";
		}

		Settings.ntpData.active = ntp_active;
		Settings.ntpData.server_ip = ntp_server;
		Settings.ntpData.server_ip_backup = ntp_server_other;

	}

	return value;
};


$(function() {
	
	$("div#settings").on( "click", "ul#ntpform input#save", function() {
		if(Settings.validateNtpServerForm()) {
			Settings.postNtpServer();
		}
	});

	$("div#settings").on( "change", "select#timezoneSelect", function() {
		Settings.ntpData['timezone'] = String($(this).prop("selectedIndex"))
	});

	$("div#settings").on( "change", "ul#ntpform li#ntp_active input", function() {
		Settings.getNtpHtml();
		var status = $(this).prop("checked");
		if(status) {
			Settings.ntpData.active = "1";
			$("li#manuel_date input#time").attr("disabled", "true");
			$("li#manuel_time input#date").attr("disabled", "true");
		} else {
			Settings.ntpData.active = "0";
			$("li#manuel_date input#date").removeAttr("disabled");
			$("li#manuel_time input#time").removeAttr("disabled");
		}
		Settings.createNtpServerForm();
	});

	$("div#settings").on( "change", "li#manuel_date input#date", function(){
		Settings.ntpData.date = this.value;
	});

	$("div#settings").on( "change", "li#manuel_time input#time", function(){
		Settings.ntpData.time = this.value;
	});

});

var gmtSelect = '<select id="timezoneSelect">'
    + '<option timezoneid="1">(GMT- 08:00) LosAngeles</option>'
    + '<option timezoneid="2">(GMT- 06:00) Chicago</option>'
    + '<option timezoneid="3">(GMT- 05:00) NewYork</option>'
	+ '<option timezoneid="4">(GMT+00:00) Greenwich</option>'
    + '<option timezoneid="5">(GMT+01:00) Amsterdam</option>'
    + '<option timezoneid="6">(GMT+01:00) Berlin</option>'
    + '<option timezoneid="7">(GMT+01:00) Madrid</option>'
	+ '<option timezoneid="8">(GMT+01:00) Paris</option>'
	+ '<option timezoneid="9">(GMT+02:00) Turkey</option>'
	+ '<option timezoneid="10">(GMT+03:00) Moscow</option>'
	+ '<option timezoneid="11">(GMT+10:00) Sydney</option>'
/*
	+ '<option timezoneid="3" gmtadjustment="GMT-10:00" usedaylighttime="0" value="-10">(GMT-10:00) Hawaii</option>'
    + ' <option timezoneid="1" gmtadjustment="GMT-12:00" usedaylighttime="0" value="-12">(GMT-12:00) International Date Line West</option>'
    + ' <option timezoneid="2" gmtadjustment="GMT-11:00" usedaylighttime="0" value="-11">(GMT-11:00) Midway Island, Samoa</option>'
    + ' <option timezoneid="3" gmtadjustment="GMT-10:00" usedaylighttime="0" value="-10">(GMT-10:00) Hawaii</option>'
    + ' <option timezoneid="4" gmtadjustment="GMT-09:00" usedaylighttime="1" value="-9">(GMT-09:00) Alaska</option>'
    + ' <option timezoneid="5" gmtadjustment="GMT-08:00" usedaylighttime="1" value="-8">(GMT-08:00) Pacific Time (US &amp; Canada)</option>'
    + ' <option timezoneid="6" gmtadjustment="GMT-08:00" usedaylighttime="1" value="-8">(GMT-08:00) Tijuana, Baja California</option>'
    + ' <option timezoneid="7" gmtadjustment="GMT-07:00" usedaylighttime="0" value="-7">(GMT-07:00) Arizona</option>'
    + ' <option timezoneid="8" gmtadjustment="GMT-07:00" usedaylighttime="1" value="-7">(GMT-07:00) Chihuahua, La Paz, Mazatlan</option>'
    + ' <option timezoneid="9" gmtadjustment="GMT-07:00" usedaylighttime="1" value="-7">(GMT-07:00) Mountain Time (US &amp; Canada)</option>'
    + ' <option timezoneid="10" gmtadjustment="GMT-06:00" usedaylighttime="0" value="-6">(GMT-06:00) Central America</option>'
    + ' <option timezoneid="11" gmtadjustment="GMT-06:00" usedaylighttime="1" value="-6">(GMT-06:00) Central Time (US &amp; Canada)</option>'
    + ' <option timezoneid="12" gmtadjustment="GMT-06:00" usedaylighttime="1" value="-6">(GMT-06:00) Guadalajara, Mexico City, Monterrey</option>'
    + ' <option timezoneid="13" gmtadjustment="GMT-06:00" usedaylighttime="0" value="-6">(GMT-06:00) Saskatchewan</option>'
    + ' <option timezoneid="14" gmtadjustment="GMT-05:00" usedaylighttime="0" value="-5">(GMT-05:00) Bogota, Lima, Quito, Rio Branco</option>'
    + ' <option timezoneid="15" gmtadjustment="GMT-05:00" usedaylighttime="1" value="-5">(GMT-05:00) Eastern Time (US &amp; Canada)</option>'
    + ' <option timezoneid="16" gmtadjustment="GMT-05:00" usedaylighttime="1" value="-5">(GMT-05:00) Indiana (East)</option>'
    + ' <option timezoneid="17" gmtadjustment="GMT-04:00" usedaylighttime="1" value="-4">(GMT-04:00) Atlantic Time (Canada)</option>'
    + ' <option timezoneid="18" gmtadjustment="GMT-04:00" usedaylighttime="0" value="-4">(GMT-04:00) Caracas, La Paz</option>'
    + ' <option timezoneid="19" gmtadjustment="GMT-04:00" usedaylighttime="0" value="-4">(GMT-04:00) Manaus</option>'
    + ' <option timezoneid="20" gmtadjustment="GMT-04:00" usedaylighttime="1" value="-4">(GMT-04:00) Santiago</option>'
    + ' <option timezoneid="21" gmtadjustment="GMT-03:00" usedaylighttime="1" value="-3">(GMT-03:00) Brasilia</option>'
    + ' <option timezoneid="22" gmtadjustment="GMT-03:00" usedaylighttime="0" value="-3">(GMT-03:00) Buenos Aires, Georgetown</option>'
    + ' <option timezoneid="23" gmtadjustment="GMT-03:00" usedaylighttime="1" value="-3">(GMT-03:00) Greenland</option>'
    + ' <option timezoneid="24" gmtadjustment="GMT-03:00" usedaylighttime="1" value="-3">(GMT-03:00) Montevideo</option>'
    + ' <option timezoneid="25" gmtadjustment="GMT-02:00" usedaylighttime="1" value="-2">(GMT-02:00) Mid-Atlantic</option>'
    + ' <option timezoneid="26" gmtadjustment="GMT-01:00" usedaylighttime="0" value="-1">(GMT-01:00) Cape Verde Is.</option>'
    + ' <option timezoneid="27" gmtadjustment="GMT-01:00" usedaylighttime="1" value="-1">(GMT-01:00) Azores</option>'
    + ' <option timezoneid="28" gmtadjustment="GMT+00:00" usedaylighttime="0" value="0">(GMT+00:00) Casablanca, Monrovia, Reykjavik</option>'
    + ' <option timezoneid="29" gmtadjustment="GMT+00:00" usedaylighttime="1" value="0">(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London</option>'
    + ' <option timezoneid="30" gmtadjustment="GMT+01:00" usedaylighttime="1" value="1">(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>'
    + ' <option timezoneid="31" gmtadjustment="GMT+01:00" usedaylighttime="1" value="1">(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>'
    + ' <option timezoneid="32" gmtadjustment="GMT+01:00" usedaylighttime="1" value="1">(GMT+01:00) Brussels, Copenhagen, Madrid, Paris</option>'
    + ' <option timezoneid="33" gmtadjustment="GMT+01:00" usedaylighttime="1" value="1">(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb</option>'
    + ' <option timezoneid="34" gmtadjustment="GMT+01:00" usedaylighttime="1" value="1">(GMT+01:00) West Central Africa</option>'
    + ' <option timezoneid="35" gmtadjustment="GMT+02:00" usedaylighttime="1" value="2">(GMT+02:00) Amman</option>'
    + ' <option timezoneid="36" gmtadjustment="GMT+02:00" usedaylighttime="1" value="2">(GMT+02:00) Athens, Bucharest, Istanbul</option>'
    + ' <option timezoneid="37" gmtadjustment="GMT+02:00" usedaylighttime="1" value="2">(GMT+02:00) Beirut</option>'
    + ' <option timezoneid="38" gmtadjustment="GMT+02:00" usedaylighttime="1" value="2">(GMT+02:00) Cairo</option>'
    + ' <option timezoneid="39" gmtadjustment="GMT+02:00" usedaylighttime="0" value="2">(GMT+02:00) Harare, Pretoria</option>'
    + ' <option timezoneid="40" gmtadjustment="GMT+02:00" usedaylighttime="1" value="2">(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius</option>'
    + ' <option timezoneid="41" gmtadjustment="GMT+02:00" usedaylighttime="1" value="2">(GMT+02:00) Jerusalem</option>'
    + ' <option timezoneid="42" gmtadjustment="GMT+02:00" usedaylighttime="1" value="2">(GMT+02:00) Minsk</option>'
    + ' <option timezoneid="43" gmtadjustment="GMT+02:00" usedaylighttime="1" value="2">(GMT+02:00) Windhoek</option>'
    + ' <option timezoneid="44" gmtadjustment="GMT+03:00" usedaylighttime="0" value="3">(GMT+03:00) Kuwait, Riyadh, Baghdad</option>'
    + ' <option timezoneid="45" gmtadjustment="GMT+03:00" usedaylighttime="1" value="3">(GMT+03:00) Moscow, St. Petersburg, Volgograd</option>'
    + ' <option timezoneid="46" gmtadjustment="GMT+03:00" usedaylighttime="0" value="3">(GMT+03:00) Nairobi</option>'
    + ' <option timezoneid="47" gmtadjustment="GMT+03:00" usedaylighttime="0" value="3">(GMT+03:00) Tbilisi</option>'
    + ' <option timezoneid="48" gmtadjustment="GMT+04:00" usedaylighttime="0" value="4">(GMT+04:00) Abu Dhabi, Muscat</option>'
    + ' <option timezoneid="49" gmtadjustment="GMT+04:00" usedaylighttime="1" value="4">(GMT+04:00) Baku</option>'
    + ' <option timezoneid="50" gmtadjustment="GMT+04:00" usedaylighttime="1" value="4">(GMT+04:00) Yerevan</option>'
    + ' <option timezoneid="51" gmtadjustment="GMT+05:00" usedaylighttime="1" value="5">(GMT+05:00) Yekaterinburg</option>'
    + ' <option timezoneid="52" gmtadjustment="GMT+05:00" usedaylighttime="0" value="5">(GMT+05:00) Islamabad, Karachi, Tashkent</option>'
    + ' <option timezoneid="53" gmtadjustment="GMT+05:00" usedaylighttime="0" value="5">(GMT+05:00) Chennai, Kolkata, Mumbai, New Delhi</option>'
    + ' <option timezoneid="54" gmtadjustment="GMT+06:00" usedaylighttime="0" value="6">(GMT+06:00) Kathmandu</option>'
    + ' <option timezoneid="55" gmtadjustment="GMT+06:00" usedaylighttime="1" value="6">(GMT+06:00) Almaty, Novosibirsk</option>'
    + ' <option timezoneid="56" gmtadjustment="GMT+06:00" usedaylighttime="0" value="6">(GMT+06:00) Astana, Dhaka</option>'
    + ' <option timezoneid="57" gmtadjustment="GMT+06:00" usedaylighttime="0" value="6">(GMT+06:00) Yangon (Rangoon)</option>'
    + ' <option timezoneid="58" gmtadjustment="GMT+07:00" usedaylighttime="0" value="7">(GMT+07:00) Bangkok, Hanoi, Jakarta</option>'
    + ' <option timezoneid="59" gmtadjustment="GMT+07:00" usedaylighttime="1" value="7">(GMT+07:00) Krasnoyarsk</option>'
    + ' <option timezoneid="60" gmtadjustment="GMT+08:00" usedaylighttime="0" value="8">(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>'
    + ' <option timezoneid="61" gmtadjustment="GMT+08:00" usedaylighttime="0" value="8">(GMT+08:00) Kuala Lumpur, Singapore</option>'
    + ' <option timezoneid="62" gmtadjustment="GMT+08:00" usedaylighttime="0" value="8">(GMT+08:00) Irkutsk, Ulaan Bataar</option>'
    + ' <option timezoneid="63" gmtadjustment="GMT+08:00" usedaylighttime="0" value="8">(GMT+08:00) Perth</option>'
    + ' <option timezoneid="64" gmtadjustment="GMT+08:00" usedaylighttime="0" value="8">(GMT+08:00) Taipei</option>'
    + ' <option timezoneid="65" gmtadjustment="GMT+09:00" usedaylighttime="0" value="9">(GMT+09:00) Osaka, Sapporo, Tokyo</option>'
    + ' <option timezoneid="66" gmtadjustment="GMT+09:00" usedaylighttime="0" value="9">(GMT+09:00) Seoul</option>'
    + ' <option timezoneid="67" gmtadjustment="GMT+09:00" usedaylighttime="1" value="9">(GMT+09:00) Yakutsk</option>'
    + ' <option timezoneid="68" gmtadjustment="GMT+09:00" usedaylighttime="0" value="9.">(GMT+09:00) Adelaide</option>'
    + ' <option timezoneid="69" gmtadjustment="GMT+09:00" usedaylighttime="0" value="9">(GMT+09:00) Darwin</option>'
    + ' <option timezoneid="70" gmtadjustment="GMT+10:00" usedaylighttime="0" value="10">(GMT+10:00) Brisbane</option>'
    + ' <option timezoneid="71" gmtadjustment="GMT+10:00" usedaylighttime="1" value="10">(GMT+10:00) Canberra, Melbourne, Sydney</option>'
    + ' <option timezoneid="72" gmtadjustment="GMT+10:00" usedaylighttime="1" value="10">(GMT+10:00) Hobart</option>'
    + ' <option timezoneid="73" gmtadjustment="GMT+10:00" usedaylighttime="0" value="10">(GMT+10:00) Guam, Port Moresby</option>'
    + ' <option timezoneid="74" gmtadjustment="GMT+10:00" usedaylighttime="1" value="10">(GMT+10:00) Vladivostok</option>'
    + ' <option timezoneid="75" gmtadjustment="GMT+11:00" usedaylighttime="1" value="11">(GMT+11:00) Magadan, Solomon Is., New Caledonia</option>'
    + ' <option timezoneid="76" gmtadjustment="GMT+12:00" usedaylighttime="1" value="12">(GMT+12:00) Auckland, Wellington</option>'
    + ' <option timezoneid="77" gmtadjustment="GMT+12:00" usedaylighttime="0" value="12">(GMT+12:00) Fiji, Kamchatka, Marshall Is.</option>'*/
+ '</select>';