Settings.smtpData = {};

Settings.callSmtp = function() {	
	$("div#settings div.form").html("");
	var html = '<div class="alert"></div>'
				+ '<ul class="element" id="smtpform" style="display:none;">'
				+	'<li id="smtpserver"><div class="left"></div><div class="right"><input type="text" id="serverInput"></div></li>'
				+	'<li id="smtpserverport"><div class="left"></div><div class="right"><input type="text" id="portInput"></div></li>'
				+	'<li id="email"><div class="left"></div><div class="right"><input type="text" id="emailInput"></div></li>'
				+	'<li id="loginname"><div class="left"></div><div class="right"><input type="text" id="loginNameInput"></div></li>'
				+	'<li id="password"><div class="left"></div><div class="right"><input type="password" id="passwordInput"></div></li>'
				+	'<li id="receiptenabled"><div class="left"></div><div class="right"><input type="checkbox" id="receiptenabledInput"></div></li>'
				+	'<li id="buttons"><input type="submit" id="save"><input type="submit" id="test"></li>'
				+ '</ul>'
				+ '<div id="windowEdit" style="width: 600px;" class="panelDiv">'
					+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="tableUpdate">'
					+ '	<tr>'
					+ '		<td id="modulsensorname" width="40%"></td>'
					+ '		<td id="voltaj" width="60%"></td>'
					+ '	</tr>'
					+ '<tr><td class="alert"></td></tr>'
					+ '</table>'
					+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="tableUpdateList">'
					+ '	<tr>'
					+ '		<td id="number" width="40%"></td>'
					+ '		<td width="60%"><input type=text id="number"></td>'
					+ '	</tr>'
					+ '</table>'
					+ '<div class="buttons">'
					+ '	<input type="submit" id="buttonTestEmail" name="buttonUpdate" class="submit">'
					+ '	<input type="submit" id="buttonCancel" name="buttonCancel" class="submit">'
					+ '</div>'
				+ '</div>';
	$("div#settings div.form").html(html);
	$("li#smtpserver div.left").html(language.Settings_smtp_server);
	$("li#smtpserverport div.left").html(language.Settings_smtp_serverport);
	$("li#email div.left").html(language.Settings_smtp_email);
	$("li#loginname div.left").html(language.Settings_smtp_loginname);
	$("li#password div.left").html(language.Settings_smtp_password);
	$("li#receiptenabled div.left").html(language.Settings_smtp_receipt_enabled);
	$("li#buttons input#save").val(language.Settings_smtp_save);
	$("li#buttons input#test").val(language.Settings_smtp_test);
	$("div#windowEdit td#number").html(language.Settings_smtp_email_test);
	$("div#windowEdit input#buttonTestEmail").val(language.test);
	$("div#windowEdit input#buttonCancel").val(language.cancel);
	$("div#settings ul.element").css("display","block");
	Settings.getSMTP();
};

Settings.getSMTP = function() {
	var data = $.getJSON("http://" + hostName + "/cgi-bin/notifymngrconf.cgi?getEmailSettings");	 
	data.success(function() {
		Settings.smtpData = jQuery.parseJSON(data.responseText);
		Settings.createSmtpForm();
		Main.unloading();
	});
	data.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Settings.postSmtp = function()
{
	Main.loading();
	json = JSON.stringify(Settings.smtpData);

	var url = "http://" + hostName + "/cgi-bin/notifymngrconf.cgi?updateEmailSettings";
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			//Settings.callSmtp();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.createSmtpForm = function() {
	var item = Settings.smtpData;

	var server = item.smtp_server;
	var port = item.smtp_port;
	var email = item.username;
	var loginname = item.loginname;
	var password = item.password;
	var receiptenabled = item.receipt_enabled;

	if(item.smtp_server == undefined) {
		server = "";
		port = "";
		email = "";
		loginname = "";
		password = "";
		receiptenabled = 0;
	}

	$("input#serverInput").val(server);
	$("input#portInput").val(port);
	$("input#emailInput").val(email);
	$("input#loginNameInput").val(loginname);
	$("input#passwordInput").val(password);
	if(parseInt(receiptenabled))
		$("input#receiptenabledInput").attr("checked",true);
	else
	    $("input#receiptenabledInput").attr("checked",false);		
};

Settings.validateSmtpForm = function(page) {	
	$("div#settings div.alert").html("");
	var alertHtml = $("div#settings div.alert").html();
	$("li#smtpserver").removeClass("alert");
	$("li#smtpserverport").removeClass("alert");
	$("li#email").removeClass("alert");
	$("li#loginname").removeClass("alert");
	$("li#password").removeClass("alert");

	var name      = $("li#smtpserver input").val();
	var port      = $("li#smtpserverport input").val();
	var email     = $("li#email input").val();
	var loginname = $("li#loginname input").val();
	var username  = $("li#username input").val();
	var password  = $("li#password input").val();
	
	if ($("li#receiptenabled input").prop('checked') == true)
		Settings.smtpData.receipt_enabled = "1";
	else if ($("li#receiptenabled input").prop('checked') == false)
		Settings.smtpData.receipt_enabled = "0";

	var isvalid = true;
	
	if (name.length == 0 && port.length == 0)
	{
		Settings.smtpData.smtp_server = "";
		Settings.smtpData.smtp_port = "";
		Settings.smtpData.username = "";
		Settings.smtpData.loginname = "";
		Settings.smtpData.password = "";
		Settings.smtpData.receipt_enabled = "0";
		return true;
	}	

	//validation of server name
	if(!Validator.validate( { "val" : name, "type" : "domain", "ismandatory" : "true", "obj" : $("li#smtpserver"), "objfocus" : $("li#smtpserver input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	else
		Settings.smtpData.smtp_server = name;

	//validation of server port
	if(!Validator.validate( { "val" : port, "type" : "port", "ismandatory" : "true", "obj" : $("li#smtpserverport"), "objfocus" : $("li#smtpserverport input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	else if(isvalid)
		Settings.smtpData.smtp_port = port;

	//validation of email address
	if(!Validator.validate( { "val" : email, "type" : "email", "ismandatory" : "true", "obj" : $("li#email"), "objfocus" : $("li#email input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	else if(isvalid)
		Settings.smtpData.username = email;
	
	//validation of loginname
	if(!Validator.validate( { "val" : loginname, "type" : "smtpuser", "ismandatory" : "false", "obj" : $("li#loginname"), "objfocus" : $("li#loginname input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	else if(isvalid)
		Settings.smtpData.loginname = loginname;
	
	//validation of password
	if(!Validator.validate( { "val" : password, "type" : "smtppassword", "ismandatory" : "false", "obj" : $("li#password"), "objfocus" : $("li#password input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	else if(isvalid)
		Settings.smtpData.password = password;

	return isvalid;
};


Settings.TestEmail = function(destination)
{
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/notifymngrconf.cgi?testMail&destination=" + destination;
	$.ajax({
	    url : url,
	    type: "POST",
	    dataType: "text",
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			if (data.toString() == "true")
	    		Main.alert(language.Settings_email_test_success);
	    	else
	    		Main.alert(language.Settings_email_test_fail + ":" + data.toString());

		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});

};

$(function() {
	$("div#settings").on( "click", "ul#smtpform input#save", function() {
		if(Settings.validateSmtpForm()) {
			Settings.postSmtp();
		}
	});

$("div#settings").on( "click", "ul#smtpform input#test", function() {
	$("div#overlay").css("display","block");
	$("div#windowEdit").css("display","block");
	$("div#windowEdit td.alert").html("");
	$("div#windowEdit td#modulsensorname").html(language.Settings_smtp_test);
	});


$("div#settings").on( "click", "div#windowEdit input#buttonTestEmail", function() {	$("div#windowEdit td.alert").html("");
	$("div#windowEdit td.alert").html("");
	$("div#windowEdit input#number").removeClass("alert");
	
	var isvalid = true;
	var email = $("div#windowEdit input#number").val();
	//validation of email address
	if(!Validator.validate( { "val" : email, "type" : "email", "ismandatory" : "true", "obj" : "div#windowEdit input#number", "page" : $("div#windowEdit .alert")})) 
		return;
	
	$("div#overlay").css("display","none");
	$("div#windowEdit").css("display","none");
	Settings.TestEmail($("div#windowEdit input#number").val());
});

$("div#settings").on( "click", "div#windowEdit input#buttonCancel", function() {
	$("div#windowEdit td.alert").html("");
	$("div#overlay").css("display","none");
	$("div#windowEdit").css("display","none");
});
});