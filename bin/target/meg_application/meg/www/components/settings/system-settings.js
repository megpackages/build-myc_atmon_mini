
Settings.callSystemSettings = function()
{
	$("div#settings div.form").html("");
	
	var html = '<div class="alert"></div>'
				+ '<ul class="element" id="systemsettingsform">'
				+	'<li id="downloadToComp">'
					+ '<input type="submit" class="submit" style="width: 590px;">'
					+ '<a style="display: none;"></a>'
				+ '</li>'
				+	'<li id="uploadFromComp">'
					+ '<input type="file" style="display: none;">'
					+ '<input type="submit" class="submit" style="width: 590px;">'
				+ '</li>'
				+	'<li id="backupToMMC">'
					+ '<input type="submit" class="submit" style="width: 590px;">'
				+ '</li>'
				+ '<li id="restoreDefaults">'
					+ '<input type="submit" class="submit" style="width: 590px;">'
				+ '</li>'
				+ '<li id="uploadToServer">'
					+ '<div class="left">'
						+ '<input type="submit" class="submit">'
					+ '</div>'
					+ '<div class="right">'
						+ '<input type="text">'
					+ '</div>'
				+ '</li>'
				+	'<li id="downloadToSPX">'
					+ '<div class="left">'
						+ '<input type="submit" class="submit">'
					+ '</div>'
					+ '<div class="right">'
						+ '<input type="text">'
					+ '</div>'
				+ '</li>'
				+	'<li id="ip">'
					+ '<div class="left"></div>'
					+ '<div class="right">'
						+ '<input type="text">'
					+ '</div>'
				+ '</li>'
				+	'<li id="port">'
					+ '<div class="left"></div>'
					+ '<div class="right">'
						+ '<input type="text">'
					+ '</div>'
				+ '</li>'
				+ '</li>'
				+	'<li id="root">'
					+ '<div class="left"></div>'
					+ '<div class="right">'
						+ '<input type="text">'
					+ '</div>'
				+ '</li>'
				+	'<li id="username">'
					+ '<div class="left"></div>'
					+ '<div class="right">'
						+ '<input type="text">'
					+ '</div>'
				+ '</li>'
				+	'<li id="password">'
					+ '<div class="left"></div>'
					+ '<div class="right">'
						+ '<input type="password">'
					+ '</div>'
				+ '</li>'
				+ '<li id="buttons">'
					+ '	<input type="submit" id="buttonSave" name="buttonSave" class="submit" style="float: left;">'
				+ '</li>'
				+ '</ul>'
				+ '<div id="restoreAlert" style="width: 400px;" class="panelDiv">'
				    + '<span class="text"></span>'
				    + '<div class="buttons">'
				        + ' <input type="submit" id="buttonRestore" name="buttonRestore" class="submit">'
				        + ' <input type="submit" id="buttonCancel" name="buttonCancel" class="submit">'
				    + '</div>'
				+ '</div>';
	
	$("div#settings div.form").html(html);

	$("div#settings div.form ul#systemsettingsform li#downloadToComp input[type='submit']").val(language.Settings_systemsettings_downloadtocomp);
	$("div#settings div.form ul#systemsettingsform li#uploadFromComp input[type='submit']").val(language.Settings_systemsettings_uploadfromcomp);
	$("div#settings div.form ul#systemsettingsform li#backupToMMC input[type='submit']").val(language.Settings_systemsettings_backuptommc);
    $("div#settings div.form ul#systemsettingsform li#restoreDefaults input[type='submit']").val(language.Settings_systemsettings_restoredefaults);
	$("div#settings div.form ul#systemsettingsform li#downloadToSPX input[type='submit']").val(language.Settings_systemsettings_downloadtospx);
	$("div#settings div.form ul#systemsettingsform li#uploadToServer input[type='submit']").val(language.Settings_systemsettings_uploadtoserver);
	$("div#settings div.form ul#systemsettingsform li#ip div.left").html(language.Settings_systemsettings_ip);
	$("div#settings div.form ul#systemsettingsform li#port div.left").html(language.Settings_systemsettings_port);
	$("div#settings div.form ul#systemsettingsform li#root div.left").html(language.Settings_systemsettings_root);
	$("div#settings div.form ul#systemsettingsform li#username div.left").html(language.Settings_systemsettings_username);
	$("div#settings div.form ul#systemsettingsform li#password div.left").html(language.Settings_systemsettings_password);
	$("div#settings div.form ul#systemsettingsform li#buttons input#buttonSave").val(language.save);
	$("div#settings div.form div#restoreAlert input#buttonRestore").val(language.restoredefault);
	$("div#settings div.form div#restoreAlert input#buttonCancel").val(language.cancel);
	$("div#settings div.form div#restoreAlert span.text").html(language.areyousurerestoredefaults);

	Settings.getSystemSettings();
};

Settings.getSystemSettings = function()
{
	Main.loading();
	var data = $.getJSON("http://" + hostName + "/cgi-bin/systemsettings.cgi?getSystemSettings");	 
	data.success(function() {
		Settings.SystemSettings = jQuery.parseJSON(data.responseText);
		Settings.createSystemSettings();
		Main.unloading();
	});
	data.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Settings.createSystemSettings = function()
{
	$("div#settings div.form ul#systemsettingsform li#ip input").val(Settings.SystemSettings.ftp_ip != undefined ? Settings.SystemSettings.ftp_ip: "");
	$("div#settings div.form ul#systemsettingsform li#port input").val(Settings.SystemSettings.ftp_port != undefined ? Settings.SystemSettings.ftp_port: "");
	$("div#settings div.form ul#systemsettingsform li#root input").val(Settings.SystemSettings.ftp_root != undefined ? Settings.SystemSettings.ftp_root: "");
	$("div#settings div.form ul#systemsettingsform li#username input").val(Settings.SystemSettings.ftp_username != undefined ? Settings.SystemSettings.ftp_username: "");
	$("div#settings div.form ul#systemsettingsform li#password input").val(Settings.SystemSettings.ftp_password != undefined ? Settings.SystemSettings.ftp_password: "");
};

Settings.validateSystemSettingsForm = function(json)
{
	$("div#settings div.alert").html("");
	$("div#settings div.form ul#systemsettingsform li").removeClass("alert");
	$("div#settings div.form ul#systemsettingsform li input").removeClass("alert");
	$("div#settings div.form ul#systemsettingsform").removeClass("alert");

	var ip       = $("div#settings div.form ul#systemsettingsform li#ip input").val();
	var port 	 = $("div#settings div.form ul#systemsettingsform li#port input").val();
	var root 	 = $("div#settings div.form ul#systemsettingsform li#root input").val();
	var username = $("div#settings div.form ul#systemsettingsform li#username input").val();
	var password = $("div#settings div.form ul#systemsettingsform li#password input").val();

	var isvalid = true;

	//validation of ip
	if(!Validator.validate( { "val" : ip, "type" : "domain", "ismandatory" : "true", "obj" : $("li#ip"), "objfocus" : $("li#ip input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	else
		json.ftp_ip = ip;
	
	if(port.length == 0 )
	{
		json.ftp_port 	  = "21";	
	}
	else
	{
		//validation of port
		if(!Validator.validate( { "val" : port, "type" : "port", "ismandatory" : "false", "obj" : $("li#port"), "objfocus" : $("li#port input"), "page" : $("div#settings div.alert")}))
			isvalid = false;
		else
			json.ftp_port = port;	
	}
	
	if(root.length == 0 )
	{
		json.ftp_root 	  = "";	
	}
	else
	{
		//validation of root
		if(!Validator.validate( { "val" : root, "type" : "url", "ismandatory" : "false", "obj" : $("li#root"), "objfocus" : $("li#root input"), "page" : $("div#settings div.alert")}))
			isvalid = false;
		else
			json.ftp_root = root;	
	}

	if(username.length == 0 )
	{
		json.ftp_username 	  = "";	
	}
	else
	{
		//validation of username
		if(!Validator.validate( { "val" : username, "type" : "ftpusername", "ismandatory" : "false", "obj" : $("li#username"), "objfocus" : $("li#username input"), "page" : $("div#settings div.alert")}))
			isvalid = false;
		else
			json.ftp_username = username;	
	}
	
	if(password.length == 0 )
	{
		json.ftp_password 	  = "";	
	}
	else
	{
		//validation of password
		if(!Validator.validate( { "val" : password, "type" : "ftppassword", "ismandatory" : "false", "obj" : $("li#password"), "objfocus" : $("li#password input"), "page" : $("div#settings div.alert")}))
			isvalid = false;
		else
			json.ftp_password = password;	
	}

	return isvalid;
};

Settings.updateSystemSettings = function(info)
{
	Main.loading();
	json = JSON.stringify(info);
	var url = "http://" + hostName + "/cgi-bin/systemsettings.cgi?updateSystemSettings";
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(Data, textStatus, jqXHR)
	    {
	    	Main.unloading();
	    	Json = jQuery.parseJSON(Data
	    		);
	    	if (Json["return"] == "true")
				Main.alert(language.Settings_systemsettings_update_success);
			else
				Main.alert(language.Settings_systemsettings_update_failed + ": " + Json["return"]);

		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.downloadToSPX = function(fileName)
{
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/systemsettings.cgi?downloadToSPX&filename=" + fileName;
	$.ajax({
	    url : url,
	    type: "POST",
        dataType: 'text',
	    success: function(Data, textStatus, jqXHR)
	    {
	    	Main.unloading();
	    	Json = jQuery.parseJSON(Data);
	    	if (Json["return"] == "true")
				Main.alert(language.Settings_configupdatefromftp_success);
			else
				Main.alert(language.Settings_configupdatefromftp_failed + ": " + Json["return"]);

		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.downloadToComp = function(fileName)
{
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/systemsettings.cgi?downloadToComp&filename=" + fileName;
	$.ajax({
	    url : url,
	    type: "POST",
        dataType: 'text',
        contentType: "text",
	    success: function(Data, textStatus, jqXHR)
	    {
	    	Main.unloading();
	    	Json = jQuery.parseJSON(Data);
	    	if (Json["return"] == "true")
	    	{
				$("li#downloadToComp a").attr("href","/SPXconfig.sqlite");
				$("li#downloadToComp a")[0].click();

	    	}
			else
				Main.alert(language.Settings_downloadtocomp_failed + ": " + Json["return"]);

		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.uploadFromComp = function(image)
{
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/systemsettings.cgi?uploadFromComp";
	$.ajax({
	    url : url,
	    type: "POST",
	    data: image,
        contentType: 'application/octet-stream',
        dataType: "text",
	    success: function(Data, textStatus, jqXHR)
	    {
	    	Main.unloading();
	    	Json = jQuery.parseJSON(Data);
	    	if (Json["return"] != "true")
	    		Main.alert(language.Settings_configupdatefromcomp_failed + ": " + Json["return"]);
	    	else
	    		Main.alert(language.Settings_configupdatefromcomp_success);
	    		

		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.uploadToServer = function(filename)
{
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/systemsettings.cgi?uploadToServer&filename=" + filename;
	$.ajax({
	    url : url,
	    type: "POST",
        dataType: 'text',
        contentType: "application/octet-stream",
	    success: function(Data, textStatus, jqXHR)
	    {
	    	Main.unloading();
	    	Json = jQuery.parseJSON(Data);
	    	if (Json["return"] == "true")
				Main.alert(language.Settings_uploadconfigtoserver_success);
			else
				Main.alert(language.Settings_uploadconfigtoserver_failed + ": " + Json["return"]);

		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.backupToMMC = function()
{
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/systemsettings.cgi?backupToMMC";
	$.ajax({
	    url : url,
	    type: "POST",
        dataType: 'text',
	    success: function(Data, textStatus, jqXHR)
	    {
	    	Main.unloading();
	    	Json = jQuery.parseJSON(Data);
	    	if (Json["return"] == "true")
				Main.alert(language.Settings_backuptommc_success);
			else
				Main.alert(language.Settings_backuptommc_failed + ": " + Json["return"]);

		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.restoreDefaults = function()
{
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/systemsettings.cgi?restoreDefaults";
	$.ajax({
	    url : url,
	    type: "POST",
        dataType: 'text',
	    success: function(Data, textStatus, jqXHR)
	    {
	    	Main.unloading();
	    	Json = jQuery.parseJSON(Data);
	    	if (Json["return"] == "true")
				Main.alert(language.Settings_restoredefaults_success);
			else
				Main.alert(language.Settings_restoredefaults_failed + ": " + Json["return"]);

		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

$(
	function()
	{
		$("div#settings").on("click", "div.form ul#systemsettingsform li#buttons input#buttonSave", function()
		{
			var json = {};
			if (Settings.validateSystemSettingsForm(json))
			{
				Settings.updateSystemSettings(json);
			}
		});

		$("div#settings").on("click", "div.form ul#systemsettingsform li#downloadToSPX input[type='submit']", function()
		{
			var fileName = $("div.form ul#systemsettingsform li#downloadToSPX input[type='text']").val();
			Settings.downloadToSPX(fileName);
		});

		$("div#settings").on("click", "div.form ul#systemsettingsform li#downloadToComp input[type='submit']", function()
		{
			var fileName = $("div.form ul#systemsettingsform li#downloadToComp input[type='text']").val();
			Settings.downloadToComp(fileName);
		});

		$("div#settings").on("click", "div.form ul#systemsettingsform li#uploadFromComp>input[type='submit']", function()
		{
			$("li#uploadFromComp input[type='file']")[0].click();
		});

		$("div#settings").on("change", "div.form ul#systemsettingsform li#uploadFromComp input[type='file']", function()
		{
			var file = $("li#uploadFromComp input[type='file']")[0].files[0]; //Files[0] = 1st file
			var reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = shipOff;

			function shipOff(event) {
			    var result = event.target.result;
			    var fileName = $("li#uploadFromComp input[type='file']")[0].files[0].name; //Should be 'picture.jpg'
			    Settings.uploadFromComp(result);
			}
		});

		$("div#settings").on("click", "div.form ul#systemsettingsform li#backupToMMC input[type='submit']", function()
		{
			Settings.backupToMMC();
		});

		$("div#settings").on("click", "div.form ul#systemsettingsform li#restoreDefaults input[type='submit']", function()
		{
			$("div#restoreAlert").css("display", "block");
			$("div#overlay").css("display", "block");
		});
		
		$("div#settings").on("click", "div#restoreAlert input#buttonRestore", function()
		{
			$("div#restoreAlert").css("display", "none");
			$("div#overlay").css("display", "none");
			Settings.restoreDefaults();
		});
		
		$("div#settings").on("click", "div#restoreAlert input#buttonCancel", function()
		{
			$("div#restoreAlert").css("display", "none");
			$("div#overlay").css("display", "none");
		});

		$("div#settings").on("click", "div.form ul#systemsettingsform li#uploadToServer input[type='submit']", function()
		{
			var fileName = $("div.form ul#systemsettingsform li#uploadToServer input[type='text']").val();
			Settings.uploadToServer(fileName);
		});


	}
);