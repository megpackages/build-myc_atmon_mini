var Rules = {
	onLoad: null,
	close: null,
	html: null,
	data : null,
	sensors: [],
	modules: [],
	physicalVar: [],
	physicalOldVar : [],
	logicalVar: [],
	logicalOldVar : [],
	rulesVar : [],
	tabs: [
		{
			label : "rulesItemListTitle",
			status: true,
			func : "getRulesList();"
		},
		{
			label : "rules_tab_newrules",
			status: false,
			func : "getNewRules();"
		}
	],
	date : [
		{
			name: "day1",
			value: 1
		},
		{
			name: "day2",
			value: 2
		},
		{
			name: "day3",
			value: 3
		},
		{
			name: "day4",
			value: 4
		},
		{
			name: "day5",
			value: 5
		},
		{
			name: "day6",
			value: 6
		},
		{
			name: "day0",
			value: 0
		},
		{
			name: "day7",
			value: 9
		},
		{
			name: "day8",
			value: 7
		},
		{
			name: "day9",
			value: 8
		}
	],
	hours : [
		{
			name: "hours0",
			value: 26
		},
		{
			name: "hours1",
			value: 24
		},
		{
			name: "hours2",
			value: 25
		},
		{
			name: "hours3",
			value: 0
		},
		{
			name: "hours4",
			value: 1
		},
		{
			name: "hours5",
			value: 2
		},
		{
			name: "hours6",
			value: 3
		},
		{
			name: "hours7",
			value: 4
		},
		{
			name: "hours8",
			value: 5
		},
		{
			name: "hours9",
			value: 6
		},
		{
			name: "hours10",
			value: 7
		},
		{
			name: "hours11",
			value: 8
		},
		{
			name: "hours12",
			value: 9
		},
		{
			name: "hours13",
			value: 10
		},
		{
			name: "hours14",
			value: 11
		},
		{
			name: "hours15",
			value: 12
		},
		{
			name: "hours16",
			value: 13
		},
		{
			name: "hours17",
			value: 14
		},
		{
			name: "hours18",
			value: 15
		},
		{
			name: "hours19",
			value: 16
		},
		{
			name: "hours20",
			value: 17
		},
		{
			name: "hours21",
			value: 18
		},
		{
			name: "hours22",
			value: 19
		},
		{
			name: "hours23",
			value: 20
		},
		{
			name: "hours24",
			value: 21
		},
		{
			name: "hours25",
			value: 22
		},
		{
			name: "hours26",
			value: 23
		},
	]



};

Rules.html = 	'<ul class="tabs"></ul>'
				+'<div class="tab" id="rulesform">'
				+ '</div>'
				+ '<div id="ruleslist" class="block">'
					+ '<div class="list" id="rulesItemList"></div>'
				+ '</div>'
				+ '<div id="rulesWindow" style="width: 600px;" class="panelDiv">'
					+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="head">'
					+ '</table>'
					+ '<div class="masked">'
						+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="list">'
						+ '</table>'
					+ '</div>'
					+ '<div class="buttons">'
					+ '	<input type="submit" id="buttonSave" name="buttonSave" class="submit">'
					+ '	<input type="submit" id="buttonCancel" name="buttonCancel" class="submit">'
					+ '</div>'
				+ '</div>'
				+ '<div id="alertDelete" style="width: 400px;" class="panelDiv">'
					+ '<span class="text"></span>'
					+ '<div class="buttons">'
					+ '	<input type="submit" id="buttonRemove" name="buttonRemove" class="submit">'
					+ '	<input type="submit" id="buttonClose" name="buttonClose" class="submit">'
					+ '</div>'
				+ '</div>';

Rules.onLoad = function() {
	Main.loading();
	$("div#rules").html(Rules.html);
	$("div#rules").css("display","block");
	Rules.createTabs();
	Rules.tabOpen();
	//Rules.getList();
};

Rules.close = function() {
	Main.loading();
	$("div#rules").html("");
	$("div#rules").css("display","none");
};

Rules.createTabs = function() {
	for(var i = 0; i < Rules.tabs.length; i++) {
		var li = document.createElement("li");
		if(Rules.tabs[i].status) {
			li.setAttribute("class","passive active");	
		} else {
			li.setAttribute("class","passive");			
		}

		li.setAttribute("data-func", Rules.tabs[i].func);	
		li.innerHTML = eval("language." + Rules.tabs[i].label);

		$("div#rules ul.tabs").append(li);
	}
};

Rules.tabOpen = function() {
	for(var i = 0; i < Rules.tabs.length; i++) {
		if(Rules.tabs[i].status) {
			eval("Rules." + Rules.tabs[i].func);
			Rules.currentTab = Rules.tabs[i].type;
		}
	}
};

Rules.getRulesList = function() {
	Rules.rulesVar = [];
	Rules.physicalVar.length = 0;
	Rules.physicalOldVar.length = 0;
	Rules.logicalVar.length = 0;
	Rules.logicalOldVar.length = 0;
	$("div#rules div#ruleslist div#rulesItemList").html("");
	$("div#rules div#ruleslist").css("display","block");
	$("div#rules div#rulesform").html("");
	$("div#rules div#rulesform").css("display","none");
	Rules.getList();
};

Rules.getNewRules = function() {
	Main.unloading();
	Rules.rulesVar = [];
	Rules.physicalVar.length = 0;
	Rules.physicalOldVar.length = 0;
	Rules.logicalVar.length = 0;
	Rules.logicalOldVar.length = 0;
	$("div#rules div#ruleslist div#rulesItemList").html("");
	$("div#rules div#ruleslist").css("display","none");
	var html = '<div id="daterules" class="block"></div>'
					+ '<div id="sensorrules" class="block"></div>'
					+ '<div id="listrules" class="block">'
						+ '<span class="text" id="ruleslisttitle"></span>'
						+ '<div class="list" id="rulesitemlist"></div>'
						+ '<div class="save" id="saveForm"></div>'
					+ '</div>';
	$("div#rules div#rulesform").html(html);
	$("div#rules div#rulesform").css("display","block");
	Rules.createPage();
};

Rules.getEditRules = function(item) {
	Main.unloading();
	Rules.physicalVar.length = 0;
	Rules.physicalOldVar.length = 0;
	Rules.logicalVar.length = 0;
	Rules.logicalOldVar.length = 0;

	$("div#rules div#ruleslist div#rulesItemList").html("");
	$("div#rules div#ruleslist").css("display","none");
	var html = '<div id="daterules" class="block"></div>'
					+ '<div id="sensorrules" class="block"></div>'
					+ '<div id="listrules" class="block">'
						+ '<span class="text" id="ruleslisttitle"></span>'
						+ '<div class="list" id="rulesitemlist"></div>'
						+ '<div class="save" id="saveForm"></div>'
					+ '</div>'
					+ '<div class="block" id="backForm"></div>';
	$("div#rules div#rulesform").html(html);
	$("div#rules div#rulesform").css("display","block");
	Rules.createPage(item);
};

Rules.getRuleItem = function(id) {
	var ruleData = $.getJSON("http://" + hostName + "/cgi-bin/rulemanager.cgi?getRule&id=" + id);		 
	ruleData.success(function() {
		var item = jQuery.parseJSON(ruleData.responseText);
		Rules.getEditRules(item);
		Main.unloading();
	});
	ruleData.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Rules.getList = function () {
	var rulesData = $.getJSON("http://" + hostName + "/cgi-bin/rulemanager.cgi?getRules");		 
	rulesData.success(function() {
		Rules.data = jQuery.parseJSON(rulesData.responseText);
		$("div#rules div#ruleslist div#rulesItemList").html("");
		for(var i = 0; i < Rules.data.length; i++) {
			Rules.createRules(Rules.data[i]);
		}

		if(Rules.data.length == 0) {
			var html = "<div class='alert'>" + language.rules_norecord +"</div>";
			$("div#rules div#ruleslist div#rulesItemList").html(html);
		}

		Main.unloading();
	});
	rulesData.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Rules.getModules = function() {
	Main.loading();
	var getModules = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?getDevices");	 
	getModules.success(function() {
		Main.unloading();
		Rules.modules = jQuery.parseJSON(getModules.responseText);
		Rules.createModules();
	});	
	getModules.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Rules.createRules = function(item) {

	$("div#rules div#ruleslist span#rulesItemListTitle").html(language.rulesItemListTitle);

	var id = item.id;
	var connector = item.connector;
	var name = item.name;

	var divTr = document.createElement("div");
	divTr.setAttribute("class","tr");
	var span0 = document.createElement("span");
	span0.setAttribute("class", "left name");
	span0.setAttribute("id", "rule"+id);
	span0.setAttribute("data-id", id);
	span0.innerHTML = name;
	var span1 = document.createElement("span");
	span1.setAttribute("class","left");
	span1.innerHTML = Rules.getConnectorName(connector);
	var span2 = document.createElement("span");
	span2.setAttribute("class","right");
	span2.setAttribute("id",id);

	divTr.appendChild(span0);
	divTr.appendChild(span1);
	divTr.appendChild(span2);

	$("div#rules div#ruleslist div#rulesItemList").append(divTr);

};

Rules.getConnectorName = function(item) {
	switch(item) {
		case 0:
		case "0":
			item = language.rulesifanyrules;
		break;
		case 1:
		case "1":
			item = language.rulesifallrules;
		break;
	}
	return item;
};

Rules.createModules = function() {
	var span = document.createElement("span");
	span.setAttribute("class","text");
	span.setAttribute("id","rulesmodulestitle");
	span.innerHTML = language.rulesmodulestitle;

	var selectModules = document.createElement("select");
	selectModules.setAttribute("class","selectmodules"); 
	selectModules.setAttribute("id","selectModules");

	var option0 = document.createElement("option");
	option0.setAttribute("value", "WEB.RULES.MODULESLCT");
	option0.innerHTML = language.rulesdevicesselect;

	var option1 = document.createElement("option");
	option1.setAttribute("value", "0");
	option1.setAttribute("data-id", "0");
	option1.innerHTML = language.rulesanydevicessselect;

	var option2 = document.createElement("option");
	option2.setAttribute("value", "-1");
	option2.setAttribute("data-id", "-1");
	option2.innerHTML = language.rulesalldevicessselect;

	selectModules.appendChild(option0);
	selectModules.appendChild(option1);
	selectModules.appendChild(option2);

	$("div#rules div#sensorrules").append(span);

	for(var i = 0; i < Rules.modules.length; i++) {
		var item = Rules.modules[i];
		var name = item.name;
		var objectid = item.objectid;
		var type = item.type;
		var id = item.id;

		var option = document.createElement("option");
		option.setAttribute("value", id);
		option.setAttribute("data-id", objectid);
		option.innerHTML = name + " - " + objectid;

		selectModules.appendChild(option);
	}

	$("div#rules div#sensorrules").append(selectModules);
};

Rules.createSensors = function(page) {

	var div = document.getElementById('sensorrules');
	var select = document.getElementById('selectSensors');
	var button = document.getElementById('buttonModules');
	var status = document.getElementById('selectStatus');

	if(select != null) {
	 	div.removeChild(select);  
	 	div.removeChild(status);  
	 	div.removeChild(button);  
	}

	if(Rules.sensors.length > 0 || page == "all" || page == "any") {
		var selectSensors = document.createElement("select");
		selectSensors.setAttribute("class","selectsensors"); 
		selectSensors.setAttribute("id","selectSensors");
		if (page == undefined)
		{
			var option1 = document.createElement("option");
			option1.setAttribute("value", "0");
			option1.innerHTML = language.rulessensorsany;
			selectSensors.appendChild(option1);
			var option2 = document.createElement("option");
			option2.setAttribute("value", "-1");
			option2.innerHTML = language.rulessensorsall;
			selectSensors.appendChild(option2);
		}
		else if (page == "all")
		{
			var option2 = document.createElement("option");
			option2.setAttribute("value", "-1");
			option2.innerHTML = language.rulessensorsall;
			selectSensors.appendChild(option2);
		}
		else if (page == "any")
		{
			var option1 = document.createElement("option");
			option1.setAttribute("value", "0");
			option1.innerHTML = language.rulessensorsany;
			selectSensors.appendChild(option1);
		}

		for(var i = 0; i < Rules.sensors.length; i++) {
			var item = Rules.sensors[i];
			var name = item.name;
			var objectid = item.objectid;
			var type = item.type;
			var id = item.id;

			var option = document.createElement("option");
			option.setAttribute("value", id);
			option.innerHTML = name;

			selectSensors.appendChild(option);
		}

		var selectStatus = document.createElement("select");
		selectStatus.setAttribute("class","selectstatus"); 
		selectStatus.setAttribute("id","selectStatus");

		var option0 = document.createElement("option");
		option0.setAttribute("value", 4);
		option0.innerHTML = language.alarm;

		var option1 = document.createElement("option");
		option1.setAttribute("value", 2);
		option1.innerHTML = language.warning;

		var option2 = document.createElement("option");
		option2.setAttribute("value", 1);
		option2.innerHTML = language.normal;

		var option3 = document.createElement("option");
		option3.setAttribute("value", 16);
		option3.innerHTML = language.inactive;

		selectStatus.appendChild(option0);
		selectStatus.appendChild(option1);
		selectStatus.appendChild(option2);
		selectStatus.appendChild(option3);

		var button = document.createElement("input");
		button.setAttribute("type","submit");
		button.setAttribute("class","sensorinput");
		button.setAttribute("id","buttonModules");
		button.setAttribute("value",language.rulessensorsinput);

		$("div#rules div#sensorrules").append(selectSensors);
		$("div#rules div#sensorrules").append(selectStatus);
		$("div#rules div#sensorrules").append(button);
	}
};

Rules.createPage = function(item) {
	var span = document.createElement("span");
	span.setAttribute("class","text");
	span.setAttribute("id","rulestimeinput");
	span.innerHTML = language.rulestimeinput;

	$("div#rules div#listrules span#ruleslisttitle").html(language.ruleslisttitle);

	var selectDateTime = document.createElement("select"); 
	selectDateTime.setAttribute("class","datetime"); 
	selectDateTime.setAttribute("id","selectDateTime");
	var option0 = document.createElement("option");
	option0.innerHTML = language.rulesdateselect;
	var option1 = document.createElement("option");
	option1.setAttribute("value", 0);
	option1.innerHTML = language.rulesday;
	var option2 = document.createElement("option");
	option2.setAttribute("value", 1);
	option2.innerHTML = language.rulestime;

	selectDateTime.appendChild(option0);
	selectDateTime.appendChild(option1);
	selectDateTime.appendChild(option2);

	$("div#rules div#daterules").append(span);
	$("div#rules div#daterules").append(selectDateTime);

	Rules.createRecordForm(item);

	Rules.getModules();

	if(item!= undefined) {
		Main.loading();
		var a = document.createElement("a");
		a.setAttribute("class","back");
		a.innerHTML = language.rules_backlist;
		$("div#rules div#backForm").append(a);
		var ruleData = $.getJSON("http://" + hostName + "/cgi-bin/rulemanager.cgi?getSubrules&ruleid=" + item.id);		 
		ruleData.success(function() {
			var items = jQuery.parseJSON(ruleData.responseText);
			$("div#rules div.block span.left div.physcal").attr("data-id", item.id);
			$("div#rules div.block span.left div.logical").attr("data-id", item.id);
			$("div#rules div.block input#rulesupdate").attr("data-id", item.id);

			for(var i = 0; i < items.length; i++) {
				var page = "period";
				var item1 = "";
				var item2 = "";
				var status = "";
				var inverted = "";

				if(items[i].sensorid != undefined) {
					page = "sensor";
					items[i].tip = 1;

					if(items[i].deviceid == 0)
						item1 = language.rulesanydevicessselect;
					else if (items[i].deviceid == -1)
						item1 = language.rulesalldevicessselect;
					else
						item1 = items[i].devicename + " - " + items[i].deviceoid;


					if(items[i].sensorid == 0)
						item2 = language.rulessensorsany;
					else if (items[i].sensorid == -1)
						item2 = language.rulessensorsall;
					else
						item2 = items[i].sensorname;
					

					status = parseInt(items[i].status);
					inverted = items[i].inverted;
				} else {
					items[i].tip = 0;
					item1 = items[i].type;
					item2 = items[i].value;
					inverted = items[i].inverted;

					if(item1 == 0) {
						item1 = language.rulesday;
						for(var k = 0; k < Rules.date.length; k++) {
							if(item2 == Rules.date[k].value) {
								item2 = eval("language." + Rules.date[k].name);
							}
						}
					} else {
						item1 = language.rulestime;
						for(var k = 0; k < Rules.hours.length; k++) {
							if(item2 == Rules.hours[k].value) {
								item2 = eval("language." + Rules.hours[k].name);
							}
						}
					}
				}

				Rules.createRuleElement(page, item1, item2, status, inverted);
			}

			Rules.rulesVar = items;

			Main.unloading();
		});
		ruleData.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
		});
	}
};

Rules.createRecordForm = function(item) {

	var name = "";
	var connector = 1;

	if(item != undefined) {
		name = item.name;
		connector = parseInt(item.connector);
	}

	var span0 = document.createElement("span");
	span0.setAttribute("class", "left");
	var span1 = document.createElement("span");
	span1.setAttribute("class", "left");
	var span2 = document.createElement("span");
	span2.setAttribute("class", "left");
	var span3 = document.createElement("span");
	span3.setAttribute("class", "right");

	var selectIf = document.createElement("select"); 
	selectIf.setAttribute("class","datetime"); 
	selectIf.setAttribute("id","selectIf");
	var option0 = document.createElement("option");
	option0.setAttribute("value",1);
	option0.innerHTML = language.rulesifallrules;
	var option1 = document.createElement("option");
	option1.setAttribute("value",0);
	option1.innerHTML = language.rulesifanyrules;

	if(connector == 1) {
		option0.setAttribute("selected","selected");
	} else {
		option1.setAttribute("selected","selected");
	}

	selectIf.appendChild(option0);
	selectIf.appendChild(option1);

	span0.appendChild(selectIf);

	var div0 = document.createElement("div");
	div0.setAttribute("class","button physcal");
	div0.innerHTML = language.rulesphyscl;

	var div1 = document.createElement("div");
	div1.setAttribute("class","button logical");
	div1.innerHTML = language.ruleslogcl2;
	
	span1.appendChild(div0);
	span1.appendChild(div1);


	var inputText = document.createElement("input");
	inputText.setAttribute("type", "text");
	inputText.setAttribute("id", "rulesname");
	inputText.setAttribute("value", name);
	inputText.setAttribute("placeholder", language.rules_name);

	span2.appendChild(inputText);

	var inputButton = document.createElement("input");
	inputButton.setAttribute("type", "submit");
	if(item != undefined) {
		inputButton.setAttribute("id", "rulesupdate");
		inputButton.setAttribute("value", language.update);
	} else {
		inputButton.setAttribute("id", "rulessave");
		inputButton.setAttribute("value", language.save);
	}

	span3.appendChild(inputButton);

	$("div#rules div#listrules div#saveForm").append(span0);
	$("div#rules div#listrules div#saveForm").append(span1);
	$("div#rules div#listrules div#saveForm").append(span2);
	$("div#rules div#listrules div#saveForm").append(span3);

};

Rules.selectDateTime = function(page) {

	var div = document.getElementById('daterules');
	var select = document.getElementById('selectTime');
	var button = document.getElementById('buttonTime');
	if(select != null) {
	 	div.removeChild(select);  
	 	div.removeChild(button);  
	}

	switch(page) {
		case language.rulesday:
			var selectDay = document.createElement("select"); 
			selectDay.setAttribute("class","day"); 
			selectDay.setAttribute("id","selectTime");

			for(var i = 0; i < Rules.date.length; i++) {
				var option = document.createElement("option");
				option.setAttribute("value", Rules.date[i].value);
				option.innerHTML = eval("language." + Rules.date[i].name);
				selectDay.appendChild(option);
			}

			$("div#rules div#daterules").append(selectDay);
			Rules.createSelectDateTimeButton();
		break;
		case language.rulestime:
			var selectTime = document.createElement("select"); 
			selectTime.setAttribute("class","day"); 
			selectTime.setAttribute("id","selectTime");

			for(var i = 0; i < Rules.hours.length; i++) {
				var option = document.createElement("option");
				option.setAttribute("value", Rules.hours[i].value);
				option.innerHTML = eval("language." + Rules.hours[i].name);
				selectTime.appendChild(option);
			}

			$("div#rules div#daterules").append(selectTime);
			Rules.createSelectDateTimeButton();

		break;
	}
};

Rules.sortSensors = function()
{
	var tmpList = [];
	var len = Rules.sensors.length;
	for (var i = 0; i < len; i++)
	{
		tmpList[parseInt(Rules.sensors[i].ionum)] = Rules.sensors[i];
	}

	var tmpList2 = []
	var k = 0;
	for (var i = 0; i < tmpList.length; i++)
	{
		if (tmpList[i] != undefined)
			tmpList2[k++] = tmpList[i]	
	}

	Rules.sensors = tmpList2;
};

Rules.selectDeviceSensors = function(objectid) {
	var id = objectid.split("/");
	var url = "getSensors&hwaddr="+ id[0] + "&portnum="+ id[1] +"&devaddr="+ id[2];

	Main.loading();
	var getItem = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?"+ url);	 
	getItem.success(function() {
		Main.unloading();
		Rules.sensors = [];
		count = 0;
		var items = jQuery.parseJSON(getItem.responseText);
		for(var i = 0; i < items.length; i++) {
			if( items[i].type != 7 && items[i].type != 3) {
				Rules.sensors[count] = items[i];
				count++;
			}
		}
		Rules.sortSensors();
		Rules.createSensors();
	});	
	getItem.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});

};

Rules.createSelectDateTimeButton = function() {
	var button = document.createElement("input");
	button.setAttribute("type","submit");
	button.setAttribute("class","timeinput");
	button.setAttribute("id","buttonTime");
	button.setAttribute("value",language.rulestimeinput);
	$("div#rules div#daterules").append(button);
};

Rules.createRuleElement = function(page, item1, item2, status, inverted) {

	var index = $("div#rules div#listrules div.list").children("div.tr").each(function () {}).length;
	var value = true;

	$("div#rules div#listrules div.list").children("div.tr").each(function () {

		var data = $(this).attr("data");

		switch(page){
			case "period":
				if(data == item2) {
					value = false;
				}
			break;
			case "sensor":
				var checkdata = item1 + item2 + status;
				if(data == checkdata) {
					value = false;
				}
			break;
		}
	});

	if(value) {
		var divTr 	= document.createElement("div");
		divTr.setAttribute("class","tr");
		divTr.setAttribute("id", "rule" + index);
		switch(page){
			case "period":
				divTr.setAttribute("data", item2);
				var data1 = $("select#selectDateTime option:selected").val();
				var data2 = $("select#selectTime option:selected").val();
				if(Rules.rulesVar[index] == undefined) {
					Rules.rulesVar[index] = {tip: "0", type: data1, value: data2, inverted: 0};
				}
			break;
			case "sensor":
				var data1 = $("select#selectModules option:selected").val();
				var data2 = $("select#selectSensors option:selected").val();
				var data3 = $("select#selectStatus option:selected").val();
				divTr.setAttribute("data", item1 + item2 + status);
				if(Rules.rulesVar[index] == undefined) {
					Rules.rulesVar[index] = {tip: "1", deviceid: data1, sensorid: data2, status: data3, inverted: 0};
				}
			break;
		}
		var divTd0 	= document.createElement("div");
		var divTd1 	= document.createElement("div");
		var divTd2 	= document.createElement("div");

		divTd0.setAttribute("class","left name");
		divTd1.setAttribute("class","left input");
		divTd2.setAttribute("class","right");
		divTd2.setAttribute("id","trash");
		divTd2.setAttribute("data-id", "rule" + index);

		var label = document.createElement("label");
		var checkbox = document.createElement("input");
		checkbox.setAttribute("type", "checkbox");
		checkbox.setAttribute("data-index", index);

		if(inverted == 1) {
			checkbox.checked = true;
		}

		label.innerHTML = language.rulesnotlabel;
		label.appendChild(checkbox);
		divTd1.appendChild(label);

		switch(page){
			case "period":
				divTd0.innerHTML = language.rulesif + " <b>" + item2 + "</b> " + language.rulesifend;
			break;
			case "sensor":
				divTd0.innerHTML = language.rulesif + " <b>" + item1 + "</b> " + language.rulesifother + " <b>" + item2 + " " + Rules.statusControl(status) + "</b> " + language.rulesifend;
			break;
		}

		divTr.appendChild(divTd0);
		divTr.appendChild(divTd1);
		divTr.appendChild(divTd2);
		$("div#rules div#listrules div.list").append(divTr);
	} else {
		Main.alert(language.rulesagain);
	}
};


Rules.statusControl = function(value) {
	var status = value;

	if(status==0) {
		status = "total";
	} else if(status==1) {
		status = language.normal;
	} else if(status==2) {
		status = language.warning;
	} else if(status==4) {
		status = language.alarm;
	} else if(status==8) {
		status = language.inactive;
	} else if(status==16) {
		status = language.lost;
	} else if(status==32) {
		status = language.output;
	}

	return status;
};

Rules.getPhyscalForm = function(id) {
	Main.loading();

	var url = "getPhysicalOutputs";

	if(id != undefined) {
		url = "getPhysicalOutputs&ruleid=" + id;
	}

	var sensors = $.getJSON("http://" + hostName + "/cgi-bin/rulemanager.cgi?" + url);

	sensors.success(function() {
		Main.unloading();
		Rules.physicalVar = JSON.parse(sensors.responseText);
		Rules.physicalOldVar = JSON.parse(sensors.responseText);
		
		$("div#overlay").css("display","block");
		$("#content div#rules div#rulesWindow").css("display","block");

		$("#content div#rules div#rulesWindow input#buttonSave").val(language.save);
		$("#content div#rules div#rulesWindow input#buttonSave").attr("data-id","physcal");
		$("#content div#rules div#rulesWindow input#buttonCancel").val(language.cancel);
		$("#content div#rules div#rulesWindow input#buttonCancel").attr("data-id","physcal");

		Rules.createPhyscalForm(Rules.physicalVar);
	});

	sensors.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Rules.getLogicalForm = function(id) {
	Main.loading();

	var url = "getLogicalOutputs";

	if(id != undefined) {
		url = "getLogicalOutputs&ruleid=" + id;
	}

	var sensors = $.getJSON("http://" + hostName + "/cgi-bin/rulemanager.cgi?" + url);	 
	sensors.success(function() {
		Main.unloading();
		Rules.logicalVar = JSON.parse(sensors.responseText);
		Rules.logicalOldVar = JSON.parse(sensors.responseText);
		var groups = $.getJSON("http://" + hostName + "/cgi-bin/userandgroups.cgi?getGroups");

		groups.success(function() {
			Settings.groups = jQuery.parseJSON(groups.responseText);
			$("div#overlay").css("display","block");
			$("#content div#rules div#rulesWindow").css("display","block");

			$("#content div#rules div#rulesWindow input#buttonSave").val(language.save);
			$("#content div#rules div#rulesWindow input#buttonSave").attr("data-id","logical");
			$("#content div#rules div#rulesWindow input#buttonCancel").val(language.cancel);

			Rules.createLogicalForm(Rules.logicalVar);
		});	

		groups.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
		});

	});

	sensors.error(function(jqXHR, textStatus, errorThrown){	
		Main.unloading();
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
	});
};

Rules.createLogicalForm = function(items) {

	if(items.length == 0) {
		items[0] = {type: 0, content: "", groupid: 2};
		Rules.logicalOldVar[0] = {type: 0, content: "", groupid: 2};
		Rules.logicalFormElement(undefined,0);
	} else {

		for (var i = 0; i < items.length; i++) {
			Rules.logicalFormElement(items[i],i);
		}

	}

};

Rules.logicalFormElement = function(item, count) {

	var html = '<tr>'
			+ '		<td id="type" width="15%"></td>'
			+ '		<td id="content" width="40%"></td>'
			+ '		<td id="group" width="30%"></td>'
			+ '		<td id="add" width="15%" align="right"></td>'
			+ '	</tr>';

	$("div#rules div#rulesWindow table.head").html(html);

	$("div#rules div#rulesWindow table.head td#type").html(language.rules_logicaltype);
	$("div#rules div#rulesWindow table.head td#content").html(language.rules_logicalcontent);
	$("div#rules div#rulesWindow table.head td#group").html(language.rules_logicalgroup);
	$("div#rules div#rulesWindow table.head td#add").html(language.rules_add);

	var ruleid = "";
	var type = "";
	var content = "";
	var groupname = "";
	var groupid = "";

	if(item != undefined) {
		ruleid = item.ruleid;
		type = parseInt(item.type);
		content = item.content;
		groupname = item.groupname;
		groupid = parseInt(item.groupid);
	}

	var tr = document.createElement("tr");
	tr.setAttribute("id","rule"+count);

	var td0 = document.createElement("td");
	td0.setAttribute("class","type");
	td0.setAttribute("width","15%");
	var td1 = document.createElement("td");
	td1.setAttribute("class","content");
	td1.setAttribute("width","40%");
	var td2 = document.createElement("td");
	td2.setAttribute("class","group");
	td2.setAttribute("width","30%");
	var td3 = document.createElement("td");
	td3.setAttribute("class","remove");
	td3.setAttribute("data-index", count);
	td3.setAttribute("id", ruleid);
	td3.setAttribute("width","15%");

	var typeSelect = document.createElement("select");
	typeSelect.setAttribute("id","rulestype");
	typeSelect.setAttribute("data-index", count);
	var option0 = document.createElement("option");
	option0.setAttribute("value", 0);
	option0.innerHTML = language.rules_logicalsms;
	var option1 = document.createElement("option");
	option1.setAttribute("value", 1);
	option1.innerHTML = language.rules_logicalmail;
	var option2 = document.createElement("option");
	option2.setAttribute("value", 4);
	option2.innerHTML = language.rules_logicaldial;

	if(type == 0) {
		option0.setAttribute("selected", "selected");
	} if(type == 1) {
		option1.setAttribute("selected", "selected");
	} if(type == 4) {
		option2.setAttribute("selected", "selected");
	}




	typeSelect.appendChild(option0);
	typeSelect.appendChild(option1);
	typeSelect.appendChild(option2);
	td0.appendChild(typeSelect);

	var contentTextarea = document.createElement("textarea");
	contentTextarea.setAttribute("id","rulescontent");
	contentTextarea.setAttribute("data-id", ruleid);
	contentTextarea.setAttribute("data-index", count);
	contentTextarea.setAttribute("class","textarea");
	contentTextarea.innerHTML = content;
	td1.appendChild(contentTextarea);

	var groupSelect = document.createElement("select");
	groupSelect.setAttribute("id","rulesgroups");
	groupSelect.setAttribute("data-index", count);
	
	for(var i = 0; i < Settings.groups.length; i++) {
		var option = document.createElement("option");
		option.setAttribute("value", Settings.groups[i].id);
		option.innerHTML = Settings.groups[i].name;
		groupSelect.appendChild(option);

		if(groupid == Settings.groups[i].id) {
			option.setAttribute("selected", "selected");
		}
	}

	td2.appendChild(groupSelect);

	tr.appendChild(td0);
	tr.appendChild(td1);
	tr.appendChild(td2);
	tr.appendChild(td3);

	$("div#rules div#rulesWindow table.list").append(tr);
	$("#rulestype").on("change",function () {
		$("#rulescontent").val("")    
		if($(this).val() == 4) {
			$("#rulescontent").attr("disabled","disabled")
			$("#rulescontent").attr("style","background:#CCC")
		}
		else {
			$("#rulescontent").removeAttr("disabled")
			$("#rulescontent").removeAttr("style")  
		}
	});
};

Rules.createPhyscalForm = function(items){

	var html = '<tr>'
			+ '		<td id="checkbox" width="10%"></td>'
			+ '		<td id="modulename" width="30%"></td>'
			+ '		<td id="sensorname" width="30%"></td>'
			+ '		<td id="ionum" width="15%"></td>'
			+ '		<td id="input" width="15%"></td>'
			+ '	</tr>';

	$("div#rules div#rulesWindow table.head").html(html);

	$("div#rules div#rulesWindow table.head td#checkbox").html(language.rules_checkbox);
	$("div#rules div#rulesWindow table.head td#modulename").html(language.rules_modulename);
	$("div#rules div#rulesWindow table.head td#ionum").html(language.rules_ionum);
	$("div#rules div#rulesWindow table.head td#sensorname").html(language.rules_sensorname);
	$("div#rules div#rulesWindow table.head td#input").html(language.rules_inputvalue);

	for (var i = 0; i < items.length; i++) {

		var item = items[i];

		var sensorid = item.sensorid;
		var ionum = item.ionum;
		var devicename = item.devicename;
		var sensorname = item.sensorname;
		var value = item.value;
		if(value == "") {
			value = 0;
		}
		var picked = item.picked;

		var tr = document.createElement("tr");
		tr.setAttribute("id","sensor"+sensorid);

		var td0 = document.createElement("td");
		td0.setAttribute("class","picked");
		td0.setAttribute("width","10%");
		var td1 = document.createElement("td");
		td1.setAttribute("class","devicename");
		td1.setAttribute("width","30%");
		td1.innerHTML = devicename;
		var td2 = document.createElement("td");
		td2.setAttribute("class","sensorname");
		td2.setAttribute("width","30%");
		td2.innerHTML = sensorname;
		var td3 = document.createElement("td");
		td3.setAttribute("class","ionum");
		td3.setAttribute("width","15%");
		td3.innerHTML = ionum;
		var td4 = document.createElement("td");
		td4.setAttribute("class","value");
		td4.setAttribute("width","15%");

		var checkbox = document.createElement("input");
		checkbox.setAttribute("type", "checkbox");
		checkbox.setAttribute("id", "picked");
		checkbox.setAttribute("data-index",i);

		if(picked == 0) {
			checkbox.checked = false;
		} else {
			checkbox.checked = true;	
		}

		td0.appendChild(checkbox);

		var input = document.createElement("input");
		input.setAttribute("type", "text");
		input.setAttribute("id", "value");
		input.setAttribute("data-id", sensorid);
		input.setAttribute("data-index",i);
		input.setAttribute("value",value);

		td4.appendChild(input);

		tr.appendChild(td0);
		tr.appendChild(td1);
		tr.appendChild(td2);
		tr.appendChild(td3);
		tr.appendChild(td4);

		$("div#rules div#rulesWindow table.list").append(tr);

	}
};

Rules.updateRule = function(rules, physcal, logical, ruleid) {
	Main.loading();
	var name = $("input#rulesname").val();

	// for(var i = 0; i < name.length; i++) {
	// 	name = name.replace("ı","i");
	// 	name = name.replace("ç","c");
	
	// 	name = name.replace("Ç","C");
	// }
	var connector = $("select#selectIf").val();

	var json = {name: name, connector: connector};
	json = JSON.stringify(json);

	var url = "http://" + hostName + "/cgi-bin/rulemanager.cgi?updateRule&id=" + ruleid + "&name="+ name + "&connector=" + connector;
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			//Main.unloading();
			Rules.saveSubRule(ruleid, rules, physcal, logical); 
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    	Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Rules.saveRule = function(rules, physcal, logical) {
	Main.loading();
	var name = $("input#rulesname").val();
	// for(var i = 0; i < name.length; i++) {
	// 	name = name.replace("ı","i");
	// 	name = name.replace("ç","c");
	// 	name = name.replace("Ç","C");
	// }
	var connector = $("select#selectIf").val();

	var json = {name: name, connector: connector};
	json = JSON.stringify(json);

	var url = "http://" + hostName + "/cgi-bin/rulemanager.cgi?addRule&name="+ name + "&connector=" + connector;
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			//Main.unloading();
			var id = JSON.parse(data);
			Rules.saveSubRule(id.id, rules, physcal, logical); 
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	   	 	Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Rules.saveSubRule = function(id, rules, physcal, logical) {
	//var json = JSON.stringify(rules);
	var url = "http://" + hostName + "/cgi-bin/rulemanager.cgi?addSubRules&ruleid="+ id;
	$.ajax({
	    url : url,
	    type: "POST",
	    data : rules,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			//Main.unloading();
			if (Rules.updatePyhsical == true)
			{
				Rules.savePhysicalRule(id, physcal, logical,0);
			}
			else if (Rules.updateLogical == true)
			{
				Rules.saveLogicalRule(id, logical);
			}
			else
				Main.unloading();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    	Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Rules.savePhysicalRule = function(id, physcal, logical, order) {
	//var json = JSON.stringify(rules);
	var url = "http://" + hostName + "/cgi-bin/rulemanager.cgi?addPhysicalOutputs&ruleid="+ id;

	$.ajax({
	    url : url,
	    type: "POST",
	    data : physcal,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			//Main.unloading();
			if (order == 0 && Rules.updateLogical == true)
				Rules.saveLogicalRule(id, logical);
			else
				Main.unloading();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
		    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Rules.saveLogicalRule = function(id, logical) {
	//var json = JSON.stringify(rules);
	var url = "http://" + hostName + "/cgi-bin/rulemanager.cgi?addLogicalOutputs&ruleid="+ id;
	$.ajax({
	    url : url,
	    type: "POST",
	    data : logical,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			$("div#rules").html("");
			$("div#rules").html(Rules.html);
			$("div#rules").css("display","block");
			Rules.tabs[0].status = true;
			Rules.tabs[1].status = false;
			Rules.createTabs();
			Rules.tabOpen();
			Main.unloading();
			Main.alert(language.rule_save_success);
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	   	 Main.alert(language.server_error + ":" + jqXHR.responseText);
	    }
	});
};

$(function() {




 

	$("div#rules").on("click","div#rulesItemList div.tr span.right", function() {
		var id = $(this).attr("id");
		$("div#overlay").css("display","block");
		$("#content div#rules div#alertDelete").css("display","block");
		$("#content div#rules div#alertDelete span.text").html(language.rules_delete_rule);
		$("#content div#rules div#alertDelete input#buttonRemove").val(language.remove);
		$("#content div#rules div#alertDelete input#buttonRemove").attr("data-id",id);
		$("#content div#rules div#alertDelete input#buttonClose").val(language.cancel);
	});

	$("div#rules").on("click","div#alertDelete input#buttonRemove", function() {

		var id = $(this).attr("data-id");
		
		$("div#overlay").css("display","none");
		$("#content div#rules div#alertDelete").css("display","none");
		Main.loading();
		var url = "http://" + hostName + "/cgi-bin/rulemanager.cgi?removeRule&id="+ id;
		$.ajax({
		    url : url,
		    type: "POST",
	        dataType: 'text',
	        contentType : 'text/json; charset=utf-8',
		    success: function(data, textStatus, jqXHR)
		    {
				$("div#rules").html("");
				$("div#rules").html(Rules.html);
				$("div#rules").css("display","block");
				Rules.tabs[0].status = true;
				Rules.tabs[1].status = false;
				Rules.createTabs();
				Rules.tabOpen();
				Main.unloading();
			},
		    error: function (jqXHR, textStatus, errorThrown)
		    {
			    Main.alert(language.server_error + ":" + jqXHR.responseText);
				Main.unloading();
		    }
		});
	});

	$("div#rules").on("click","div#alertDelete input#buttonClose", function() {
		$("div#overlay").css("display","none");
		$("#content div#rules div#alertDelete").css("display","none");
	});

	$("div#rules").on("change","div#daterules select#selectDateTime", function() {
		$("div#daterules select#selectDateTime option:selected").each(function() {
			var page = $(this).text();
			Rules.selectDateTime(page);
		});
	});

	$("div#rules").on("click","div#rulesWindow input#buttonSave", function() {
		var page = $(this).attr("data-id");
		switch(page) {
			case "physcal":
				Rules.updatePyhsical = true;
				Rules.physicalVar = $.map(Rules.physicalOldVar, function (obj) {
                	return $.extend(true, {}, obj);
                });
				$("div#overlay").css("display","none");
				$("#content div#rules div#rulesWindow").css("display","none");
			break;
			case "logical":
				Rules.updateLogical = true;
				Rules.logicalVar = $.map(Rules.logicalOldVar, function (obj) {
                	return $.extend(true, {}, obj);
                });
				$("div#overlay").css("display","none");
				$("#content div#rules div#rulesWindow").css("display","none");
			break;
		}
	});

	$("div#rules").on("click","div#daterules input#buttonTime", function() {
		var date = $("div#daterules select#selectDateTime").val();
		var time = $("div#daterules select#selectTime option:selected").text();
		Rules.createRuleElement("period", date, time);
	});

	$("div#rules").on("click","div#sensorrules input#buttonModules", function() {
		var module = $("div#sensorrules select#selectModules option:selected").text();
		var sensor = $("div#sensorrules select#selectSensors option:selected").text();
		var status = $("div#sensorrules select#selectStatus option:selected").text();
		Rules.createRuleElement("sensor", module, sensor, status);
	});

	$("div#rules").on("click","div#listrules div#trash", function() {
		var id = $(this).attr("data-id");
		var index = $("div#listrules div.tr#" + id).index();
		Rules.rulesVar.splice(index,1);
		var list = document.getElementById("rulesitemlist");
		var trashDiv = document.getElementById(id);
		list.removeChild(trashDiv);
	});

	$("div#rules").on("change","div#sensorrules select#selectModules", function() {
		$( "div#sensorrules select#selectModules option:selected" ).each(function() {
			var val = $(this).attr("data-id");
			switch(val) {
				case "WEB.RULES.MODULESLCT":
					Rules.sensors = [];
					Rules.createSensors();
				break;
				case 0:
				case "0":
					Rules.sensors = [];
					Rules.createSensors("any");
				break;
				case -1:
				case "-1":
					Rules.sensors = [];
					Rules.createSensors("all");
				break;
				default:
					Rules.selectDeviceSensors(val);
				break;
			}
		});
	});

	$("div#rules").on("click","div.block span.left div.physcal", function() {
		$("div#rules div#rulesWindow table.list").html("");
		$("div#rules div#rulesWindow table.head").html("");
		var id = $(this).attr("data-id");
		
		if(Rules.physicalVar.length == 0) {
			if(id != undefined) {
				Rules.getPhyscalForm(id);
			} else {
				Rules.getPhyscalForm();
			}
		} else {
			$("div#overlay").css("display","block");
			$("#content div#rules div#rulesWindow").css("display","block");
			$("#content div#rules div#rulesWindow input#buttonSave").val(language.save);
			$("#content div#rules div#rulesWindow input#buttonSave").attr("data-id","physcal");
			$("#content div#rules div#rulesWindow input#buttonCancel").val(language.cancel);
			$("#content div#rules div#rulesWindow input#buttonCancel").attr("data-id","physcal");
			Rules.createPhyscalForm(Rules.physicalVar);
		}
	});

	$("div#rules").on("click","div.block span.left div.logical", function() {
		$("div#rules div#rulesWindow table.head").html("");
		$("div#rules div#rulesWindow table.list").html("");
		var id = $(this).attr("data-id");
		if(Rules.logicalVar.length == 0) {
			if(id != undefined) {
				Rules.getLogicalForm(id);
			} else {
				Rules.getLogicalForm();
			}
		} else {
			$("div#overlay").css("display","block");
			$("#content div#rules div#rulesWindow").css("display","block");
			$("#content div#rules div#rulesWindow input#buttonSave").val(language.save);
			$("#content div#rules div#rulesWindow input#buttonSave").attr("data-id","logical");
			$("#content div#rules div#rulesWindow input#buttonCancel").val(language.cancel);
			$("#content div#rules div#rulesWindow input#buttonCancel").attr("data-id","logical");
			Rules.createLogicalForm(Rules.logicalVar);
		}
	});

	$("div#rules").on("click","div#rulesWindow table.list tr td.remove", function() {
		if(Rules.logicalOldVar.length != 1) {
			var id = $(this).attr("data-index");
			var index = $("div#rulesWindow table.list tr#rule" + id).index();
			$("div#rulesWindow table.list tr#rule" + id).remove();
			Rules.logicalOldVar.splice(index,1);
		}
	});

	$("div#rules").on("click","div#rulesWindow input#picked", function() {
		var index = $(this).attr("data-index");
		var checked = $(this).prop("checked");
		var value = "0";
		if(checked) {
			value = "1";
		}
		Rules.physicalOldVar[index].picked = value;
	});

	$("div#rules").on("keydown","div#rulesWindow input#value", function(event) {
		var id = $(this).attr("data-id");
		var index = $(this).attr("data-index");
		
		if ( $.inArray(event.keyCode,[46,8,9,27,13,190]) !== -1 ||
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }  else {
				setTimeout(function() {
			    	var val = $("div#rulesWindow tr#sensor" + id + " input#value").val();
					Rules.physicalOldVar[index].value = val;
					console.log("value:"+val);
				}, 100);
            }
        }
	});

	$("div#rules").on("click","div#rulesWindow input#buttonCancel", function() {
		$("div#overlay").css("display","none");
		$("#content div#rules div#rulesWindow").css("display","none");

		var page = $(this).attr("data-id");
		switch(page) {
			case "physcal":
				Rules.physicalOldVar = $.map(Rules.physicalVar, function (obj) {
                	return $.extend(true, {}, obj);
                });
			break;
			case "logical":
				Rules.logicalOldVar = $.map(Rules.logicalVar, function (obj) {
                	return $.extend(true, {}, obj);
                });
			break;
		}

	});

	$("div#rules").on("click","div#rulesWindow select#rulestype", function() {
		var index = $(this).attr("data-index");
		var type = $(this).val();
		Rules.logicalOldVar[index].type = type;
	});

	$("div#rules").on("click","div#rulesWindow select#rulesgroups", function() {
		var index = $(this).attr("data-index");
		var id = $(this).val();
		Rules.logicalOldVar[index].groupid = id;
	});

	$("div#rules").on("keydown","div#rulesWindow textarea#rulescontent", function() {
		var id = $(this).attr("data-id");
		var index = $(this).attr("data-index");
		setTimeout(function() {
			var content = $("div#rulesWindow tr#rule" + index + " textarea#rulescontent").val();
			Rules.logicalOldVar[index].content = content;
		}, 100);
	});

	$("div#rules").on( "click", "div#rulesWindow table.head td#add", function() {
		var index = $("div#rules div#rulesWindow table.list tr").length;
		Rules.logicalOldVar[index] = {type: 0, content: "", groupid: 2};
		Rules.logicalFormElement(undefined,index);
	});

	$("div#rules").on( "click", "ul.tabs li", function() {
		Main.loading();
		var func = $(this).data("func");
		$("div#rules ul.tabs li.active").removeClass("active");
		$(this).addClass("active");
		eval("Rules." + func);
	});

	$("div#rules").on( "click", "div#ruleslist div.tr span.name", function() {
		Main.loading();
		Rules.updatePyhsical = false;
		Rules.updateLogical = false;
		var id = $(this).attr("data-id");
		$("div#rules ul.tabs li").removeClass("active");
		Rules.getRuleItem(id);
	});

	$("div#rules").on( "click", "div#backForm a.back", function() {
		Main.loading();
		$("div#rules ul.tabs li:first").addClass("active");
		Rules.getRulesList();
	});
	
	$("div#rules").on( "click", "div.tr label input[type='checkbox']", function() {
		var status = $(this).prop("checked");
		var id = $(this).attr("data-index");

		var index = $("div#rules div.block div.tr#rule" + id).index();

		if(status){
			status = 1;
		} else {
			status = 0;
		}

		Rules.rulesVar[index].inverted = status;

	});

	// $("div#rules").on("keydown","input#rulesname", function(e) {
		
	// 	if ( $.inArray(event.keyCode,[46,8,9,27,13,190]) !== -1 ||
 //             // Allow: Ctrl+A
 //            (event.keyCode == 65 && event.ctrlKey === true) || 
 //             // Allow: home, end, left, right
 //            (event.keyCode >= 35 && event.keyCode <= 39)) {
 //                 // let it happen, don't do anything
 //                 return;
 //        }
 //        else {
 //            // Ensure that it is a number and stop the keypress
 //            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )  && (event.keyCode < 65 || event.keyCode > 90 )) {
 //                event.preventDefault(); 
 //            }
 //        }
	// });

	$("div#rules").on( "click", "div#listrules input#rulessave", function() {

		var name = $("input#rulesname").val();

		var physicalItem = [];
		var logicalItem = [];
		var rulesItem = [];
		var itemPhysical = "{";
		var itemLogical = "{";
		var itemRules = "{";
		var countPhysical = 0;

		for(var i = 0; i < Rules.physicalVar.length; i ++) {
			if(Rules.physicalVar[i].picked == 1) {
				physicalItem[countPhysical] =  '"' +Rules.physicalVar[i].sensorid + '": "' + Rules.physicalVar[i].value + '"';
				countPhysical++;
			}
		}
		for(var i = 0; i < physicalItem.length; i ++) {
			if(i == physicalItem.length-1) {
				itemPhysical+=physicalItem[i];
			} else {
				itemPhysical+=physicalItem[i] + ',';
			}
		}
		itemPhysical+= "}";

		for(var i = 0; i < Rules.logicalVar.length; i ++) {
			logicalItem[i] =  '"' + i + '": "' + Rules.logicalVar[i].type + ','  + Rules.logicalVar[i].content + ',' + Rules.logicalVar[i].groupid + '"';
		}
		for(var i = 0; i < logicalItem.length; i ++) {
			if(i == logicalItem.length-1) {
				itemLogical+=logicalItem[i];
			} else {
				itemLogical+=logicalItem[i] + ',';
			}
		}
		itemLogical+= "}";

		for(var i = 0; i < Rules.rulesVar.length; i ++) {
			if(Rules.rulesVar[i].tip == 0) {
				rulesItem[i] =  '"' + i + '": "' + Rules.rulesVar[i].type + ','  + Rules.rulesVar[i].value + ',' + Rules.rulesVar[i].inverted + '"';
			} else {
				rulesItem[i] =  '"' + i + '": "' + Rules.rulesVar[i].deviceid + ','  + Rules.rulesVar[i].sensorid + ',' + Rules.rulesVar[i].status+ ',' + Rules.rulesVar[i].inverted + '"';
			}
		}
		for(var i = 0; i < rulesItem.length; i ++) {
			if(i == rulesItem.length-1) {
				itemRules+=rulesItem[i];
			} else {
				itemRules+=rulesItem[i] + ',';
			}
		}
		itemRules+= "}";

		if(name == "") {
			Main.alert(language.rules_nullname);
		} else if(itemRules != "{}") {
			Rules.saveRule(itemRules, itemPhysical, itemLogical);
		} else {
			Main.alert(language.rules_norule);
		}
	});

	$("div#rules").on( "click", "div#listrules input#rulesupdate", function() {

		var name = $("input#rulesname").val();
		var ruleid = $(this).attr("data-id");

		var physicalItem = [];
		var logicalItem = [];
		var rulesItem = [];
		var itemPhysical = "{";
		var itemLogical = "{";
		var itemRules = "{";
		var countPhysical = 0;

		for(var i = 0; i < Rules.physicalVar.length; i ++) {
			if(Rules.physicalVar[i].picked == 1) {
				physicalItem[countPhysical] =  '"' +Rules.physicalVar[i].sensorid + '": "' + Rules.physicalVar[i].value + '"';
				countPhysical++;
			}
		}
		for(var i = 0; i < physicalItem.length; i ++) {
			if(i == physicalItem.length-1) {
				itemPhysical+=physicalItem[i];
			} else {
				itemPhysical+=physicalItem[i] + ',';
			}
		}
		itemPhysical+= "}";

		var index = 0;
		for(var i = 0; i < Rules.logicalVar.length; i ++) {
			if (Rules.logicalVar[i].content == "")
				continue
			logicalItem[index++] =  '"' + i + '": "' + Rules.logicalVar[i].type + ','  + Rules.logicalVar[i].content + ',' + Rules.logicalVar[i].groupid + '"';
		}
		for(var i = 0; i < logicalItem.length; i ++) {
			if(i == logicalItem.length-1) {
				itemLogical+=logicalItem[i];
			} else {
				itemLogical+=logicalItem[i] + ',';
			}
		}
		itemLogical+= "}";

		for(var i = 0; i < Rules.rulesVar.length; i ++) {
			if(Rules.rulesVar[i].tip == 0) {
				rulesItem[i] =  '"' + i + '": "' + Rules.rulesVar[i].type + ','  + Rules.rulesVar[i].value + ',' + Rules.rulesVar[i].inverted + '"';
			} else {
				rulesItem[i] =  '"' + i + '": "' + Rules.rulesVar[i].deviceid + ','  + Rules.rulesVar[i].sensorid + ',' + Rules.rulesVar[i].status+ ',' + Rules.rulesVar[i].inverted + '"';
			}
		}
		for(var i = 0; i < rulesItem.length; i ++) {
			if(i == rulesItem.length-1) {
				itemRules+=rulesItem[i];
			} else {
				itemRules+=rulesItem[i] + ',';
			}
		}
		itemRules+= "}";

		console.log(itemRules)

		if(name == "") {
			Main.alert(language.rules_nullname);
		} else if(itemRules != "{}") {
			Rules.updateRule(itemRules, itemPhysical, itemLogical, ruleid);
		} else {
			Main.alert(language.rules_norule);
		}
	});


});