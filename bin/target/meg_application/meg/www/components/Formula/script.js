
var Formula = {
    onLoad:null,
    close:[],
    choosenXdevices:[],
    existVirtualDevice:[],
    objId:0,
    iconsArr:[
              '&#960;',             '&#x78;&#x21;',       '&#x28;', '&#x29;', '&#x25;', '&#x41&#x43;',
              '&#x73;&#x69;&#x6E;', '&#x6C;&#x6E;',       '&#x37;', '&#x38;', '&#x39;', '&#xF7;',
              '&#x63;&#x6F;&#x73;', '&#x6C;&#x6F;&#x67;', '&#x34;', '&#x35;', '&#x36;', '&#xD7;',
              '&#x74;&#x61;&#x6E;', '√',                  '&#x31;', '&#x32;', '&#x33;', '&#x96;',
              '&#x65;',             'x<sup>y</sup>',      '&#x30;', '&#x2E;', '&#x3D;', '&#x2B;'
    ]
};


Formula.html =  '<div id="formulaDeviceListX" class="block">'
               +'</div>'
               +'<div id="formulaDeviceListY" class="block">'
               +'</div>'
               +'<div id="choosenXdevices">'
               +'</div>'
               +'<div id="filterXDevices">'
               +'</div>'
               +'<div id="iconsContainer">'
               +'</div>'
               +'<div id="filterYDevices">'
               +'</div>'
               +'<div id="result">'
               +'</div>'
               +'<img src="../../images/icon/equals-symbol.png" />'
               +'<div class="button">'
               +'<div id="resultY">'
               +'</div>'
               +'<input type="submit" onclick="Formula.sendFormula()" id="formulasubmitbutton" value="kaydet">'
               +'</div>';

Formula.onLoad = function () {

    $("div#formula").css("display","block");
    Formula.objId = 0;
    Formula.createPageXDevice();
    Formula.createPageYSensor();
    Formula.createOperationsTable();
    document.getElementById('result').contentEditable='true';
    Main.loading();
    Formula.getFilterDevices(); 
    Main.unloading();

};

Formula.close = function () {

    Main.loading();
    $("div#formula").html("");
    $("div#formula").css("display","none");
    Formula.choosenXdevices = [];
    Formula.objId = 0;
};

/*********************************************************************/
Formula.createPageXDevice = function(){//X cihazları için boş sayfa oluşturur.

      var span = document.createElement("span");
      var select = document.createElement("select");
      var option0 = document.createElement("option");
      span.setAttribute("class", "text");
      select.setAttribute("id", "XDeviceSelect");
      span.innerHTML = language.Formula_x_device_title;
      option0.innerHTML = language.Formula_select_device;
      option0.setAttribute("class","novalue");
      $('#formula').html(Formula.html);
      $('#formulaDeviceListX').html(span);
      $('#formulaDeviceListX').append('<select></select>');
      $('#formulaDeviceListX select').append(option0);
      $('#formulaDeviceListX select').attr("id","XDeviceSelect");
      $("#formulaDeviceListX select").on("change",function(data){
          Formula.getFilterXSensors();
      })
      $("#XDeviceSelect option:first").attr("disabled","disabled");
};

Formula.createPageXSensor = function(){//X Sensörleri için boş sayfa oluşturur.

      var select = document.createElement("select");
      var option = document.createElement("option");
      option.setAttribute("class", "novalue");
      option.innerHTML = language.Formula_select_sensor;

      try
      {
          $("#formulaDeviceListX #XSensorList").remove();
      }
      catch(e)
      {
      }

      select.setAttribute("id","XSensorList");
      $('#formulaDeviceListX').append(select);
      $("#formulaDeviceListX #XSensorList").append(option);
      $("select#XSensorList").on("change",function(data){
          Formula.createXvariable();
      })
    $("#XSensorList option:first").attr("disabled","disabled")
};

Formula.createXvariable = function (){

  var pElement = document.createElement("p");
  var selectedXDeviceClass = $("select#XDeviceSelect option:selected").attr("class");
  var selectedXSensorClass = $("select#XSensorList option:selected").attr("class");

  if(selectedXDeviceClass && selectedXSensorClass)
  {
    var choosenXDevice = $("select#XDeviceSelect option:selected").val();
    var choosenXSensor = $("select#XSensorList option:selected").attr("class");
    var deviceObjId = [];

    for(var i = 0; i < Formula.choosenXdevices.length; i++)
      deviceObjId.push(Formula.choosenXdevices[i].name);

    if ($.inArray(choosenXSensor, deviceObjId) == -1)
    {
      var obj = {};
      var x_code;

      obj.name = choosenXSensor;
      obj.is_used = false;
      Formula.objId++;
      obj.id = Formula.objId;
      Formula.choosenXdevices.push(obj);

      for(var i = 0; i < Formula.choosenXdevices.length;i++)
          Formula.choosenXdevices[i].xCode = 'X'+(i+1);

      for(var i = 0; i < Formula.choosenXdevices.length; i++)
      {
          if(Formula.choosenXdevices[i]["name"] == obj.name)
          {
              x_code = Formula.choosenXdevices[i].xCode;
          }
      }  

      var html = '<div class="deviceContainer">'
                 +'<span class="devicename" id="devicename' + obj.id + '">'
                 + x_code + ':' + choosenXSensor
                 +'</span>'
                 +'<span class="del-icon-x" id="delicon' + obj.id + '">'
                 +'</span>'
                 +'</div>';

      $("#choosenXdevices").append(html);

      if($(".deviceContainer").find("span").text() != null){
          $(".alert").remove();
          $("#choosenXdevices").css("border","1px solid #dedede");
      }
      
      $("#delicon" + obj.id).on("click", function()
      {
          var currentChoosenDevices=[];
          var selectedCode;
          for(var i = 0; i < Formula.choosenXdevices.length; i++)
          {
             if(Formula.choosenXdevices[i].id == obj.id)
              {
                  var selectedCode = Formula.choosenXdevices[i].xCode;
                  if(i>-1)
                  {
                      Formula.choosenXdevices.splice(i,1);
                      currentChoosenDevices=Formula.choosenXdevices;
                      $("#devicename"+obj.id).parent().remove();
                  }
              }
          }

          for(var i = 0; i < currentChoosenDevices.length;i++)
          {
              Formula.choosenXdevices[i].xCode = 'X'+(i+1);
              $("#devicename"+Formula.choosenXdevices[i].id).text(Formula.choosenXdevices[i].xCode+":"+Formula.choosenXdevices[i].name);
              if($("#result").text() != null && $("#result").text().indexOf(selectedCode) >-1)
              {
                // var text= $("#result").text().replace(selectedCode,"");
                $("#result").text("");
              }
          }


      });

      $("#devicename"+ obj.id).on("click", function()
      {
          var selectedDevice = $(this).text();
          for(var i = 0; i < Formula.choosenXdevices.length; i++)
          {
              if(Formula.choosenXdevices[i].xCode + ':' + Formula.choosenXdevices[i].name == selectedDevice)
              {
                  $("#result").append(Formula.choosenXdevices[i].xCode);

                  if($("#result").find("span").text() != null){
                      $(".alert").remove();
                      $("#result").css("border","1px solid #dedede");
                  }
                  break;
              }
          }
      })
    }
  }
};


Formula.createPageYSensor = function(){//Y Sensörleri için boş sayfa oluşturur.

      var span = document.createElement("span");
      var select = document.createElement("select");
      var option0 = document.createElement("option");
      span.setAttribute("class", "text");
      span.innerHTML = language.Formula_y_sensor_title;
      option0.innerHTML = language.Formula_select_sensor;
      option0.setAttribute("class", "novalue");

      $("#formulaDeviceListY").append(span);
      $("#formulaDeviceListY").append('<select></select>');
      $("#formulaDeviceListY select").attr("id","YDeviceSelect");
      $("#formulaDeviceListY select").append(option0);
      $("#YDeviceSelect option:first").attr("disabled","disabled")
      $("#formulaDeviceListY").append("<input type='text' id='sensorName' class='sensorTextInput'/>")
      $("#formulaDeviceListY").append("<input type='text' id='sensorUnit' class='sensorTextInput'/>")
      $("#formulaDeviceListY select").on("change",function(data){
         var selectedVal =  $("#YDeviceSelect option:selected").attr('class');
         if(selectedVal != 'novalue'){
             $("#resultY").html("");
             $("#resultY").append(selectedVal);
         }
          if($("#resultY").text() !=null){
              $(".alert").remove();
              $("#resultY").css("border","1px solid #dedede");
          }
      })
};

Formula.createYvariable = function (){

  var pElement = document.createElement("p");
  var selectedXSensorClass = $("select#YSensorList option:selected").attr("class");
  var selectedYDeviceClass = $("select#YDeviceSelect option:selected").attr("class");
};

Formula.createOperationsTable = function(){//Işlem yapılacak olan boş tabloyu oluşturur.

    var icon = '<div class="iconcontainer">'
             + '</div>';

    $("#iconsContainer").html(icon);
    var allicons = Formula.iconsArr;

    for(var i=0;i<allicons.length;i++)
    {

        var buttonElement = '<button type="button"  width=>'
                          + allicons[i]
                          + '</button>';

        $(".iconcontainer").append(buttonElement);
    }


    $(".iconcontainer button").on("click", function(){

        var chr = $(this).text();
        if($(this).text() == 'Inv')
           $("#result").append('');
        else if($(this).text() == 'Ans')
          $("#result").append('');
        else if($(this).text() == 'Rad')
          $("#result").append('');
        else if($(this).text() == 'Deg')
          $("#result").append('');
        else if($(this).text() == 'xy')
          $("#result").append('^');
        else if($(this).text() == '÷')
          $("#result").append('/');
        else if($(this).text() == '×')
          $("#result").append('x');
        else if($(this).text() == 'π')
          $("#result").append('_pi');
        else if($(this).text() == 'sin' || $(this).text() == 'cos' || $(this).text() == 'tan' ||
                $(this).text() == 'ln'  || $(this).text() == 'log' || $(this).text() == '√' )
          $("#result").append(chr + "(");
        else if($(this).text() == 'x!')
          $("#result").append('!');
        else if($(this).text() == 'AC')
          $("#result").empty();
        else
          $("#result").append(chr);
    });
};


Formula.getFilterDevices = function(){//X için tüm cihazları Y içinse virtual cihazları getirir.

  var getDevicesData = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?getDevices");
  getDevicesData.success(function() {
    Formula.devices = jQuery.parseJSON(getDevicesData.responseText);

    var maxDevAddr = 0;
    var maxDevAddrObj = [];

    for(var i = 0; i < Formula.devices.length; i++)
    {
      var deviceType = Formula.devices[i].type;
      if(deviceType == "12")
      {

        Formula.existVirtualDevice.push(Formula.devices[i].objectid);

        if(parseInt(Formula.devices[i].addr) > maxDevAddr)
        {
          maxDevAddr = parseInt(Formula.devices[i].addr);
          maxDevAddrObj = Formula.devices[i];
        }

        var splitDevObjId = Formula.devices[i].objectid.split("/");
        var hwaddr  = splitDevObjId[0];
        var portnum = splitDevObjId[1];
        var devaddr = splitDevObjId[2];

        var url = "getSensors&hwaddr=" + hwaddr + "&portnum=" + portnum + "&devaddr=" + devaddr;
        Formula.getYDeviceSensors(url, Formula.devices[i]);
      }

      Formula.createXparameterDeviceList(Formula.devices[i]);
    }

    Formula.createYparameterSensorList(0, maxDevAddr, maxDevAddrObj); // Y sensörleri arasında yeni bir device göstermek için

  });
  getDevicesData.error(function(jqXHR, textStatus, errorThrown){
      Main.alert(language.server_error + ":" + jqXHR.responseText);
      Main.unloading();
  });

};

Formula.getYDeviceSensors = function (url, deviceObject){

      var getSensorsData =  $.getJSON('http://' + hostName + '/cgi-bin/mdlmngr.cgi?' + url);
      getSensorsData.success(function() {
        Formula.sensors = jQuery.parseJSON(getSensorsData.responseText);

        var maxIonum = 0;
        for(var i = 0; i < Formula.sensors.length; i++)
        {
          if ( parseInt(Formula.sensors[i].ionum) > maxIonum)
          {
            maxIonum = parseInt(Formula.sensors[i].ionum);
          }
        }

        Formula.createYparameterSensorList(maxIonum, 0, deviceObject);

      }); 
      getSensorsData.error(function(jqXHR, textStatus, errorThrown){
        Main.alert(language.server_error + ":" + jqXHR.responseText);
        Main.unloading();
      });
};


Formula.getFilterXSensors = function(){

  var devObjId = $("#formulaDeviceListX select option:selected").attr("class");

  var splitDevObjId = devObjId.split("/");
  var hwaddr  = splitDevObjId[0];
  var portnum = splitDevObjId[1];
  var devaddr = splitDevObjId[2];

  var url = "getSensors&hwaddr=" + hwaddr + "&portnum=" + portnum + "&devaddr=" + devaddr;
  Main.loading();
  Formula.getXDeviceSensors(url);

};

Formula.getXDeviceSensors = function (url){

      var getSensorsData =  $.getJSON('http://' + hostName + '/cgi-bin/mdlmngr.cgi?' + url);
      getSensorsData.success(function() {
        Formula.sensors = jQuery.parseJSON(getSensorsData.responseText);

        var isFloatOrBinary = false;
        for(var i = 0; i < Formula.sensors.length; i++)
        {
          var sensorType = Formula.sensors[i].type;
          if(isFloat(sensorType) || isBinary(sensorType) || isOutputAndBinary(sensorType) )
          {
            isFloatOrBinary = true;

            if(i == 0) //boş oluşturmak için
            {
              Formula.createPageXSensor();
            }

            Formula.createXparameterSensorList(Formula.sensors[i]);
          }
        }

        if(!isFloatOrBinary)
        {
          try
          {
              $("#formulaDeviceListX #XSensorList").remove();
          }
          catch(e)
          {
          }

          Main.alert("Başka bir cihaz seçiniz. Seçtiğniz cihazda float ya da binary sensor yok.");
        }

        Main.unloading();

      }); 
      getSensorsData.error(function(jqXHR, textStatus, errorThrown){
        Main.alert(language.server_error + ":" + jqXHR.responseText);
        Main.unloading();
      });
};

Formula.createXparameterDeviceList = function(data){

    var option = document.createElement("option");
    option.innerHTML = data.name+'-'+data.objectid;
    option.setAttribute("class", data.objectid);

    $("#formulaDeviceListX select").append(option);
};


Formula.createXparameterSensorList = function (sensorData){

    var option = document.createElement("option");
    option.innerHTML = sensorData.name;
    option.setAttribute("class",sensorData.objectid);
    $("#formulaDeviceListX #XSensorList").append(option);

};

Formula.createYparameterSensorList = function(maxIonum ,maxDevAddr, data){

    if (data.length == 0 && maxDevAddr == 0)
    {
      var devAddr = maxDevAddr + 1;
      var sensObjId = "FF-00-0C-00/0/" + devAddr + "/1"
      var option = document.createElement("option");
      option.innerHTML = 'NewVirtualDevice-' + devAddr + '_Sens-1_[' + sensObjId + ']';
      option.setAttribute("class", sensObjId);

      $("#formulaDeviceListY select").append(option);

    }
    else if(parseInt(data.addr) == maxDevAddr)
    {

      var devAddr = maxDevAddr + 1;
      var sensObjId = "FF-00-0C-00/0/" + devAddr + "/1"
      var option = document.createElement("option");
      option.innerHTML = 'NewVirtualDevice-' + devAddr + '_Sens-1_[' + sensObjId + ']';
      option.setAttribute("class", sensObjId);

      $("#formulaDeviceListY select").append(option);
    }
    else
    {
      var ionum = maxIonum + 1;
      var sensObjId = data.objectid + "/" + ionum;
      var option = document.createElement("option");
      option.innerHTML = data.name + '_Sens-' + ionum + '_[' + sensObjId + ']';
      option.setAttribute("class", sensObjId);

      $("#formulaDeviceListY select").append(option);
    }
};


Formula.sendFormula = function(){

  var jsonSensor = {

        "ionum": "0",
        "type": "786433",
        "name": "NewVirtualSensor",

        "unit": "",
        "warning_low": "10",
        "warning_high": "30",
        "alarm_low": "5",
        "alarm_high": "35",
        "filter_type": "0",
        "filter_value": "1",
        "multiplier": "1",
        "offset": "0",

        "varnum": "",
        "variables": "",
        "formula": ""

      };

  var strVarnum = "" + Formula.choosenXdevices.length;
  var strVariables = "";

  if(Formula.choosenXdevices.length != 0)
  {
    for(var i = 0; i < Formula.choosenXdevices.length; i++)
      strVariables += Formula.choosenXdevices[i].name + ";"

    strVariables = strVariables.substring(0, (strVariables.length - 1));
    jsonSensor.variables = strVariables;
    jsonSensor.varnum = strVarnum;
  }
  else
  {

      if($(".alert").text() == ""){
          $("#formulaDeviceListX").parent().prepend('<div class="alert"> X Sensör listesinden en az bir tane sensör seciniz.</div>')
          $("#choosenXdevices").css("border","1px solid #f06161");

      }
    return;
  }


  var strFormula = $("#result").html();
  for(var t = 0; t < Formula.choosenXdevices.length; t++)
  {
    var splitFormula = strFormula.split("X" + (t+1));

    for(var u = 0; u < splitFormula.length; u++)
    {
      if(splitFormula[u] == strFormula)
      {
          if($(".alert").text() == ""){
          $("#formulaDeviceListX").parent().prepend('<div class="alert"> Formülasyonda kullanılmayan sensörleri siliniz..</div>')
          $("#result").css("border","1px solid #f06161");
        }
        return;
      }
    }
  }

  // if(!Validator.validate( { "val" : $("#sensorName").val(), "type" : "s", "ismandatory" : "true", "obj" : $("#sensorName"), "objfocus" : $("#sensorName"), "page" : $("#formulaDeviceListX")}))
  //   return;

  if ($("#sensorName").val().length  == 0)
  {
    if($(".alert").text() == ""){
    $("#formulaDeviceListX").parent().prepend('<div class="alert"> Sensör adı giriniz...</div>')
    $("#sensorName").css("border","1px solid #f06161");
    }
    return;
  };

  jsonSensor.name = $("#sensorName").val();
  jsonSensor.unit = $("#sensorUnit").val();

  jsonSensor.formula = strFormula;

  var splitSensObjId = $("#resultY").html().split("/");
  if(splitSensObjId.length != 4)
  {
      $("#formulaDeviceListX").parent().prepend('<div class="alert"> Y Sensör listesinden bir tane sensör seciniz.</div>')
      $("#resultY").css("border","1px solid #f06161");

    return;
  }


  var hwaddr  = splitSensObjId[0];
  var portnum = splitSensObjId[1];
  var devaddr = splitSensObjId[2];
  var ionum   = splitSensObjId[3];


  var urlAddSens = "http://" + hostName + "/cgi-bin/mdlmngr.cgi?addSensor&hwaddr=" + hwaddr +"&portnum=" + portnum +"&devaddr=" + devaddr;
  jsonSensor.ionum = ionum;
  // jsonSensor.name  = jsonSensor.name + "_" + ionum;

  var isVirtualDeviceExist = false;
  for(var i = 0; i < Formula.existVirtualDevice.length; i++)
  {
    var splitDevObjId = Formula.existVirtualDevice[i].split("/");
    var existDevAddr = splitDevObjId[2];

    if(parseInt(existDevAddr) == parseInt(devaddr))
    {
      isVirtualDeviceExist = true;
      break;
    }

  }

  if(!isVirtualDeviceExist)
  {

    console.log("Device ekli değil.")
    var urlAddDev = "http://" + hostName + "/cgi-bin/mdlmngr.cgi?addDevice&hwaddr=FF-00-0C-00&portnum=0"

    jsonDevice =
    {
      "type": "12",
      "hwaddr": "FF-00-0C-00",
      "number": "0",
      "name": "NewVirtualDevice",
      "addr": "0",
      "request_timeout": "25",
      "read_period": "25"
    }

    jsonDevice.addr = devaddr;
    jsonDevice.name = jsonDevice.name + "_" + devaddr;

    Formula.addVirtualDevice(jsonDevice, jsonSensor, urlAddDev, urlAddSens);

  }
  else
  {
    console.log("Device ekli .")
    Formula.addVirtualSensor(jsonSensor, urlAddSens);
  }




};


Formula.addVirtualDevice = function(jsonDev, jsonSens, urlAddDev, urlAddSens)
{   

    Main.loading();
    var json = JSON.stringify(jsonDev);
 
    $.ajax({
        url : urlAddDev,
        type: "POST",
        data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
        success: function(veri, textStatus, jqXHR)
        {
            jsON = jQuery.parseJSON(veri);
            if (jsON["return"] == "true")
            {
                Formula.addVirtualSensor(jsonSens, urlAddSens);

                // var url = $("div#portList table td.active a").attr("data-id");
                // var deviceData = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?getConnectors");     
                // deviceData.success(function() {
                //     Main.unloading();
                //     var url = "getDevices" + $("div#portList table td.active a").attr("data-id");
                //     Devices.getDevices(url);
                // });
            }
            else
            {
                Main.alert(language.devices_add_fail + ":" + jsON["return"]);
                Main.unloading();

            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            Main.unloading();
            Main.alert(jqXHR.responseText);
        }
    });
};

Formula.addVirtualSensor = function(jsonSens, urlAddSens)
{

    Main.loading();
    var json = JSON.stringify(jsonSens);

    $.ajax({
        url : urlAddSens,
        type: "POST",
        data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
        success: function(data, textStatus, jqXHR)
        {
          var jSon = jQuery.parseJSON(data);
          Main.unloading();
          if (jSon["return"] == "true")
          { 
            Formula.close();
            Formula.onLoad();
            Main.alert(language.sensors_add_success);
            $( "div#edits ul.filters li select option:selected" ).each(function() {
                Main.loading();
              var lastSelected = sensorTypesToStr2(DataTypeFromSensType(parseInt(type)));
            var lastDisp = $("div#edits table tr.head div.switch input:checked").attr("id");
            var url = $(this).val();
            Settings.getEditSensorsList(url, lastSelected, lastDisp);
            });
          }
          else
          {
            Main.alert(language.sensors_add_fail + ":" + jSon["return"]);
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        { 
          var Exception = "Exception :" + jqXHR.responseText;
          console.log(Exception);
          console.log("*************error");
          Main.alert(language.server_error + ":" + jqXHR.responseText);
        Main.unloading();
        }
    });
};

