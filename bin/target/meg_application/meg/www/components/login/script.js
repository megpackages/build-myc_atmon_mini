var Login = {
	onLoad: null,
    html: null,
    ldapSettingsReady: false,
    keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
};

Login.ldapData = {};

Login.ldapData.ldap_server = "";
Login.ldapData.ldap_server_port = "";
Login.ldapData.ldap_base_dn = "";

Login.html = '<div class="logo"></div><div class="alertText"></div>';

Login.onLoad = function() {
	$("div#login").html(Login.html);
	Login.createElement();	
	Login.forgotYourPassCreate();

	$("div#login ul.fypUL").css("display","none");
	$("div#login ul.loginUL").css("display","block");

	Main.unloading();
};

Login.close = function() {
	$("div#login").html("");
	Main.onLoad();
};

Login.createElement = function() {

	var ul = document.createElement("ul");
	ul.setAttribute("class","loginUL");
	var li1 = document.createElement("li");
	var li2 = document.createElement("li");
	var li3 = document.createElement("li");
	var li4 = document.createElement("li");

	var user = document.createElement("input");
	var pass = document.createElement("input");
    var button = document.createElement("input");
    var button2 = document.createElement("input");
    var a_button2 = document.createElement("a");
    var a = document.createElement("a");

    var divLeft1 = document.createElement("div");
    divLeft1.setAttribute("class","left");
	divLeft1.innerHTML = "Kullanıcı Adı / Username"; // language.username;

	var divRight1 = document.createElement("div");
	var divAlert1 = document.createElement("div");
	divRight1.setAttribute("class","left");
	divAlert1.setAttribute("class","alert");
	divAlert1.setAttribute("id","alertUser");

	user.setAttribute("type","text");
	user.setAttribute("id","username");
	user.setAttribute("class","input");

	divRight1.appendChild(user);
	divRight1.appendChild(divAlert1);

	var divLeft2 = document.createElement("div");
	divLeft2.setAttribute("class","left");
	divLeft2.innerHTML = "Şifre / Password"; //language.password;

	var divRight2 = document.createElement("div");
	var divAlert2 = document.createElement("div");
	divRight2.setAttribute("class","left");
	divAlert2.setAttribute("class","alert");
	divAlert2.setAttribute("id","alertPass");

	pass.setAttribute("type","password");
	pass.setAttribute("id","password");
	pass.setAttribute("class","input");

	divRight2.appendChild(pass);
	divRight2.appendChild(divAlert2);

	var divLeft3 = document.createElement("div");
	divLeft3.setAttribute("class","left");

	var divRight3 = document.createElement("div");
	divRight3.setAttribute("class","left");

	button.setAttribute("type","submit");
	button.setAttribute("id","button");
	button.setAttribute("class","button");
	button.setAttribute("value","Giriş / Enter"); //language.login

	divRight3.appendChild(button);

    button2.setAttribute("type","checkbox");
    button2.setAttribute("id","ldapcheck");
    button2.setAttribute("style","margin-left:50px;");
    a_button2.innerHTML = "Active Directory";
    a_button2.setAttribute("style","margin-left:20px;");

    divRight3.appendChild(button2);
    divRight3.appendChild(a_button2);

    var divLeft4 = document.createElement("div");
    divLeft4.setAttribute("class","left");

    var divRight4 = document.createElement("div");
    divRight4.setAttribute("class","left");

    a.setAttribute("id","forgotYourPass");
	a.innerHTML = "Şifremi unuttum? / Forgot your password?"; //language.forgotyourpass;

	divRight4.appendChild(a);

	li1.appendChild(divLeft1);
	li1.appendChild(divRight1);
	li2.appendChild(divLeft2);
	li2.appendChild(divRight2);
	li3.appendChild(divLeft3);
	li3.appendChild(divRight3);
	li3.appendChild(divLeft4);
	li3.appendChild(divRight4);

	ul.appendChild(li1);
	ul.appendChild(li2);
	ul.appendChild(li3);
	ul.appendChild(li4);
	
	$("div#login").append(ul);
};

Login.status = function() {
	//delCookie("Main.user");

	var status = false;
	var user = getCookie("Main.user");
    var authToken = getCookie("AuthHeader");

    if(!user || !authToken) {
      Login.onLoad();
      status = false;
  } else {
      user = Base64.decode(getCookie("Main.user"));
      var indexOf = user.indexOf("error");

      if(indexOf == 0) {
         Login.onLoad();
         status = false;
     } else {
         user = JSON.parse(user);
         Main.user = user;
         status = true;
     }

     $.ajaxSetup({
        headers: {
            "Authorization": authToken
        }
    });

 }
 return status;
};

Login.getControl = function(item) {

    if(Login.ldapSettingsReady == false)
    {
        var posUser = $.post( "http://" + hostName + "/cgi-bin/megweb.cgi?login", item);
        posUser.success(function() {
            var user = JSON.parse(posUser.responseText);
            if(user.result == "false") {
            $("div.alertText").html("Kullanıcı Adı veya Şifre hatalı. / Username or password is incorrect."); //language.loginalert
        } else if(user.result == "true") {

//             window.onbeforeunload = function (e) {
//                 var message =  "Çıkış yapmadan bu pencereyi kapatmak üzeresiniz. Otomatik olarak çıkış yapılcak!";
//                 var firefox = /Firefox[\/\s](\d+)/.test(navigator.userAgent);
//                 if (firefox) {
//         //Add custom dialog
//         //Firefox does not accept window.showModalDialog(), window.alert(), window.confirm(), and window.prompt() furthermore
//         var dialog = document.createElement("div");
//         document.body.appendChild(dialog);
//         dialog.id = "dialog";
//         dialog.style.visibility = "hidden";
//         dialog.innerHTML = message; 
//         var left = document.body.clientWidth / 2 - dialog.clientWidth / 2;
//         dialog.style.left = left + "px";
//         dialog.style.visibility = "visible";  
//         var shadow = document.createElement("div");
//         document.body.appendChild(shadow);
//         shadow.id = "shadow";       
//         //tip with setTimeout
//         setTimeout(function () {
//             document.body.removeChild(document.getElementById("dialog"));
//             document.body.removeChild(document.getElementById("shadow"));
//         }, 0);
//     }
//     return message;
// }
 
Main.user = user;
var userToken = JSON.parse(item);
var tok = userToken.username + ':' + Base64.decode(userToken.password);
var hash = Base64.encode(tok);
var authInf =  "Basic " + hash;
setCookie("AuthHeader",authInf,parseInt(Main.user.cookie_timeout));

user = Base64.encode(JSON.stringify(Main.user));
setCookie("Main.user",user,parseInt(Main.user.cookie_timeout));
Login.close();
}

Main.unloading();
});
posUser.error(function(jqXHR, textStatus, errorThrown) {
    Main.unloading();
    Main.alert("Sistemde sorun oluştu lütfen bir süre sonra tekrar deneyin. / An error occured please try again." + jqXHR.responseText);
});

}
else
{
    var posUser = $.post( "http://" + hostName + "/cgi-bin/megweb.cgi?ldaplogin", item);
    posUser.success(function() {
        var user = JSON.parse(posUser.responseText);
        if(user.result == "false") {
            $("div.alertText").html("Kullanıcı Adı veya Şifre hatalı. / Username or password is incorrect."); //language.loginalert
        } else if(user.result == "true") {
            Main.user = user;
            user = Base64.encode(JSON.stringify(Main.user));
            setCookie("Main.user",user,parseInt(Main.user.cookie_timeout));
            Login.close();
        }

        Main.unloading();
    });
    posUser.error(function(jqXHR, textStatus, errorThrown) {
        Main.unloading();
        Main.alert("Sistemde sorun oluştu lütfen bir süre sonra tekrar deneyin. / An error occured please try again." + jqXHR.responseText);
    });

}
};

Login.postMail = function(json) {
	var posMail = $.getJSON( "http://" + hostName + "/cgi-bin/megweb.cgi?lostPassword&email_addr=" + json);
	posMail.success(function() {
		var mail = JSON.parse(posMail.responseText);
		if(mail.result == "false") {
			$("div#login div.alertText").html("Email adresi herhangi bir kullanıcının email adresiyle eşleşmiyor / Your email address doesn't match any user's email address");	
		} else if(mail.result == "true") {
			setTimeout(function() {
				Login.loginScreen();
			},2000);
			$("div#login div.alertText").html("Kayıtlı adresinize Kullanıcı Adı ve Şifreniz gönderildi. Giriş ekranına otomatik yönlendirileceksiniz. / Username and Password was sended to your email address. You wil be redirected to login page.");
		}

		Main.unloading();
	});
	posMail.error(function(jqXHR, textStatus, errorThrown) {
     Main.unloading();
     Main.alert("Sistemde sorun oluştu lütfen bir süre sonra tekrar deneyin. / An error occured please try again." + jqXHR.responseText);
 });
};

Login.validateLogin = function() {
	var status = true;
	$("div#alertUser").css("display","none");
	$("div#alertPass").css("display","none");
	$("input#username").removeClass("alert");
	$("input#password").removeClass("alert");
	if(($("input#username").val() == "" || $("input#username").val() == null)) {
		$("div#alertUser").css("display","block");
		$("input#username").addClass("alert");
		status = false;
	}
	if(($("input#password").val() == "" || $("input#password").val() == null)) {
		$("div#alertPass").css("display","block");
		$("input#password").addClass("alert");
		status = false;
	}
	return status;
};

Login.validateFYP = function() {
	var str = $("input#mail").val();
	var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	var status = true;

	$("div#login div.alertText").html("");	
	$("div#alertMail").css("display","none");
	$("input#mail").removeClass("alert");

	if($("input#mail").val() != "" && filter.test(str)) {
		status = true;
	} else {
		$("div#alertMail").css("display","block");
		$("input#mail").addClass("alert");
		status = false;
	}

	return status;
};

Login.forgotYourPass = function () {
	$("div.alertText").html(""); 
	$("div#login ul.fypUL").css("display","block");
	$("div#login ul.loginUL").css("display","none");
};

Login.forgotYourPassCreate = function() {

	var ul = document.createElement("ul");
	ul.setAttribute("class","fypUL");
	var li1 = document.createElement("li");
	var li2 = document.createElement("li");

	var mail = document.createElement("input");
	var button = document.createElement("input");
	var a = document.createElement("a");

	var divLeft1 = document.createElement("div");
	divLeft1.setAttribute("class","left");
	divLeft1.innerHTML = "Kayıtlı e-posta adresinizi giriniz / Enter your email address registered to system"; // language.email;

	var divRight1 = document.createElement("div");
	var divAlert1 = document.createElement("div");
	divRight1.setAttribute("class","left");
	divAlert1.setAttribute("class","alert");
	divAlert1.setAttribute("id","alertMail");

	mail.setAttribute("type","text");
	mail.setAttribute("id","mail");
	mail.setAttribute("class","input");

	divRight1.appendChild(mail);
	divRight1.appendChild(divAlert1);

	var divLeft3 = document.createElement("div");
	divLeft3.setAttribute("class","left");

	var divRight3 = document.createElement("div");
	divRight3.setAttribute("class","left");

	button.setAttribute("type","submit");
	button.setAttribute("id","button");
	button.setAttribute("class","button");
	button.style.fontSize = "14px";
	button.setAttribute("value","Devam et / Continue"); //language.login

	divRight3.appendChild(button);

	var divLeft4 = document.createElement("div");
	divLeft4.setAttribute("class","left");

	var divRight4 = document.createElement("div");
	divRight4.setAttribute("class","left");

	a.setAttribute("id","backto");
	a.innerHTML = "&laquo; Giriş ekranına geri dön / Return to login page"; //language.forgotyourpass;

	divRight4.appendChild(a);

	li1.appendChild(divLeft1);
	li1.appendChild(divRight1);
	li2.appendChild(divRight3);
	li2.appendChild(divRight4);

	ul.appendChild(li1);
	ul.appendChild(li2);
	
	$("div#login").append(ul);

};

Login.formEnter = function() {

	if($("div#login ul.loginUL").css("display") != "none") {


		if(Login.validateLogin()) {
			Main.loading();
			var item = {
				username : $("input#username").val(),
                password : Base64.encode($("input#password").val())
			};
			var usr = JSON.stringify(item);
			Login.getControl(usr);
		}
	} else if($("div#login ul.fypUL").css("display") != "none") {
		if(Login.validateFYP()) {
			Main.loading();
			var mail = $("input#mail").val();
			Login.postMail(mail);
		}
	}

};

Login.loginScreen = function() {
    $("div.alertText").html(""); 
    $("div#login ul.fypUL").css("display","none");
    $("div#login ul.loginUL").css("display","block");
    $("div#alertUser").css("display","none");
    $("div#alertPass").css("display","none");
    $("input#username").removeClass("alert");
    $("input#password").removeClass("alert");
    $("input#username").val("");
    $("input#password").val("");        
    $("div#alertMail").css("display","none");
    $("input#mail").removeClass("alert");
};
$(document).keypress(function(e) {
    if(e.which == 13) {
        Login.formEnter();
    }
});

Login.checkLdapSettings = function() {    
    Main.loading();
    var data = $.getJSON("http://" + hostName + "/cgi-bin/ldapsettings.cgi?getLdapSettings");    
    data.success(function() {
        Login.ldapData = jQuery.parseJSON(data.responseText);
        if( Login.ldapData.ldap_server != "" && 
            Login.ldapData.ldap_server_port != "" && 
            Login.ldapData.ldap_base_dn != "" &&
            Login.ldapData.ldap_server != undefined && 
            Login.ldapData.ldap_server_port != undefined && 
            Login.ldapData.ldap_base_dn != undefined) {
            $("div.alertText").html("Active Directory ile giriş yapılabilir.<br>Active Directory is ready."); 
        Login.ldapSettingsReady = true;
    }
    else {
        $("div.alertText").html("Active Directory Ayarları girilmemiş.<br>Active Directory Settings is not presented."); 
        Login.ldapSettingsReady = false;
        $("#ldapcheck").prop("checked", false);
    }      
    Main.unloading();
});
    data.error(function(jqXHR, textStatus, errorThrown){
        Main.alert(language.server_error + ":" + jqXHR.responseText);
        Main.unloading();
    });
};


$(function() {
	$("div#login").on( "click", "ul.loginUL input.button", function() {
		Login.formEnter();
	});

	$("div#login").on( "click", "ul.fypUL input.button", function() {
		Login.formEnter();
	});

	$("div#login").on( "click", "a#forgotYourPass", function() {
		Login.forgotYourPass();
	});

	$("div#login").on( "click", "a#backto", function() {
		Login.loginScreen();
	});

    $("div#login").on( "change", "ul.loginUL input#ldapcheck", function() {
        var status = $(this).prop("checked");        
        if(status) {
            Login.checkLdapSettings();      
        }
        else
            Login.ldapSettingsReady = false;
    });


});

Login.encode64 = function(input) {
    input = escape(input);
    var output = "";
    var chr1, chr2, chr3 = "";
    var enc1, enc2, enc3, enc4 = "";
    var i = 0;
    do
    {
        chr1 = input.charCodeAt(i++);
        chr2 = input.charCodeAt(i++);
        chr3 = input.charCodeAt(i++);
        enc1 = chr1 >> 2;
        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
        enc4 = chr3 & 63;
        if (isNaN(chr2))
        {
            enc3 = enc4 = 64;
        }
        else if (isNaN(chr3))
        {
            enc4 = 64;
        }
        output = output +
        Login.keyStr.charAt(enc1) +
        Login.keyStr.charAt(enc2) +
        Login.keyStr.charAt(enc3) +
        Login.keyStr.charAt(enc4);
        chr1 = chr2 = chr3 = "";
        enc1 = enc2 = enc3 = enc4 = "";
    }
    while(i < input.length);
    return output;
};

Login.decode64 = function(input)
{
    var output = "";
    var chr1, chr2, chr3 = "";
    var enc1, enc2, enc3, enc4 = "";
    var i = 0;
    // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
    var base64test = /[^A-Za-z0-9\+\/\=]/g;
    if (base64test.exec(input))
    {
        output = "error";
    }
    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
    do
    {
        enc1 = Login.keyStr.indexOf(input.charAt(i++));
        enc2 = Login.keyStr.indexOf(input.charAt(i++));
        enc3 = Login.keyStr.indexOf(input.charAt(i++));
        enc4 = Login.keyStr.indexOf(input.charAt(i++));
        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;
        output = output + String.fromCharCode(chr1);
        if (enc3 != 64)
        {
            output = output + String.fromCharCode(chr2);
        }
        if (enc4 != 64)
        {
            output = output + String.fromCharCode(chr3);
        }
        chr1 = chr2 = chr3 = "";
        enc1 = enc2 = enc3 = enc4 = "";
    }
    while (i < input.length);
    return unescape(output);
};

var Base64 = {

        _keyStr:
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",


    encode:function(e){
        var t="";
        var n,r,i,s,o,u,a;
        var f=0;
        e=Base64._utf8_encode(e);
        while(f<e.length){
            n=e.charCodeAt(f++);
            r=e.charCodeAt(f++);
            i=e.charCodeAt(f++);
            s=n>>2;
            o=(n&3)<<4|r>>4;
            u=(r&15)<<2|i>>6;
            a=i&63;

            if(isNaN(r)){
                u=a=64
            }
            else if(isNaN(i)){
                a=64
            }

            t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)
        }
        return t
    },
    decode:function(e){
        var t="";
        var n,r,i;
        var s,o,u,a;
        var f=0;
        e=e.replace(/[^A-Za-z0-9+/=]/g,"");
        while(f<e.length){
            s=this._keyStr.indexOf(e.charAt(f++));
            o=this._keyStr.indexOf(e.charAt(f++));
            u=this._keyStr.indexOf(e.charAt(f++));
            a=this._keyStr.indexOf(e.charAt(f++));
            n=s<<2|o>>4;
            r=(o&15)<<4|u>>2;
            i=(u&3)<<6|a;
            t=t+String.fromCharCode(n);
            if(u!=64){
                t=t+String.fromCharCode(r)
            }
            if(a!=64){
                t=t+String.fromCharCode(i)
            }
        }
        t=Base64._utf8_decode(t);
        return t
    },
    _utf8_encode:function(e){
        e=e.replace(/rn/g,"n");
        var t="";

        for(var n=0; n<e.length; n++){
            var r=e.charCodeAt(n);

            if(r<128){
                t+=String.fromCharCode(r)
            }
            else if(r>127&&r<2048){
                t+=String.fromCharCode(r>>6|192);
                t+=String.fromCharCode(r&63|128)
            }else{
                t+=String.fromCharCode(r>>12|224);
                t+=String.fromCharCode(r>>6&63|128);
                t+=String.fromCharCode(r&63|128)
            }
        }
        return t
    },
    _utf8_decode:function(e){
        var t="";
        var n=0;
        var r=c1=c2=0;
        while(n<e.length){
            r=e.charCodeAt(n);

            if(r<128){
                t+=String.fromCharCode(r);
                n++
            }
            else if(r>191&&r<224){
                c2=e.charCodeAt(n+1);
                t+=String.fromCharCode((r&31)<<6|c2&63);
                n+=2
            }
            else{
                c2=e.charCodeAt(n+1);
                c3=e.charCodeAt(n+2);
                t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);
                 n+=3
            }
        }
        return t
    }
}