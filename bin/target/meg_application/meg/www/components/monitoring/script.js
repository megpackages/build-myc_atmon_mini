var Monitoring = {
	createTable : null,
	pagerCount: 10,
	sensorTimeout: null,
	deviceTimeout: null,
	html: null,
	searchArray: [],
	devsenArray: [],
	refreshStatus: true,
	firstLoading: false
};

Monitoring.html = '<div id="alertDelete" style="width: 400px;" class="panelDiv">'
					+ '<span class="text"></span>'
					+ '<div class="buttons">'
					+ '	<input type="submit" id="buttonRemove" name="buttonRemove" class="submit">'
					+ '	<input type="submit" id="buttonClose" name="buttonClose" class="submit">'
					+ '</div>'
				+ '</div>'
				
				+ '<div id="SelectUpdate" style="width: 400px;" class="panelDiv">'
					+ '<span class="text"></span>'
					+ '<div class="buttons">'
					+ '	<input type="submit" id="readValue" name="readValue" class="submit">'
					+ '	<input type="submit" id="writeValue" name="writeValue" class="submit">'
					+ '</div>'
				+ '</div>'	
						
				+ '<div id="windowEdit" style="width: 600px;" class="panelDiv">'
					+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="tableUpdate">'
					+ '	<tr>'
					+ '		<td id="modulsensorname" width="40%"></td>'
					+ '		<td id="voltaj" width="60%"></td>'
					+ '	</tr>'
					+ '</table>'
					+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="tableUpdateList">'
					+ '</table>'
					+ '<div class="buttons">'
					+ '	<input type="submit" id="buttonUpdate" name="buttonUpdate" class="submit">'
					+ '	<input type="submit" id="buttonCancel" name="buttonCancel" class="submit">'
					+ '</div>'
				+ '</div>'
				
				+ '<div id="windowStatus" style="width: 600px;" class="panelDiv">'
					+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="tableStatusUpdate">'
					+ '	<tr>'
					+ '		<td id="modulsensorname" width="40%"></td>'
					+ '		<td id="voltaj" width="60%"></td>'
					+ '	</tr>'
					+ '</table>'
					+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="tableStatusList">'
					+ '</table>'
					+ '<div class="buttons">'
					+ '	<input type="submit" id="buttonStatusUpdate" name="buttonStatusUpdate" class="submit">'
					+ '	<input type="submit" id="buttonStatusCancel" name="buttonStatusCancel" class="submit">'
					+ '</div>'
				+ '</div>'
				
				+ '<div id="windowFiltre" style="width: 600px;" class="panelDiv">'
					+ '<div class="alert"></div>'
					+ '<div class="tableFiltreList">'
					+ '</div>'
					+ '<div class="buttons">'
					+ '	<input type="submit" id="buttonSelectAll" name="buttonSelectAll" class="submit">'
					+ '	<input type="submit" id="buttonUnSelect" name="buttonUnSelect" class="submit">'
					+ '	<input type="submit" id="buttonFiltre" name="buttonFiltre" class="submit">'
					+ '	<input type="submit" id="buttonCancel" name="buttonCancel" class="submit">'
					+ '</div>'
				+ '</div>'
				
				+ '<div class="NotificationArea" style="display: none;">'
					+ '<table id="header" cellpadding="0" cellspacing="0" border="0" width="100%">'
					+ '	<tr class="head" id="header">'
					+ '		<td><a id="description"></a></td>'
					+ '	</tr>'
					+ '</table>'
					+ '<div id="noNotification"></div>'
				+ '</div>'
			
				+ '<div class="tableContainer">'
					+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="tableSensors">'
					+ '	<tr class="subject">'
					+ '		<td colspan="3" id="tableSubject"></td>'
					+ '		<td colspan="3"><span id="tableFiltreName"></span><span id="tableRefresh"></span></td>'
					+ '	</tr>'
					+ '	<tr class="head">'
					+ '		<td width="18%"><input name="deviceName" id="deviceName" type="text" class="tableInput"><div class="delete" id="deviceNameDel"></div></td>'
					+ '		<td width="25%"><input name="sensorName" id="sensorName" type="text" class="tableInput"><div class="delete" id="sensorNameDel"></div></td>'
					+ '		<td width="20%" id="tableValue"></td>'
					+ '		<td width="10%" id="tableStatus"></td>'
					+ '		<td width="10%" id="tableTime"></td>'
					+ '		<td width="17%"><select id="tablePaging">'
					+ '				<option value="10">10</option>'
					+ '				<option value="20">20</option>'
					+ '				<option value="30">30</option>'
					+ '				<option value="40">40</option>'
					+ '				<option value="50">50</option>'
					+ '			</select></td>'
					+ '	</tr>'
					+ '</table>'
					+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" id="tableList">'
					+ '</table>'
				+ '</div>'
				
				+ '<div id="pager"></div>';
			

var qSensor = null;
var qDevice = null;

Monitoring.onLoad = function (status) {
	Main.refreshTimer = Main.user.refresh_period * 1000;
	document.getElementById("monitoring").innerHTML = Monitoring.html;
	if(status == undefined) {
		Monitoring.cachePG();
		Monitoring.itemsCall(1,"new");
	}
	$("div#monitoring").css("display","block");

	$("#tableSubject").html(language.sensorlist);
	$("#tableFiltreName").html(language.filtre_name);
	$("#tableRefresh").html(language.refresh + "<b></b>");
	$("#deviceName").val(language.devicename);
	$("#sensorName").val(language.sensorname);
	$("#tableValue").html(language.value);
	$("#tableStatus").html(Monitoring.createStatusCombo());
	$("#tableTime").html(language.time);
	$("div#SelectUpdate span").html(language.readOrWriteValue);
	$("div#SelectUpdate input#readValue").val(language.readNewValue);
	$("div#SelectUpdate input#writeValue").val(language.writeNewValue);
	
	Monitoring.startTimer();
	Main.loading();
};

Monitoring.close = function () {
	Main.loading();
	clearInterval(Main.autoTimer);
	clearInterval(Main.countInterval);
	Monitoring.refreshStatus = true;
	document.getElementById("monitoring").innerHTML = "";
	$("div#monitoring").css("display","none");

	Monitoring.pagerCount = 10;
	Monitoring.sensorTimeout = null;
	Monitoring.deviceTimeout = null;
	Monitoring.searchArray = [];
	Monitoring.devsenArray = [];
	refreshType = null;
	qSensor = null;
	qDevice = null;
};

Monitoring.createStatusCombo = function() {
	var select = document.createElement("select");
	select.setAttribute("style","width:54px");
	var all =  document.createElement("option");
	all.setAttribute("value",0);
	all.setAttribute("selected","selected");
	all.innerHTML = language.all;
	var normal =  document.createElement("option");
	normal.setAttribute("value",1);
	normal.innerHTML = language.normal;
	var warning =  document.createElement("option");
	warning.setAttribute("value",2);
	warning.innerHTML = language.warning;
	var alarm =  document.createElement("option");
	alarm.setAttribute("value",4);
	alarm.innerHTML = language.alarm;
	var inactive =  document.createElement("option");
	inactive.setAttribute("value",8);
	inactive.innerHTML = language.inactive;
	var lost =  document.createElement("option");
	lost.setAttribute("value",16);
	lost.innerHTML = language.lost;
	var output =  document.createElement("option");
	output.setAttribute("value",32);
	output.innerHTML = language.output;

	select.appendChild(all);
	select.appendChild(normal);
	select.appendChild(alarm);
	select.appendChild(warning);
	select.appendChild(inactive);
	select.appendChild(output);
	select.appendChild(lost);

	return select;
};

Monitoring.startTimer = function() {
	Main.autoTimer = setInterval( function() {
		clearInterval(Main.countInterval);
		$("div.tableContainer span#tableRefresh").addClass("passive");
		Monitoring.refreshStatus = false;
		var currentPage = parseInt($("#monitoring div#pager li.bttn.act").data("page"));
		//Monitoring.clearElement();
		Monitoring.itemsCall(currentPage, "new");
	  	Monitoring.refreshFalse();
	},Main.refreshTimer);
};

Monitoring.refreshTable = function() {
	  	clearInterval(Main.autoTimer);
	  	Monitoring.startTimer();
		$("div.tableContainer span#tableRefresh").addClass("passive");
		Monitoring.refreshStatus = false;
		var currentPage = parseInt($("#monitoring div#pager li.bttn.act").data("page"));
		//Monitoring.clearElement();
		Monitoring.itemsCall(currentPage, "new");
	  	Monitoring.refreshFalse();
};

Monitoring.refreshFalse = function () {
	var timer = 20;
	$("div.tableContainer span#tableRefresh b").html(" 00:20");

	Main.countInterval = setInterval(function() {
		if(timer == 0) {
			clearInterval(Main.countInterval);
			Monitoring.refreshStatus = true;
			$("div.tableContainer span#tableRefresh").removeClass("passive");
			$("div.tableContainer span#tableRefresh b").html("");
		} else {
			timer--;
			if(timer < 10) {
				timer = "0"+timer;
			}
			
			if(!$("div.tableContainer span#tableRefresh").hasClass("passive")) {
				$("div.tableContainer span#tableRefresh").addClass("passive");
			}

			$("div.tableContainer span#tableRefresh b").html(" 00:" + timer);
		}
	}, 1000);
};

Monitoring.cachePG = function() {	
	var tablePagingStatus = getCookie("tablePaging");
	if(tablePagingStatus == null || tablePagingStatus == undefined || tablePagingStatus == "") {
		tablePagingStatus = Monitoring.pagerCount;
		$( "select#tablePaging option" ).each(function() {
	      	var str = parseInt($( this ).text());
			if(str == parseInt(tablePagingStatus)) {
				$( this ).attr("selected","selected");
			}
	    });
		setCookie("tablePaging",tablePagingStatus,365);
	} else {
		Monitoring.pagerCount = tablePagingStatus;
		$( "select#tablePaging option" ).each(function() {
	      	var str = parseInt($( this ).text());
			if(str == parseInt(tablePagingStatus)) {
				$( this ).prop('selected', true);
			}
	    });
	}
	return tablePagingStatus;
};

Monitoring.characterLimit = function (text, limit) {
	var text = text;

	if(text.length > limit) {
		text = text.substr(0, limit) + " ...";
	}

	return text;
};

Monitoring.createOutputTable = function (item) {
	var getID = item.objectid;
	var value = item.value;

	var tr = document.createElement("tr");
	var trText = document.createElement("tr");

	var tdValueName = document.createElement("td");
	var tdValue = document.createElement("td");
	var valueInput = document.createElement("input");
	tdValueName.setAttribute("width","40%");
	tdValueName.innerHTML = language.value;

	valueInput.setAttribute("type","text");
	valueInput.setAttribute("id","tdValue");
	valueInput.setAttribute("value",value);
	tdValue.setAttribute("id","tdValueID");
	tdValue.appendChild(valueInput);

	var tdText = document.createElement("td");
	tdText.setAttribute("colspan",2);
	tdText.setAttribute("class","notification");
	tdText.innerHTML = language.outputsensornotification;

	tr.appendChild(tdValueName);
	tr.appendChild(tdValue);
	trText.appendChild(tdText);

	$("#windowEdit table.tableUpdateList").append(tr);
	//$("#windowEdit table.tableUpdateList").append(trText);
};

Monitoring.createTable = function (item) {
	var getID = item.objectid;
	var hwaddr = item.objectid;
	var objectid = hwaddr;
	var unit = item.unit;
	var deviceName = Monitoring.characterLimit(item.devicename, 15);
	var sensorName = Monitoring.characterLimit(item.sensorname, 20);
	var sensorid = item.sensorid;
	var type = parseInt(item.type);
	var read_write = parseInt(item.read_write);
	var name = deviceName + " &raquo; " + sensorName;
	var time = Monitoring.timeFormat(item.time, item.gmtoff, true);
	var titleTime = Monitoring.timeFormat(item.time, item.gmtoff, false);
	var status = Monitoring.statusControl(item.status);
	var value = Monitoring.characterLimit(item.value == "n/a" ? "": item.value, 10);
	var table = null;
	var ID = getID.split("/");
	var tr = document.createElement("tr");
	tr.setAttribute("class",status);
	var tdDevice = document.createElement("td");
	tdDevice.setAttribute("width","18%");
	var spanDevice =  document.createElement("span");
	spanDevice.innerHTML = deviceName;
	spanDevice.setAttribute("class","device");
	if(item.devicename.length > 15) spanDevice.setAttribute("data-title",item.devicename);
	tdDevice.appendChild(spanDevice);

	var tdSensor = document.createElement("td");
	tdSensor.setAttribute("id", "name");
	tdSensor.setAttribute("objectid", item.objectid);
	tdSensor.setAttribute("width","25%");
	var spanSensor =  document.createElement("span");
	spanSensor.innerHTML = sensorName;
	spanSensor.setAttribute("class","sensor");
	if(item.sensorname.length > 20) spanSensor.setAttribute("data-title",item.sensorname);
	tdSensor.appendChild(spanSensor);

	var tdValue = document.createElement("td");
	tdValue.setAttribute("width","20%");
	var spanValue =  document.createElement("span");
	spanValue.innerHTML = value;
	spanValue.setAttribute("class","sensor");
	if(item.value.length > 10) spanValue.setAttribute("data-title",item.value);
	tdValue.appendChild(spanValue);
	var tdStatus = document.createElement("td");
	tdStatus.setAttribute("width","10%");
	var spanStatus = document.createElement("span");
	if(status==language.normal) {
		tdStatus.setAttribute("class","normal");
	} else if(status==language.warning) {
		tdStatus.setAttribute("class","warning");
	} else if(status==language.alarm) {
		tdStatus.setAttribute("class","alarm");
	} else if(status==language.inactive) {
		tdStatus.setAttribute("class","inactive");
	} else if(status==language.output) {
		tdStatus.setAttribute("class","output");
	} else if(status==language.lost) {
		tdStatus.setAttribute("class","lost");
	}
	spanStatus.innerHTML = status;
	tdStatus.appendChild(spanStatus);
	var tdTime = document.createElement("td");
	tdTime.setAttribute("width","10%");
	var spanTime =  document.createElement("span");
	spanTime.innerHTML = time;
	spanTime.setAttribute("class","time");
	spanTime.setAttribute("data-title",titleTime);
	tdTime.appendChild(spanTime);
	var tdButtons = document.createElement("td");
	tdButtons.setAttribute("width","17%");
	tdButtons.setAttribute("align","right");

	var getSensor = "getSensor&hwaddr="+ ID[0] + "&portnum="+ ID[1] +"&devaddr="+ ID[2] + "&ionum=" + ID[3];
	var getReports = "getSensor&hwaddr="+ ID[0] + "&portnum="+ ID[1] +"&devaddr="+ ID[2] + "&ionum=" + ID[3];
	var removeSensor = "removeSensor&hwaddr="+ ID[0] + "&portnum="+ ID[1] +"&devaddr="+ ID[2] + "&ionum=" + ID[3];
	var sensorUpdate = getID;

	switch(Main.user.grouptype) {
		case "0":
		case 0:
		case "1":
		case 1:
			tdButtons.innerHTML = "<span class='update' id='sensorUpdate' data-type='" + type + "' data-name='" + name + "' data-id='"+ sensorUpdate + "' data-read-write='" + read_write + "'></span><span id='sensorEdit' data-type='" + type + "' data-name='" + name + "' class='edit' data-id='" + getSensor + "' objectid=" + objectid+ "></span><span class='rapor' id='sensorRapor' data-hwaddr='" + hwaddr +"' data-type='" + type + "' data-unit='" + unit + "' data-name='" + name + "' data-id ='" + sensorid + "' data-record='" + item.record + "'></span>";
		break;
		case "2":
		case 2:
			tdButtons.innerHTML = "<span class='rapor' id='sensorRapor' data-name='" + name + "' data-id ='" + sensorid + "' data-record='" + item.record + "' data-unit='" + unit + "' data-hwaddr='" + hwaddr + "'></span>";
		break;
	}

	tr.appendChild(tdDevice);
	tr.appendChild(tdSensor);
	tr.appendChild(tdValue);
	tr.appendChild(tdStatus);
	tr.appendChild(tdTime);
	tr.appendChild(tdButtons);
	$("#monitoring table#tableList").append(tr);

	$("#monitoring table#tableList span#sensorUpdate").attr("data-title",language.sensorsettings);
	$("#monitoring table#tableList span#sensorRapor").attr("data-title",language.sensorrapor);
	$("#monitoring table#tableList span#sensorEdit").attr("data-title",language.sensoredit);
};

Monitoring.createPaging = function (count, page) {
	var pageNum = parseInt(count / Monitoring.cachePG());
	if(count != Monitoring.cachePG() * pageNum) {
		pageNum++;
	}
	if(page > pageNum) {
		page = pageNum;
	}
	
	if(pageNum > 1 && (notificationActive == false || $("div.NotificationArea table#suspendedDevices tr").length == 0)) {
		$("#monitoring div#pager").css("display","block");
	} else {
		$("#monitoring div#pager").css("display","none");		
	}
	if($("#monitoring div#pager").html() == "") {
		var startCount = ((page-1) * Monitoring.cachePG()) + 1;
		var finishCount = (page * Monitoring.cachePG());
		
		if(finishCount > count) {
			finishCount = count;
		}

		var divCount = '<div><b>' + startCount + "</b>|<b>" + finishCount + '</b></div>';
		var divPrev = '<div class="prev" data-page="1">' + language.previous + '</div>';
		var divNext = '<div class="next" data-page="'+ pageNum +'">' + language.next +'</div>';

		$("#monitoring div#pager").append(divCount);
		$("#monitoring div#pager").append(divPrev);

		var divCont = document.createElement("ul");
		divCont.setAttribute("id","nums");
		$("#monitoring div#pager").append(divCont);

		var startPage = page-5;

		if(startPage <= 0) {
			startPage = 0;
		}

		var finishPage = startPage+10;

		if(finishPage > pageNum) {
			finishPage = pageNum;
		}

		if(startPage > 0) {
			for(var i=0; i<1; i++) {
				var div = document.createElement("li");
					div.setAttribute("data-page",(i+1));

				if(page == (i+1)) { 
					div.setAttribute("class","act bttn");
				} else {
					div.setAttribute("class","pas bttn");			
				}

				var sCount = ((i) * Monitoring.cachePG()) + 1;
				var fCount = ((i+1) * Monitoring.cachePG());
				if(fCount > count) {
					fCount = count;
				}

				div.innerHTML = (i+1);
				$("#monitoring div#pager ul#nums").append(div);
			}
		}

		if(startPage > 1) {
			var li = document.createElement("li");
			li.setAttribute("class","pas");
			li.innerHTML = "...";
			$("#monitoring div#pager ul#nums").append(li);
		}

		for(var i=startPage; i<finishPage; i++) {
			var div = document.createElement("li");
				div.setAttribute("data-page",(i+1));

			if(page == (i+1)) { 
				div.setAttribute("class","act bttn");
			} else {
				div.setAttribute("class","pas bttn");			
			}

			var sCount = ((i) * Monitoring.cachePG()) + 1;
			var fCount = ((i+1) * Monitoring.cachePG());
			if(fCount > count) {
				fCount = count;
			}

			div.innerHTML = (i+1);
			$("#monitoring div#pager ul#nums").append(div);
		}

		if(page < pageNum-6) {
			var li = document.createElement("li");
			li.setAttribute("class","pas");
			li.innerHTML = "...";
			$("#monitoring div#pager ul#nums").append(li);
		}

		if(pageNum > 10 && page < pageNum-5) {
			for(var i=pageNum-1; i<pageNum; i++) {
				var div = document.createElement("li");
					div.setAttribute("data-page",(i+1));

				if(page == (i+1)) { 
					div.setAttribute("class","act bttn");
				} else {
					div.setAttribute("class","pas bttn");			
				}

				var sCount = ((i) * Monitoring.cachePG()) + 1;
				var fCount = ((i+1) * Monitoring.cachePG());
				if(fCount > count) {
					fCount = count;
				}

				div.innerHTML = (i+1);
				$("#monitoring div#pager ul#nums").append(div);
			}
		}

		$("#monitoring div#pager").append(divNext);
	} else {
		$("#monitoring div#pager div.bttn:nth-child(" + (page+1) + ")").removeClass("pas").addClass("act");
	}
};

Monitoring.clearElement = function() {
	$("#monitoring table#tableList").html("");
	$("#monitoring div#pager").html("");
};

Monitoring.statusControl = function(value) {
	var status = value;

	if(status==0) {
		status = "total";
	} else if(status==1) {
		status = language.normal;
	} else if(status==2) {
		status = language.warning;
	} else if(status==4) {
		status = language.alarm;
	} else if(status==8) {
		status = language.inactive;
	} else if(status==16) {
		status = language.lost;
	} else if(status==32) {
		status = language.output;
	}

	return status;
};

Monitoring.timeFormat = function(time, gmtoff, type) {
	var item = null;
	var time = parseInt(time)*1000;
	var gmtoff = parseInt(gmtoff)*1000;
	time = time + gmtoff;
	var date = new Date(time);
	var theyear=date.getUTCFullYear();
	var themonth=date.getUTCMonth()+1;
	var thetoday=date.getUTCDate();
	var thehours=date.getUTCHours();
	var theminutes=date.getUTCMinutes();
	var themseconds=date.getUTCSeconds();

	if(themonth<10) themonth = "0" + themonth;
	if(thetoday<10) thetoday = "0" + thetoday;
	if(thehours<10) thehours = "0" + thehours;
	if(theminutes<10) theminutes = "0" + theminutes;
	if(themseconds<10) themseconds = "0" + themseconds;

	//return thetoday+"."+themonth+"."+theyear+" " + thehours +":" +theminutes+":" +themseconds;
	if(type) {
		item = thehours +":" +theminutes+":" +themseconds;
	} else {
		item = thetoday+"."+themonth+"."+theyear+" " + thehours +":" +theminutes+":" +themseconds;
	}
	return item;
};

Monitoring.createSettingTable = function(item) {
	item.type = parseInt(item.type);

	var alarm_active = item.alarm_active;
	var record = item.record;
	var read_enable = item.read_enable;

	var tr1 = document.createElement("tr");
	var tr2 = document.createElement("tr");
	var tr3 = document.createElement("tr");

	var tdAlarmActiveName = document.createElement("td");
	var tdAlarmActive = document.createElement("td");
	var AlarmActiveInput = document.createElement("select");

	var tdRecordName = document.createElement("td");
	var tdRecord = document.createElement("td");
	var RecordInput = document.createElement("select");

	var tdReadEnableName = document.createElement("td");
	var tdReadEnable = document.createElement("td");
	var ReadEnableInput = document.createElement("select");

	tdAlarmActiveName.setAttribute("width","40%");
	tdRecordName.setAttribute("width","40%");
	tdReadEnableName.setAttribute("width","40%");

	tdAlarmActiveName.innerHTML = language.alarm_active;
	tdRecordName.innerHTML = language.record;
	tdReadEnableName.innerHTML = language.read_enable;

	AlarmActiveInput.setAttribute("id","AlarmActive");
	AlarmActiveInput.setAttribute("name","AlarmActive");

	var off = document.createElement("option");
	off.setAttribute("value",0);
	off.innerHTML = language.off;

	var on = document.createElement("option");
	on.setAttribute("value",1);
	on.innerHTML = language.on;

	if(parseInt(alarm_active) == 0) {
		off.setAttribute("selected","selected");
	} else {
		on.setAttribute("selected","selected");	
	}

	AlarmActiveInput.appendChild(on);
	AlarmActiveInput.appendChild(off);

	tdAlarmActive.appendChild(AlarmActiveInput);

	tr1.appendChild(tdAlarmActiveName);
	tr1.appendChild(tdAlarmActive);

	RecordInput.setAttribute("id","Record");
	RecordInput.setAttribute("name","Record");
	
	var off = document.createElement("option");
	off.setAttribute("value",0);
	off.innerHTML = language.off;

	var on = document.createElement("option");
	on.setAttribute("value",1);
	on.innerHTML = language.on;

	if(parseInt(record) == 0) {
		off.setAttribute("selected","selected");
	} else {
		on.setAttribute("selected","selected");	
	}

	RecordInput.appendChild(on);
	RecordInput.appendChild(off);

	tdRecord.appendChild(RecordInput);

	tr2.appendChild(tdRecordName);
	tr2.appendChild(tdRecord);

	ReadEnableInput.setAttribute("id","ReadEnable");
	ReadEnableInput.setAttribute("name","ReadEnable");
	
	var off = document.createElement("option");
	off.setAttribute("value",0);
	off.innerHTML = language.off;

	var on = document.createElement("option");
	on.setAttribute("value",1);
	on.innerHTML = language.on;

	if(parseInt(read_enable) == 0) {
		off.setAttribute("selected","selected");
	} else {
		on.setAttribute("selected","selected");	
	}

	ReadEnableInput.appendChild(on);
	ReadEnableInput.appendChild(off);

	tdReadEnable.appendChild(ReadEnableInput);

	tr3.appendChild(tdReadEnableName);
	tr3.appendChild(tdReadEnable);

	$("div#windowStatus table.tableStatusList").append(tr1);
	$("div#windowStatus table.tableStatusList").append(tr2);
	$("div#windowStatus table.tableStatusList").append(tr3);
};

Monitoring.clearItemsCall = function (page) {

	//Monitoring.clearElement();
	/*$('#sensorName').val(language.sensorname);
	$('#deviceName').val(language.devicename);
	$("#deviceNameDel").css("display","none");
  	$('#deviceName').removeClass("focus");
  	$("#sensorNameDel").css("display","none");
  	$('#sensorName').removeClass("focus");*/

	if(page == undefined) {
		page = 1;
	}

	if(($('#sensorName').val() == language.sensorname && $('#deviceName').val() == language.devicename) || ($('#sensorName').val() == "" && $('#deviceName').val() == "")) {
		Main.getRequest("getGroupSensorsData",page);				
	} else if($('#sensorName').val() != language.sensorname && $('#deviceName').val() != language.devicename) {
		Main.getRequest("getGroupSensorsData",page);	
	} else{				
		Main.getRequest("getGroupSensorsData",page);	
	}

	Monitoring.createAlertPanel();
};

Monitoring.itemsCall = function (page, refresh)
{

	if(!(parseInt(page) % 1 === 0))
	{
		page = 1;
	}

	if(refresh == "new") // ilk acilis
	{
		Main.getDeviceList("getGroupSensorsData",page);
	}
	else if(($('#sensorName').val() == language.sensorname && $('#deviceName').val() == language.devicename) 
	        || ($('#sensorName').val() == "" && $('#deviceName').val() == "")) // filtreleme yapilmamis
	{
		if(page == 1)
		{
			var finishElementCount = Monitoring.cachePG() * page;
			var startElementCount = finishElementCount - Monitoring.cachePG();

			if(finishElementCount > jsonList.length)
			{
				finishElementCount = jsonList.length;
			}

			Monitoring.clearElement();

			for(var i=startElementCount; i < finishElementCount; i++)
			{
				Monitoring.createTable(jsonList[i]);
			}

			Monitoring.createPaging(jsonList.length, page);
		}
		else
		{
			var finishElementCount = Monitoring.cachePG() * page;
			var startElementCount = finishElementCount - Monitoring.cachePG();

			if(finishElementCount > jsonList.length)
			{
				finishElementCount = jsonList.length;
			}
			Monitoring.clearElement();

			for(var i=startElementCount; i < finishElementCount; i++)
			{
				Monitoring.createTable(jsonList[i]);
			}

			Monitoring.createPaging(jsonList.length, page);
		}	
	}
	else if($('#sensorName').val() != language.sensorname && $('#deviceName').val() != language.devicename)
	{ // hem device hem sensor name e gore filtreleme var 
		Main.searchRequest(Monitoring.devsenArray,page);
	}
	else // ya device ya da sensore gore filtreleme var
	{				
		Main.searchRequest(Monitoring.searchArray,page);
	}
};

Monitoring.getDeviceFiltre = function() {
	for(var i = 0; i < deviceList.length; i++) {
		var devName = deviceList[i].name;
		var div = document.createElement("div");
		div.setAttribute("class","checkbox");

		var label = document.createElement("label");
		label.setAttribute("for","checkbox"+i);
		label.innerHTML = devName;
		
		var checkbox = document.createElement("input");
		checkbox.setAttribute("class","checkbox");	
		checkbox.setAttribute("type","checkbox");	
		checkbox.setAttribute("name","checkbox"+i);	
		checkbox.setAttribute("id","checkbox"+i);	
		checkbox.setAttribute("data-index",i);
		checkbox.checked = deviceList[i].checked;
		div.appendChild(checkbox);
		div.appendChild(label);

		$("#windowFiltre div.tableFiltreList").append(div);
	}
};

Monitoring.refreshItems = function() {

	var counter = 0;
	var filterArray = new Array();

  	$("#sensorNameDel").css("display","none");
  	$('#sensorName').removeClass("focus");
  	$('#sensorName').val(language.sensorname);

  	$("#deviceNameDel").css("display","none");
  	$('#deviceName').removeClass("focus");
  	$('#deviceName').val(language.devicename);
  	
	for(var k = 0; k < deviceList.length; k++) {
		if (deviceList[k].checkedBuf)
			counter++;
	}
	
	if (counter == 0)
	{
		$("div#windowFiltre div.alert").html(language.nodeviceselect);
	}
	else
	{
		counter = 0;
		for(var k = 0; k < deviceList.length; k++) {
			deviceList[k].checked = deviceList[k].checkedBuf;		
		}
		
		for(var i=0; i < Main.fullItems.length; i++) {
			for(var k = 0; k < deviceList.length; k++) {
				if(Main.fullItems[i].deviceoid == deviceList[k].objectid) {
					if(deviceList[k].checked) {
						filterArray[counter] = Main.fullItems[i];
						counter++;
					}
				}
	
			}
		}
		setCookie("deviceList",JSON.stringify(deviceList),365);
	
		jsonList = filterArray;
		//Monitoring.clearElement();
		Main.searchRequest(jsonList, 1);
		$("div#windowFiltre div.alert").html("");
		$("div#overlay").css("display","none");
		$("div#windowFiltre").css("display","none");
	}

	Monitoring.createAlertPanel();
};

Monitoring.sensorSearch = function(timer) {
	Monitoring.sensorTimeout = setTimeout(function(){
      		if(($('#sensorName').val() == null || $('#sensorName').val() == "") && $('#deviceName').val() == language.devicename) {

	      		switch(refreshType) {
      				case "total":
			      		Monitoring.searchArray = new Array;
						jsonList = Main.filterControl();
					    //Monitoring.clearElement();
					    Main.searchRequest(jsonList,1);
      				break;
      				case "normal":
						Monitoring.searchArray = normalJSON;
					    //Monitoring.clearElement();
					    Main.searchRequest(Monitoring.searchArray,1);
      				break;
					case "warning":
						Monitoring.searchArray = warningJSON;
					    //Monitoring.clearElement();
					    Main.searchRequest(Monitoring.searchArray,1);
					break;
					case "alarm":
						Monitoring.searchArray = alarmJSON;
					    //Monitoring.clearElement();
					    Main.searchRequest(Monitoring.searchArray,1);
					break;
					case "inactive":
						Monitoring.searchArray = inactiveJSON;
					    //Monitoring.clearElement();
					    Main.searchRequest(Monitoring.searchArray,1);
					break;
					case "output":
						Monitoring.searchArray = outputJSON;
					    //Monitoring.clearElement();
					    Main.searchRequest(Monitoring.searchArray,1);
					break;
					case "lost":
						Monitoring.searchArray = lostJSON;
					    //Monitoring.clearElement();
					    Main.searchRequest(Monitoring.searchArray,1);
					break;
					case "search":
						//Monitoring.searchArray = lostJSON;
					    //Monitoring.clearElement();
					    Main.searchRequest(Monitoring.searchArray,1);
					break;
					default:
			      		Monitoring.searchArray = new Array;
						jsonList = Main.filterControl();
					    //Monitoring.clearElement();
					    Main.searchRequest(jsonList,1);
					break;
      			}

		  		$("#sensorNameDel").css("display","none");
		  		$('#sensorName').removeClass("focus");
			} else if($('#sensorName').val() == null || $('#sensorName').val() == "") {
				//$('#sensorName').val(language.sensorname);
				switch(refreshType) {
      				case "total":
						jsonList = Main.filterControl();
      				break;
      				case "normal":
						jsonList = normalJSON;
      				break;
					case "warning":
						jsonList = warningJSON;
					break;
					case "alarm":
						jsonList = alarmJSON;
					break;
					case "inactive":
						jsonList = inactiveJSON;
					break;
					case "output":
						jsonList = outputJSON;
					break;
					case "lost":
						jsonList = lostJSON;
					break;
					default:
						jsonList = Main.filterControl();
					break;
      			}

		  		$("#sensorNameDel").css("display","none");
		  		$('#sensorName').removeClass("focus");

				qDevice = $('#deviceName').val().toLowerCase();
	      		
	      		Monitoring.searchArray = new Array;
	      		var newCount = 0;
	      		for(var i=0; i < jsonList.length; i++) {
	      			var tolowercase = jsonList[i].devicename.toLowerCase();
		      		var search = tolowercase.indexOf(qDevice);
			      	if(search != -1) {
			      		Monitoring.searchArray[newCount] = jsonList[i];
			      		newCount++;
		      		}   			
	      		}

			    //Monitoring.clearElement();
			    Main.searchRequest(Monitoring.searchArray,1);
			} else if(qSensor != $('#sensorName').val() && $('#deviceName').val() != language.devicename) {
	      			
	      		switch(refreshType) {
      				case "total":
						jsonList = Main.filterControl();
      				break;
      				case "normal":
						jsonList = normalJSON;
      				break;
					case "warning":
						jsonList = warningJSON;
					break;
					case "alarm":
						jsonList = alarmJSON;
					break;
					case "inactive":
						jsonList = inactiveJSON;
					break;
					case "output":
						jsonList = outputJSON;
					break;
					case "lost":
						jsonList = lostJSON;
					break;
					default:
						jsonList = Main.filterControl();
					break;
      			} 

					qSensor = $('#sensorName').val().toLowerCase();
		      		Monitoring.searchArray = new Array;
		      		var newCount = 0;
		      		for(var i=0; i < jsonList.length; i++) {
		      			var tolowercase = jsonList[i].sensorname.toLowerCase();
			      		var search = tolowercase.indexOf(qSensor);
				      	if(search != -1) {
				      		Monitoring.searchArray[newCount] = jsonList[i];
				      		newCount++;
			      		}   			
		      		}

		      		qDevice = $('#deviceName').val().toLowerCase();
		      		Monitoring.devsenArray = new Array;
		      		var newCount2 = 0;
		      		for(var i=0; i < Monitoring.searchArray.length; i++) {
		      			var tolowercase = Monitoring.searchArray[i].devicename.toLowerCase();
			      		var search = tolowercase.indexOf(qDevice);
				      	if(search != -1) {
				      		Monitoring.devsenArray[newCount2] = Monitoring.searchArray[i];
				      		newCount2++;
			      		}   			
		      		}

				    //Monitoring.clearElement();
				    Main.searchRequest(Monitoring.devsenArray,1);


			} else if(qSensor != $('#sensorName').val() && $('#deviceName').val() == language.devicename) {
		      	qSensor = $('#sensorName').val().toLowerCase();
	      		
		      	switch(refreshType) {
      				case "total":
						jsonList = Main.filterControl();
      				break;
      				case "normal":
						jsonList = normalJSON;
      				break;
					case "warning":
						jsonList = warningJSON;
					break;
					case "alarm":
						jsonList = alarmJSON;
					break;
					case "inactive":
						jsonList = inactiveJSON;
					break;
					case "output":
						jsonList = outputJSON;
					break;
					case "lost":
						jsonList = lostJSON;
					break;
					default:
						jsonList = Main.filterControl();
					break;
      			} 

		      	Monitoring.searchArray = new Array;
		      	var newCount = 0;
		      	for(var i=0; i < jsonList.length; i++) {
		      		var tolowercase = jsonList[i].sensorname.toLowerCase();
			      	var search = tolowercase.indexOf(qSensor);
				    if(search != -1) {
				    	Monitoring.searchArray[newCount] = jsonList[i];
				    	newCount++;
			      	}   			
		      	}

				//Monitoring.clearElement();
				Main.searchRequest(Monitoring.searchArray,1);
			}
    	}, timer); 
};

Monitoring.deviceSearch = function(timer) {
	Monitoring.deviceTimeout = setTimeout(function(){
      		if(($('#deviceName').val() == null || $('#deviceName').val() == "") && $('#sensorName').val() == language.sensorname)  {
      			//$('#deviceName').val(language.devicename);
      			switch(refreshType) {
      				case "total":
			      		Monitoring.searchArray = new Array;
						jsonList = Main.filterControl();
					    //Monitoring.clearElement();
					    Main.searchRequest(jsonList,1);
      				break;
      				case "normal":
						Monitoring.searchArray = normalJSON;
					    //Monitoring.clearElement();
					    Main.searchRequest(Monitoring.searchArray,1);
      				break;
					case "warning":
						Monitoring.searchArray = warningJSON;
					    //Monitoring.clearElement();
					    Main.searchRequest(Monitoring.searchArray,1);
					break;
					case "alarm":
						Monitoring.searchArray = alarmJSON;
					    //Monitoring.clearElement();
					    Main.searchRequest(Monitoring.searchArray,1);
					break;
					case "inactive":
						Monitoring.searchArray = inactiveJSON;
					    //Monitoring.clearElement();
					    Main.searchRequest(Monitoring.searchArray,1);
					break;
					case "output":
						Monitoring.searchArray = outputJSON;
					    //Monitoring.clearElement();
					    Main.searchRequest(Monitoring.searchArray,1);
					break;
					case "lost":
						Monitoring.searchArray = lostJSON;
					    //Monitoring.clearElement();
					    Main.searchRequest(Monitoring.searchArray,1);
					break;
					case "search":
					    //Monitoring.clearElement();
					    Main.searchRequest(Monitoring.searchArray,1);
					break;
					default:
			      		Monitoring.searchArray = new Array;
						jsonList = Main.filterControl();
					    //Monitoring.clearElement();
					    Main.searchRequest(jsonList,1);
					break;
      			}

		  		$("#deviceNameDel").css("display","none");
		  		$('#deviceName').removeClass("focus");

			} else if($('#deviceName').val() == null || $('#deviceName').val() == "") {
				switch(refreshType) {
      				case "total":
						jsonList = Main.filterControl();
      				break;
      				case "normal":
						jsonList = normalJSON;
      				break;
					case "warning":
						jsonList = warningJSON;
					break;
					case "alarm":
						jsonList = alarmJSON;
					break;
					case "inactive":
						jsonList = inactiveJSON;
					break;
					case "output":
						jsonList = outputJSON;
					break;
					case "lost":
						jsonList = lostJSON;
					break;
					default:
						jsonList = Main.filterControl();
					break;
      			}

		  		$("#deviceNameDel").css("display","none");
		  		$('#deviceName').removeClass("focus");
				Monitoring.searchArray = new Array;
				qSensor = $('#sensorName').val().toLowerCase();
		      	var newCount = 0;
		      	for(var i=0; i < jsonList.length; i++) {
		      		var tolowercase = jsonList[i].sensorname.toLowerCase();
			      	var search = tolowercase.indexOf(qSensor);
				    if(search != -1) {
				      	Monitoring.searchArray[newCount] = jsonList[i];
				      	newCount++;
			      	}   			
		      	}

			    //Monitoring.clearElement();
			    Main.searchRequest(Monitoring.searchArray,1);

      		} else if(qDevice != $('#deviceName').val() && $('#sensorName').val() != language.sensorname) {
      			switch(refreshType) {
      				case "total":
						jsonList = Main.filterControl();
      				break;
      				case "normal":
						jsonList = normalJSON;
      				break;
					case "warning":
						jsonList = warningJSON;
					break;
					case "alarm":
						jsonList = alarmJSON;
					break;
					case "inactive":
						jsonList = inactiveJSON;
					break;
					case "output":
						jsonList = outputJSON;
					break;
					case "lost":
						jsonList = lostJSON;
					break;
					default:
						jsonList = Main.filterControl();
					break;
      			}

	      		qDevice = $('#deviceName').val().toLowerCase();
	      		Monitoring.searchArray = new Array;
	      		var newCount = 0;
	      		for(var i=0; i < jsonList.length; i++) {
	      			var tolowercase = jsonList[i].devicename.toLowerCase();
		      		var search = tolowercase.indexOf(qDevice);
			      	if(search != -1) {
			      		Monitoring.searchArray[newCount] = jsonList[i];
			      		newCount++;
		      		}   			
	      		}

	      		qSensor = $('#sensorName').val().toLowerCase();
		      	Monitoring.devsenArray = new Array;
		      	var newCount2 = 0;
		      	for(var i=0; i < Monitoring.searchArray.length; i++) {
		      		var tolowercase = Monitoring.searchArray[i].sensorname.toLowerCase();
			      	var search = tolowercase.indexOf(qSensor);
				    if(search != -1) {
				      	Monitoring.devsenArray[newCount2] = Monitoring.searchArray[i];
				      	newCount2++;
			      	}   			
		      	}

			    //Monitoring.clearElement();
			    Main.searchRequest(Monitoring.devsenArray,1);
			} else if(qDevice != $('#deviceName').val() && $('#sensorName').val() == language.sensorname) {
				switch(refreshType) {
      				case "total":
						jsonList = Main.filterControl();
      				break;
      				case "normal":
						jsonList = normalJSON;
      				break;
					case "warning":
						jsonList = warningJSON;
					break;
					case "alarm":
						jsonList = alarmJSON;
					break;
					case "inactive":
						jsonList = inactiveJSON;
					break;
					case "output":
						jsonList = outputJSON;
					break;
					case "lost":
						jsonList = lostJSON;
					break;
					default:
						jsonList = Main.filterControl();
					break;
      			} 
		      	qDevice = $('#deviceName').val().toLowerCase();
		      	Monitoring.searchArray = new Array;
		      	var newCount = 0;
		      	for(var i=0; i < jsonList.length; i++) {
		      		var tolowercase = jsonList[i].devicename.toLowerCase();
			      	var search = tolowercase.indexOf(qDevice);
				    if(search != -1) {
				      	Monitoring.searchArray[newCount] = jsonList[i];
				      	newCount++;
			      	}   			
		      	}

				//Monitoring.clearElement();
				Main.searchRequest(Monitoring.searchArray,1);				
			}

    	}, timer); 
};

Monitoring.updateValidate = function() {
	var value = true;
	var str0 = $("div#windowEdit input#tdValue").val();
	$("div#windowEdit input#tdValue").removeClass("alert");
	var div0 = document.createElement("div");
	div0.setAttribute("id","alertDiv0");

	if($("div#windowEdit td#tdValueID div").html() != undefined) {
		var d = document.getElementById('tdValueID');
		var olddiv = document.getElementById('alertDiv0');
		d.removeChild(olddiv);  	
	}
	if(str0.match(/[^\d]/)) {
	    $("div#windowEdit input#tdValue").addClass("alert");
	    div0.innerHTML = language.alert_msg;
	    $("div#windowEdit td#tdValueID").append(div0);
		$("div#windowEdit input#tdValue").focus();
		value = false;
	}
	return value;
};


$(function() {
	$("div#monitoring").on( "click", "table#tableList span#sensorDelete", function() {
		var url = $(this).data("id");
		$("div#overlay").css("display","block");
		$("div#alertDelete").css("display","block");

		$("div#alertDelete input#buttonRemove").val(language.remove);
		$("div#alertDelete input#buttonRemove").attr("data-id", url);
		$("div#alertDelete input#buttonClose").val(language.close);
		$("div#alertDelete span.text").html(language.removemessage);
	});

	$("div#monitoring").on( "click", "table#tableList span#sensorEdit", function() {
		$("div#editWindow td.alert").html("");
		var url = $(this).data("id");
		var name = $(this).data("name");
		$("div#overlay").css("display","block");
		$("div#editWindow").css("display","block");
		$("div#editWindow input#buttonUpdate").val(language.update);
		$("div#editWindow input#buttonCancel").val(language.cancel);
		$("div#editWindow td#modulsensorname").html(name);
		//$("div#editWindow td#modulsensorname").html(language.updatedevice + ": " + $(this).attr("objectid"));
		$("div#editWindow").attr("objectid", $(this).attr("objectid"));
		Devices.getEditSensor(url);
	});

	$("div#monitoring").on( "click", "table#tableList span#sensorRapor", function() {
		Reports.selects[0] = $(this).data("id");

		var unit = $(this).data("unit");
		var record = $(this).data("record");
		Reports.recordEnable = record.toString();
		var hwaddr =  "hwaddr=" + $(this).data("hwaddr");
		
		if(unit == "undefined") {
			unit = "&nbsp;";
		}

		Reports.selectnames[0] = {
			name: $(this).data("name"),
			value: $(this).data("id"),
			hwaddr: hwaddr,
			unit: unit,
			sensortype: $(this).data("type"),
		};

		Reports.createReportScreen("json", 1);
	});

	$("div#monitoring").on( "click", "table#tableList span#sensorUpdate", function() {
		var url = $(this).attr("data-id");
		var read_write = $(this).attr("data-read-write");
		var name = $(this).attr("data-name");
		var updateurl = url.split("/");
		//updateurl[0] = "updateSensor";

		var link = "&hwaddr="+ updateurl[0] + "&portnum="+ updateurl[1] +"&devaddr="+ updateurl[2] + "&ionum=" + updateurl[3];
		//updateurl = "updateSensor&" +updateurl[0] + "&"+ updateurl[1] +"&"+ updateurl[2] + "&" + updateurl[3] + "&" + updateurl[4];
		
		if(read_write == 0)
		{
			Main.getNowUpdateSensor(url);
		}
		else if (read_write == 1)
		{
			$("div#overlay").css("display","block");
			$("div#windowEdit").css("display","block");
			$("div#windowEdit input#buttonUpdate").val(language.update);
			$("div#windowEdit input#buttonUpdate").attr("data-id", "writeValue&objectid="+url);
			$("div#windowEdit input#buttonUpdate").attr("data-read-write", read_write);
			$("div#windowEdit input#buttonUpdate").attr("data-page", "updatenow");

			$("div#windowEdit input#buttonCancel").val(language.cancel);
			$("div#windowEdit td#modulsensorname").html(name);
			$("div#windowEdit td#voltaj").html(language.voltaj);
			$("div#windowEdit table.tableUpdateList").html("");
			Main.getOutputSensor("getSensor"+link);
		}
		else if (read_write == 2)
		{
			$("div#overlay").css("display","block");
			$("div#SelectUpdate").css("display", "block");
			$("div#SelectUpdate input#readValue").attr("url", url);
			$("div#SelectUpdate input#writeValue").attr("link", link);
			$("div#SelectUpdate input#writeValue").attr("url", url);
			$("div#SelectUpdate input#writeValue").attr("name", name);
			$("div#SelectUpdate input#writeValue").attr("read_write", read_write);
		}
	});


	$("div#monitoring").on( "click", "div.tableContainer span#tableRefresh", function() {
		if(Monitoring.refreshStatus) {
			Monitoring.refreshTable();
		}
	});


	$("div#monitoring").on( "click", "div.tableContainer span#tableFiltreName", function() {
		$("#windowFiltre div.tableFiltreList").html("");
		$("div#overlay").css("display","block");
		$("div#windowFiltre").css("display","block");

		$("div#windowFiltre input#buttonSelectAll").val(language.selectall);
		$("div#windowFiltre input#buttonUnSelect").val(language.unselect);
		$("div#windowFiltre input#buttonFiltre").val(language.update);
		$("div#windowFiltre input#buttonCancel").val(language.cancel);

		Monitoring.getDeviceFiltre();
	});

	$("div#monitoring").on( "click", "div#windowFiltre input#buttonFiltre", function() {
		Monitoring.refreshItems();
	});

	$("div#monitoring").on( "click", "div#windowFiltre input#buttonUnSelect", function() {
		for(var i = 0; i < deviceList.length; i++) {
			deviceList[i].checkedBuf = false;			
		}
	});

	$("div#monitoring").on( "click", "div#windowFiltre input#buttonSelectAll", function() {
		for(var i = 0; i < deviceList.length; i++) {
			deviceList[i].checkedBuf = true;			
		}
	});

	$("div#monitoring").on( "click", "div.tableFiltreList input.checkbox", function() {
		var index = $(this).data("index");
		var checked = $(this).prop("checked");
		deviceList[index].checkedBuf = checked;
	});

	$("div#monitoring").on( "click", "div#windowEdit input#buttonCancel", function() {
		$("div#overlay").css("display","none");
		$("div#windowEdit").css("display","none");
	});

	$("div#monitoring").on( "click", "div#windowStatus input#buttonStatusCancel", function() {
		$("div#overlay").css("display","none");
		$("div#windowStatus").css("display","none");		
	});

	$("div#monitoring").on( "click", "div#alertDelete input#buttonClose", function() {
		$("div#overlay").css("display","none");
		$("div#alertDelete").css("display","none");
	});
	

	$("div#monitoring").on( "click", "div#windowFiltre input#buttonCancel", function() {
		for (var i = 0; i < deviceList.length; i++)
			deviceList[i].checkedBuf = deviceList[i].checked;
		$("div#windowFiltre div.alert").html("");
		$("div#overlay").css("display","none");
		$("div#windowFiltre").css("display","none");
	});

	$("div#monitoring").on( "click", "div#windowFiltre input#buttonSelectAll", function() {
		for(var i = 0; i < deviceList.length; i++) {
			document.getElementById("checkbox"+i).checked = true;	
		}
	});

	$("div#monitoring").on( "click", "div#windowFiltre input#buttonUnSelect", function() {
		for(var i = 0; i < deviceList.length; i++) {
			document.getElementById("checkbox"+i).checked = false;	
		}
	});

	$("div#monitoring").on( "click", "div#windowStatus input#buttonStatusUpdate", function() {
		var url = $(this).data("id");
		var type = parseInt($(this).data("type"));
		
		var item = {
			alarm_active: $("div#windowStatus select#AlarmActive").val(),
			record: $("div#windowStatus select#Record").val(),
			read_enable: $("div#windowStatus select#ReadEnable").val()
		};

		Main.settingSensor(url, item);
	});

	$("div#monitoring").on( "click", "div#windowEdit input#buttonUpdate", function() {
		var url = $(this).attr("data-id");
		var type = parseInt($(this).attr("data-type"));
		var page = $(this).attr("data-page");
		if(Monitoring.updateValidate()) {
			var link = url + "&value=" + $("div#windowEdit input#tdValue").val();
			Main.updateOutputSensor(link);
			$("div#overlay").css("display","none");
			$("div#windowEdit").css("display","none");
		}
	});

	$("div#monitoring").on( "click", "div#pager li.bttn", function() {
		var currentPage = parseInt($("#monitoring div#pager li.bttn.act").data("page"));
	  	var page = parseInt($(this).data("page"));
		if(currentPage !=page) {
			$("#monitoring div#pager li.bttn.act").removeClass("act").addClass("pas");
			//Monitoring.clearElement();
			Main.refreshTypeControl(page);
		}
	});

	$("div#monitoring").on( "click", "div#pager div.prev", function() {
		var currentPage = parseInt($("#monitoring div#pager ul li.bttn.act").attr("data-page"));
		if(currentPage > 1) {	
			currentPage = currentPage - 1;
			$("#monitoring div#pager li.bttn.act").removeClass("act").addClass("pas");
			$("div#pager").find('li.bttn:nth-child(' +currentPage+ ')').removeClass('pas').addClass('act');
	    	//Monitoring.clearElement();
			Main.refreshTypeControl(currentPage);

		}
	});

	$("div#monitoring").on( "click", "div#pager div.next", function() {
		var currentPage = parseInt($("#monitoring div#pager ul li.bttn.act").html());
		var fullLi = parseInt($(this).attr("data-page"));

		if(currentPage != fullLi) {	
			currentPage = currentPage + 1;
			$("#monitoring div#pager li.bttn.act").removeClass("act").addClass("pas");
			$("div#pager").find('li.bttn:nth-child(' +currentPage+ ')').removeClass('pas').addClass('act');
			//Monitoring.clearElement();
			Main.refreshTypeControl(currentPage);
		}
	});
	
	$("div#monitoring").on( "change", "#tableStatus select", function() {
	    var status = null;
	    var cacheRefreshType = null;
	    $( "#tableStatus select option:selected" ).each(function() {
	      	status = parseInt($( this ).val());


	      	if(status==0) {
				cacheRefreshType = "total";
			} else if(status==1) {
				cacheRefreshType = "normal";
			} else if(status==2) {
				cacheRefreshType = "warning";
			} else if(status==4) {
				cacheRefreshType = "alarm";
			} else if(status==8) {
				cacheRefreshType = "inactive";
			} else if(status==16) {
				cacheRefreshType = "lost";
			} else if(status==32) {
				cacheRefreshType = "output";
			}

			var cacheSearchArray = new Array();
			var cacheJsonList = new Array();
			cacheJsonList = Main.filterControl();
			var count = 0;
			if(status != 0) {
				for(var i = 0; i < cacheJsonList.length; i++) {
					if(cacheJsonList[i].status == status) {
						cacheSearchArray[count] = cacheJsonList[i];
						count++;
					}
				}

  				if(cacheSearchArray.length == 0) {
  					Main.alert(language.notstatusalert);
 	  				Main.unloading();
 	  			} else {
 	  				refreshType = cacheRefreshType;
 	  				jsonList = eval(refreshType + "JSON");

					$("#sensorNameDel").css("display","none");
				  	$('#sensorName').removeClass("focus");
				  	$('#sensorName').val(language.sensorname);

				  	$("#deviceNameDel").css("display","none");
				  	$('#deviceName').removeClass("focus");
				  	$('#deviceName').val(language.devicename);

 	  				Monitoring.searchArray = cacheSearchArray;
					$("div#alertPanel div.balloon").removeClass("active");
					$("div#notifications div.balloon").removeClass("active");
					$("div#alertPanel div#" + refreshType).addClass("active");
				    //Monitoring.clearElement();
					Main.searchRequest(Monitoring.searchArray,"1");
				}
			} else {
 	  			refreshType = cacheRefreshType;
 	  			jsonList = eval(refreshType + "JSON");
				$("div#alertPanel div.balloon").removeClass("active");
				$("div#notifications div.balloon").removeClass("active");
				$("div#alertPanel div#" + refreshType).addClass("active");
			    //Monitoring.clearElement();
			    Monitoring.itemsCall();
			}
	    });	
  	});
	
	$("div#monitoring").on( "change", "#tablePaging", function() {
	    var str = null;
	    $( "select#tablePaging option:selected" ).each(function() {
	      	str = parseInt($( this ).text());
		    setCookie("tablePaging",str,365);
		    Monitoring.pagerCount = str;
		    //Monitoring.clearElement();
		    Monitoring.itemsCall();	
	    });	
  	});

	$("div#monitoring").on( "click", "div.tableContainer div#deviceNameDel", function() {
  		$("#deviceNameDel").css("display","none");
  		$('#deviceName').removeClass("focus");
  		$('#deviceName').val("");
		clearTimeout(Monitoring.deviceTimeout);
		Monitoring.deviceSearch(0);
  		setTimeout(function() {
  			$('#deviceName').val(language.devicename);
  		},50);
	});

	$("div#monitoring").on( "click", "div.tableContainer div#sensorNameDel", function() {
  		$("#sensorNameDel").css("display","none");
  		$('#sensorName').removeClass("focus");
  		$('#sensorName').val("");
		clearTimeout(Monitoring.sensorTimeout);
		Monitoring.sensorSearch(0);
		setTimeout(function() {
  			$('#sensorName').val(language.sensorname);
  		},50);
	});

	$("div#monitoring").on( "focus", "#deviceName", function() {
  		if($('#deviceName').val() == language.devicename) {
  			$('#deviceName').val("");
  		}
  	});

	$("div#monitoring").on( "blur", "#deviceName", function() {
  		if($('#deviceName').val() == "" || $('#deviceName').val() == null) {
	  		clearTimeout(Monitoring.deviceTimeout);
	  		Monitoring.deviceSearch(0);
  			setTimeout(function() {
  				$('#deviceName').val(language.devicename);
  			},50);
  		}
  	});

	$("div#monitoring").on( "focus", "#sensorName", function() {
  		if($('#sensorName').val() == language.sensorname) {
  			$('#sensorName').val("");     	
  		}
  	});

	$("div#monitoring").on( "blur", "#sensorName", function() {
  		if($('#sensorName').val() == "" || $('#sensorName').val() == null) {
  			clearTimeout(Monitoring.sensorTimeout); 
			Monitoring.sensorSearch(0); 
			setTimeout(function() {
  				$('#sensorName').val(language.sensorname);
  			},50);
  		}
  	});

	$("div#monitoring").on( "keydown", "#deviceName", function() {
  		$("#deviceNameDel").css("display","block");
  		$('#deviceName').addClass("focus");

  		clearTimeout(Monitoring.deviceTimeout);
  		Monitoring.deviceSearch(500);
    });

	$("div#monitoring").on( "keydown", "#sensorName", function() {
  		$("#sensorNameDel").css("display","block");
  		$('#sensorName').addClass("focus");
  		clearTimeout(Monitoring.sensorTimeout); 
		Monitoring.sensorSearch(500);     	
    }); 

    
	$("div#monitoring").on( "click", "div#SelectUpdate input#readValue", function() {
		$("div#SelectUpdate").css("display","none");
		Main.getNowUpdateSensor($(this).attr("url"));
	});
	
	
	$("div#monitoring").on( "click", "div#SelectUpdate input#writeValue", function() {
		$("div#SelectUpdate").css("display","none");
		$("div#windowEdit").css("display","block");
		$("div#windowEdit input#buttonUpdate").val(language.update);
		$("div#windowEdit input#buttonUpdate").attr("data-id", "writeValue&objectid="+$(this).attr("url"));
		$("div#windowEdit input#buttonUpdate").attr("data-read-write", $(this).attr("read_write"));
		$("div#windowEdit input#buttonUpdate").attr("data-page", "updatenow");

		$("div#windowEdit input#buttonCancel").val(language.cancel);
		$("div#windowEdit td#modulsensorname").html($(this).attr("name"));
		$("div#windowEdit td#voltaj").html(language.voltaj);
		$("div#windowEdit table.tableUpdateList").html("");
		Main.getOutputSensor("getSensor"+$(this).attr("link"));
	});
});