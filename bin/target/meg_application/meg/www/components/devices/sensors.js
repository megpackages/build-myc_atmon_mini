var displayStatus = "select";
var allSaved = [];

Settings.callEdits = function(objectid)
{
	$("div#alertDelete input#buttonRemove").removeClass("deviceDelete");
	$("div#alertDelete input#buttonRemove").removeClass("connectorDelete");
	$("div#devices div.form").html("");
	var html = 	'<div id="edits">'
				+ '<ul class="filters">'
					+ '<li class="devices"><div class="name" id="devicename"></div><select id="list"></select></li>'
					+ '<li class="checkboxs">'
						+ '<div class="name" id="typename"></div>'
						+ '<label for="binary" id="binaryName" style="display: none;"></label>'
						+ '<label for="float" id="floatName" style="display: none;"></label>'
						+ '<label for="output" id="outputName" style="display: none;"></label>'
						+ '<label for="string" id="stringName" style="display: none;"></label>'
						+ '<label for="bitstring" id="bitstringName" style="display: none;"></label>'
					+ '</li>'
				+ '</ul>'
				+ '<div class="alert"></div>'
				+ '<div id="Header" style="display: block;">'
					+ '<table cellpadding="0" cellspacing="0" border="0" width="100%">'
					+ '</table>'			
				+ '</div>'
				+ '<div id="editSensors">'
					+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="list">'
					+ '</table>'				
				+ '</div>'
				+ '<div class="information"><div class="infos">'
					+ '<div class="blk" id="infoEdit"><input id="edit" type="submit"><b></b></div>'
					+ '<div class="blk" id="infoSave"><input id="save" type="submit"><b></b></div>'
					+ '<div class="blk" id="infoDel"><input id="delete" type="submit"><b></b></div>'
					+ '<div class="blk" id="infoCancel"><input id="cancel" type="submit"><b></b></div>'
				+ '</div>'
				+ '<div id="buttons" class="buttons">'
					+ '<input id="saveall" type="submit">'
				+ '</div>'
				+ '</div>'
			+ '</div>';

	$("div#devices div.form").html(html);

	$("div#devices div#edits li.devices div#devicename").append(language.Settings_edits_devicename);
	$("div#devices div#edits li.checkboxs div#typename").append(language.Settings_edits_typename);

	$("div#devices div#edits div.information div#infoDel b").html(language.Settings_edits_info_delete);
	$("div#devices div#edits div.information div#infoCancel b").html(language.Settings_edits_info_cancel);
	$("div#devices div#edits div.information div#infoSave b").html(language.Settings_edits_info_save);
	$("div#devices div#edits div.information div#infoEdit b").html(language.Settings_edits_info_edit);

	$("div#devices div#edits div.information div#infoDel input").val(language.Settings_edits_delete);
	$("div#devices div#edits div.information div#infoCancel input").val(language.Settings_edits_cancel);
	$("div#devices div#edits div.information div#infoSave input").val(language.Settings_edits_save);
	$("div#devices div#edits div.information div#infoEdit input").val(language.Settings_edits_edit);

	$("div#devices div#edits div.information input#saveall").val(language.Settings_edits_saveall);
	$("div#devices div#edits div.information input#newsensor").val(language.Settings_edits_newsensor);
    SelectType = undefined;
	Settings.getEditDeviceList(objectid);
};

Settings.removeSensor = function(link)
{

	var currentPage = $("div#devices li.devices select option:selected").val();
	var url = "http://" + hostName + "/cgi-bin/mdlmngr.cgi?"+ link;

	$.ajax({
	    url : url,
	    type : "POST",
	    data : {},
        dataType : 'text',
        contentType : 'text/json; charset=utf-8',
	    success : function()
	    {
	    	var lastSelected = $("ul.filters li.checkboxs label.selected").attr("id").split("Name")[0];
			
			var lastDisp = $("div#edits table tr.head div.switch input:checked").attr("id");
			Settings.getEditSensorsList(currentPage, lastSelected, lastDisp);
			Main.unloading();
	    },
	    error : function (jqXHR, textStatus, errorThrown)
	    {
	    	Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Devices.addSensor = function(json)
{
	Main.loading();
	json.ionum = $("div#edits ul.filters li option:selected").attr("newionum");
	var type = json.type;
	if (json.ionum == undefined)
		json.ionum = "1";
	if (json.read_period == undefined)
		json.read_period = "300";
	if (json.read_enable == undefined)
		json.read_enable = "1";
	if (json.resolution == undefined)
		json.resolution = "0";

	json.filter_value = json.resolution
	var json = JSON.stringify(json);
	var addSensor = $("div#edits ul.filters li option:selected").attr("addsensor");
	var url = "http://" + hostName + "/cgi-bin/mdlmngr.cgi?" + addSensor;
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
	    	var jSon = jQuery.parseJSON(data);
	    	Main.unloading();
	    	if (jSon["return"] == "true")
	    	{	
	    		Main.alert(language.sensors_add_success);
	    		$( "div#edits ul.filters li select option:selected" ).each(function() {
			      	Main.loading();
			    	var lastSelected = sensorTypesToStr2(DataTypeFromSensType(parseInt(type)));
					var lastDisp = $("div#edits table tr.head div.switch input:checked").attr("id");
					var url = $(this).val();
					Settings.getEditSensorsList(url, lastSelected, lastDisp);
		    	});
	    	}
	    	else
	    	{
	    		Main.alert(language.sensors_add_fail + ":" + jSon["return"]);
	    	}
	    },
	    error: function (jqXHR, textStatus, errorThrown)
	    {	
	    	Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Devices.updateSensor = function(objectid,json)
{
	var objectId = objectid.split("/");
	Main.loading();
	var name = json.name;
	var record = json.record;
	var json = JSON.stringify(json);
	var url = "http://" + hostName + "/cgi-bin/mdlmngr.cgi?updateSensor&hwaddr=" + objectId[0] + "&portnum=" + objectId[1] + "&devaddr=" + objectId[2] + "&ionum=" + objectId[3];	
	var parsedObj = JSON.parse(json);
	if(parsedObj.hasOwnProperty('filter_type')){
		if(parsedObj.filter_type == 0)
			parsedObj.filter_value = parsedObj.resolution;
		else
			parsedObj.filter_value = parsedObj.change_rate;
	}

	if(parsedObj.hasOwnProperty('periodtype'))
		parsedObj.periodtype = 	"" + Math.pow(2, parsedObj.periodtype);

	json = JSON.stringify(parsedObj);
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
	    	var jSon = jQuery.parseJSON(data);
	    	Main.unloading();
	    	if (jSon["return"] == "true")
	    	{	
	    		Main.alert(language.sensors_update_success);
	    		var sensOnMonitor = $("#monitoring table#tableList tr td#name[objectid='" + objectid + "'] span");
	    		if (sensOnMonitor != undefined)
	    		{
	    			sensOnMonitor.html(Monitoring.characterLimit(name, 20));
					if(name.length > 20) sensOnMonitor.attr("data-title",name);
					else sensOnMonitor.removeAttr("data-title");
	    		}
	    		
	    		var reportSpan = $("#monitoring table#tableList tr td span[data-hwaddr='" + objectid + "']#sensorRapor");
	    		if (reportSpan != undefined)
	    		{
	    			reportSpan.data("record", record);
	    		}
	    	}
	    	else
	    	{
	    		Main.alert(language.sensors_update_fail + ":" + jSon["return"]);
	    	}
	    },
	    error: function(jqXHR, textStatus, errorThrown) {
			Main.unloading();
	    	Main.alert(language.server_error + ":" + jqXHR.responseText);
	    }
	});
};

Settings.updateEditSensor = function(url, json, id, func)
{
	Main.loading();
	json.periodtype = "" + Math.pow(2, json.periodtype);
	json.filter_value = json.resolution;
	delete json.resolution; 
	var json = JSON.stringify(json);

	var url = "http://" + hostName + "/cgi-bin/mdlmngr.cgi?" + url;
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
	    	if(id != undefined) {
				$("tr#" + id + " td input#edit").css("display","inline-block");
				$("tr#" + id + " td input#delete").css("display","inline-block");
				$("tr#" + id + " td input#save").css("display","none");
				$("tr#" + id + " td input#cancel").css("display","none");

				$("tr#" + id + " input[type='text']").each(function() {
					$(this).prop('disabled', true);
				});

				$("tr#" + id + " select").each(function() {
					$(this).prop('disabled', true);
				});			

				$("tr#" + id + " input[type='checkbox']").each(function() {
					$(this).prop('disabled', true);
				});

			} else {
				$("div#editSensors tr td input#edit").css("display","inline-block");
				$("div#editSensors tr td input#delete").css("display","inline-block");
				$("div#editSensors tr td input#save").css("display","none");
				$("div#editSensors tr td input#cancel").css("display","none");

				$("div#editSensors tr input[type='text']").each(function() {
					$(this).prop('disabled', true);
				});

				$("div#editSensors tr select").each(function() {
					$(this).prop('disabled', true);
				});				

				$("div#editSensors tr input[type='checkbox']").each(function() {
					$(this).prop('disabled', true);
				});
			}

			if(func == "all") {				
				var index = allSaved.indexOf(id);
				allSaved.splice(index,1);
			}

			if(func == "all" && allSaved.length != 0)  {
				Main.unloading();
				Settings.sensorSaveAll();
			} else {
				var url = $("div#devices div#edits ul.filters li select option:selected").val();
				var data = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?"+ url);
				data.success(function() {
					Settings.editsSensorList = jQuery.parseJSON(data.responseText);
					Main.unloading();
				});				
				data.error(function (jqXHR, textStatus, errorThrown){	
	    	Main.alert(language.server_error + ":" + jqXHR.responseText);
					Main.unloading();
				});
			}
	    },
	    error: function (jqXHR, textStatus, errorThrown)
	    {	
	    	Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.getEditDeviceList = function(objectid)
{
	var data = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?getDevices");	 
	data.success(function() {
		Settings.editsDeviceList = jQuery.parseJSON(data.responseText);
		if (Settings.editsDeviceList.length == 0)
		{
			Main.unloading();
			Main.alert(language.device_list_empty);
			return;
		}
		Settings.createEditDeviceList(Settings.editsDeviceList, objectid);
	});
	data.error(function (jqXHR, textStatus, errorThrown){	
	    	Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Settings.sortSensors = function()
{
	maxIonum = "0";
	var tmpList = [];
	var len = Settings.editsSensorList.length;
	for (var i = 0; i < len; i++)
	{
		if (parseInt(Settings.editsSensorList[i].ionum) > parseInt(maxIonum))
			maxIonum = Settings.editsSensorList[i].ionum;
		tmpList[parseInt(Settings.editsSensorList[i].ionum)] = Settings.editsSensorList[i];
	}

	var tmpList2 = [];
	var k = 0;
	for (var i = 0; i < tmpList.length; i++)
	{
		if (tmpList[i] != undefined)
			tmpList2[k++] = tmpList[i];
	}

	Settings.editsSensorList = tmpList2;
};

Settings.getEditSensorsList = function(url ,lastSelect, lastDisp)
{
	var data = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?"+ url);	 
	data.success(function() {
		Settings.editsSensorList = jQuery.parseJSON(data.responseText);
		if(Settings.editsSensorList.length == 0) { 
			Main.alert(language.noselectsensor);	
			$("div#editSensors table").html("");
			$("div#Header table").html("");
			$("div#edits ul.filters li.checkboxs label").css("display", "none");
			
				var devType = parseInt($( "div#edits ul.filters li select option:selected" ).attr("device-type"));
				var dType = deviceTypesToStr0(devType);
				if (deviceTypes[dType].sensorAddable)
				{
					if ($("div#edits div#buttons input#newsensor").length == 0)
					{
						var newSensor = document.createElement("input");
						newSensor.setAttribute("type","submit");
						newSensor.setAttribute("id","newsensor");
						$("div#edits div#buttons input#saveall").before(newSensor);
						$("div#devices div#edits div#buttons input#newsensor").val(language.Settings_edits_newsensor);
					}
				}	
				else
				{
					if ($("div#edits input#newsensor").length != 0)
					{
						$("div#edits input#newsensor").remove();
					}
				}
		} else {
			Settings.sortSensors();
			Settings.createEditSensorsList(Settings.editsSensorList,lastSelect, lastDisp);
		}
		Main.unloading();
	});
	data.error(function (jqXHR, textStatus, errorThrown){	
	    	Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Settings.createEditDeviceList = function(items, objectid)
{
	$("div#devices div.alert").html("");
	var selectedIndex = 0;
	for(var i = 0; i < items.length; i ++)
	{
		var ID = items[i].objectid.split("/");
		var getSensor = "getSensors&hwaddr="+ ID[0] + "&portnum="+ ID[1] +"&devaddr="+ ID[2];
		addSensor = "addSensor&hwaddr="+ ID[0] + "&portnum="+ ID[1] +"&devaddr="+ ID[2];
		var option = document.createElement("option");
		option.setAttribute("value",getSensor);
		option.setAttribute("addSensor",addSensor);
		option.setAttribute("objectid",items[i].objectid);
		option.setAttribute("device-type",items[i].type);
		option.setAttribute("device-active",items[i].active);
		if (objectid == undefined)
		{
			if(i == 0)
			{
				option.checked = true;
				Settings.getEditSensorsList(getSensor);
				if (parseInt(items[0].type) == deviceTypes.modbus_rtu.value || 
					parseInt(items[0].type) == deviceTypes.modbus_ascii.value || 
					parseInt(items[0].type) == deviceTypes.modbus_tcp.value || 
					parseInt(items[0].type) == deviceTypes.meter.value || 
					parseInt(items[0].type) == deviceTypes.snmp.value || 
					parseInt(items[0].type) == deviceTypes.iec61850.value || 
					parseInt(items[0].type) == deviceTypes.ping.value || 
					parseInt(items[0].type) == deviceTypes.telnet.value || 
					parseInt(items[0].type) == deviceTypes.virtual.value ||
					parseInt(items[0].type) == deviceTypes.infrared.value ||
					parseInt(items[0].type) == deviceTypes.pcomm.value)
				{
					var newSensor = document.createElement("input");
					newSensor.setAttribute("type","submit");
					newSensor.setAttribute("id","newsensor");
					$("div#edits div#buttons input#saveall").before(newSensor);
					//$(newSensor).insertAfter();
					$("div#devices div#edits div#buttons input#newsensor").val(language.Settings_edits_newsensor);
				}
			}
			else
			{
				option.checked = false;			
			}
		}
		else if (items[i].objectid == objectid)
		{
			selectedIndex = i;
			Settings.getEditSensorsList(getSensor);
			if (parseInt(items[0].type) == deviceTypes.modbus_rtu.value || 
				parseInt(items[0].type) == deviceTypes.modbus_ascii.value || 
				parseInt(items[0].type) == deviceTypes.modbus_tcp.value || 
				parseInt(items[0].type) == deviceTypes.meter.value || 
				parseInt(items[0].type) == deviceTypes.snmp.value || 
				parseInt(items[0].type) == deviceTypes.iec61850.value || 
				parseInt(items[0].type) == deviceTypes.ping.value || 
				parseInt(items[0].type) == deviceTypes.telnet.value || 
				parseInt(items[0].type) == deviceTypes.virtual.value ||
				parseInt(items[0].type) == deviceTypes.infrared.value ||
				parseInt(items[0].type) == deviceTypes.pcomm.value )
			{
				var newSensor = document.createElement("input");
				newSensor.setAttribute("type","submit");
				newSensor.setAttribute("id","newsensor");
				$("div#edits div#buttons input#saveall").before(newSensor);
				//$(newSensor).insertAfter();
				$("div#devices div#edits div#buttons input#newsensor").val(language.Settings_edits_newsensor);
			}

		}
		option.innerHTML = items[i].name + " : " + items[i].objectid;
		$("div#devices div#edits li.devices select#list").append(option);
	}
	$("div#devices div#edits li.devices select#list").prop("selectedIndex",selectedIndex);
};

Settings.createEditSensorsList = function(items,  lastSelect, lastDisp)
{
	allSaved = [];
	displayStatus = "input";
	$("div#Header table").html("");
	$("div#editSensors table").html("");
	
	$("div#devices div#edits label#binaryName").html(language.Settings_edits_binary);
	$("div#devices div#edits label#floatName").html(language.Settings_edits_float);
	$("div#devices div#edits label#outputName").html(language.Settings_edits_output);
	$("div#devices div#edits label#stringName").html(language.Settings_edits_string);
	$("div#devices div#edits label#bitstringName").html(language.Settings_edits_bitstring);
	$("div#devices div#edits label#snmpName").html(language.Settings_edits_snmp);

	$("ul.filters li.checkboxs label#binaryName").css("display","none");
	$("ul.filters li.checkboxs label#floatName").css("display","none");
	$("ul.filters li.checkboxs label#outputName").css("display","none");
	$("ul.filters li.checkboxs label#stringName").css("display","none");
	$("ul.filters li.checkboxs label#bitstringName").css("display","none");
	$("ul.filters li.checkboxs label#snmpName").css("display","none");
	$("ul.filters li.checkboxs label#binaryName").removeClass("selected");
	$("ul.filters li.checkboxs label#floatName").removeClass("selected");
	$("ul.filters li.checkboxs label#outputName").removeClass("selected");
	$("ul.filters li.checkboxs label#stringName").removeClass("selected");
	$("ul.filters li.checkboxs label#bitstringName").removeClass("selected");
	$("ul.filters li.checkboxs label#snmpName").removeClass("selected");
	
	var active = parseInt($("div#devices div#edits li.devices select#list option:selected").attr("device-active"));		

	var maxionum = 0;
	var tbody = document.createElement("tbody");
	for ( var i = 0; i < items.length; i++)
	{

		var item = items[i];
		if (parseInt(item.ionum) > maxionum)
			maxionum = parseInt(item.ionum);
		var ID = item.objectid.split("/");
		var sensType = sensorTypesToStr2(parseInt(item.type));
		var tr = document.createElement("tr");
		tr.setAttribute("class", sensType);
		tr.setAttribute("id","sensor"+item.id);

		var params = sensorParams[sensType];
		for (var j = 0; j < params.length; j++)
		{
			var param = params[j];
			var opt = param.disp;
			if (opt == undefined)
				continue;
			if (active)
			{
				if (param.name == "read_period")
					continue;
				if (param.name == "read_enable")
					continue;
			}
			else
			{
				if (param.name == "change_rate")
					continue;
			}
				if (param.name == "change_rate")
					continue;
			var td1 = document.createElement("td");
			td1.setAttribute("id", param.name);
			td1.setAttribute("access", (param.access).toString());
			td1.setAttribute("class",opt);
			
			var value = eval("item." + param.name);
		 	 if (param.name == "resolution")
				 value = item.filter_value
			if (param.html == "text")
			{
				if (param.access == access.rw)
				{
					//$(td1).addClass('input');
					var input = document.createElement("input");
					input.setAttribute("type","text");	
					input.setAttribute("data-type", param.type);
					input.setAttribute("data-name", param.name);
					if (param.min != undefined)
						input.setAttribute("minval", param.min);
					if (param.max != undefined)
						input.setAttribute("maxval", param.max);
					if (param["default"] != undefined)
						input.setAttribute("default", param["default"]);
					input.setAttribute("value", value);
					input.setAttribute("disabled", "disabled");
					td1.appendChild(input);
				}
				else
				{
					var b = document.createElement("b");
					b.setAttribute("type","text");	
					b.innerHTML = value;
					td1.appendChild(b);
				}
			}
			else if (param.html == "checkbox")
			{
				if (param.access == access.rw)
				{
					var div = document.createElement("div");
					var input = document.createElement("input");
					input.setAttribute("type","checkbox");	
					input.setAttribute("data-type", param.type);
					input.setAttribute("data-name", param.name);
					input.checked = (value == "1" ? true:false);
					input.setAttribute("disabled", "disabled");
					input.setAttribute("class", "squaredFour");
					div.appendChild(input);
					td1.appendChild(div);
				}
				else
				{

					var b = document.createElement("b");
					b.setAttribute("type","text");	
					b.innerHTML = value;
					td1.appendChild(b);
				}
			}
			else if (param.html == "select")
			{
				if (param.access == access.rw)
				{
					if(param.name == "resolution" && item.filter_type == 1){ 

							var input = document.createElement("input");
					input.setAttribute("type","text");	
					input.setAttribute("data-type", 'f');
					input.setAttribute("data-name", param.name);
					if (param.min != undefined)
						input.setAttribute("minval", param.min);
					if (param.max != undefined)
						input.setAttribute("maxval", param.max);
					if (param["default"] != undefined)
						input.setAttribute("default", param["default"]);
						input.setAttribute("value",value);
						input.setAttribute("disabled", "disabled");
						td1.appendChild(input);
					}else{

					var select = document.createElement("select");	
					select.setAttribute("func", param.func);
					select.setAttribute("data-name", param.name);
					select.selectedIndex = parseInt(value);
					select.setAttribute("disabled", "disabled");
					 

					if (param.name == "alarm_value")
					{
						var opt0 = document.createElement("option");
						opt0.innerHTML =  item.zero_name;
						var opt1 = document.createElement("option");
						opt1.innerHTML = item.one_name;
						select.appendChild(opt0);
						select.appendChild(opt1);
					}
					
					if (param.name == "resolution")
					{
						var opt0 = document.createElement("option");
						opt0.innerHTML =  language.Settings_edits_two_digit;
						var opt1 = document.createElement("option");
						opt1.innerHTML = language.Settings_edits_one_digit;
						var opt2 = document.createElement("option");
						opt2.innerHTML = language.Settings_edits_zero_digit;
						select.appendChild(opt0);
						select.appendChild(opt1);
						select.appendChild(opt2);
					}

					if (param.name == "periodtype")
					{
						value = "" + (Math.log(value) /  Math.log(2));

						var opt0 = document.createElement("option");
						opt0.innerHTML =  language.Settings_edits_minute;
						var opt1 = document.createElement("option");
						opt1.innerHTML = language.Settings_edits_hour;
						var opt2 = document.createElement("option");
						opt2.innerHTML = language.Settings_edits_day;
						var opt3 = document.createElement("option");
						opt3.innerHTML = language.Settings_edits_week;
						var opt4 = document.createElement("option");
						opt4.innerHTML = language.Settings_edits_month;
						var opt5 = document.createElement("option");
						opt5.innerHTML = language.Settings_edits_year;
						select.appendChild(opt0);
						select.appendChild(opt1);
						select.appendChild(opt2);
						select.appendChild(opt3);
						select.appendChild(opt4);
						select.appendChild(opt5);
					}

					if(param.name == "filter_type") {
						var opt0 = document.createElement("option");
						opt0.value = 0;
						opt0.innerHTML =  language.Settings_edits_resolution;
						var opt1 = document.createElement("option");
						opt1.value = 1;
						opt1.innerHTML = language.Settings_edits_changerate;
						select.appendChild(opt0);
						select.appendChild(opt1);
						$(select).change(function () {
						 	if($(this).find('option:selected').val() == 0) {
						 		$(this).parent().parent().find("#resolution").html('<select func="createResolutionSelect();" data-name="resolution" data-val="Tamsayı"><option>'+language.Settings_edits_two_digit+'</option><option>'+language.Settings_edits_one_digit+'</option><option>'+language.Settings_edits_zero_digit+'</option></select>')
						 	}else{
						 		$(this).parent().parent().find("#resolution").html('<input type="text" data-type="f" data-name="resolution" value="0" data-val="">');
						 	}
						})  
					}


					
					if (param.name == "apply")
					{
					    for (var k = 0; k < poweronVals.length; k++)
					    {
					        var opt0 = document.createElement("option");
					        opt0.setAttribute("id", poweronVals[k]);
					        opt0.setAttribute("value", k);
					        opt0.innerHTML = eval("language.Settings_edits_apply_" + poweronVals[k]);
					        select.appendChild(opt0);
					    }
					}

					select.selectedIndex = parseInt(value);
					td1.appendChild(select);
					}
				}
				else
				{
					var b = document.createElement("b");
					b.setAttribute("type","text");	
					b.innerHTML = eval("item." + param.name);
					if (param.name == "read_write")
					{
						b.innerHTML = eval("language.Settings_edits_" + readWrite[parseInt(eval("item." + param.name))]);
					}
					td1.appendChild(b);
				}
			}

			tr.appendChild(td1);
		}

		var url = "&hwaddr="+ ID[0] + "&portnum="+ ID[1] +"&devaddr="+ ID[2] + "&ionum=" + ID[3];

		var tdButtons = document.createElement("td");
		tdButtons.setAttribute("class","buttons");
		var edit = document.createElement("input");
		var save = document.createElement("input");
		var cancel = document.createElement("input");
		var del = document.createElement("input");
		edit.setAttribute("type","submit");
		edit.setAttribute("id","edit");
		edit.setAttribute("data-id","sensor"+item.id);
		edit.setAttribute("data-url",url);
		edit.setAttribute("value",language.Settings_edits_edit);
		save.setAttribute("type","submit");
		save.setAttribute("id","save");
		save.setAttribute("data-id","sensor"+item.id);
		save.setAttribute("data-type",item.type);
		save.setAttribute("data-url",url);
		save.setAttribute("value",language.Settings_edits_save);
		cancel.setAttribute("type","submit");
		cancel.setAttribute("id","cancel");
		cancel.setAttribute("data-url",url);
		cancel.setAttribute("data-id","sensor"+item.id);
		cancel.setAttribute("value",language.Settings_edits_cancel);
		del.setAttribute("type","submit");
		del.setAttribute("id","delete");
		del.setAttribute("data-id","sensor"+item.id);
		del.setAttribute("data-url",url);
		del.setAttribute("value",language.Settings_edits_delete);

		tdButtons.appendChild(del);
		tdButtons.appendChild(edit);
		tdButtons.appendChild(cancel);
		tdButtons.appendChild(save);
		tr.appendChild(tdButtons);

		var page = $("li.checkboxs label.selected").attr("id");
		$(tbody).append(tr);
	}
	$("div#editSensors table").append(tbody);


	var binaryCount = 0; 
	var floatCount = 0; 
	var outputCount = 0;
	var stringCount = 0;
	var bitstringCount = 0;
	var snmpCount = 0;
	var firstSelect = "";
	
	for(var j = 0; j < items.length; j++)
	{
		var item = items[j];
		var devType = parseInt($("div#edits ul.filters li option:selected").attr("device-type"));

		if (isBinary(item.type))
		{
			if(firstSelect == "") firstSelect = "binaryName";
			binaryCount++;
		}
		else if (isFloat(item.type))
		{
			if(firstSelect == "") firstSelect = "floatName";
			floatCount++;
		}
		else if (isOutput(item.type))
		{
			if(firstSelect == "") firstSelect = "outputName";
			outputCount++;
		}
		else if (isString(item.type))
		{
			if(firstSelect == "") firstSelect = "stringName";
			stringCount++;
		}
		else if (isBitString(item.type))
		{
			if(firstSelect == "") firstSelect = "bitstringName";
			bitstringCount++;
		}
	}
	
	if (binaryCount != 0)
		$("ul.filters li.checkboxs label#binaryName").css("display","block");
	if (floatCount != 0)
		$("ul.filters li.checkboxs label#floatName").css("display","block");
	if (outputCount != 0)
		$("ul.filters li.checkboxs label#outputName").css("display","block");
	if (stringCount != 0)
		$("ul.filters li.checkboxs label#stringName").css("display","block");
	if (bitstringCount != 0)
		$("ul.filters li.checkboxs label#bitstringName").css("display","block");

	
	$("div#edits ul.filters li option:selected").attr("newionum", (maxionum + 1).toString());
	var sensorType = sensorTypes.string;
		if (firstSelect == "outputName")
		sensorType = sensorTypes.output;
	else if (firstSelect == "binaryName")
		sensorType = sensorTypes.binary;
	else if (firstSelect == "floatName")
		sensorType = sensorTypes.float;
	else if (firstSelect == "stringName")
		sensorType = sensorTypes.string;
	else if (firstSelect == "bitstringName")
		sensorType = sensorTypes.bitstring;
		
	var devType = $( "div#edits ul.filters li select option:selected" ).attr("device-type");
	
	var disp = "input";
	if (lastDisp != undefined)
		disp = lastDisp;
		
	if (lastSelect != undefined && eval(lastSelect + "Count") != 0)
	{
		sensorType = strToSensorType(lastSelect);
	}
		
	$("ul.filters li.checkboxs label#binaryName").append(" (<b>" + binaryCount + "</b>)");
	$("ul.filters li.checkboxs label#floatName").append(" (<b>" + floatCount + "</b>)");
	$("ul.filters li.checkboxs label#outputName").append(" (<b>" + outputCount + "</b>)");
	$("ul.filters li.checkboxs label#stringName").append(" (<b>" + stringCount + "</b>)");
	$("ul.filters li.checkboxs label#bitstringName").append(" (<b>" + bitstringCount + "</b>)");			

	var devType1 = parseInt($( "div#edits ul.filters li select option:selected" ).attr("device-type"));
	var dType = deviceTypesToStr0(devType1);
	if (deviceTypes[dType].sensorAddable)
	{
		if ($("div#edits div#buttons input#newsensor").length == 0)
		{
			var newSensor = document.createElement("input");
			newSensor.setAttribute("type","submit");
			newSensor.setAttribute("id","newsensor");
			$("div#edits div#buttons input#saveall").before(newSensor);
			$("div#devices div#edits div#buttons input#newsensor").val(language.Settings_edits_newsensor);
		}
	}	
	else
	{
		if ($("div#edits input#newsensor").length != 0)
		{
			$("div#edits input#newsensor").remove();
		}
	}

	Settings.activateFields(devType, sensorType, disp);

};

Settings.createEditListHeader = function(devType, sensorType)
{
	if ($("div#Header table tr").length != 0)
		return;
		
	var active = parseInt($("div#devices div#edits li.devices select#list option:selected").attr("device-active"));		

	var thead = document.createElement("thead");
	var tr = document.createElement("tr");
	tr.setAttribute("class","head");
	var thName = document.createElement("th");
	thName.setAttribute("id", "name");
	var div0 = document.createElement("div");
	div0.setAttribute("class", "switch");

	div0.innerHTML = '<input type="radio" class="switch-input spec" name="view" value="input" id="input" checked>'
				       + '<label for="input" class="switch-label switch-label-on spec" id="input"></label>'
				       + '<input type="radio" class="switch-input spec" name="view" value="select" id="select">'
				       + '<label for="select" class="switch-label switch-label-off spec" id="select"></label>'
				       + '<input type="radio" class="switch-input spec" name="view" value="spec" id="spec">'
				       + '<label for="spec" class="switch-label switch-label-on spec" id="spec"></label>'
				       + '<span class="switch-selection spec"></span>'
				     + '</div>';
	
	thName.appendChild(div0);
	tr.appendChild(thName);
	
	var type = Devices.createSensorType(parseInt(devType), parseInt(sensorType));
	var filter = sensorTypesToStr(type);
	
	item = sensorParams[filter];
	if (item != undefined)
	{
		for (var i = 0; i < item.length; i++)
		{
			if (item[i].name == "name")
				continue;
			if (item[i].name == "change_rate")
				continue;
			
			if (active)
			{
				if (item[i].name == "read_period")
					continue;
				if (item[i].name == "read_enable")
					continue;
			}
			else
			{
				if (item[i].name == "resolution")
					continue;
			}

			var disp = item[i].disp;
			if (disp == undefined)
				continue;

			var th = document.createElement("th");
			th.innerHTML = eval("language.sensors_" + item[i].name);
			th.setAttribute("class", disp);
			th.setAttribute("id", item[i].name);
			tr.appendChild(th);
		}
	}

	var thButtons = document.createElement("th");
	thButtons.setAttribute("class", "buttons");
	tr.appendChild(thButtons);
	thead.appendChild(tr);
	$("div#edits div#Header table").append(thead);
};

Settings.activateFields = function(devType, sensorType, disp)
{
	Settings.createEditListHeader(devType, sensorType);

	$("div#edits table tr.head div.switch input").prop("checked", false);
	$("div#edits table tr.head div.switch input#" + disp).prop("checked", true);
		
	$("div#edits table tr.head div.switch label#input").html(language.Settings_edits_inputname);
	$("div#edits table tr.head div.switch label#select").html(language.Settings_edits_selectname);
	$("div#edits table tr.head div.switch label#spec").html(language.Settings_edits_spec);
	
	var type = Devices.createSensorType(parseInt(devType), parseInt(sensorType));
	var filter = sensorTypesToStr(type);

	$("div#edits table tr").css("display", "none");
	$("div#edits table tr td").css("display", "none");
	$("div#edits table tr th").css("display", "none");

	$("div#edits table tr." + filter).css("display", "table-row");
	$("div#edits table tr." + filter + " td." + disp).css("display", "table-cell");

	$("div#edits table tr.head").css("display", "table-row");
	$("div#edits table tr.head th." + disp).css("display", "table-cell");


	$("div#edits table tr th#name").css("display", "table-cell");
	$("div#edits table tr th.buttons").css("display", "table-cell");
	$("div#edits table tr td#name").css("display", "table-cell");
	$("div#edits table tr td.buttons").css("display", "table-cell");

	if ($("div#devices div#edits  div#Header table tr.head th.spec").length == 0)
	{
		$("div#Header table tr label#spec").css("display", "none");
		$("div#editSensors table tr input#spec").css("display", "none");
		$("div#Header table tr label").removeClass("spec");
		$("div#Header table tr input").removeClass("spec");
		$("div#Header table tr span").removeClass("spec");
		$("div#devices div#edits div#Header table tr th:first-child").removeClass("spec");
		$("div#devices div#edits div#editSensors table tr td:first-child").removeClass("spec");
	}
	else
	{
		$("div#devices div#edits div#Header table tr th:first-child").addClass("spec");
		$("div#devices div#edits div#editSensors table tr td:first-child").addClass("spec");
	}

	$("ul.filters li.checkboxs label").removeClass("selected");
	$("ul.filters li.checkboxs label#" + sensorTypesToStr(sensorType) + "Name").addClass("selected");
	
	var childs = $("div#devices div#edits div#editSensors table tr:visible:first td:visible");
	for (var i = 0; i < childs.length; i++)
	{
		var child = $(childs[i]);
		var header = $("div#devices div#edits div#Header table tr th:visible#" + $(childs[i]).attr("id"));
		var  headerWidth= header.css("width");
		var childWidth = child.css("width");
		
		if (parseInt(headerWidth) > parseInt(childWidth))
		{
			child.css("min-width",headerWidth);
			child.css("max-width",headerWidth);
			header.css("min-width",headerWidth);
			header.css("max-width",headerWidth);
		}
		else
		{
			header.css("min-width",childWidth);
			header.css("max-width",childWidth);
			child.css("min-width",childWidth);
			child.css("max-width",childWidth);
		}
	}
	
};

Settings.editSensorsValidateClear = function(id)
{
	$("div#devices div.alert").html("");
	$("div#editSensors table tr td input").removeClass("alert");
};

Settings.editSensorsValidate = function(id,type,ionum, json)
{
	var sensType = sensorTypesToStr(parseInt(type));
	var elements = $("div#editSensors table tr#" + id + " td input,\
					 div#editSensors table tr#" + id + " td select");
	var page = $("li.checkboxs label.selected").attr("id");
	var alertHtml = "";
			
	//$("div#devices div.alert").html("");
	var readF = undefined;
	var writeF = undefined;
	
	var isvalid = true;
	for (var i = 0; i < elements.length; i++)
	{
		var element 	= $(elements[i]);
		var dataType 	= element.attr("data-type");
		var dataName    = element.attr("data-name");
		var dflt 		= element.attr("default");
		var minval 		= element.attr("minval");
		var maxval 		= element.attr("maxval");
		var jsON = {
			type: dataType,
			min: minval,
			max: maxval
		};

		if (element.is("input") && element.attr("type") != "checkbox")
		{
        	if (dataName == "read_function_code")
        		readF = true;
        	if (dataName == "write_function_code")
        		writeF = true;
        		
			var value = element.val();
			
			if(value.length == 0)
			{
	        	if (dataName == "read_function_code")
	        		readF = false;
	        	if (dataName == "write_function_code")
	        		writeF = false;
        		
				if (dflt != undefined)
				{
					json[dataName] = dflt;
					continue;
				}			
			}
			
			jsON.val = value;
			jsON.ismandatory = "true";
			jsON.obj  		 = element;
			jsON.objfocus    = element;
			jsON.page        = $("div.form div.alert");
			jsON.errprefix   = eval("language.sensors_" + dataName) + ":";
			
			//validation of element
			if(!Validator.validate(jsON))
				isvalid = false;
			else if(isvalid)
				json[dataName] = value;
			
		}
		else if (element.is("select"))
		{
			json[dataName] = element.prop("selectedIndex").toString();
		}
		else if (element.is("input") && element.attr("type") == "checkbox")
		{
			json[dataName] = element.prop("checked") ? "1":"0";
		}
	}
	
	if (isvalid)
	{
		if (isFloat(type))
		{
			var alarmLow = parseFloat($("div#editSensors table tr#" + id + " td input[data-name='alarm_low']").val());
			var warningLow = parseFloat($("div#editSensors table tr#" + id + " td input[data-name='warning_low']").val());
			var warningHigh = parseFloat($("div#editSensors table tr#" + id + " td input[data-name='warning_high']").val());
			var alarmHigh = parseFloat($("div#editSensors table tr#" + id + " td input[data-name='alarm_high']").val());
			
			if (!((alarmLow <= warningLow) && (warningLow <= warningHigh) && (warningHigh <= alarmHigh) && (alarmLow <= warningLow)))
			{
				$("div#editSensors table tr#" + id + " td input[data-name='alarm_low']").addClass("alert");
				$("div#editSensors table tr#" + id + " td input[data-name='warning_low']").addClass("alert");
				$("div#editSensors table tr#" + id + " td input[data-name='warning_high']").addClass("alert");
				$("div#editSensors table tr#" + id + " td input[data-name='alarm_high']").addClass("alert");
				alertHtml = $("div#devices div.alert").html() + "<br />" +  language.Settings_edits_notvalidlimits;
				$("div#devices div.alert").html(alertHtml);
				isvalid = false;
			}	
		}
		if ($("div#editSensors table tr#" + id + " td input[data-name='read_period']").length != 0) // pasifse
		{
			var recordPeriod = parseInt($("div#editSensors table tr#" + id + " td input[data-name='record_period']").val());
			var readPeriod = parseInt($("div#editSensors table tr#" + id + " td input[data-name='read_period']").val());
			if (recordPeriod < readPeriod)
			{
				$("div#editSensors table tr#" + id + " td input[data-name='record_period']").addClass("alert");
				$("div#editSensors table tr#" + id + " td input[data-name='read_period']").addClass("alert");
				alertHtml = $("div#devices div.alert").html() + "<br />" +  language.Settings_edits_readperiodgreaterthanrecordperiod;
				$("div#devices div.alert").html(alertHtml);
				isvalid = false;
			}
		}
	
	   	if (readF != undefined && writeF != undefined)
	   	{
	   		if (readF && writeF)
	   			json["read_write"] = "2";
	   		else if (readF)
	   			json["read_write"] = "0";
	   		else if (writeF)
	   			json["read_write"] = "1";
	   		else
	   		{
	   			$("div#editSensors table tr#" + id + " td input[data-name='read_function_code']").addClass("alert");
	   			$("div#editSensors table tr#" + id + " td input[data-name='write_function_code']").addClass("alert");
	   			alertHtml = $("div#devices div.alert").html() + "<br />" +  language.Settings_edits_read_write_function_code_define;
				$("div#devices div.alert").html(alertHtml);
				isvalid = false;
	   		}
	   	}
		
	}
	
	return isvalid;
};

Settings.sensorSaveAll = function()
{
	Settings.editSensorsValidateClear("all");
	var error = 0;
	var list = $("div#editSensors tr:not(.head) input#save.editt");
	for(var i = 0; i < list.length; i++) {
		var item = $(list[i]);
			var url = "updateSensor" + item.data("url");
			var id = item.data("id");
			var type = item.data("type");

			var ionum = url.split("=");
			ionum = ionum[4];

			var json = {};
			var ret= Settings.editSensorsValidate(id,type,ionum, json);

			if(ret == false)
			{
				error++;
				continue;
			}
			Settings.updateEditSensor(url,json,id);
			$(list[i]).removeClass("editt");
	}
	if (error == 0)
	{
		Settings.editSensorsValidateClear("all");
	}
};


$(function() {
		$("div#devices").on( "change", "div#edits ul.filters li select", function() {
			$( "div#edits ul.filters li select option:selected" ).each(function() {
		      	Main.loading();
				var url = $(this).val();
				addSensor = $(this).attr("addSensor");
				var devType = parseInt($(this).attr("device-type"));
				var dType = deviceTypesToStr0(devType);
				if (deviceTypes[dType].sensorAddable)
				{
					if ($("div#edits div#buttons input#newsensor").length == 0)
					{
						var newSensor = document.createElement("input");
						newSensor.setAttribute("type","submit");
						newSensor.setAttribute("id","newsensor");
						$("div#edits div#buttons input#saveall").before(newSensor);
						$("div#devices div#edits div#buttons input#newsensor").val(language.Settings_edits_newsensor);
					}
				}	
				else
				{
					if ($("div#edits input#newsensor").length != 0)
					{
						$("div#edits input#newsensor").remove();
					}
				}
					
				Settings.getEditSensorsList(url);
		    });	
		});

		$("div#devices").on( "click", "div#editSensors table tr td input#save", function() {
			$("div#devices div.alert").html("");
			var url = "updateSensor" + $(this).data("url");
			var id = $(this).data("id");
			var type = $(this).data("type");

			var ionum = url.split("=");
			ionum = ionum[4];

			var index = allSaved.indexOf(id);
			allSaved.splice(index,1);
			var json = {};
			var ret= Settings.editSensorsValidate(id,type,ionum, json);

			if(ret != false) {
				Settings.updateEditSensor(url,json,id);
				Settings.editSensorsValidateClear("all");
			}
		});

		$("div#devices").on( "click", "div#editSensors table tr td input#edit", function() {
			var id = $(this).data("id");
			var status = false;
			$($(this).parent().children("input#save")[0]).addClass("editt");

			allSaved.push(id);

			$("tr#" + id + " td input#edit").css("display","none");
			$("tr#" + id + " td input#delete").css("display","none");
			$("tr#" + id + " td input#save").css("display","inline-block");
			$("tr#" + id + " td input#cancel").css("display","inline-block");

			$("div#editSensors table tr#" + id + " input[type='text']").each(function() {
				$(this).prop('disabled', status);
				var val = $(this).val();
				$(this).attr("data-val", val);
			});

			$("div#editSensors table tr#" + id + " input[type='checkbox']").each(function() {
				$(this).prop('disabled', status);
				var val = $(this).val();
				$(this).attr("data-val", val);
			});

			$("div#editSensors table tr#" + id + " select").each(function() {
				$(this).prop('disabled', status);
				var val = $(this).val();
				$(this).attr("data-val", val);
			});
		});

		$("div#devices").on( "click", "div#editSensors table tr td input#cancel", function() {
			var id = $(this).data("id");
			Settings.editSensorsValidateClear(id);
			$($(this).parent().children("input#save")[0]).removeClass("editt");
			var status = true;
			var index = allSaved.indexOf(id);
			allSaved.splice(index,1);

			$("div#editSensors table tr#" + id + " td input#edit").css("display","inline-block");
			$("div#editSensors table tr#" + id + " td input#delete").css("display","inline-block");
			$("div#editSensors table tr#" + id + " td input#save").css("display","none");
			$("div#editSensors table tr#" + id + " td input#cancel").css("display","none");

			$("div#editSensors table tr#" + id + " input[type='text']").each(function() {
				$(this).prop('disabled', status);
				var val = $(this).attr("data-val");
				$(this).val(val);
			});

			$("div#editSensors table tr#" + id + " input[type='checkbox']").each(function() {
				$(this).prop('disabled', status);
				var val = parseInt($(this).attr("data-val"));
				$(this).val(val);
				if(val == 0) {
					$(this).prop('checked', false);
				} else {
					$(this).prop('checked', true);
				}
			});

			$("div#editSensors table tr#" + id + " select").each(function() {
				$(this).prop('disabled', status);
				var dataval = parseInt($(this).attr("data-val"));
				$(this).find("option").each(function(){
					var val = parseInt($(this).val());
					if(val == dataval) {
						$( this ).prop('selected', true);
					} else {
						$( this ).prop('selected', false);
					}
				});
			});
		});

		$("div#devices").on( "click", "div#edits input#saveall", function() {
				Settings.sensorSaveAll();
		});

		$("div#devices").on( "click", "div#edits table tr.head div.switch input", function() {
			if(displayStatus != $(this).val()) {
				displayStatus = $(this).val();
				var devType = $( "div#edits ul.filters li select option:selected" ).attr("device-type");
				var sensType = $("li.checkboxs label.selected").attr("for");
				var sensorType = sensorTypes[sensType];
				Settings.activateFields(devType, sensorType, displayStatus);
			}
		});

		$("div#devices").on( "click", "div#edits ul.filters li label", function() {
			//Main.loading();
			if(!$(this).hasClass("selected")) {
				$("div#edits ul.filters li label").removeClass("selected");
				$(this).addClass("selected");
				var id = $(this).attr("for");
				var devType = $( "div#edits ul.filters li select option:selected" ).attr("device-type");
				var sensorType = sensorTypes[id];
				
				$("div#Header table").html("");
				Settings.activateFields(devType, sensorType, displayStatus);
			}
		});
		
		$("div#devices").on( "click", "div#editSensors table tr td input#delete", function() {
			var url = "removeSensor" + $(this).data("url");

			$("div#overlay").css("display","block");
			$("div#alertDelete").css("display","block");

			$("div#alertDelete input#buttonRemove").val(language.remove);
			$("div#alertDelete input#buttonRemove").data("url", url);
			$("div#alertDelete input#buttonRemove").addClass("sensorDelete");
			$("div#alertDelete input#buttonClose").val(language.close);
			$("div#alertDelete span.text").html(language.Settings_delete_sensor);	
		});

		$("div#devices").on( "click", "div#alertDelete input#buttonClose", function() {
			$("div#overlay").css("display","none");
			$("div#alertDelete").css("display","none");
		});

		$("div#devices").on( "click", "div#alertDelete input.sensorDelete", function() {
			Main.loading();
			var url = $(this).data("url");
			$("div#overlay").css("display","none");
			$("div#alertDelete").css("display","none");
			$("div#alertDelete input#buttonRemove").removeClass("sensorDelete");
			Settings.removeSensor(url);
		});	

		$("div#devices").on( "click", "div#edits input#newsensor", function() {

			var devType = parseInt($("div#edits ul.filters li option:selected").attr("device-type"));

			if(parseInt(devType) == 12)
			{
				var page ="Formula";
				var index = 5;
				$("div#menu li.active").removeClass("active");
				$("div#menu li#Formula").addClass("active");
				Main.pagesClose();
				Main.pages[index].status = true;
				Main.pageOpen();
			}
			else
			{
				SelectType = undefined;
				var sensorType = Devices.createSensorType(devType, sensorTypes.float);
				if (devType == deviceTypes.ping.value || devType == deviceTypes.telnet.value)
					sensorType = Devices.createSensorType(devType, sensorTypes.binary);
				var html = language.Settings_edits_addnewsensor + "<br>" + $("div#edits ul.filters li option:selected").html();
				$("div#editWindow td#modulsensorname").html(html);
				$("div#editWindow tr#trtype select").prop("selectedIndex", 0);
				$("div#editWindow td.alert").html("");
				$("div#overlay").css("display","block");
				$("div#editWindow td.alert").html("");
				$("div#editWindow").css("display","block");
				$("div#editWindow input#buttonUpdate").val(language.save);
				$("div#editWindow input#buttonCancel").val(language.cancel);
				$("div#editWindow").attr("window-type", "add");
				$("div#editWindow").attr("object", "sensors");
				$("div#editWindow").attr("object-type", sensorTypesToStr(sensorType));
				Devices.createEditWindow();
				var device = $("div#edits ul.filters li option:selected");
				if (device.attr("device-active") != "1")
				{
					$("div#editWindow tr#trresolution").remove();
				}
				Devices.DeviceFieldsToDefaults();
			}
		});
});