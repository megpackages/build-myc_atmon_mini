Exports = {};

Devices.callExportUnexport = function()
{
	$("div#devices div.form").html("");
	var html = 	'<div id="exportunexport">'
					+ '<div id="exportables">'
						+ '<table></table>'
					+ '</div>'
					+ '<div class="space"></div>'
					+ '<div id="deviceList">'
						+ '<table></table>'
					+ '</div>'
					+ '<div class="space"></div>'
					+ '<div id="sensorList">'
						+ '<table></table>'
					+ '</div>'
					+ '<div id="exportWindow" style="width: 600px;" class="panelDiv">'
						+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="tableEditHead">'
							+ '<tr>'
								+ '<td id="modulsensorname"></td>'
							+ '</tr>'
							+ '<tr>'
								+ '<td id="alert" class="alert"></td>'
							+ '</tr>'
						+ '</table>'
						+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="tableEditList">'
						+ '</table>'
						+ '<div class="buttons">'
							+ '<input type="submit" id="buttonUpdate" name="buttonUpdate" class="submit">'
							+ '<input type="submit" id="buttonCancel" name="buttonCancel" class="submit">'
						+ '</div>'
					+ '</div>'
					+ '<div id="editAlert" style="width: 400px;" class="panelDiv">'
				    + '<span class="text"></span>'
				    + '<div class="buttons">'
				        + ' <input type="submit" id="buttonOk" name="buttonOk" class="submit">'
				        + ' <input type="submit" id="buttonClose" name="buttonClose" class="submit">'
				    + '</div>'
				+ '</div>'
				+ '<div class="information"><div class="infos">'
					+ '<div class="blk" id="infoExport"><input type="submit"><b></b></div>' // export
					+ '<div class="blk" id="infoUnexport"><input type="submit"><b></b></div>' // edit
					+ '<div class="blk" id="infoEdit"><input type="submit"><b></b></div>' // unexport
				+ '</div>'
				+ '</div>';
	$("div#devices div.form").html(html);

	$("div#devices div#exportunexport div.information div#infoExport b").html(language.Settings_exports_info_export);
	$("div#devices div#exportunexport div.information div#infoEdit b").html(language.Settings_exports_info_editexport);
	$("div#devices div#exportunexport div.information div#infoUnexport b").html(language.Settings_exports_info_unexport);

	$("div#devices div#exportunexport div.information div#infoExport input").val(language.Settings_exports_export);
	$("div#devices div#exportunexport div.information div#infoEdit input").val(language.Settings_exports_editexport);
	$("div#devices div#exportunexport div.information div#infoUnexport input").val(language.Settings_exports_unexport);
	Exports.createExportableList();
	//Exports.createExportWindow();
};

Exports.isBase = function(DeviceName){

	var format = exportFormats[DeviceName.toLowerCase()];
	if(format == "-1"){
		return true;
	}
	else{
		return false;
	}
};

Exports.createExportableList = function()
{
	Main.loading();
	var formats = $.getJSON("http://" + hostName + "/cgi-bin/sensorexport.cgi?getExportFormats");
	formats.success(function(exportList) {
		$.each(exportList, function(key, value){

			exportTypes[value.protocol_name] = key;
			exportFormats[value.protocol_name] = value.format;
		});
		Main.unloading();
		var prop = JSON.parse(formats.responseText);
		var prop2 = {};
		for (var i =0; i < prop.length; i++)
			prop2[prop[i].protocol_name] = prop[i].format;
		prop = prop2;
		var trH = document.createElement("tr");
		trH.setAttribute("class", "head");
		var tdH = document.createElement("td");
		tdH.innerHTML = language.Settings_export_exportablelist;
		trH.appendChild(tdH);
		var first = true;
		$("div#exportunexport div#exportables table").append(trH);

		$.each(exportList, function(keyy, value){
			var key = value.protocol_name;
			//var exportable = exportTypes[key];
			var tr = document.createElement("tr");
			var td = document.createElement("td");
			td.setAttribute("value", eval("exportTypes." + key).toString());
			td.setAttribute("option", eval("prop." + key));
			var a = document.createElement("a");
			if (key == "modbustcp" && key == "iec60870" )
				a.style.width = "100%";
			//a.innerHTML = eval("language.Settings_exports_" + key);
			a.innerHTML = key.toUpperCase();
			if (first == true)
			{
				td.setAttribute("class", "active");
				first = false;
			}
			td.appendChild(a);

			var span = document.createElement("span");
			span.setAttribute("class", "exports");
			var input = document.createElement("input");
			input.setAttribute("type", "submit");
			input.setAttribute("id", "export_edit");
			input.setAttribute("value", language.Settings_exports_editexport);
			span.appendChild(input);
			if (key=="iec60870")
				td.appendChild(span);

			var span = document.createElement("span");
			span.setAttribute("class", "exports");
			var input = document.createElement("input");
			input.setAttribute("type", "submit");
			input.setAttribute("id", "export_modbusregister");
			input.setAttribute("value", "R");
			span.appendChild(input);
			if (key == "modbustcp" )
				td.appendChild(span);


			tr.appendChild(td);
			$("div#exportunexport div#exportables table").append(tr);
		});

		$("div#exportunexport div#exportables table tr:not(.head):first input").prop("checked", true);
		Exports.getDeviceList(0);
	});
	formats.error(function(jqXHR, textStatus, errorThrown){
		Main.unloading();
		Main.alert(jqXHR.responseText);
	});
};

Exports.getDeviceList = function(exportableNum)
{
	Main.loading();
	var protocol_name;
	$.each(exportTypes, function(key, value){
		if(value == exportableNum){
			protocol_name = key;
		}
	});
	var isBase = Exports.isBase(protocol_name);
	if(isBase){

		var deviceData = $.getJSON("http://" + hostName + "/cgi-bin/sensorexport.cgi?getDevicesExportedTobase&protocol_name="+protocol_name);

	}else{

		var deviceData = $.getJSON("http://" + hostName + "/cgi-bin/sensorexport.cgi?getDevicesExportedTo" + exportTypesToStr(exportableNum));
	}
	deviceData.success(function() {
		Main.unloading();
		var lst = JSON.parse(deviceData.responseText);
		Exports.createDeviceList(lst, exportableNum);
	});
	deviceData.error(function(jqXHR, textStatus, errorThrown){
		Main.unloading();
		Main.alert(language.server_error + ":" + jqXHR.responseText);
	});
};

Exports.createDeviceList = function(lst, exportableNum)
{
	$("div#exportunexport div#deviceList table").html("");
	var trH = document.createElement("tr");
	trH.setAttribute("class", "head");
	var tdH = document.createElement("td");
	tdH.innerHTML = language.Settings_export_devicelist;
	trH.appendChild(tdH);
	$("div#exportunexport div#deviceList table").append(trH);
	
	for (var i = 0; i < lst.length; i++)
	{
		var dev = lst[i];
		var tr = document.createElement("tr");
		tr = $(tr);
		var td = document.createElement("td");
		td = $(td);
		var name = Monitoring.characterLimit(dev.name, 18);
		
		var A = document.createElement("a");
		A.innerHTML = name;
		td.append(A);		
		var span = document.createElement("span");
		span.setAttribute("class", "exports");
		var iExport = document.createElement("input");
		iExport.setAttribute("type", "submit");
		iExport.setAttribute("id", "export");
		iExport.setAttribute("value", language.Settings_exports_export);
		iExport.setAttribute("objectid", dev.objectid);
		iExport.setAttribute("moduleid", dev.id);
		iExport.setAttribute("name", dev.name);
		iExport.setAttribute("devicetype", dev.type);
		var iUnexport = document.createElement("input");
		iUnexport.setAttribute("type", "submit");
		iUnexport.setAttribute("id", "unexport");
		iUnexport.setAttribute("value", language.Settings_exports_unexport);
		iUnexport.setAttribute("objectid", dev.objectid);
		span.appendChild(iExport);
		span.appendChild(iUnexport);
		td.append(span);
		if (dev.allUnexported == "1")
		{
			iUnexport.style.display = "none";
		}
		if (dev.allExported == "1")
		{
			iExport.style.display = "none";
		}
		var tr = $(document.createElement("tr"));
		if (dev.name.length > 18)
			td.attr("data-title", dev.name);
		td.attr("objectid", dev.objectid);
		tr.append(td);
		$("div#exportunexport div#deviceList table").append(tr);
	}
  	var devoid = $("div#exportunexport div#deviceList table tr:not(.head) td").first().attr("objectid");
  	$("div#exportunexport div#deviceList table tr:not(.head) td").first().addClass("active");
  	Exports.getSensorList(devoid,exportableNum);
};

Exports.getSensorList = function(devoid, exportableNum)
{
	var obj = devoid.split("/");
	Main.loading();
	var mdl = "";
	try{

		$.each(exportTypes, function(key, value){

			if(value == exportableNum){
				mdl = key.toUpperCase();
			}
		});
	}catch (e){

		Main.unloading();
		return;
	}
	var isBase = Exports.isBase(mdl);
	if(isBase){
		var sensorData = $.getJSON("http://" + hostName + "/cgi-bin/sensorexport.cgi?getExportsToBASE&hwaddr=" + obj[0] + "&portnum=" + obj[1] + "&devaddr=" + obj[2] + "&protocol_name=" + mdl.toLowerCase());
	}else{
		var sensorData = $.getJSON("http://" + hostName + "/cgi-bin/sensorexport.cgi?getExportsTo" + mdl + "&hwaddr=" + obj[0] + "&portnum=" + obj[1] + "&devaddr=" + obj[2]);
	}
	sensorData.success(function() {
		Main.unloading();
		var lst = JSON.parse(sensorData.responseText);
		Exports.createSensorList(lst, exportableNum);
	});
	sensorData.error(function(jqXHR, textStatus, errorThrown){	
		Main.unloading();
		Main.alert(language.server_error + ":" + jqXHR.responseText);
	});
};

Exports.createSensorList = function(lst, exportableNum)
{
	$("div#exportunexport div#sensorList table").html("");
	var trH = document.createElement("tr");
	trH.setAttribute("class", "head");
	var tdH = document.createElement("td");
	tdH.innerHTML = language.Settings_export_sensorlist;
	trH.appendChild(tdH);
	$("div#exportunexport div#sensorList table").append(trH);
	var allExported = true;
	var allUnexported = true;
	for (var i = 0; i < lst.length; i++)
	{
		var sensor = lst[i];
		var tr = document.createElement("tr");
		tr = $(tr);
		var td = document.createElement("td");
		td = $(td);
		
		var name = Monitoring.characterLimit(sensor.name, 22);
		
		var A = document.createElement("a");
		A.innerHTML = name;
		td.append(A);		
		var span = document.createElement("span");
		span.setAttribute("class", "exports");
		var iExportAdd = document.createElement("input");
		iExportAdd.setAttribute("type", "submit");
		iExportAdd.setAttribute("id", "export_add");
		iExportAdd.setAttribute("value", language.Settings_exports_export);
		iExportAdd.setAttribute("objectid", sensor.objectid);
		iExportAdd.setAttribute("sensorid", sensor.id);
		iExportAdd.setAttribute("name", sensor.name);
		var iExportEdit = document.createElement("input");
		iExportEdit.setAttribute("type", "submit");
		iExportEdit.setAttribute("id", "export_edit");
		iExportEdit.setAttribute("value", language.Settings_exports_editexport);
		iExportEdit.setAttribute("objectid", sensor.objectid);
		iExportEdit.setAttribute("sensorid", sensor.id);
		iExportEdit.setAttribute("address", sensor.address);
		var iUnexport = document.createElement("input");
		iUnexport.setAttribute("type", "submit");
		iUnexport.setAttribute("id", "unexport");
		iUnexport.setAttribute("value", language.Settings_exports_unexport);
		iUnexport.setAttribute("objectid", sensor.objectid);
		iUnexport.setAttribute("sensorid", sensor.id);
		
		if (parseInt(sensor.exported) == 1)
		{
			iExportAdd.style.display = "none";	
		}
		else
		{
			iExportEdit.style.display = "none";
			iUnexport.style.display = "none";
		}
		
		span.appendChild(iExportAdd);
		if (exportTypesToStr(exportableNum) == "iec60870")
			span.appendChild(iExportEdit);
		span.appendChild(iUnexport);
		td.append(span);	
		td.attr("objectid", sensor.objectid);
		if (sensor.name.length > 22)
			td.attr("data-title", sensor.name);
		tr.append(td);
		$("div#exportunexport div#sensorList table").append(tr);
	}	
};

Exports.ExportSensor = function(To, objectid, option)
{
	Main.loading();
	var isBase = Exports.isBase(To);
	if(isBase){
		var url = "http://" + hostName + "/cgi-bin/sensorexport.cgi?exportToBASE&objectid=" + objectid + "&option=" + option + "&protocol_name="+To.toLowerCase();
	}else{
		var url = "http://" + hostName + "/cgi-bin/sensorexport.cgi?exportTo" + To + "&objectid=" + objectid + "&option=" + option;
	}
	var oid = objectid.split("/");
	var devoid = oid[0] + "/" + oid[1] + "/" + oid[2];
	
	$.ajax({
	    url : url,
	    type : "POST",
	    data : {},
	    dataType : 'text',
	    contentType : 'text/json; charset=utf-8',
	    success : function(data, textStatus, jqXHR)
	    {
	    	var jSon = jQuery.parseJSON(data);
			Main.unloading();
			if (oid.length == 4) // sensor export
			{
				$("div#exportunexport div#sensorList table tr input[objectid='" + objectid + "']#unexport").css("display", "block");
				$("div#exportunexport div#sensorList table tr input[objectid='" + objectid + "']#export_add").css("display", "none");
				$("div#exportunexport div#sensorList table tr input[objectid='" + objectid + "']#export_edit").css("display", "block");
				
				$("div#exportunexport div#deviceList table tr input[objectid='" + devoid + "']#unexport").css("display", "block");
				$("div#exportunexport div#deviceList table tr input[objectid='" + devoid + "']#export").css("display", "block");
				if ($("div#exportunexport div#sensorList table tr input[id='export_add']:visible").length == 0)
				{
					$("div#exportunexport div#deviceList table tr input[objectid='" + devoid + "']#export").css("display", "none");
				}
				Main.alert(language.Settings_exports_export_success);
			}
			else if (oid.length == 3) // device export
			{
				var input = $("div#exportunexport div#deviceList table tr input[objectid='" + devoid + "']#export");
				$("div#exportunexport div#deviceList table tr input[objectid='" + devoid + "']#unexport").css("display", "block");
				$("div#exportunexport div#deviceList table tr input[objectid='" + devoid + "']#export").css("display", "none");
				if (input.closest("td").hasClass("active"))
				{
					$("div#exportunexport div#sensorList table tr input#unexport").css("display", "block");
					$("div#exportunexport div#sensorList table tr input#export_add").css("display", "none");
					$("div#exportunexport div#sensorList table tr input#export_edit").css("display", "block");
				}
				Main.alert(language.Settings_exports_device_export_success);
			}
			var length = jSon.length;
			for (var i = 0; i < length; i++)
			{
				var exSens = jSon[i];
				var button = $("div#exportunexport div#sensorList table tr input[sensorid='" + exSens.sensorid + "']#export_edit");
				if (button != undefined)
					button.attr("address", exSens.address);
			}
		},
	    error : function (jqXHR, textStatus, errorThrown)
	    {	
			Main.unloading();
			if (oid.length == 4) // sensor export
			{
	    		Main.alert(language.Settings_exports_export_failed + ":" + jqXHR.responseText);
			}
			else if (oid.length == 3) // device export
			{
	    		Main.alert(language.Settings_exports_device_export_failed + ":" + jqXHR.responseText);
			}
	    }
	});
};

Exports.UnexportSensor = function(From, objectid)
{
	Main.loading();
	var oid = objectid.split("/");
	var devoid = oid[0] + "/" + oid[1] + "/" + oid[2];
	var isBase = Exports.isBase(From);
	if(Exports.isBase(From)){
		var url = "http://" + hostName + "/cgi-bin/sensorexport.cgi?unexportFromBASE&objectid=" + objectid+"&protocol_name="+From.toLowerCase();
	}else{
		var url = "http://" + hostName + "/cgi-bin/sensorexport.cgi?unexportFrom" + From + "&objectid=" + objectid;
	}
	$.ajax({
	    url : url,
	    type : "POST",
	    data : {},
	    dataType : 'text',
	    contentType : 'text/json; charset=utf-8',
	    success : function()
	    {
			Main.unloading();
			if (oid.length == 4) // sensor unexport
			{
				$("div#exportunexport div#sensorList table tr input[objectid='" + objectid + "']#unexport").css("display", "none");
				$("div#exportunexport div#sensorList table tr input[objectid='" + objectid + "']#export_add").css("display", "block");
				$("div#exportunexport div#sensorList table tr input[objectid='" + objectid + "']#export_edit").css("display", "none");
				$("div#exportunexport div#deviceList table tr input[objectid='" + devoid + "']#export").css("display", "block");
				$("div#exportunexport div#deviceList table tr input[objectid='" + devoid + "']#unexport").css("display", "block");
				if ($("div#exportunexport div#sensorList table tr input[id='unexport']:visible").length == 0)
				{
					$("div#exportunexport div#deviceList table tr input[objectid='" + devoid + "']#unexport").css("display", "none");
				}
				Main.alert(language.Settings_exports_unexport_success);
			}
			else if (oid.length == 3)// device unexport
			{
				var input = $("div#exportunexport div#deviceList table tr input[objectid='" + devoid + "']#unexport");
				$("div#exportunexport div#deviceList table tr input[objectid='" + devoid + "']#unexport").css("display", "none");
				$("div#exportunexport div#deviceList table tr input[objectid='" + devoid + "']#export").css("display", "block");
				if (input.closest("td").hasClass("active"))
				{
					$("div#exportunexport div#sensorList table tr input#unexport").css("display", "none");
					$("div#exportunexport div#sensorList table tr input#export_add").css("display", "block");
					$("div#exportunexport div#sensorList table tr input#export_edit").css("display", "none");
				}
				Main.alert(language.Settings_exports_device_unexport_success);	
			}
		},
	    error : function (jqXHR, textStatus, errorThrown)
	    {	
			Main.unloading();
			if (oid.length == 4) // sensor unexport
			{
	    		Main.alert(language.Settings_exports_unexport_failed + ":" + jqXHR.responseText);
			}
			else if (oid.length == 3) // device unexport
			{
	    		Main.alert(language.Settings_exports_device_unexport_failed + ":" + jqXHR.responseText);
			}
	    }
	});
};

Exports.createBaseExportableWindow = function(DeviceName){

	$("div#exportunexport div#exportWindow table.tableEditList").html("");
	var type = "export"+DeviceName.toUpperCase()+"Format";
	$("div#exportunexport div#exportWindow").attr("type", type);
	var option = $("div#exportunexport div#exportables tr td[value='2']").attr("option");
	var table = $("div#exportunexport div#exportWindow table.tableEditList");

	var trOption = document.createElement("tr");
	trOption.setAttribute("id", type);
	var td0 = document.createElement("td");
	td0.setAttribute("width","40%");

	var select = document.createElement("select");
	select.style.width = "90%";
	select.style.height = "80%";
	var opt1 = document.createElement("option");
	opt1.innerHTML = language.Settings_export_iec60870_opt1;
	select.appendChild(opt1);
	td0.appendChild(select);

	var td1 = document.createElement("td");
	td1.innerHTML = "1.3.6.1.4.1.26518.1.1.1.2.1.4.sensorid";
	trOption.appendChild(td0);
	trOption.appendChild(td1);
	$("div#exportunexport div#exportWindow table.tableEditList").append(trOption);
	$("div#exportunexport div#exportWindow table.tableEditList tr select").prop("selectedIndex", parseInt(option));

	$("div#exportunexport div#exportWindow div.buttons input#buttonUpdate").val(language.save);
	$("div#exportunexport div#exportWindow div.buttons input#buttonCancel").val(language.cancel);
};

Exports.createIec60870ExportableWindow = function()
{
	$("div#exportunexport div#exportWindow table.tableEditList").html("");
	$("div#exportunexport div#exportWindow").attr("type", "exportIec60870Format");
	var option = $("div#exportunexport div#exportables tr td[value='0']").attr("option");
	var table = $("div#exportunexport div#exportWindow table.tableEditList");

	var trOption = document.createElement("tr");
	trOption.setAttribute("id", "exportIec60870Format");
	var td0 = document.createElement("td");
	td0.setAttribute("width","40%");
	
	var select = document.createElement("select");
	select.style.width = "90%";
	select.style.height = "80%";
	var opt1 = document.createElement("option");
	opt1.innerHTML = language.Settings_export_iec60870_opt1 + "(X-X-X)";
	var opt2 = document.createElement("option");
	opt2.innerHTML = language.Settings_export_iec60870_opt2 + "(XX-X)";
	var opt3 = document.createElement("option");
	opt3.innerHTML = language.Settings_export_iec60870_opt3 + "(X-XX)";
	select.appendChild(opt1);
	select.appendChild(opt2);
	select.appendChild(opt3);
	td0.appendChild(select);
	
	var td1 = document.createElement("td");	
	
	if (option == "0")
	{
		td1.innerHTML = language.devicetype + " | " + language.deviceid + " | " + language.ionum;
	}
	else if (option == "1")
	{
		td1.innerHTML = language.deviceid + " | " + language.ionum;
	}
	else if (option == "2")
	{
		td1.innerHTML = language.deviceid + " | " + language.sensorid;
	}
	
	trOption.appendChild(td0);
	trOption.appendChild(td1);
	$("div#exportunexport div#exportWindow table.tableEditList").append(trOption);
	$("div#exportunexport div#exportWindow table.tableEditList tr select").prop("selectedIndex", parseInt(option));

	$("div#exportunexport div#exportWindow div.buttons input#buttonUpdate").val(language.save);
	$("div#exportunexport div#exportWindow div.buttons input#buttonCancel").val(language.cancel);
};


Exports.updateIec60870ExportFormat = function(opt)
{
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/sensorexport.cgi?updateExportFormats&iec60870=" + opt.toString();
	$.ajax({
	    url : url,
	    type : "POST",
	    data : {},
	    dataType : 'text',
	    contentType : 'text/json; charset=utf-8',
	    success : function()
	    {
			Main.unloading();
			$("div#exportunexport div#exportables tr td[value='0']").attr("option", opt.toString());
			$("div#overlay").css("display", "none");
			$("div#devices div#exportunexport div#exportWindow").css("display", "none");
			$("div#editAlert").css("display","none");
			Exports.getDeviceList(0);
		},
	    error : function (jqXHR, textStatus, errorThrown)
	    {	
			Main.unloading();
	    	Main.alert(jqXHR.responseText);
	    }
	});
	
};


Exports.createSNMPExportableWindow = function()
{
	$("div#exportunexport div#exportWindow table.tableEditList").html("");
	$("div#exportunexport div#exportWindow").attr("type", "exportSNMPFormat");
	var option = $("div#exportunexport div#exportables tr td[value='1']").attr("option");
	var table = $("div#exportunexport div#exportWindow table.tableEditList");

	var trOption = document.createElement("tr");
	trOption.setAttribute("id", "exportSNMPFormat");
	var td0 = document.createElement("td");
	td0.setAttribute("width","40%");
	
	var select = document.createElement("select");
	select.style.width = "90%";
	select.style.height = "80%";
	var opt1 = document.createElement("option");
	opt1.innerHTML = language.Settings_export_iec60870_opt1;
	select.appendChild(opt1);
	td0.appendChild(select);
	
	var td1 = document.createElement("td");	
	td1.innerHTML = "1.3.6.1.4.1.26518.1.1.1.2.1.4.sensorid";
	trOption.appendChild(td0);
	trOption.appendChild(td1);
	$("div#exportunexport div#exportWindow table.tableEditList").append(trOption);
	$("div#exportunexport div#exportWindow table.tableEditList tr select").prop("selectedIndex", parseInt(option));

	$("div#exportunexport div#exportWindow div.buttons input#buttonUpdate").val(language.save);
	$("div#exportunexport div#exportWindow div.buttons input#buttonCancel").val(language.cancel);
};


Exports.updateSNMPExportFormat = function(opt)
{
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/sensorexport.cgi?updateExportFormats&snmp=" + opt.toString();
	$.ajax({
	    url : url,
	    type : "POST",
	    data : {},
	    dataType : 'text',
	    contentType : 'text/json; charset=utf-8',
	    success : function()
	    {
			Main.unloading();
			$("div#exportunexport div#exportables tr td[value='1']").attr("option", opt.toString());
			$("div#overlay").css("display", "none");
			$("div#devices div#exportunexport div#exportWindow").css("display", "none");
		},
	    error : function (jqXHR, textStatus, errorThrown)
	    {	
			Main.unloading();
	    	Main.alert(jqXHR.responseText);
	    }
	});
	
};


Exports.updateBASEExportFormat = function(opt, DeviceName){
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/sensorexport.cgi?updateExportFormats&"+DeviceName.toLowerCase()+"=" + opt.toString();
	$.ajax({
	    url : url,
	    type : "POST",
	    data : {},
	    dataType : 'text',
	    contentType : 'text/json; charset=utf-8',
	    success : function()
	    {
			Main.unloading();
			$("div#exportunexport div#exportables tr td[value='2']").attr("option", opt.toString());
			$("div#overlay").css("display", "none");
			$("div#devices div#exportunexport div#exportWindow").css("display", "none");
		},
	    error : function (jqXHR, textStatus, errorThrown)
	    {	
			Main.unloading();
	    	Main.alert(jqXHR.responseText);
	    }
	});
	
};




Exports.createMODBUSTCPExportableWindow = function()
{
	$("div#exportunexport div#exportWindow table.tableEditList").html("");
	$("div#exportunexport div#exportWindow").attr("type", "exportMODBUSTCPFormat");
	var option = $("div#exportunexport div#exportables tr td[value='3']").attr("option");
	var table = $("div#exportunexport div#exportWindow table.tableEditList");

	var trOption = document.createElement("tr");
	trOption.setAttribute("id", "exportMODBUSTCPFormat");
	var td0 = document.createElement("td");
	td0.setAttribute("width","40%");
	
	var select = document.createElement("select");
	select.style.width = "90%";
	select.style.height = "80%";
	var opt1 = document.createElement("option");
	opt1.innerHTML = language.Settings_export_iec60870_opt1;
	select.appendChild(opt1);
	td0.appendChild(select);
	
	var td1 = document.createElement("td");	
	td1.innerHTML = "1.3.6.1.4.1.26518.1.1.1.2.1.4.sensorid";
	trOption.appendChild(td0);
	trOption.appendChild(td1);
	$("div#exportunexport div#exportWindow table.tableEditList").append(trOption);
	$("div#exportunexport div#exportWindow table.tableEditList tr select").prop("selectedIndex", parseInt(option));

	$("div#exportunexport div#exportWindow div.buttons input#buttonUpdate").val(language.save);
	$("div#exportunexport div#exportWindow div.buttons input#buttonCancel").val(language.cancel);
};


Exports.updateMODBUSTCPExportFormat = function(opt)
{
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/sensorexport.cgi?updateExportFormats&modbustcp=" + opt.toString();
	$.ajax({
	    url : url,
	    type : "POST",
	    data : {},
	    dataType : 'text',
	    contentType : 'text/json; charset=utf-8',
	    success : function()
	    {
			Main.unloading();
			$("div#exportunexport div#exportables tr td[value='3']").attr("option", opt.toString());
			$("div#overlay").css("display", "none");
			$("div#devices div#exportunexport div#exportWindow").css("display", "none");
		},
	    error : function (jqXHR, textStatus, errorThrown)
	    {	
			Main.unloading();
	    	Main.alert(jqXHR.responseText);
	    }
	});
	
};



Exports.createEditExportWindow = function(sensorid,address)
{
	var exportNum = $("div#exportunexport div#exportables table tr:not(.head) td.active").attr("value");
	var option = $("div#exportunexport div#exportables tr td[value='0']").attr("option");
	var exportable = exportTypesToStr(exportNum);
	$("div#exportunexport div#exportWindow table.tableEditList").html("");
	$("div#exportunexport div#exportWindow").attr("type", "editExportSensor");
	$("div#exportunexport div#exportWindow").attr("sensorid", sensorid);
	var table = $("div#exportunexport div#exportWindow table.tableEditList");

	var tr = document.createElement("tr");
	tr.setAttribute("id", "editExportSensor");
	var td0 = document.createElement("td");
	td0.setAttribute("width","40%");
	td0.innerHTML = language.Settings_export_exportaddress;
	
	
	var td1 = document.createElement("td");	
	var isBase = Exports.isBase(exportable);
	if (exportable == "iec60870")
	{
		if (parseInt(option) == 0)
		{
			var input1 = document.createElement("input");
			input1.setAttribute("type", "text");
			input1.setAttribute("id", "input1");
			input1.setAttribute("value", (parseInt(address) >>> 16).toString());
			input1.style.width = "20%";
			input1.style.heigth = "70%";
			var input2 = document.createElement("input");
			input2.setAttribute("type", "text");
			input2.setAttribute("id", "input2");
			input2.setAttribute("value", ((parseInt(address) << 16) >>> 24).toString());
			input2.style.width = "20%";
			input2.style.heigth = "70%";
			var input3 = document.createElement("input");
			input3.setAttribute("type", "text");
			input3.setAttribute("id", "input3");
			input3.setAttribute("value", ((parseInt(address) << 24) >>> 24).toString());
			input3.style.width = "20%";
			input3.style.heigth = "70%";
			td1.appendChild(input1);
			td1.appendChild(input2);
			td1.appendChild(input3);
		}
		else if (parseInt(option) == 1)
		{
			var input1 = document.createElement("input");
			input1.setAttribute("type", "text");
			input1.setAttribute("id", "input1");
			input1.setAttribute("value", (parseInt(address) >>> 8).toString());
			input1.style.width = "40%";
			input1.style.heigth = "70%";
			var input2 = document.createElement("input");
			input2.setAttribute("type", "text");
			input2.setAttribute("id", "input2");
			input2.setAttribute("value", ((parseInt(address) << 24) >>> 24).toString());
			input2.style.width = "20%";
			input2.style.heigth = "70%";
			td1.appendChild(input1);
			td1.appendChild(input2);
			
		}
		else if (parseInt(option) == 2)
		{
			var input1 = document.createElement("input");
			input1.setAttribute("type", "text");
			input1.setAttribute("id", "input1");
			input1.setAttribute("value", (parseInt(address) >>> 16).toString());
			input1.style.width = "20%";
			input1.style.heigth = "70%";
			var input2 = document.createElement("input");
			input2.setAttribute("type", "text");
			input2.setAttribute("id", "input2");
			input2.setAttribute("value", ((parseInt(address) << 16) >>> 16).toString());
			input2.style.width = "40%";
			input2.style.heigth = "70%";
			td1.appendChild(input1);
			td1.appendChild(input2);
		}
		var a = document.createElement("a");
		a.innerHTML = "&nbsp;  =&nbsp;  " + address;
		td1.appendChild(a);
		
	}
	else if (exportable == "snmp")
	{
		var input = document.createElement("input");
		input.setAttribute("type", "text");
		input.setAttribute("value", address);
		input.setAttribute("width", "%70");
		input.setAttribute("heigth", "%70");
		td1.appendChild(input);
	}
	else if(isBase)
	{
		var input = document.createElement("input");
		input.setAttribute("type", "text");
		input.setAttribute("value", address);
		input.setAttribute("width", "%70");
		input.setAttribute("heigth", "%70");
		td1.appendChild(input);
	}
	else if (exportable == "modbustcp")
	{
		var input = document.createElement("input");
		input.setAttribute("type", "text");
		input.setAttribute("value", address);
		input.setAttribute("width", "%70");
		input.setAttribute("heigth", "%70");
		td1.appendChild(input);
	}
	tr.appendChild(td0);
	tr.appendChild(td1);
	$("div#exportunexport div#exportWindow table.tableEditList").append(tr);

	$("div#exportunexport div#exportWindow div.buttons input#buttonUpdate").val(language.save);
	$("div#exportunexport div#exportWindow div.buttons input#buttonCancel").val(language.cancel);
};


Exports.updateExportSensor = function(To, sensorid, address)
{
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/sensorexport.cgi?updateExportSensorTo" + To + "&sensorid=" + sensorid + "&address=" + address;
	$.ajax({
	    url : url,
	    type : "POST",
	    data : {},
	    dataType : 'text',
	    contentType : 'text/json; charset=utf-8',
	    success : function()
	    {
			var sensorid = $("div#exportunexport div#exportWindow").attr("sensorid");
			$("div#exportunexport div#sensorList table tr input[sensorid='" + sensorid + "']#export_edit").attr("address", address);
			$("div#overlay").css("display", "none");
			$("div#devices div#exportunexport div#exportWindow").css("display", "none");
			Main.unloading();
		},
	    error : function (jqXHR, textStatus, errorThrown)
	    {	
			Main.unloading();
	    	Main.alert(jqXHR.responseText);
	    }
	});
};


$(function() {
		$("div#devices").on( "click", "div#exportunexport div#exportables td", function() {
			if ($(this).hasClass("active"))
				return;
			$("div#exportunexport div#exportables td").removeClass("active");
			$(this).addClass("active");
			Exports.getDeviceList($(this).attr("value"));
		});
		
		$("div#devices").on( "click", "div#exportunexport div#deviceList tr td a", function() {
			var exportNum = $("div#exportunexport div#exportables table tr:not(.head) td.active").attr("value");
			var parentTd = $(this).closest("td");
			if(parentTd.hasClass("active") == false) {
				$("div#exportunexport div#deviceList tr td").removeClass("active");
				parentTd.addClass("active");
				Exports.getSensorList($(this).parent().get(0).getAttribute("objectid"),exportNum);
			}
		});
		
		$("div#devices").on( "click", "div#exportunexport div#deviceList tr td input#export", function() {
			var objectid = $(this).attr("objectid");
			var exportNum = $("div#exportunexport div#exportables table tr:not(.head) td.active").attr("value");
		var exportText = $("div#exportunexport div#exportables table tr:not(.head) td.active").text();
			if (exportTypesToStr(exportNum) == "iec60870")
			{	
				var option = $("div#exportunexport div#exportables tr td[value='0']").attr("option");
				Exports.ExportSensor("IEC60870", objectid, option);
			}
		else{
			Exports.ExportSensor(exportText, objectid, "");
		}
	});
		
		$("div#devices").on( "click", "div#exportunexport div#deviceList tr td input#unexport", function() {
			var objectid = $(this).attr("objectid");
			var exportNum = $("div#exportunexport div#exportables table tr:not(.head) td.active").attr("value");
		var exportName = exportTypesToStr(exportNum);

		Exports.UnexportSensor(exportName.toUpperCase(), objectid);
	});
		
		
		$("div#devices").on( "click", "div#exportunexport div#sensorList tr td input#export_add", function() {
			var objectid = $(this).attr("objectid");
			var exportNum = $("div#exportunexport div#exportables table tr:not(.head) td.active").attr("value");
			if (exportTypesToStr(exportNum) == "iec60870")
			{	
				var option = $("div#exportunexport div#exportables tr td[value='0']").attr("option");
				Exports.ExportSensor("IEC60870", objectid, option);
			}
		var exportName = exportTypesToStr(exportNum);
		Exports.ExportSensor(exportName.toUpperCase(), objectid, "");
	});
		
		$("div#devices").on( "click", "div#exportunexport div#sensorList tr td input#export_edit", function() {
			var objectid = $(this).attr("objectid");
			var address = $(this).attr("address");
			var sensorid = $(this).attr("sensorid");
			var exportNum = $("div#exportunexport div#exportables table tr:not(.head) td.active").attr("value");
			var exportable = exportTypesToStr(exportNum);
			Exports.createEditExportWindow(sensorid,address);
			
			$("div#overlay").css("display", "block");
			$("div#devices div#exportunexport div#exportWindow").css("display", "block");
			$("div#devices div#exportunexport div#exportWindow td#modulsensorname").html(language.Settings_exports_editexport_header);
		});
		
		$("div#devices").on( "click", "div#exportunexport div#sensorList tr td input#unexport", function() {
			var objectid = $(this).attr("objectid");
			var exportNum = $("div#exportunexport div#exportables table tr:not(.head) td.active").attr("value");
			var exportText = $("div#exportunexport div#exportables table tr:not(.head) td.active").text();
		Exports.UnexportSensor(exportText, objectid);
	});
		
		$("div#devices").on( "click", "div#exportunexport div#exportables tr td input#export_edit", function() {
			var exportNum = parseInt($(this).closest("td").attr("value"));
			var exportable = exportTypesToStr(exportNum);
		var isBase = Exports.isBase(exportable);
		if(isBase){

			Exports.createBaseExportableWindow(DeviceName);
		}
			else if (exportable == "iec60870")
			{
				Exports.createIec60870ExportableWindow();	
			}
			else if (exportable == "snmp")
			{
				Exports.createSNMPExportableWindow();
			}
			else if (exportable == "modbustcp")
			{
				Exports.createMODBUSTCPExportableWindow();
			}
			$("div#overlay").css("display", "block");
			$("div#devices div#exportunexport div#exportWindow").css("display", "block");
			$("div#devices div#exportunexport div#exportWindow td#modulsensorname").html(language.Settings_exports_editexportformat_header);
		});
		
		$("div#devices").on( "change", "div#exportunexport div#exportWindow table.tableEditList tr#exportIec60870Format select", function() {
			var opt = $("div#exportunexport div#exportWindow table.tableEditList tr#exportIec60870Format select").prop("selectedIndex");
			var td = $("div#exportunexport div#exportWindow table.tableEditList tr#exportIec60870Format td:eq(1)");
	
			if (opt == 0)
			{
				td.html(language.devicetype + " | " + language.deviceid + " | " + language.ionum);
			}
			else if (opt == 1)
			{
				td.html(language.deviceid + " | " + language.ionum);
			}
			else if (opt == 2)
			{
				td.html(language.deviceid + " | " + language.sensorid);
			}
		});
		
		$("div#devices").on( "click", "div#exportunexport div#exportWindow input#buttonUpdate", function() {
			$("div#exportWindow table.tableEditHead td#alert").html("");
			$("div#exportunexport div#exportWindow table.tableEditList tr#editExportSensor td input").removeClass("alert");
			var type = $("div#exportunexport div#exportWindow").attr("type");
		var device;
		var subType = type.split("export")[1];
		device = subType.split("Format")[0];
		var isBasee = Exports.isBase(device);
		if(isBasee){

			var optt = "div#exportunexport div#exportWindow table.tableEditList tr#export"+device.toUpperCase()+"Format select";
			var opt = $(optt).prop("selectedIndex");
			Exports.updateBASEExportFormat(opt, device);
		}
			if (type == "exportIec60870Format")
			{
				$("div#editAlert input#buttonOk").val(language.OK);
				$("div#editAlert input#buttonClose").val(language.NO);
				$("div#editAlert span").html(language.allsensors_export_format);
				$("div#editAlert").css("display", "block");
			}
			else if (type == "exportSNMPFormat")
			{
				var opt = $("div#exportunexport div#exportWindow table.tableEditList tr#exportSNMPFormat select").prop("selectedIndex");
				Exports.updateSNMPExportFormat(opt);
			}
			else if (type == "exportMODBUSTCPFormat")
			{
				var opt = $("div#exportunexport div#exportWindow table.tableEditList tr#exportMODBUSTCPFormat select").prop("selectedIndex");
				Exports.updateMODBUSTCPExportFormat(opt);
			}
			else if (type == "editExportSensor")
			{
				var exportNum = $("div#exportunexport div#exportables table tr:not(.head) td.active").attr("value");
				var exportable = exportTypesToStr(exportNum);
				var sensorid = $("div#exportunexport div#exportWindow").attr("sensorid");
				var address = 0;
				var valid = true;
			var isBase = Exports.isBase(exportable);

				if (exportable == "iec60870")
				{
					var option = $("div#exportunexport div#exportables tr td[value='0']").attr("option");
					var address = "";
					if (option == "0")
					{
						var val1 = $("div#exportunexport div#exportWindow table.tableEditList tr#editExportSensor td input#input1").val();
						var val2 = $("div#exportunexport div#exportWindow table.tableEditList tr#editExportSensor td input#input2").val();
						var val3 = $("div#exportunexport div#exportWindow table.tableEditList tr#editExportSensor td input#input3").val();
						var alertHtml = "";
						if (!isFinite(val1) || parseInt(val1) > 255)
						{
							alertHtml += language.enter_number_smallerthan_8bit;
							$("div#exportunexport div#exportWindow table.tableEditList tr#editExportSensor td input#input1").addClass("alert");
							valid = false;
						}
						if (!isFinite(val2) || parseInt(val2) > 255)
						{
							alertHtml += "<br />" + language.enter_number_smallerthan_8bit;
							$("div#exportunexport div#exportWindow table.tableEditList tr#editExportSensor td input#input2").addClass("alert");
							valid = false;
						}
						if (!isFinite(val3) || parseInt(val3) > 255)
						{
							alertHtml += "<br />" + language.enter_number_smallerthan_8bit;
							$("div#exportunexport div#exportWindow table.tableEditList tr#editExportSensor td input#input3").addClass("alert");
							valid = false;
						}
						if (!valid)
						{	
							$("div#exportWindow table.tableEditHead td#alert").html(alertHtml);
							return;
						}
						else
						{
							address =(parseInt(val1) << 16) + (parseInt(val2) << 8) + parseInt(val3);	
						}
					}
					else if (option == "1")
					{
						var val1 = $("div#exportunexport div#exportWindow table.tableEditList tr#editExportSensor td input#input1").val();
						var val2 = $("div#exportunexport div#exportWindow table.tableEditList tr#editExportSensor td input#input2").val();
						var alertHtml = "";
						if (!isFinite(val1) || parseInt(val1) > 65535)
						{
							alertHtml += language.enter_number_smallerthan_16bit;
							$("div#exportunexport div#exportWindow table.tableEditList tr#editExportSensor td input#input1").addClass("alert");
							valid = false;
						}
						if (!isFinite(val2) || parseInt(val2) > 255)
						{
							alertHtml += "<br />" + language.enter_number_smallerthan_8bit;
							$("div#exportunexport div#exportWindow table.tableEditList tr#editExportSensor td input#input2").addClass("alert");
							valid = false;
						}
						if (!valid)
						{	
							$("div#exportWindow table.tableEditHead td#alert").html(alertHtml);
							return;
						}
						else
						{
							address =(parseInt(val1) << 8) + parseInt(val2);	
						}
						
					}
					else if (option == "2")
					{
						var val1 = $("div#exportunexport div#exportWindow table.tableEditList tr#editExportSensor td input#input1").val();
						var val2 = $("div#exportunexport div#exportWindow table.tableEditList tr#editExportSensor td input#input2").val();
						var alertHtml = "";
						if (!isFinite(val1) || parseInt(val1) > 255)
						{
							alertHtml += language.enter_number_smallerthan_8bit;
							$("div#exportunexport div#exportWindow table.tableEditList tr#editExportSensor td input#input1").addClass("alert");
							valid = false;
						}
						if (!isFinite(val2) || parseInt(val2) > 65535)
						{
							alertHtml += "<br />" + language.enter_number_smallerthan_16bit;
							$("div#exportunexport div#exportWindow table.tableEditList tr#editExportSensor td input#input2").addClass("alert");
							valid = false;
						}
						if (!valid)
						{	
							$("div#exportWindow table.tableEditHead td#alert").html(alertHtml);
							return;
						}
						else
						{
							address =(parseInt(val1) << 16) + parseInt(val2);	
						}
					}
					Exports.updateExportSensor("IEC60870", sensorid,address.toString());
				}
				else if (exportable == "snmp")
				{
					var address = $("div#exportunexport div#exportWindow table.tableEditList tr#editExportSensor td input[type='text']").val();
					var option = $("div#exportunexport div#exportables tr td[value='0']").attr("option");
					Exports.updateExportSensor("SNMP", sensorid,address);
				}
				else if(isBase)
				{
					var address = $("div#exportunexport div#exportWindow table.tableEditList tr#editExportSensor td input[type='text']").val();
					var option = $("div#exportunexport div#exportables tr td[value='0']").attr("option");
				Exports.updateExportSensor(exportable.toUpperCase(), sensorid,address);
				}
				else if (exportable == "modbustcp")
				{
					var address = $("div#exportunexport div#exportWindow table.tableEditList tr#editExportSensor td input[type='text']").val();
					var option = $("div#exportunexport div#exportables tr td[value='0']").attr("option");
					Exports.updateExportSensor("MODBUSTCP", sensorid,address);
				}
			}
		});
		
		$("div#devices").on( "click", "div#exportunexport div#exportWindow input#buttonCancel", function() {
			$("div#overlay").css("display", "none");
			$("div#exportWindow table.tableEditHead td#alert").html("");
			$("div#exportunexport div#exportWindow table.tableEditList tr#editExportSensor td input").removeClass("alert");
			$("div#devices div#exportunexport div#exportWindow").css("display", "none");
		});
		
		
		$("div#devices").on("click", "div#editAlert input#buttonOk", function() {
			var opt = $("div#exportunexport div#exportWindow table.tableEditList tr#exportIec60870Format select").prop("selectedIndex");
			Exports.updateIec60870ExportFormat(opt);
		});
		
		$("div#devices").on("click", "div#editAlert input#buttonClose", function() {
			$("div#overlay").css("display", "none");
			$("div#exportWindow table.tableEditHead td#alert").html("");
			$("div#exportunexport div#exportWindow table.tableEditList tr#editExportSensor td input").removeClass("alert");
			$("div#devices div#exportunexport div#exportWindow").css("display", "none");
			$("div#editAlert").css("display","none");
			
		});
		
	$("div#devices").on("mouseover", "div#exportunexport table tr td", function(){
  		var title = $(this).attr("data-title");
		if(title != undefined ) {
			var t = document.createElement("b");
			t.setAttribute("id","tooltip");
			t.innerHTML = title;
			$(this).append(t);
			$(this).addClass("tooltip");
		}
	});
	
	$("div#devices").on("mouseout", "div#exportunexport table tr td.tooltip", function(){
		$(this).removeClass("tooltip");
		$("#tooltip").remove();
		$("div#settings div#exportunexport").css("overflow", "hidden");
		$("div#settings div#exportunexport").css("overflow", "auto");
	});
	
	
	$("div#devices").on( "click", "div#exportunexport div#exportables tr td input#export_modbusregister", function() {
		
	    Main.loading();
	    Exports.createModbusRegisterPDF ();
	});

	
});
		

Exports.createModbusRegisterPDF = function()
{
    var exportData = $.getJSON("http://" + hostName + "/cgi-bin/sensorexport.cgi?getExportsToMODBUSTCP");  
    exportData.success(function() {

	var exportSensors = JSON.parse(exportData.responseText);


    var strToWrite = "\n\n\n";
    strToWrite +=  "Sensplorer Modbus Register Table\n";
    var strCoils = "Coils\n";
    var strDiscereteInputs = "DiscereteInputs\n";
    var strHoldingRegisters = "HoldingRegisters\n";
    var strDiscereteRegisters = "DiscereteRegisters\n";
    for(var i=0; i < exportSensors.length; ++i)
    {
    	if(exportSensors[i].exported == 1 && exportSensors[i].data_type == 0)
    	{
    		strCoils += exportSensors[i].objectid + " " + exportSensors[i].register_addr + " " + exportSensors[i].lenght_offset + "\n";
    	}
    	else if(exportSensors[i].exported == 1 && exportSensors[i].data_type == 1)
    	{
    		strDiscereteInputs += exportSensors[i].objectid + " " + exportSensors[i].register_addr + " " + exportSensors[i].lenght_offset  + "\n";
    	}
    	else if(exportSensors[i].exported == 1 && exportSensors[i].data_type == 2)
    	{
    		strHoldingRegisters += exportSensors[i].objectid + " " + exportSensors[i].register_addr + " " + exportSensors[i].lenght_offset  + "\n";
    	}
    	else if(exportSensors[i].exported == 1 && exportSensors[i].data_type == 3)
    	{
    		strDiscereteRegisters += exportSensors[i].objectid + " " + exportSensors[i].register_addr + " " + exportSensors[i].lenght_offset  + "\n";
    	}
    }

    strToWrite += strCoils + strDiscereteInputs + strHoldingRegisters + strDiscereteRegisters



	var columns1 = ["Name", "Objectid", "Register Adress", "Data-Type"];
	var columns2 = ["Name", "Objectid", "Register Adress", "Data-Type"];
	var rows0 = [];
	var rows1 = [];
	var rows2 = [];
	var rows3 = [];

    for(var i=0; i < exportSensors.length; ++i)
	{
		var parsed = exportSensors[i];
		var arr = [];

		if(parsed["exported"] == "1" )
		{
			var nm = parsed["name"];
			var ob = parsed["objectid"];
			var ra = parsed["register_addr"]; 
	  		arr.push(nm);
	  		arr.push(ob);
	  		arr.push(ra);

			if(parsed["data_type"] == "0" )
	  			arr.push("1-bit");
	        else if(parsed["data_type"] == "1" )
	  			arr.push("1-bit");
	        else if(parsed["data_type"] == "2" )
	  			arr.push("32-bit");
	        else if(parsed["data_type"] == "3" )
	  			arr.push("32-bit");

			if(parsed["data_type"] == "0" )
	        	rows0.push(arr);
	        else if(parsed["data_type"] == "1" )
	        	rows1.push(arr);
	        else if(parsed["data_type"] == "2" )
	        	rows2.push(arr);
	        else if(parsed["data_type"] == "3" )
	        	rows3.push(arr);
		}


    }
	var doc = new jsPDF('p', 'pt');
    doc.text("\nCoils FC:read:0x01 single write:0x05 multiple write:0x0F\n", 30, 50);
	doc.autoTable(columns1, rows0, {
            startY: 80,
            pageBreak: 'avoid',
        });
    doc.text("DiscereteInputs FC:read:0x02\n", 30, doc.autoTableEndPosY() + 30);
	doc.autoTable(columns1, rows1, {
            startY: doc.autoTableEndPosY() + 50,
            pageBreak: 'avoid',
        });
    doc.text("HoldingRegisters FC:read:0x03 single write:0x06 multiple  write:0x10\n", 30, doc.autoTableEndPosY() + 30);
	doc.autoTable(columns2, rows2, {
            startY: doc.autoTableEndPosY() + 50,
            pageBreak: 'avoid',
        });
    doc.text("DiscereteRegisters FC:read:0x04\n", 30, doc.autoTableEndPosY() + 30);
	doc.autoTable(columns2, rows3, {
            startY: doc.autoTableEndPosY() + 50,
            pageBreak: 'avoid',
        });

		doc.save('table.pdf');
		doc.output('dataurlnewwindow');
		Main.unloading();

	});
	exportData.error(function(jqXHR, textStatus, errorThrown){
		Main.unloading();
		Main.alert(language.Settings_exports_modbustcp_getRegister_Error + ":" + jqXHR.responseText);
	});
};

		