var Devices = {
	onLoad: null,
	close: [],
	tabs: [
		{
			label : "Devices_devices",
			status: false,
			func : "getConnectors();"
		},
		{
			label : "Devices_sensors",
			status: true,
			func : "callEdits();"
		},
		{
			label : "Devices_export",
			status: false,
			func : "callExportUnexport();"
		}
	]
};

Devices.html =  '<ul class="tabs"></ul>'
				+ '<div class="container">'	
				+'</div>';

Devices.onLoad = function ()
{
	Main.loading();
	$("div#devices").css("display","block");
	$("div#devices").html(Devices.html);




	var html = 	  '<div id="alertDelete" style="width: 400px;" class="panelDiv">'
				    + '<span class="text"></span>'
				    + '<div class="buttons">'
				        + ' <input type="submit" id="buttonRemove" name="buttonRemove" class="submit">'
				        + ' <input type="submit" id="buttonClose" name="buttonClose" class="submit">'
				    + '</div>'
				+ '</div>'
				+ '<div class="form">'
				+ '</div>';

	$("div#devices div.container").html(html);

	Devices.createTabs();
	Devices.tabOpen();
	//Devices.getConnectors();
};

Devices.callEdits = function()
{
	Settings.callEdits();
};

Devices.createTabs = function()
{
	for(var i = 0; i < Devices.tabs.length; i++) {
		var li = document.createElement("li");
		if(Devices.tabs[i].status) {
			li.setAttribute("class","passive active");	
		} else {
			li.setAttribute("class","passive");			
		}

		li.setAttribute("data-func", Devices.tabs[i].func);	
		li.innerHTML = eval("language." + Devices.tabs[i].label);
		li.setAttribute("id", Devices.tabs[i].label);

		$("div#devices ul.tabs").append(li);
	}
};

Devices.tabOpen = function()
{
	for(var i = 0; i < Devices.tabs.length; i++) {
		if(Devices.tabs[i].status) {
			eval("Devices." + Devices.tabs[i].func);
			Devices.currentTab = Devices.tabs[i].type;
		}
	}
};

Devices.close = function ()
{
	$("div#devices").html("");
	$("div#devices").css("display","none");
	Main.loading();
};

$(function(){
	$("div#devices").on( "click", "ul.tabs li", function() {
		Main.loading();
		var func = $(this).data("func");
		$("div#devices ul.tabs li.active").removeClass("active");
		$(this).addClass("active");
		eval("Devices." + func);
	});
});