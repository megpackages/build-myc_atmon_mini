#!/usr/bin/python

import os
import subprocess
import sys

queryString = os.getenv("QUERY_STRING");
if queryString != None:
        strTemp = queryString.replace("?", "&")
        os.putenv("QUERY_STRING", "getDeviceData&" + strTemp.replace("_","/"))
else:
        os.putenv("QUERY_STRING", "getDeviceData")

p = subprocess.Popen(["/meg/www/cgi-bin/megweb.cgi"], stdout=subprocess.PIPE )
communicate = p.communicate()
if communicate[0] != None:
        sys.stdout.write(communicate[0]) # stdout
if communicate[1] != None:
        sys.stdout.write(communicate[1]) # stderr
sys.exit(0)#(p.returncode)