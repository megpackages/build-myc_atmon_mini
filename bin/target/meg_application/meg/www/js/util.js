
var Util = {};
Util.feedZero = function(val, len, type)
{
	var lenn = val.length;
	if (lenn >= len)
		return val;

	var feed = "";
	for (var i = 0; i < len - lenn; i++)
		feed = feed + "0";

	switch(type)
	{
	case "right":
		return val + feed;
		break;
	case "left":
		return feed + val;
		break;
	default:
		return val;
		break;
	}
};

Util.httpReq_S = function(req)
{
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/" + req.url;
	$.ajax({
	    url : url,
	    type: req.type,
	    data : req.data,
        contentType : req.contentType,
	    success: function()
	    {
	    	Main.unloading();
	    	if (req.success != undefined)
				Main.alert(req.success);
			if (req.callback != undefined)
				eval(req.callback + "();");
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
			Main.alert(language.server_error);
			Main.unloading();
	    }
	});
};

Util.httpReq_R = function(req)
{
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/" + req.url;
	$.ajax({
	    url : url,
	    type: req.type,
        dataType: req.dataType,
	    success: function(Data, textStatus, jqXHR)
	    {
	    	Main.unloading();
	    	if (req.success != undefined)
				Main.alert(req.success);
			if (req.callback != undefined)
				eval(req.callback + "(Data);");

		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
			Main.alert(language.server_error);
			Main.unloading();
	    }
	});
};


Util.httpReq_SR = function(req)
{
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/" + req.url;
	$.ajax({
	    url : url,
	    type: req.type,
	    data: req.data,
        dataType: req.dataType,
        contentType: req.contentType,
	    success: function(Data, textStatus, jqXHR)
	    {
	    	Main.unloading();
	    	if (req.success != undefined)
				Main.alert(req.success);
			if (req.callback != undefined)
				eval(req.callback + "(Data);");
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
			Main.alert(language.server_error);
			Main.unloading();
	    }
	});
};