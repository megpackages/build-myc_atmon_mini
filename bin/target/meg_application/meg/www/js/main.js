var Main = {
	onLoad: null,
	pages: [{
	            "type": "Monitoring",
	            "status": true
	        },
	    	{
	            "type": "Reports",
	            "status": false
	        },
	    	{
	            "type": "Devices",
	            "status": false
	        },
	    	{
	            "type": "Rules",
	            "status": false
	        },
	    	{
	            "type": "Settings",
	            "status": false
	        },
		    {
				"type": "Formula",
				"status":false
			},
		    {
				"type": "About",
				"status":false
	        }
/*			,{
				"type" : "Logs",
				"status": false,
			}*/
	        ],
	languages: [
		{
			name: "Türkçe",
			value: "tr"
		},
		{
			name: "English",
			value: "en"
		}
	],
	getTableRequest: null,
	languageControl: null,
	langLoad: null,
	fullItems: [],
	currentPage: null,
	user: [],
	refreshTimer: 30000,
};

var language = {};
var hostName =  location.host;
var jsonString = null;
var jsonList = new Array;
var deviceList = null;
var getData = null;
var getSensorData= null;
var refreshType = null;

Main.onLoad = function (first) {
	//window['console']['log'] = function() {};
	if (first != undefined)
	{
		getDeviceName = $.getJSON("http://" + hostName + "/cgi-bin/systeminfo.cgi?getDeviceName");	 
		getDeviceName.success(function() {
			var json = jQuery.parseJSON(getDeviceName.responseText);
			var devicename = json.devicename;
			$("head title").html(devicename);
		});
		getDeviceName.error(function(jqXHR, textStatus, errorThrown){
		});		
	}
	if(Login.status()) {
		Main.languageControl();
	}
};

Main.close = function () {
	deleteAllCookies();
	location.reload();
};

var errorTimeout = 0;
Main.getRequest = function (jsonString, page) {
	getData = $.getJSON("http://" + hostName + "/cgi-bin/megweb.cgi?"+ jsonString + "&groupname=" + Main.user.groupname);	 
	//Main.loading();
	getData.success(function() {
		errorTimeout = 0;
		Main.fullItems = jQuery.parseJSON(getData.responseText);
		var devices = jQuery.parseJSON(getData.responseText);
		var alarms = [];
		var warnings = [];
		var losts = [];
		localStorage.setItem("isWarning", false);
		localStorage.setItem("isAlert", false);

		try{
			$("#alarmm")[0].pause();
			$("#uyari")[0].pause();
			$("#kayip")[0].pause();
		}catch(e){

		}
		for(var i=0;i<devices.length;i++){
			if(devices[i].status == 4){
				alarms.push(i);
			}
			if(devices[i].status == 2){
				warnings.push(i);
			}
			if(devices[i].status == 16){
				losts.push(i);
			}
		}
		if(alarms.length != 0){

			$("#alarmm")[0].volume = localStorage.getItem("volume");
			$("#alarmm")[0].loop = true;
			$("#alarmm")[0].play();
			localStorage.setItem("isAlert", true);
		}
		if(warnings.length != 0){
			localStorage.setItem("isWarning", true);
		}else{
			localStorage.setItem("isWarning", false);
		}
		if(losts.length !=0){
			localStorage.setItem("isLost", true);
		}else{
			localStorage.setItem("isLost", false);
		}
		if(alarms.length == 0){
			localStorage.setItem("isAlert", false);
		}
		if(alarms.length == 0 && warnings.length != 0 && losts.length == 0){

			var warningInterval = setInterval(function(){
				$("#uyari")[0].volume = localStorage.getItem("volume");
				$("#uyari")[0].loop = false;
				if(localStorage.getItem("isAlert") == "true" || localStorage.getItem("isLost") == "true")
					clearInterval(warningInterval);
				if(localStorage.getItem("isAlert") == "false" && localStorage.getItem("isLost") == "false")
					$("#uyari")[0].play();
			},5000);
		}
		if(alarms.length == 0 && losts.length !=0){
			var lostInterval = setInterval(function(){
				$("#kayip")[0].volume = localStorage.getItem("volume");
				$("#kayip")[0].loop = false;
				if(localStorage.getItem("isAlert") == "true" || localStorage.getItem("isLost") == "false")
					clearInterval(lostInterval);
				if(localStorage.getItem("isAlert") == "false" && localStorage.getItem("isLost") == "true")
					$("#kayip")[0].play();
			},3000);
		}
		if (jsonString == "getGroupSensorsData")
		{
			if (Main.fullItems["return"] != undefined)
			{
				Main.unloading();
				return;
			}
		}
		Main.createDeviceList(page);
	});
	getData.error(function(jqXHR, textStatus, errorThrown){
		errorTimeout++;
		if (errorTimeout == 10)
		{
			errorTimeout = 0;
	    	Main.alert(language.cannotgetsensordata);
		}
		Main.unloading();
	});
};

Main.refreshTypeControl = function (page) {
	if(page == undefined) {
		page = 1;
	}

	switch(refreshType) {
			case "total":
				jsonList = Main.searchItems();			
				var items = jsonList.length;
				var pageNum = parseInt(items / Monitoring.cachePG());
				
				if(items != Monitoring.cachePG() * pageNum) {
					pageNum++;
				}

				if(page > pageNum) {
					page = pageNum;
				}

				var finishElementCount = Monitoring.cachePG() * page;
				var startElementCount = finishElementCount - Monitoring.cachePG();

				if(finishElementCount > jsonList.length) {
					finishElementCount = jsonList.length;
				}

				Monitoring.clearElement();

				for(var i=startElementCount; i < finishElementCount; i++) {
					Monitoring.createTable(jsonList[i]);
				}

				Monitoring.createPaging(jsonList.length, page);
				Monitoring.createAlertPanel();
				$("div#alertPanel div.balloon").removeClass("active");
				$("div#alertPanel div#total").addClass("active");
			break;
			case "search":
				jsonList = Main.searchItems();
				var statusList = new Array();

				for(var i = 0; i < jsonList.length; i++) {
					for(var k = 0; k < Monitoring.searchArray.length; k++) {
						if(Monitoring.searchArray[k].sensorid == jsonList[i].sensorid) {
							//Monitoring.searchArray[k] = jsonList[i];
							statusList[k] = i;
						}
					}
				}

				statusList = statusList.sort(function(a,b){return a-b});

				for(var i = 0; i < statusList.length; i++) {
					for(var k = 0; k < jsonList.length; k++) {
						if(statusList[i] == k) {
							Monitoring.searchArray[i] = jsonList[k];
						}
					}
				}

				var items = Monitoring.searchArray.length;
				var pageNum = parseInt(items / Monitoring.cachePG());

				if(items != Monitoring.cachePG() * pageNum) {
					pageNum++;
				}

				if(page > pageNum) {
					page = pageNum;
				}

				Monitoring.itemsCall(page);
				Monitoring.createAlertPanel();
				$("div#alertPanel div.balloon").removeClass("active");
			break;
			case "normal":
				jsonList = Main.searchItems();	
				Monitoring.searchArray = new Array();
				var count = 0;
				for(var i = 0; i < jsonList.length; i++) {
					if(jsonList[i].status == 1) {
						Monitoring.searchArray[count] = jsonList[i];
						count++;
					}
				}

				var items = Monitoring.searchArray.length;
				var pageNum = parseInt(items / Monitoring.cachePG());

				if(items != Monitoring.cachePG() * pageNum) {
					pageNum++;
				}

				if(page > pageNum) {
					page = pageNum;
				}

				Main.searchRequest(Monitoring.searchArray,page);
				Monitoring.createAlertPanel();
				$("div#alertPanel div.balloon").removeClass("active");
			break;
			case "warning":
				jsonList = Main.searchItems();	
				Monitoring.searchArray = new Array();
				var count = 0;
				for(var i = 0; i < jsonList.length; i++) {
					if(jsonList[i].status == 2) {
						Monitoring.searchArray[count] = jsonList[i];
						count++;
					}
				}

				var items = Monitoring.searchArray.length;
				var pageNum = parseInt(items / Monitoring.cachePG());

				if(items != Monitoring.cachePG() * pageNum) {
					pageNum++;
				}

				if(page > pageNum) {
					page = pageNum;
				}

				Main.searchRequest(Monitoring.searchArray,page);
				Monitoring.createAlertPanel();
				$("div#alertPanel div.balloon").removeClass("active");
				$("div#alertPanel div#warning").addClass("active");
			break;
			case "alarm":
				jsonList = Main.searchItems();	
				Monitoring.searchArray = new Array();
				var count = 0;
				for(var i = 0; i < jsonList.length; i++) {
					if(jsonList[i].status == 4) {
						Monitoring.searchArray[count] = jsonList[i];
						count++;
					}
				}

				var items = Monitoring.searchArray.length;
				var pageNum = parseInt(items / Monitoring.cachePG());

				if(items != Monitoring.cachePG() * pageNum) {
					pageNum++;
				}

				if(page > pageNum) {
					page = pageNum;
				}

				Main.searchRequest(Monitoring.searchArray,page);
				Monitoring.createAlertPanel();
				$("div#alertPanel div.balloon").removeClass("active");
				$("div#alertPanel div#alarm").addClass("active");
			break;
			case "inactive":
				jsonList = Main.searchItems();	
				Monitoring.searchArray = new Array();
				var count = 0;
				for(var i = 0; i < jsonList.length; i++) {
					if(jsonList[i].status == 8) {
						Monitoring.searchArray[count] = jsonList[i];
						count++;
					}
				}

				var items = Monitoring.searchArray.length;
				var pageNum = parseInt(items / Monitoring.cachePG());

				if(items != Monitoring.cachePG() * pageNum) {
					pageNum++;
				}

				if(page > pageNum) {
					page = pageNum;
				}

				Main.searchRequest(Monitoring.searchArray,page);
				Monitoring.createAlertPanel();
				$("div#alertPanel div.balloon").removeClass("active");
			break;
			case "lost":
				jsonList = Main.searchItems();	
				Monitoring.searchArray = new Array();
				var count = 0;
				for(var i = 0; i < jsonList.length; i++) {
					if(jsonList[i].status == 16) {
						Monitoring.searchArray[count] = jsonList[i];
						count++;
					}
				}

				var items = Monitoring.searchArray.length;
				var pageNum = parseInt(items / Monitoring.cachePG());
				
				if(items != Monitoring.cachePG() * pageNum) {
					pageNum++;
				}

				if(page > pageNum) {
					page = pageNum;
				}

				Main.searchRequest(Monitoring.searchArray,page);
				Monitoring.createAlertPanel();
				$("div#alertPanel div.balloon").removeClass("active");
				$("div#alertPanel div#lost").addClass("active");
			break;
			case "output":
				jsonList = Main.searchItems();	
				Monitoring.searchArray = new Array();
				var count = 0;
				for(var i = 0; i < jsonList.length; i++) {
					if(jsonList[i].status == 32) {
						Monitoring.searchArray[count] = jsonList[i];
						count++;
					}
				}

				var items = Monitoring.searchArray.length;
				var pageNum = parseInt(items / Monitoring.cachePG());
				
				if(items != Monitoring.cachePG() * pageNum) {
					pageNum++;
				}

				if(page > pageNum) {
					page = pageNum;
				}

				Main.searchRequest(Monitoring.searchArray,page);
				Monitoring.createAlertPanel();
				$("div#alertPanel div.balloon").removeClass("active");
			break;
			default:
				jsonList = Main.searchItems();	
				var finishElementCount = Monitoring.cachePG() * page;
				var startElementCount = finishElementCount - Monitoring.cachePG();

				if(finishElementCount > jsonList.length) {
					finishElementCount = jsonList.length;
				}

				Monitoring.clearElement();

				for(var i=startElementCount; i < finishElementCount; i++) {
					Monitoring.createTable(jsonList[i]);
				}

				var items = jsonList.length;
				var pageNum = parseInt(items / Monitoring.cachePG());
				if(items != Monitoring.cachePG() * pageNum) {
					pageNum++;
				}
				if(page > pageNum) {
					page = pageNum;
				}

				Monitoring.createPaging(jsonList.length, page);
				Monitoring.createAlertPanel();
				$("div#alertPanel div.balloon").removeClass("active");
				$("div#alertPanel div#total").addClass("active");
			break;
		}
		if (notificationActive && $("div.NotificationArea table#suspendedDevices tr").length != 0)
		{
			$("div#alertPanel div.balloon").removeClass("active");
			$("div#notificationArea div#notifications").addClass("active");
		}
		else
			$("div#notificationArea div#notifications").removeClass("active");
};

Main.searchItems = function() {
	var qDevice = $('#deviceName').val().toLowerCase();
	var qSensor = $('#sensorName').val().toLowerCase();

	var deviceArray = new Array;
	var sensorArray = new Array;
	var totalArray = jsonList;

	if($('#deviceName').val() != language.devicename) {
		var newCount = 0;
		for(var i=0; i < totalArray.length; i++) {
			var tolowercase = totalArray[i].devicename.toLowerCase();
			var search = tolowercase.indexOf(qDevice);
		  	if(search != -1) {
		  		deviceArray[newCount] = totalArray[i];
		  		newCount++;
			}   			
		}

		totalArray = deviceArray;
	}

	if($('#sensorName').val() != language.sensorname) {

		var newCount2 = 0;
		for(var i=0; i < totalArray.length; i++) {
			var tolowercase = totalArray[i].sensorname.toLowerCase();
		  	var search = tolowercase.indexOf(qSensor);
		    if(search != -1) {
		      	sensorArray[newCount2] = totalArray[i];
		      	newCount2++;
		  	}   			
		}

		totalArray = sensorArray;
	}

	return totalArray;
};

Main.createSuspendedDeviceList = function()
{	
	getSuspendedDevices = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?getSuspendedDevices");	 
	//Main.loading();
	getSuspendedDevices.success(function() {
		var SuspendedDeviceList = jQuery.parseJSON(getSuspendedDevices.responseText);

		if (SuspendedDeviceList.length != 0)
		{
			//$("#content div#main div.NotificationArea table tr:not(#header)").html("");
			if ($("#content div#main div.NotificationArea table#suspendedDevices").length == 0)
			{
				/*'<table id="suspendedDevices" cellpadding="0" cellspacing="0" border="0" width="100%">'
				'</table>'*/
				var tableSuspended = document.createElement("table");
				tableSuspended.setAttribute("id", "suspendedDevices");
				tableSuspended.setAttribute("cellpadding", "0");
				tableSuspended.setAttribute("cellspacing", "0");
				tableSuspended.setAttribute("border", "0");
				tableSuspended.setAttribute("width", "100%");
				$("#content div#main div.NotificationArea").append(tableSuspended);
			}
			
			if ($("#content div#main div.NotificationArea table#suspendedDevices tr#header").length == 0)
			{
				var trHeader = document.createElement("tr");
				trHeader.setAttribute("id", "header");
				var tdHeader = document.createElement("td");
				tdHeader.setAttribute("colspan", "5");
				var aHeader = document.createElement("a");
				aHeader.innerHTML = language.there_are_suspended_devices + " (" + Object.keys(SuspendedDeviceList).length + ")";
				var spanHeader = document.createElement("span");
				spanHeader.setAttribute("class", "downArrow");
				spanHeader.setAttribute("id", "arrow");
				tdHeader.appendChild(aHeader);
				tdHeader.appendChild(spanHeader);
				trHeader.appendChild(tdHeader);
				$("#content div#main div.NotificationArea table#suspendedDevices").append(trHeader);
			}
			
			if ($("#content div#main div.NotificationArea table#suspendedDevices tr#activateAll").length == 0)
			{
				var trAllDevice = document.createElement("tr");
				trAllDevice.setAttribute("id", "activateAll");
				
				var tdConnector = document.createElement("td");
				tdConnector.innerHTML = language.connector;
				var tdPort = document.createElement("td");
				tdPort.innerHTML = language.port;
				var tdSuspendedDevice = document.createElement("td");
				tdSuspendedDevice.innerHTML = language.suspended_device;
				var tdLostDevice = document.createElement("td");
				tdLostDevice.innerHTML = language.lost_device;
		
				var tdAllDevice = document.createElement("td");
				tdAllDevice.setAttribute("colspan", "5");
				var inputAllDevice = document.createElement("input");
				inputAllDevice.setAttribute("type", "submit");
				inputAllDevice.setAttribute("value", language.activate_all_suspended_devices);
				tdAllDevice.appendChild(inputAllDevice);
				trAllDevice.appendChild(tdConnector);
				trAllDevice.appendChild(tdPort);
				trAllDevice.appendChild(tdLostDevice);
				trAllDevice.appendChild(tdSuspendedDevice);
				trAllDevice.appendChild(tdAllDevice);
				$("#content div#main div.NotificationArea table#suspendedDevices").append(trAllDevice);
			}
			
			
			var SuspendedDeviceOidList = {};
			for (var i = 0; i < SuspendedDeviceList.length; i++)
			{
				var port = SuspendedDeviceList[i];
				var hwaddr = port.hwaddr;
				var portnum = port.portnum;
				var suspendedDevaddr = port.suspended_devaddr;
				var suspendedDevName = port.suspended_devname;
				var connDescription = port.conn_description;
				var lostDevaddr = port.lost_devaddr;
				var lostDevName = port.lost_devname;
				var portOid = hwaddr + "/" + portnum;
				var suspendedOid = portOid + "/" + suspendedDevaddr;
				var lostOid = portOid + "/" + lostDevaddr;
				SuspendedDeviceOidList[suspendedOid] = "";
				
				if ($("#content div#main div.NotificationArea table#suspendedDevices tr input[objectid='" + suspendedOid + "']").length != 0)
					continue;	
				
				var trDevice = document.createElement("tr");
				var tdConnector = document.createElement("td");
				tdConnector.innerHTML = connDescription + "(" + hwaddr + ")";
				var tdPort = document.createElement("td");
				tdPort.innerHTML = portnum;
				var tdSuspended = document.createElement("td");
				tdSuspended.innerHTML = suspendedDevName + "(" + suspendedOid + ")";
				var tdLost = document.createElement("td");
				if (port.lost_devname == undefined)
				{
					tdLost.innerHTML = language.no_lost_module;
				}
				else
					tdLost.innerHTML = lostDevName + "(" + lostOid + ")";
				
				var tdInput = document.createElement("td");
				var inputDevice = document.createElement("input");
				inputDevice.setAttribute("type", "submit");
				inputDevice.setAttribute("value", language.activate_suspended_device);
				inputDevice.setAttribute("objectid", suspendedOid);
				tdInput.appendChild(inputDevice);
				trDevice.appendChild(tdConnector);
				trDevice.appendChild(tdPort);
				trDevice.appendChild(tdSuspended);
				trDevice.appendChild(tdLost);
				trDevice.appendChild(tdInput);
				if ($("#content div#main div.NotificationArea table#suspendedDevices tr#activateAll").length == 0)
					$("#content div#main div.NotificationArea table#suspendedDevices").append(trDevice);
				else
					$(trDevice).insertAfter("#content div#main div.NotificationArea table#suspendedDevices tr#activateAll");
				
			}
			
			var devices = $("#content div#main div.NotificationArea table#suspendedDevices tr:not(#header)");
			for (var i = 0; i < devices.length; i++)
			{
				var obj = $(devices[i]).children("td:last").children("input:first").attr("objectid");
				if ($(devices[i]).attr("id") != "activateAll" && !(obj in SuspendedDeviceOidList))
					$(devices[i]).remove();
			}
		}
		
		if ($("#content div#notificationArea").children("div").length == 0)
		{
			var divNotif = document.createElement("div");
			divNotif.setAttribute("id", "notifications");
			divNotif.setAttribute("class", "balloon");
			var spanName = document.createElement("span");
			spanName.setAttribute("class", "name");
			var spanCount = document.createElement("span");
			spanCount.setAttribute("class", "count");
			divNotif.appendChild(spanName);
			divNotif.appendChild(spanCount);
			$("#content div#notificationArea").append(divNotif);
		}
		/*
				<div id="notifications" class="balloon">
					<span class="name"></span>
					<span class="count"></span>
				</div>*/
		$("div#notificationArea div#notifications span.name").html(language.notifications);
		$("div#notificationArea div#notifications span.count").html(Object.keys(SuspendedDeviceList).length.toString());
		if (Object.keys(SuspendedDeviceList).length == 0)
			$("div#notificationArea div#notifications").addClass("zerocount");
		else
			$("div#notificationArea div#notifications").removeClass("zerocount");
		
		if (notificationActive && $("div.NotificationArea table#suspendedDevices tr").length != 0) 
		{
			$("div#monitoring div.tableContainer").css("display", "none");
			$("div#monitoring div#pager").css("display", "none");
			$("div#monitoring div.NotificationArea").css("display","block");
			if ($("div.NotificationArea table#suspendedDevices tr:not(#header)").length != 0)
			{
				$("div#monitoring div.NotificationArea table#header tr a").html(language.notification_pano);
				$("#content div#main div.NotificationArea div#noNotification").css("display", "none");
			}
			$("div.NotificationArea table#suspendedDevices tr#header").css("display", "table-row");
			if ($("div.NotificationArea table#suspendedDevices tr#header span").hasClass("upArrow"))
			{
				$("div.NotificationArea table#suspendedDevices tr:not(#header)").css("display", "table-row");
			}
			else
			{
				$("div.NotificationArea table#suspendedDevices tr:not(#header)").css("display", "none");
			}
		}
	});
	getSuspendedDevices.error(function(jqXHR, textStatus, errorThrown){
	    //Main.alert(language.cannotgetsensordata);
	});
	

};



Main.createDeviceList = function(page)
{
	getDevices = $.getJSON("http://" + hostName + "/cgi-bin/megweb.cgi?getGroupDevices&groupname=" + Main.user.groupname);	 
	getDevices.success(function() {
		Main.groupDeviceList = jQuery.parseJSON(getDevices.responseText);
		Main.deviceFilterCreate(page);
		Main.unloading();
	});
	getDevices.error(function(jqXHR, textStatus, errorThrown){
		Main.deviceFilterCreate(page);
		Main.unloading();
	});
};

Main.deviceFilterCreate = function(page)
{
	if (!(parseInt(page)%1 === 0))
		page = 1;
		
	if (deviceList == null) // f5 ya da ilk login
	{
		deviceList = new Array;
		var oldDeviceList = JSON.parse(getCookie("deviceList"));
		if (oldDeviceList != null) // cookie oluşturulduysa al
			deviceList = oldDeviceList;
	}
	
	var groupDeviceList = {};
	if (Main.groupDeviceList != undefined)
		for (var i = 0; i < Main.groupDeviceList.length; i++)
		{
			groupDeviceList[Main.groupDeviceList[i].objectid] = true;
		}
		
	for (var i = 0; i < Main.fullItems.length; i++) {
		var j;
		for (j = 0; j < deviceList.length; j++) {
			if (deviceList[j].objectid == Main.fullItems[i].deviceoid)
				break;
		}
		if (j == deviceList.length)
		{
			deviceList[j] = new Object();
			deviceList[j].objectid = Main.fullItems[i].deviceoid;
			deviceList[j].name = Main.fullItems[i].devicename;
			deviceList[j].status = Main.fullItems[i].devicestatus;
			deviceList[j].checked = true;
			deviceList[j].checkedBuf = true;
		}
	}
	
	var filterDeviceList = {};
	for (var i = 0; i < deviceList.length; i++)
	{
		filterDeviceList[deviceList[i].objectid] = true;
	}
	
	if (Main.groupDeviceList != undefined)
	{
		for (var objectid in filterDeviceList)
		{
			if (groupDeviceList[objectid] == undefined)
			{
				for (var i = 0; i < deviceList.length; i++)
				{
					if (deviceList[i].objectid == objectid)
					{
						delete deviceList[i];
						deviceList.splice(i, 1);
						break;
					}
				}
			}
		}
	}
	setCookie("deviceList",JSON.stringify(deviceList),365);
	
	jsonList = Main.filterControl();
	Main.refreshTypeControl(page);
};

Main.filterControl = function() {
	var counter = 0;
	var filterArray = new Array();


	for(var i=0; i < Main.fullItems.length; i++) {
		for(var k = 0; k < deviceList.length; k++) {
				if(Main.fullItems[i].deviceoid == deviceList[k].objectid) {
					if(deviceList[k].checked) {
						filterArray[counter] = Main.fullItems[i];
						counter++;
					}
				}
		}
	}

	return filterArray;
};

Main.getDeviceList = function(jsonString, page) {
	//Main.loading();
	Main.createSuspendedDeviceList();
	Main.getRequest(jsonString, page);
};

Main.getOutputSensor = function(url) {
	Main.loading();
	var data = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?"+ url);	 
	data.success(function() {
		Main.unloading();
		var items = jQuery.parseJSON(data.responseText);
		Monitoring.createOutputTable(items);
	});	
	data.error(function(){
	    	Main.alert(language.server_error + ":" + jqXHR.responseText);	
		Main.unloading();
	});
};

Main.getUserSettings = function() {
	Main.loading();
	var getUserData = $.getJSON("http://" + hostName + "/cgi-bin/userandgroups.cgi?getUser&username=" + Main.user.name);	 
	getUserData.success(function() {
		Main.user = jQuery.parseJSON(getUserData.responseText);
		setCookie("Main.user",JSON.stringify(Main.user),parseInt(Main.user.cookie_timeout));
		Main.userSettingsTable();
		Main.unloading();
	});	
	getUserData.error(function(jqXHR, textStatus, errorThrown){
	    	Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Main.updateUser = function(usr, jSon) {
	Main.loading();
	json = JSON.stringify(jSon);

	var url = "http://" + hostName + "/cgi-bin/userandgroups.cgi?updateUser&username="+ Main.user.name;
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			var isLangUpdated = (jSon.language != Main.user.language);
			if (!isLangUpdated)
				Main.unloading();
			for (var key in jSon)
			{	
				if (key == undefined)
				continue;
				Main.user[key] = jSon[key];
			}
			var user = Base64.encode(JSON.stringify(Main.user));
			setCookie("Main.user",user,parseInt(user.cookie_timeout)); 
			if (isLangUpdated)
			{
				location.reload();
			}
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    	Main.alert(language.users_update_fail + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Main.updateOutputSensor = function(url) {
	Main.loading();
	var update = $.post("http://" + hostName + "/cgi-bin/megweb.cgi?"+ url);	 
	update.success(function() {
		Main.alert(language.nowupdatealert);
		Main.unloading();
	});	
	update.error(function(jqXHR, textStatus, errorThrown){
	    	Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Main.getNowUpdateSensor = function(objectid) {
	Main.loading();
	var getNowUpdateData = $.post("http://" + hostName + "/cgi-bin/megweb.cgi?readValue&objectid="+ objectid);	 
	getNowUpdateData.success(function() {
		Main.alert(language.nowupdatealert);
		Main.unloading();
	});	
	getNowUpdateData.error(function(jqXHR, textStatus, errorThrown){
	    	Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Main.getSettingSensor = function(url) {
	Main.loading();
	getSensorData = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?"+ url);	 
	getSensorData.success(function() {
		var sensorUpdateItem = jQuery.parseJSON(getSensorData.responseText);
		Monitoring.createSettingTable(sensorUpdateItem);
		Main.unloading();
	});	
	getSensorData.error(function(jqXHR, textStatus, errorThrown){
	    	Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Main.removeSensor = function(url) {
	Main.loading();
	var currentPage = parseInt($("#monitoring div#pager li.bttn.act").attr("data-page"));
	var removeSensorPost = $.post("http://" + hostName + "/cgi-bin/mdlmngr.cgi?"+ url);
	removeSensorPost.success(function() {
		Main.unloading();
		Monitoring.clearItemsCall(currentPage);
	});	
	removeSensorPost.error(function(jqXHR, textStatus, errorThrown){
	    	Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Main.settingSensor = function(url,item) {
	Main.loading();
	item = JSON.stringify(item);
	var currentPage = parseInt($("#monitoring div#pager li.bttn.act").attr("data-page"));
	var settingSensorPost = $.post( "http://" + hostName + "/cgi-bin/mdlmngr.cgi?"+ url, item);
	settingSensorPost.success(function() {
		Main.unloading();
		Monitoring.clearItemsCall(currentPage);
		$("div#overlay").css("display","none");
		$("div#windowStatus").css("display","none");
		$("div#windowStatus table.tableUpdateList").html("");
	});	
	settingSensorPost.error(function(jqXHR, textStatus, errorThrown){
	    	Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Main.updateSensor = function(url,item) {
	item = JSON.stringify(item);
	var currentPage = parseInt($("#monitoring div#pager li.bttn.act").attr("data-page"));
	var updateSensorPost = $.post( "http://" + hostName + "/cgi-bin/mdlmngr.cgi?"+ url, item);
	updateSensorPost.success(function() {
		Monitoring.clearItemsCall(currentPage);
		$("div#overlay").css("display","none");
		$("div#windowEdit").css("display","none");
		$("#windowEdit table.tableUpdateList").html("");
		Main.unloading();
	});	
	updateSensorPost.error(function(jqXHR, textStatus, errorThrown){	
		$("div#overlay").css("display","none");
		$("div#windowEdit").css("display","none");
		Main.unloading();	
	    	Main.alert(language.server_error + ":" + jqXHR.responseText);

	});
};

Main.searchRequest = function (items, page) {
	var finishElementCount = Monitoring.cachePG() * page;
	var startElementCount = finishElementCount - Monitoring.cachePG();
	if(finishElementCount > items.length) {
		finishElementCount = items.length;
	}
	setTimeout(function() {
		Monitoring.clearElement();
		for(var i=startElementCount; i < finishElementCount; i++) {
			Monitoring.createTable(items[i]);
		}
			Monitoring.createPaging(items.length, page);
	}, 0);
};

Main.languageControl = function() {
	var oHead = document.getElementsByTagName('HEAD').item(0);
	var oScript= document.createElement("script");
	oScript.type = "text/javascript";
	oScript.src="lang/" + Main.user.language + ".js";

	var dScript= document.createElement("script");
	dScript.type = "text/javascript";
	dScript.src="js/ui/i18n/jquery.ui.datepicker-" + Main.user.language + ".min.js";

	oHead.appendChild(oScript);
	oHead.appendChild(dScript);
   	
   	oScript.onload = Main.languageSuccess;
};


Main.languageSuccess = function () {		
	$("div#main").css("display","block");
	$("div#header").css("display","block");

	Main.createMenu();
	Main.pageOpen();
	Main.userPanel();

	var oHead = document.getElementsByTagName('HEAD').item(0);
	var oScript= document.createElement("script");
	oScript.type = "text/javascript";
	oScript.src="components/settings/calender.js";
	oHead.appendChild(oScript);

   	//oScript.onload = loadCalender();
};

Main.userPanel = function() {
	var li1 = document.createElement("li");
	var li2 = document.createElement("li");
	var li3 = document.createElement("li");

	var b = document.createElement("b");
	b.innerHTML = language.hello + ", " + Main.user.name + " ";
	li1.appendChild(b);

	var setLink = document.createElement("a");
	setLink.setAttribute("class","settings");
	setLink.innerHTML = language.user_settings;
	li2.appendChild(setLink);

	var a = document.createElement("a");
	a.setAttribute("class","logout");
	a.innerHTML = language.logout;
	li3.appendChild(a);

	$("div#header div#userPanel ul").append(li1);
	$("div#header div#userPanel ul").append(li2);
	$("div#header div#userPanel ul").append(li3);

};

Main.pageOpen = function() {
	for(var i = 0; i < Main.pages.length; i++) {
		if(Main.pages[i].status) {
			eval(Main.pages[i].type + ".onLoad()");
			Main.currentPage = Main.pages[i].type;
		}
	}
};

Main.pagesClose = function() {
	for(var i = 0; i < Main.pages.length; i++) {
		if(Main.pages[i].status) {
			Main.pages[i].status = false;
			eval(Main.pages[i].type + ".close()");
		}
	}
};

Main.alert = function(msg) {	
	$("div#overlay").css("display","block");
	$("div#alertPopup").css("display","block");
	$("div#alertPopup div.text").html(msg);
	$("div#alertPopup div.button").html(language.close);
};

Main.loading = function() {
	$("div#loading").css("display","block");	
};

Main.unloading = function() {
	$("div#loading").css("display","none");
};

Main.createMenu = function() {
	var ul = document.createElement("ul");
	var control = true;
	for(var i = 0; i < Main.pages.length; i++) {

		if(Main.pages[i].type == "Settings") {
			switch(Main.user.grouptype) {
				case "0":
				case 0:
					control = true;
				break;
				case "1":
				case 1:
					control = false;
				break;
				case "2":
				case 2:
					control = false;
				break;
			}
		}


		if(Main.pages[i].type == "Rules") {
			switch(Main.user.grouptype) {
				case "0":
				case 0:
					control = true;
				break;
				case "1":
				case 1:
					control = true;
				break;
				case "2":
				case 2:
					control = false;
				break;
			}
		}


		if(Main.pages[i].type == "Devices") {
			switch(Main.user.grouptype) {
				case "0":
				case 0:
					control = true;
				break;
				case "1":
				case 1:
					control = false;
				break;
				case "2":
				case 2:
					control = false;
				break;
			}
		}
		if(Main.pages[i].type == "About") {
			switch(Main.user.grouptype) {
				case "0":
				case 0:
					control = true;
					break;
				case "1":
				case 1:
					control = true;
					break;
				case "2":
				case 2:
					control = false;
					break;
			}
		}

		if(Main.pages[i].type == "Formula") {
			switch(Main.user.grouptype) {
				case "0":
				case 0:
					control = true;
					break;
				case "1":
				case 1:
					control = true;
					break;
				case "2":
				case 2:
					control = false;
					break;
			}
		}

		if(Main.pages[i].type == "Logs") {
			switch(Main.user.grouptype) {
				case "0":
				case 0:
				break;
				case "1":
				case 1:
				break;
				case "2":
				case 2:
					control = true;
				break;
			}
		}

		if(control) {
			var li = document.createElement("li");
			li.setAttribute("id", Main.pages[i].type);
			li.setAttribute("data-index", i);
			if(Main.pages[i].status) li.setAttribute("class","active");

			var div = document.createElement("div");
			div.setAttribute("class", "icon");

			var b = document.createElement("b");

			//b.innerHTML = Main.pages[i].type;
			b.innerHTML = eval("language." + Main.pages[i].type);
			li.appendChild(div);
			li.appendChild(b);

			ul.appendChild(li);
		}
	}

	$("div#menu").append(ul);
};

Main.userFormCheck = function() {
	var password = document.createElement("input");
	password.setAttribute("type","password");
	$("tr#password td.left").html(language.Settings_users_password);
	$("tr#password td.right").append(password);
};

Main.userSettingsTable = function() {
	var password = document.createElement("input");
	password.setAttribute("type","password");
	$("tr#password td.left").html(language.Settings_users_password);
	$("tr#password td.right").append(password);

	var passwordagain = document.createElement("input");
	passwordagain.setAttribute("type","password");
	$("tr#passwordagain td.left").html(language.Settings_users_passwordagain);
	$("tr#passwordagain td.right").append(passwordagain);

	var mail = document.createElement("input");
	mail.setAttribute("type","text");
	$("tr#mail td.left").html(language.Settings_users_mail);
	$("tr#mail td.right").append(mail);

	var mailagain = document.createElement("input");
	mailagain.setAttribute("type","text");
	$("tr#mailagain td.left").html(language.Settings_users_mailagain);
	$("tr#mailagain td.right").append(mailagain);

	var refreshperiod = document.createElement("input");
	refreshperiod.setAttribute("type","text");
	$("tr#refreshperiod td.left").html(language.Settings_refresh_period);
	$("tr#refreshperiod td.right").append(refreshperiod);
	
	
	var cookietimeout = document.createElement("input");
	cookietimeout.setAttribute("type","text");
	$("tr#cookietimeout td.left").html(language.Settings_cookie_timeout);
	$("tr#cookietimeout td.right").append(cookietimeout);


	var phone = document.createElement("input");
	phone.setAttribute("type","text");
	$("tr#phone td.left").html(language.Settings_users_phone);
	$("tr#phone td.right").append(phone);

	var passReminder = document.createElement("input");
	passReminder.setAttribute("type","text");
	$("tr#passreminder td.left").html(language.Settings_users_passreminder);
	$("tr#passreminder td.right").append(passReminder);

	var languageSelect = document.createElement("select");
	$("tr#language td.left").html(language.Settings_users_lang);
	$("tr#language td.right").append(languageSelect);

	for(var i = 0; i < Main.languages.length; i++) {
		var option = document.createElement("option");
		option.setAttribute("id", Main.languages[i].value);
		option.setAttribute("value", Main.languages[i].value);
		option.innerHTML = Main.languages[i].name;
		languageSelect.appendChild(option);
	}

	var delbutton = document.createElement("input");
	delbutton.setAttribute("type","submit");
	delbutton.setAttribute("id","delete");
	delbutton.setAttribute("value", language.Settings_users_delete);

	var update = document.createElement("input");
	update.setAttribute("type","submit");
	update.setAttribute("id","update");
	update.setAttribute("value", language.Settings_users_update);

	$("tr#mail input").val(Main.user.email_addr);
	$("tr#mailagain input").val(Main.user.email_addr);
	$("tr#refreshperiod input").val(Main.user.refresh_period);
	$("tr#cookietimeout input").val(Main.user.cookie_timeout);
	$("tr#phone input").val(Main.user.phone_number);		
	$("tr#passreminder input").val(Main.user.password_reminder);
	$("tr#language option").each(function(){
		var val = $(this).val();
		if(val == Main.user.language){
			$( this ).prop('selected', true);
		} else {
			$( this ).prop('selected', false);
		}
	});
};

Main.userFormValidate = function() {
	
	$("table.userHeader td#alert").html("");
	var alertHtml = $("table.userHeader td#alert").html();
	$("tr#password").removeClass("alert");
	$("tr#passwordagain").removeClass("alert");
	$("tr#mail").removeClass("alert");
	$("tr#mailagain").removeClass("alert");
	$("tr#phone").removeClass("alert");
	$("tr#refreshperiod").removeClass("alert");
	$("tr#cookietimeout").removeClass("alert");
	$("tr#passreminder").removeClass("alert");
	$("tr#language").removeClass("alert");

	var password       = $("tr#password input").val();
	var passwordagain  = $("tr#passwordagain input").val();
	var mail           = $("tr#mail input").val();
	var mailagain      = $("tr#mailagain input").val();
	var phone          = $("tr#phone input").val();
	var refreshperiod  = $("tr#refreshperiod input").val();
	var cookietimeout  = $("tr#cookietimeout input").val();
	var passreminder   = $("tr#passreminder input").val();

	var isvalid = true;
	
	//validation of password
	if(!Validator.validate( { "val" : password, "type" : "userpassword", "ismandatory" : "false", "obj" : $("tr#password"), "objfocus" : $("tr#password input"), "page" : $("table.userHeader td#alert")}))
		isvalid = false;

	if(passwordagain != password)
	{	
		$("tr#password").addClass("alert");
		$("tr#passwordagain").addClass("alert");
		alertHtml = $("table.userHeader td#alert").html() + "<br />" + language.Settings_users_alert_passerror;
		$("table.userHeader td#alert").html(alertHtml);			
		isvalid = false;
	}
	
	//validation of email address
	if(!Validator.validate( { "val" : mail, "type" : "email", "ismandatory" : "false", "obj" : $("tr#mail"), "objfocus" : $("tr#mail input"), "page" : $("table.userHeader td#alert")}))
		isvalid = false;

	if(mail != mailagain)
	{	
		$("li#mail").addClass("alert");
		$("li#mailagain").addClass("alert");
		alertHtml = $("table.userHeader td#alert").html() + "<br />" + language.Settings_users_alert_mailerror;
		$("table.userHeader td#alert").html(alertHtml);			
		isvalid = false;
	}
	
	//validation of phone
	if(!Validator.validate( { "val" : phone, "type" : "phonenumber", "ismandatory" : "false", "obj" : $("tr#phone"), "objfocus" : $("tr#phone input"), "page" : $("table.userHeader td#alert")}))
		isvalid = false;
	
	//validation of refreshperiod
	if(!Validator.validate( { "val" : refreshperiod, "type" : "i", "min" : "30", "ismandatory" : "false", "obj" : $("tr#refreshperiod"), "objfocus" : $("tr#refreshperiod input"), "page" : $("table.userHeader td#alert")}))
		isvalid = false;
	
	//validation of cookietimeout
	if(!Validator.validate( { "val" : cookietimeout, "type" : "i", "min" : "1",  "max" : "365", "ismandatory" : "false", "obj" : $("tr#cookietimeout"), "objfocus" : $("tr#cookietimeout input"), "page" : $("table.userHeader td#alert")}))
		isvalid = false;
	
	//validation of passreminder
	if(!Validator.validate( { "val" : passreminder, "type" : "username", "ismandatory" : "false", "obj" : $("tr#passreminder"), "objfocus" : $("tr#passreminder input"), "page" : $("table.userHeader td#alert")}))
		isvalid = false;

	return isvalid;
};

Main.createUserForm = function () {
	$("div#userSettings div.table").html("");
	var html = '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="userHeader">'
	   + '<tr><td id="name" width="40%"></td>'
	   + '<td id="alert" width="60%"></td>'
	   + '</tr></table>'
	   + '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="user">'
	   +	'<tr id="password"><td class="left"></td><td class="right"></td></tr>'
	   +	'<tr id="passwordagain"><td class="left"></td><td class="right"></td></tr>'
	   +	'<tr id="mail"><td class="left"></td><td class="right"></td></tr>'
	   +	'<tr id="mailagain"><td class="left"></td><td class="right"></td></tr>'
	   +	'<tr id="phone"><td class="left"></td><td class="right"></td></tr>'
	   +	'<tr id="refreshperiod"><td class="left"></td><td class="right"></td></tr>'
	   +	'<tr id="cookietimeout"><td class="left"></td><td class="right"></td></tr>'
	   +	'<tr id="passreminder"><td class="left"></td><td class="right"></td></tr>'
	   +	'<tr id="language"><td class="left"></td><td class="right"></td></tr>'
	+ '</table>';

	$("div#overlay").css("display","block");
	$("div#userSettings").css("display","block");
	$("div#userSettings div.table").html(html);
	$("div#userSettings input#passwordCheck").val(language.update);
	$("div#userSettings table.userHeader td#name").html(language.userupdate);
	$("div#userSettings input#passwordCheck").attr("id", "buttonUpdate");

	$("div#userSettings input#buttonCancel").val(language.cancel);

	Main.getUserSettings();
};

Main.activateDevice = function(objectid, tr)
{
	var objectId = objectid.split("/");
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/mdlmngr.cgi?activateDevice&hwaddr=" + objectId[0] + "&portnum=" + objectId[1] + "&devaddr=" + objectId[2];	

	$.ajax({
	    url : url,
	    type: "POST",
        dataType: 'text',
	    success: function(data, textStatus, jqXHR)
	    {
	    	var jSon = jQuery.parseJSON(data);
	    	Main.unloading();
	    	if (jSon["return"] != "true")
	    	{
	    		Main.alert(language.device_activate_fail + ":" + jSon["return"]);
	    	}
	    	else
	    	{
	    		tr.remove();
	    		var cnt = parseInt($("div#notificationArea div#notifications span.count").html());
	    		$("div#notificationArea div#notifications span.count").html(cnt - 1);
	    		if ($("div.NotificationArea table#suspendedDevices tr:not(#activateAll) input").length == 0)
	    		{
	    			$("div.NotificationArea table#suspendedDevices tr:not(#activateAll)").remove();
  					$("#content div#main div.NotificationArea table#suspendedDevices").remove();
  					$("div#notificationArea div#notifications span.count").html("0");
  					$("div#notificationArea div#notifications").addClass("zerocount");
					$("#content div#main div.NotificationArea div#noNotification").css("display", "block");
					$("#content div#main div.NotificationArea div#noNotification").html(language.thereis_no_notification_to_see);
	    		}
	    	}
	    },
	    error: function(jqXHR, textStatus, errorThrown) {
			Main.unloading();
	    	Main.alert(language.server_error + ":" + jqXHR.responseText);
	    }
	});
};

Main.activateAllDevices = function()
{
  	var device = $("div.NotificationArea table#suspendedDevices tr:not(#activateAll) input:first");
  	if (device.length != 1)
  	{
  		$("div.NotificationArea table#suspendedDevices tr#activateAll").remove();
  		$("#content div#main div.NotificationArea table#suspendedDevices").remove();
		$("div#notificationArea div#notifications span.count").html("0");
		$("div#notificationArea div#notifications").addClass("zerocount");
		$("#content div#main div.NotificationArea div#noNotification").css("display", "block");
		$("#content div#main div.NotificationArea div#noNotification").html(language.thereis_no_notification_to_see);
  		Main.unloading();
  		return;
  	}
  	
	Main.loading();  	
	var objectid = device.attr("objectid");
	var objectId = objectid.split("/");
	var url = "http://" + hostName + "/cgi-bin/mdlmngr.cgi?activateDevice&hwaddr=" + objectId[0] + "&portnum=" + objectId[1] + "&devaddr=" + objectId[2];	
  	
	$.ajax({
	    url : url,
	    type: "POST",
        dataType: 'text',
	    success: function(data, textStatus, jqXHR)
	    {
	    	var jSon = jQuery.parseJSON(data);
	    	Main.unloading();
	    	if (jSon["return"] != "true")
	    	{
	    		Main.alert(language.device_activate_fail + ":" + jSon["return"]);
	    	}
	    	else
	    	{
	    		device.closest("tr").remove();
	    		var cnt = parseInt($("div#notificationArea div#notifications span.count").html());
	    		$("div#notificationArea div#notifications span.count").html(cnt - 1);
	    		Main.activateAllDevices();
	    	}
	    },
	    error: function(jqXHR, textStatus, errorThrown) {
			Main.unloading();
	    	Main.alert(language.server_error + ":" + jqXHR.responseText);
	    }
	});
};


$(function() {
	$.datepicker.setDefaults($.datepicker.regional[Main.user.language]);

	$("#content").on("mouseover", "span", function() {
		var title = $(this).attr("data-title");
		if(title != undefined ) {
			var t = document.createElement("b");
			t.setAttribute("id","tooltip");
			t.innerHTML = title;
			$(this).append(t);
		}
	});

	$("#content").on("mouseout", "span", function() {
		$("#tooltip").remove();
	});

	$("div#alertPopup").on( "click", "div.button", function() {
		$("div#overlay").css("display","none");
		$("div#alertPopup").css("display","none");
		$("div#alertPopup div.text").html("");
		$("div#alertPopup div.button").html("");	
	});

	$("div#menu").on( "click", "li", function() {
		//if(Main.currentPage != $(this).attr("id")) {
			//notificationActive = false;
			var page = $(this).attr("id");
			var index = $(this).data("index");
			$("div#menu li.active").removeClass("active");
			$(this).addClass("active");
			Main.pagesClose();
			Main.pages[index].status = true;
			Main.pageOpen();
		//}
	});

	$("div#header").on( "click", "div#userPanel a.logout", function() {
		delCookie("Main.user");
		delCookie("AuthHeader");
		// window.onbeforeunload= function () {

		// 	return null;
		// } ;
		Main.close();
	});

	$("div#header").on( "click", "div#userPanel a.settings", function() {
		$("div#userSettings div.table").html("");
		var html = '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="userHeader">'
		   + '<tr><td id="name" width="40%"></td>'
		   + '<td id="alert" width="60%"></td>'
		   + '</tr></table>'
		   + '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="user">'
		   +	'<tr id="password"><td class="left" width="40%"></td><td class="right" width="60%"></td></tr>'
		+ '</table>';

		$("div#overlay").css("display","block");
		$("div#userSettings").css("display","block");
		$("div#userSettings div.table").html(html);
		$("div#userSettings input#buttonUpdate").val(language.Settings_users_continue);
		$("div#userSettings table.userHeader td#name").html(language.userupdate);
		$("div#userSettings input#buttonUpdate").attr("id", "passwordCheck");

		$("div#userSettings input#buttonCancel").val(language.cancel);

		Main.userFormCheck();
	});

	$("div#content").on( "click", "div#userSettings input#buttonUpdate", function() {
		if(Main.userFormValidate()) {
			$("div#overlay").css("display","none");
			$("div#userSettings").css("display","none");
			var json = null;
			var user = $("tr#username input").val();
			if($("tr#password input").val() == "" || $("tr#password input").val() == null) {
				json = {
					"email_addr": $("tr#mail input").val(),
					"language": $("tr#language select").val(),
					"password_reminder": $("tr#passreminder input").val(),
					"refresh_period": $("tr#refreshperiod input").val(),
					"cookie_timeout": $("tr#cookietimeout input").val(),
					"phone_number": $("tr#phone input").val()
				};
			} else {
				json = {
					"name": $("tr#username input").val(),
					"email_addr": $("tr#mail input").val(),
					"language": $("tr#language select").val(),
					"password": $("tr#password input").val(),
					"password_reminder": $("tr#passreminder input").val(),
					"refresh_period": $("tr#refreshperiod input").val(),
					"cookie_timeout": $("tr#cookietimeout input").val(),
					"phone_number": $("tr#phone input").val()
				};
			}

			Main.updateUser(user, json);
		}
	});

	$("div#content").on( "click", "div#userSettings input#passwordCheck", function() {
		if($("tr#password input").val() == Main.user.password) {
			Main.createUserForm();
		} else {
			$("table.userHeader td#alert").html("");
			$("tr#password").addClass("alert");
			alertHtml = language.userpassworderror;
			$("table.userHeader td#alert").html(alertHtml);
		}
	});

	$("div#content").on( "click", "div#userSettings input#buttonCancel", function() {
		$("div#overlay").css("display","none");
		$("div#userSettings").css("display","none");
	});
	
	$("div#content").on( "click", "div.NotificationArea table#suspendedDevices tr#header td span#arrow", function(){
		if ($(this).attr("class") == "upArrow")
		{
			 $("div.NotificationArea table#suspendedDevices tr:not(#header)").css("display", "none");
			 $(this).attr("class", "downArrow");
		}
		else
		{
			 $("div.NotificationArea table#suspendedDevices tr:not(#header)").css("display", "table-row");
			 $(this).attr("class", "upArrow");
		}
	});
	
	///////  SUSPENDED DEVICES TRIGGER
	$("div#content").on("click", "div.NotificationArea table#suspendedDevices tr:not(#activateAll) input", function(){
		var objectid = $(this).attr("objectid");
		Main.activateDevice(objectid, $(this).closest("tr"));
	});
	
	$("div#content").on("click", "div.NotificationArea table#suspendedDevices tr#activateAll input", function(){
		Main.activateAllDevices();
	});
});
