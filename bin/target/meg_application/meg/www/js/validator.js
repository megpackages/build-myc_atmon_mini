
var Validator = {};

Validator._string = function(json)
{
	var RegEx = /^[^!@#$%^&*+=\[\]\\\';,\/{}|\":<>?é]*$/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_string;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._integer = function(json)
{
	var RegEx = /^[-]?[0-9][0-9]*$/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
	
	var min = json.min;
	var max = json.max;
	var errormsg = language.Validate_error_integer;
	if (min != "" && max != "" && min != undefined && max != undefined )
		errormsg = errormsg + "(" + min + " - " + max + ")";
	else if (min != "" && min != undefined)
		errormsg = errormsg + "( > " + min + ")";
	else if (max != "" && max != undefined)
		errormsg = errormsg + "( < " + max + ")";
	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + errormsg;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	
	if (parseInt(json.val) < parseInt(min) || parseInt(json.val) > parseInt(max))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + errormsg;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._float = function(json)
{
	var RegEx = /^[-]?(([1-9][0-9]*)|0)([.][0-9]+)?$/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
	
	var min = json.min;
	var max = json.max;
	var errormsg = language.Validate_error_float;
	if (min != "" && max != "" && min != undefined && max != undefined )
		errormsg = errormsg + "(" + min + " - " + max + ")";
	else if (min != "" && min != undefined)
		errormsg = errormsg + "( > " + min + ")";
	else if (max != "" && max != undefined)
		errormsg = errormsg + "( < " + max + ")";
	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + errormsg;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	
	if (parseInt(json.val) < parseInt(min) || parseInt(json.val) > parseInt(max))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + errormsg;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._hex = function(json)
{
	var RegEx = /^[0-9A-F]*$/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_hex;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._ip = function(json)
{
	var RegEx = /^(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))$/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_ip;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._port = function(json)
{
	var RegEx = /^[-]?[0-9][0-9]*$/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
	
	var min = "1";
	var max = "65535";
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_port;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	
	if (parseInt(json.val) < parseInt(min) || parseInt(json.val) > parseInt(max))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_port;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._domain = function(json)
{
	var RegEx = /^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_domain;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._modulename = function(json)
{
	var RegEx = /^[^!@#$%^&*+=\[\]\\\';,\/{}|\":<>?é]*$/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_modulename;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._username = function(json)
{
	var RegEx = /^[^!@#$%^&*()+=\[\]\\\';,.\/{}|\":<>?\s]*$/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_username;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._userpassword = function(json)
{
    var RegEx = /.*/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_userpassword;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._phonenumber = function(json)
{
    var RegEx = /^\d{12}$/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_phonenumber;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._email = function(json)
{
    var RegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_email;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._ldapbasedn = function(json) 		//TODO Regex will be changed according to ldap base dn user constraint
{
    var RegEx = /.*/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
    	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_ldapbasedn;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._ldapuser = function(json) 		//TODO Regex will be changed according to ldap user constraint
{
    var RegEx = /.*/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
    	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_ldapuser;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._ldappassword = function(json) 	//TODO Regex will be changed according to ldap password constraint
{
    var RegEx = /.*/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
    	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_ldappassword;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._snmpcommunity = function(json) 	//TODO Regex will be changed according to snmp community constraint
{
    var RegEx = /.*/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
    	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_snmpcommunity;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._snmpsecurity = function(json) 	//TODO Regex will be changed according to snmp security constraint
{
    var RegEx = /.*/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
    	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_snmpsecurity;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._snmppassword = function(json) 	//TODO Regex will be changed according to snmp password constraint
{
    var RegEx = /.*/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
    	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_snmppassword;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._snmpoid = function(json) 	
{
	var RegEx = /^(([0-9]+)\.)*([0-9]+)$/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
    	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_snmpoid;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._snmpauthkey = function(json) 	//TODO Regex will be changed according to snmp auth key constraint
{
    var RegEx = /.*/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
    	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_snmpauthkey;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._snmpprivkey = function(json) 	//TODO Regex will be changed according to snmp priv key constraint
{
    var RegEx = /.*/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
    	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_snmpprivkey;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._meterserialno = function(json) 	
{
	var RegEx = /^(([0-9]{8})|([A-Z]{3}[0-9]{8}))$/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
    	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_meterserialno;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._meterobis = function(json) 	
{
	var RegEx = /^([0-9]*)\.([0-9]*)\.([0-9]*)$/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
    	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_meterobis;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._regexpattern = function(json) 	//TODO Regex will be changed according to regexpattern constraint
{
    var RegEx = /.*/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
    	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_regexpattern;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._regexreplace = function(json) 	//TODO Regex will be changed according to regexreplace constraint
{
    var RegEx = /.*/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
    	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_regexreplace;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._url = function(json) 			//TODO Regex will be changed according to url constraint
{
    var RegEx = /.*/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
    	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_url;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._ftpusername = function(json)  //TODO Regex will be changed according to ftp username constraint
{
    var RegEx = /.*/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
    	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_ftpusername;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._ftppassword = function(json)  //TODO Regex will be changed according to ftp password constraint
{
    var RegEx = /.*/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
    	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_ftppassword;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._camerausername = function(json)  //TODO Regex will be changed according to camera username constraint
{
    var RegEx = /.*/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
    	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_camerausername;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._camerapassword = function(json) 	//TODO Regex will be changed according to camera password constraint
{
    var RegEx = /.*/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
    	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_camerapassword;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._sensplorerversion = function(json) 
{
	var RegEx = /^([0-9]+)((\.([0-9]+)){2})?$/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
    	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_sensplorerversion;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._smtpuser = function(json) 		//TODO Regex will be changed according to smtp user constraint
{
    var RegEx = /.*/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
    	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_smtpuser;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator._smtppassword = function(json) 	//TODO Regex will be changed according to smtp password constraint
{
    var RegEx = /.*/;
	var errprefix = "";
	if(json.errprefix != undefined)
		errprefix = json.errprefix;
    	
	if(json.val.length == 0)
	{
		if(JSON.parse(json.ismandatory))
		{
		    json.obj.addClass("alert");
		    alertHtml =  errprefix + language.Validate_error_noempty;
			json.page.html(json.page.html() + "<br />" +  alertHtml);
		    json.objfocus.focus();
		    return false;		
		}	
	}
	else if (!RegEx.test(json.val))
	{
	    json.obj.addClass("alert");
	    alertHtml =  errprefix + language.Validate_error_smtppassword;
		json.page.html(json.page.html() + "<br />" +  alertHtml);
	    json.objfocus.focus();
	    return false;		
	}
	return true;
};

Validator.validate = function(json) // json keys : val, type, [min, max,] ismandatory, object, objectfocus, page, [errprefix]
{
	var isvalid = true;
	var type = json.type;
         if (type == "s"){					isvalid = Validator._string(json);}
	else if (type == "i"){					isvalid = Validator._integer(json);}
	else if (type == "f"){					isvalid = Validator._float(json);}
	else if (type == "h"){					isvalid = Validator._hex(json);}
//	else if (type == "date"){}
//	else if (type == "time"){}
	else if (type == "ip"){					isvalid = Validator._ip(json);}
	else if (type == "port"){				isvalid = Validator._port(json);}
	else if (type == "domain"){				isvalid = Validator._domain(json);}
	else if (type == "modulename"){			isvalid = Validator._modulename(json);}
	else if (type == "username"){     		isvalid = Validator._username(json);}
	else if (type == "userpassword"){     	isvalid = Validator._userpassword(json);}
	else if (type == "phonenumber"){     	isvalid = Validator._phonenumber(json);}
	else if (type == "email"){				isvalid = Validator._email(json);}
//	else if (type == "emailcontent"){}
	else if (type == "ldapbasedn"){			isvalid = Validator._ldapbasedn(json);}
	else if (type == "ldapuser"){			isvalid = Validator._ldapuser(json);}
	else if (type == "ldappassword"){     	isvalid = Validator._ldappassword(json);}
	else if (type == "snmpcommunity"){     	isvalid = Validator._snmpcommunity(json);}
	else if (type == "snmpsecurity"){     	isvalid = Validator._snmpsecurity(json);}
	else if (type == "snmppassword"){     	isvalid = Validator._snmppassword(json);}
	else if (type == "snmpoid"){     	    isvalid = Validator._snmpoid(json);}
	else if (type == "snmpauthkey"){     	isvalid = Validator._snmpauthkey(json);}
	else if (type == "snmpprivkey"){     	isvalid = Validator._snmpprivkey(json);}
//	else if (type == "iec61850dataset"){}
//	else if (type == "iec61850itemid"){}
	else if (type == "meterserialno"){     	isvalid = Validator._meterserialno(json);}
	else if (type == "meterobis"){     		isvalid = Validator._meterobis(json);}
	else if (type == "regexpattern"){     	isvalid = Validator._regexpattern(json);}
	else if (type == "regexreplace"){     	isvalid = Validator._regexreplace(json);}
//	else if (type == "gsmusername"){}
//	else if (type == "gsmpassword"){}
//	else if (type == "gsmapn"){}
	else if (type == "url"){				isvalid = Validator._url(json);}
	else if (type == "ftpusername"){		isvalid = Validator._ftpusername(json);}
	else if (type == "ftppassword"){		isvalid = Validator._ftppassword(json);}
	else if (type == "camerausername"){		isvalid = Validator._camerausername(json);}
	else if (type == "camerapassword"){		isvalid = Validator._camerapassword(json);}
//	else if (type == "sensplorerwebsms"){}
	else if (type == "sensplorerversion"){  isvalid = Validator._sensplorerversion(json);}
	else if (type == "smtpuser"){			isvalid = Validator._smtpuser(json);}
	else if (type == "smtppassword"){     	isvalid = Validator._smtppassword(json);}
	return isvalid;
};
