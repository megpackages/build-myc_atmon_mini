#!/bin/sh +x

startProcesses()
{
    execute "service-ctrl -s adc-app"       1
    execute "service-ctrl -s atmon-app"     1
    execute "service-ctrl -s buttonlcdmngr" 1
    execute "service-ctrl -s serial"        1
    execute "service-ctrl -s rs485hub"      1
    execute "service-ctrl -s relay"         1
    execute "service-ctrl -s drycontact"    1
    execute "service-ctrl -s ledmngr"       1
}

execute()
{
    if [ "$#" -ne "2" ];then
        return 1
    fi

    echo "executing command:{ $1 }"

    for i in $(seq 1 $2)
    do
        if [ "$SILENT" = "1" ];then
            $1 1>/dev/null 2>/dev/null
        else
            $1
        fi
        if [ $? -eq 0 ]; then
            return 0
        fi
        sleep 0.1
    done  
    return 1
}

executeOrExit()
{
    if [ "$#" -ne "2" ];then
      return 1
    fi
    execute "$1" "$2"
    if [ $? -ne 0 ];then
        echo "processes can not be started"
    else
        echo "started all process"
    fi
}

executeOrExit startProcesses  1 

exit 0
