#!/bin/sh +x

stopProcesses()
{

    if [ "$(pidof adc-app)" ];then
        execute "service-ctrl -k adc-app"       5
    fi
    if [ "$(pidof atmon-app)" ];then
        execute "service-ctrl -k atmon-app"  	5
    fi
    if [ "$(pidof buttonlcdmngr)" ];then
        execute "service-ctrl -k buttonlcdmngr"	5
    fi
    if [ "$(pidof serial)" ];then
        execute "service-ctrl -k serial"       	5
    fi
    if [ "$(pidof rs485hub)" ];then
        execute "service-ctrl -k rs485hub"      5
    fi
    if [ "$(pidof relay)" ];then
        execute "service-ctrl -k relay"         5
    fi
    if [ "$(pidof drycontact)" ];then
        execute "service-ctrl -k drycontact"    5
    fi
    if [ "$(pidof ledmngr)" ];then
        execute "service-ctrl -k ledmngr"       5
    fi
}

execute()
{

    if [ "$#" -ne "2" ];then
      return 1
    fi

    for i in $(seq 1 $2)
    do
        echo "executing command:{ $1 } try count:{ $i }"
        if [ "$SILENT" = "1" ];then
            $1 1>/dev/null 2>/dev/null
        else
            $1
        fi
        if [ $? -eq 0 ]; then
            return 0
        fi
        sleep 0.1
    done  
    return 1
}

executeOrExit()
{
    if [ "$#" -ne "2" ];then
      return 1
    fi
    execute "$1" "$2"
    if [ $? -ne 0 ];then
      echo "processes can not be stopped"
    else
      echo "stopped all process"
    fi
}

executeOrExit stopProcesses  5 
exit 0
