#!/usr/bin/python
import os, sys, shutil
import imp
import optparse
import tarfile
import ftplib
import time

updatetime = time.strftime("%Y%m%d.%H%M")
updatefile = "/media/mmc/update.txt"
loglevel = 255
islogset = False
if os.path.exists("/meg/lib/python/spxlogmngr.py"):
    sys.path.append("/meg/lib/python/")
    import spxlogmngr
    islogset = True
    
def logger(lvl, log="", cntinue=True):
    global loglevel
    if islogset:
        spxlogmngr.Logger(loglevel, "/media/mmc/megixinstaller.%s.log" % updatetime, True).log(lvl, log, 2, cntinue)
    else:        
        print "%14s  %-8s %s" % (time.strftime("%6b%2d-%2H:%2M"), lvl,  log) 

def getversion(package_name):
    logger("ENTER")
    version = ""
    pos1 = package_name.rfind(".tar")
    pos1last = pos1
    pos0last = pos1
    pos0 = pos1
    
    while pos0last != -1:
        pos0last = package_name[0:pos1last].rfind(".")
        temp = package_name[pos0last+1:pos1last]
        try:
            int(temp)
        except ValueError:
            break
        pos1last = pos0last
    pos0 = pos1last
    version = package_name[pos0+1:pos1]
    
    logger("INFO", "Current version:%s" % version)
    logger("EXIT")
    return version

parser = optparse.OptionParser(usage="megixinstaller.py [OPTIONS]")

def option():    
    parser.add_option("-r", "--remote", 
                   action="store_true",
                   default=False 
                   )  
    parser.add_option("-n","--package-name", 
                   help="package name megix.[xxx.]<version>.tar",
                   )  
    parser.add_option("-d","--package-directory", 
                   help="package directory </dev/shm>",
                   default="/dev/shm" 
                   )  
    parser.add_option("-t","--temp", 
                   default="/dev/shm/temp",  
                   help="temporarily location </dev/shm/temp>" 
                   )
    parser.add_option("-l","--loglevel", 
                   default="255",  
                   help="log level of installation." 
                   )
    group = optparse.OptionGroup(parser, "FTP option",
                        "FTP host address, username, password ")
    
    group.add_option("-f","--ftp-address", 
                   help="FTP address  " 
                   )
    group.add_option("-u","--ftp-username",
                   help="FTP username " 
                   )
    group.add_option("-p","--ftp-password", 
                   help="FTP password " 
                   )
    parser.add_option_group(group)

    (options, args) = parser.parse_args()
    
    global loglevel
    loglevel     = int(options.loglevel)
        
    
    
    if options.remote:
        options.local = False
    
    if options.package_name is None:   #
        parser.error('Package name not given!!!')
    if not (options.package_name.startswith("megix.") and options.package_name.endswith(".tar")):
        parser.error('Not Proper package name "megix.[xxx.]<version>.tar"!!!')

    version = getversion(options.package_name)
    if not version:
        parser.error('Not proper package name with version "megix.[xxx.]<version>.tar"!!!')
        
    parser.add_option("-v", "--version",
                      default = version
                   ) 
    
    if options.remote:
        if not options.ftp_address:
            parser.error('FTP address not given!!!')
            
    if os.path.exists(options.temp):
        if not os.path.isdir(options.temp):
            parser.error('Temporarily location is not a directory!!!')
    else:
        os.mkdir(options.temp)  
    
    (options, args) = parser.parse_args() 
    if options.remote:
        options.local = False   
    return  (options, args)

def extract(pkg_file, destination):
    logger("ENTER")
    logger("INFO", "Package <%s> will be extracted into <%s>" % (pkg_file, destination))
    tar = tarfile.open(pkg_file, 'r:')
#     tar.list(True)
    tar.extractall(destination)
    lst = tar.getnames()
    tar.close()
    
    for i in lst:
        logger("DEBUG", "Extracted file:%s" % i)
    logger("INFO", "Extraction is completed. Number of files: %s" % str(len(lst)))
    
    logger("EXIT")
    return lst

def install(temp, version):   
    logger("ENTER")
    if not os.path.exists(os.path.join(temp, "install.py")):
        raise Exception(("Installation file is missing!!!: <%s>" % "install.py"))
    sys.path.append(temp)
    import install
    install.onPre(temp, version)
    install.onBegin(temp, version)
    install.onEnd()   
  
    logger("EXIT")
    
def rmoldupdatelogs():
    logpath = "/etc/updatelogs"
    filelst = os.listdir(logpath)
    if len(filelst) <= 3:
        return
    
    filelst.sort()
    for i in range(len(filelst)-3):
        os.remove(os.path.join(logpath, filelst[i]))

def main():
    global loglevel
    iRslt=1   
    templocation=""
    logger("INFO", "updating is started")
    try:      
        (options, args) = option()
        templocation = options.temp
        
        if options.remote:
            command     =  "wget -T 100 ftp://" + os.path.join(options.ftp_address, options.package_name) +\
             " -P " + options.package_directory
            if options.ftp_username and not options.ftp_password:
                command =  "wget -T 100 ftp://"+ options.ftp_username + "@" + os.path.join(options.ftp_address, options.package_name) +\
                 " -P " + options.package_directory
            elif options.ftp_username and options.ftp_password:
                command =  "wget -T 100 ftp://"+ options.ftp_username + ":" + options.ftp_password + "@" + os.path.join(options.ftp_address, options.package_name) +\
                 " -P " + options.package_directory
            logger("INFO", "command:%s" % command)
            os.system( command)
            logger("DEBUG", "Package %s is gotten from %s" %(options.package_name,options.ftp_address))
        
        
        if not os.path.isfile(os.path.join(options.package_directory, options.package_name)):
            raise Exception("Package does not exist !!! <" + str(os.path.join(options.package_directory, options.package_name)) + ">")

        extract(os.path.join(options.package_directory, options.package_name), options.temp);      
        install(temp=options.temp, version=options.version)
        
        package = os.path.join(options.package_directory, options.package_name)
        os.remove(package)
        logger("DEBUG", "Package is deleted:%s" % package)

        iRslt=0
    except BaseException, e:
        logger("EXCEPTION", str(e))
    finally:
        if templocation:
            shutil.rmtree(templocation)
            logger("DEBUG", "Temp location is deleted:%s" % templocation)
            if iRslt == 0:
                logger("INFO", "Updating has been finished succesfully")
            else:
                logger("ERROR", "An error has occurred while updating system!", False)
            
            spxlogmngr.Logger(loglevel, "/media/mmc/megixinstaller.%s.log" % updatetime, True).flush()
            time.sleep(0.1)
            # update files to ro location
            os.system("mount -o remount,rw /")
            if not os.path.isdir("/etc/updatelogs"):
                os.mkdir("/etc/updatelogs")
            os.system("mv /media/mmc/megixinstaller.%s.log /etc/updatelogs/" % updatetime)
            rmoldupdatelogs()
            time.sleep(0.1)
            if iRslt == 0:
                logger("INFO", "Device will be restarted in a few minutes", False)
        
        print "Bye Bye"
    sys.exit(iRslt)

if __name__ == "__main__":
    main()